﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ECO._2015.Startup))]
namespace ECO._2015
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.Use(typeof(ECO._2015.Bussiness.WebApi.WebApiServiceMiddleware));
        }
    }
}

﻿
$(function(){
    'use strict';
    $('.file_upload').each(function () {
        var progress_bar =  $(this).find('.progress .progress-bar');
        var fileupload = $(this).find('.fileupload');
        var film = $(fileupload).attr("data-film");
        var id = $(fileupload).attr("data-id");
        var display = $(this).find('.display');
        var img = $(this).find('.display img');
        var url = $(fileupload).attr("data-url");

        $(fileupload).fileupload({
            url: url,
            acceptFileTypes: /(.|\/)(gif|jpe?g|png)$/i,
            dataType: 'json',
            formData: { id: id, IdShortFilm: film },
            done: function (e, data) {
                //var src = window.location.origin = window.location.protocol+"//"+window.location.host + data.result.pathfile;
                var src =  data.result.pathfile;
                $(img).attr("src" , src);
                $(display).fadeIn();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(progress_bar).css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });

});

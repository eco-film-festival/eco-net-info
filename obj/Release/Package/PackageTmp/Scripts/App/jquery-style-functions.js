﻿$(document).ready(function () {
    console.info("jquery-style-functions::loaded");

    String.prototype.capitalizeFirstLetter = function () {
        return this.trim().charAt(0).toUpperCase() + this.trim().slice(1).toString().toLowerCase();
    }   

    //breadcrumb - active
    if ($('ol.breadcrumb li.active ').length) {
        $('ol.breadcrumb li.active').each(function () {
            var str = $(this).html();
            str = str.split('|');
            if (str.length === 2 && str[0].length > 0 && str[1].length > 0) {
                $(this).html('<strong>' + str[0] + '</strong>' +
                    '  <span class="text-navy e15-sub-eng">' + str[1] + '</span>')
            };
        })
    }
    //breadcrumb
    if ($('ol.breadcrumb li').length) {
        $('ol.breadcrumb li').each(function () {
            var str = $(this).html();
            str = str.split('|');
            if (str.length === 2 && str[0].length > 0 && str[1].length > 0) {
                $(this).html('' + str[0] + '' +
                    '  <span class="text-navy e15-sub-eng">' + str[1] + '</span>')
            };
        })
    }
    //ibox-title > h5
    if ($('div.ibox-title h5').length) {
        $('h5').each(function () {
            var str = $(this).html();
            str = str.split('|');
            if (str.length === 2 && str[0].length > 0 && str[1].length > 0) {
                $(this).html('<strong>' + str[0] + '</strong>' +
                    '  <span class="text-navy"> ' + str[1].capitalizeFirstLetter() + '</span>')
            };
        })
    } 
    //h5
    if ($('h5').length) {
        $('h5').each(function () {
            var str = $(this).html();
            str = str.split('|');
            if (str.length === 2 && str[0].length > 0 && str[1].length > 0) {
                $(this).html('<strong>' + str[0] + '</strong>' +
                    '  <span class="text-navy e15-sub-eng">' + str[1].capitalizeFirstLetter() + '</span>')
            };
        })
    }
    //h4
    if ($('h4').length) {
        $('h4').each(function () {
            var str = $(this).html();
            str = str.split('|');
            if (str.length === 2 && str[0].length > 0 && str[1].length > 0) {
                $(this).html('<strong>' + str[0].toUpperCase() + '</strong>' +
                    '  <span class="text-success e15-sub-eng">' + str[1].capitalizeFirstLetter() + '</span>')
            };
        })
    }
    //table.tableclosevideo tbody tr td
    if ($('table.tableclosevideo tbody tr td').length) {
        $('table.tableclosevideo tbody tr td').each(function () {
            var str = $(this).html();
            str = str.split('|');
            if (str.length === 2 && str[0].length > 0 && str[1].length > 0) {
                $(this).html('<span style="font-weight: 700;">' + str[0].capitalizeFirstLetter() + '</span>' +
                    '  <span class="text-success e15-sub-eng">' + str[1].capitalizeFirstLetter() + '</span>')
            };
        })
    }
    //div.form-group label
    if ($('div.form-group label').length) {
        $('div.form-group label').each(function () {
            var str = $(this).html();
            str = str.split('|');
            if (str.length === 2 && str[0].length > 0 && str[1].length > 0) {
                $(this).html('' + str[0].capitalizeFirstLetter() + ' ' +
                    '  <span class="text-success">' + str[1].capitalizeFirstLetter() + '</span>')
            };
        })
    }
    
});

$(function () {
    // any validation summary items should be encapsulated by a class alert and alert-danger
    $('.validation-summary-errors').each(function () {
        $(this).addClass('alert');
        $(this).addClass('alert-danger');
    });

    // update validation fields on submission of form
    $('form').submit(function () {
        if ($(this).valid()) {
            $(this).find('div.control-group').each(function () {
                if ($(this).find('span.field-validation-error').length == 0) {
                    $(this).removeClass('has-error');
                    $(this).addClass('has-success');
                }
            });
        }
        else {
            $(this).find('div.control-group').each(function () {
                if ($(this).find('span.field-validation-error').length > 0) {
                    $(this).removeClass('has-success');
                    $(this).addClass('has-error');
                }
            });
            $('.validation-summary-errors').each(function () {
                if ($(this).hasClass('alert-danger') == false) {
                    $(this).addClass('alert');
                    $(this).addClass('alert-danger');
                }
            });
        }
    });

    // check each form-group for errors on ready
    $('form').each(function () {
        $(this).find('div.form-group').each(function () {
            if ($(this).find('span.field-validation-error').length > 0) {
                $(this).addClass('has-error');
            }
        });
    });
});

//var page = function () {
//    //Update the validator
//    $.validator.setDefaults({
//        highlight: function (element) {
//            $(element).closest(".form-group").addClass("has-error");
//            $(element).closest(".form-group").removeClass("has-success");
//        },
//        unhighlight: function (element) {
//            $(element).closest(".form-group").removeClass("has-error");
//            $(element).closest(".form-group").addClass("has-success");
//        }
//    });
//}();
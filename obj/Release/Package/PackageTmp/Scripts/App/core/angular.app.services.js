﻿angular.module('app.services', [])
    .service('ApiService', function ($http, $q) {
        this.get = function (url, result, id) {
            var q = $q.defer();

            if (id != null) url = url + id;

            $http.get(url)
                .success(function (response, status, headers, config) {
                    if (angular.isFunction(result)) {
                        result(response, status, headers, config);
                    }

                    if (angular.isArray(result) && angular.isArray(response)) {
                        angular.forEach(response, function (v) {
                            result.push(v);
                        });
                    }
                    q.resolve(response);
                })
                .error(function (response, status, headers, config) {
                    q.reject(response, status, headers, config);
                });
            return q.promise;
        };

        this.update = function (url, result , id, model) {
            var q = $q.defer();
            $http.put(url + id, model)
                .success(function (response, status, headers, config) {
                    if (angular.isFunction(result)) {
                        result(response, status, headers, config);
                    }
                    if (angular.isArray(result) && angular.isObject(response)) {
                        angular.forEach(result, function (v) {
                            if (v.id == response.id) v == response;
                        });
                    }
                    q.resolve(response);
                })
			    .error(function (response, status, headers, config) {
			        q.reject(response, status, headers, config);
			    });
            return q.promise;
        };

        this.delete = function (url, result, id, index) {
            console.info('index' ,index);
            var q = $q.defer();

            if (index != null) {

                $http.delete(url + id)
                .success(function (response, status, headers, config) {
                    if (angular.isFunction(result)) {
                        result(response, status, headers, config);
                    }
                    if (angular.isArray(result)) {
                        result.splice(index, 1);
                    }
                    q.resolve(response);
                })
                .error(function (response, status, headers, config) {
                    q.reject(response, status, headers, config);
                });

            } else {
                q.reject();
            }
            return q.promise;
        };

        this.create = function (url, result, model) {
            var q = $q.defer();
            $http.post(url, model)
                .success(function (response, status, headers, config) {
                    if (angular.isFunction(result)) {
                        result(response, status, headers, config);
                    }
                    if (angular.isArray(result) && angular.isObject(response)) {
                        result.push(response);
                    }
                    q.resolve(response);
                })
			    .error(function (response, status, headers, config) {
			        q.reject(response, status, headers, config);
			    });
            return q.promise;
        };
    })
;
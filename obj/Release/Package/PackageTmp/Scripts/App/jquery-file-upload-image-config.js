﻿$(function () {
    window.filesUpload = {};
    'use strict';
    $('.file_upload').each(function () {
        var progress_bar =  $(this).find('.progress .progress-bar');
        var fileupload = $(this).find('.fileupload');
        var film = $(fileupload).attr("data-film");
        var id = $(fileupload).attr("data-id");
        var display = $(this).find('.display');
        var img = $(this).find('.display img');
        var url = $(fileupload).attr("data-url");

        $(fileupload).fileupload({
            url: url,
            acceptFileTypes: /(.|\/)(gif|jpe?g|png)$/i,
            dataType: 'json',
            formData: { id: id, IdShortFilm: film },
            add: function (e, data) {
                console.log(data);
                data.submit();
            },
            done: function (e, data) {
                console.log(data);
                console.log(data.data);
                window.filesUpload[id] = data.result.pathfile;
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(progress_bar).css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

});

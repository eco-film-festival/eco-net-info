﻿using ECO._2015.Bussiness;
using ECO._2015.Controllers.Services;
using ECO._2015.Models.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Controllers.Config
{
    public class PatrocinadorController : UserBussines
    {
        /*
        private DBContextConfig _db = new DBContextConfig();

        // GET: Patrocinador
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Detail(int id)
        {
            var _entity = _db.Patrocinador.Find(id);
            if (_entity != null)
            {
                return View((PatrocinadorViewModel)_entity);
            }
            return RedirectToAction("Create");
        }

        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            var _entity = _db.Patrocinador.Find(id);
            if (_entity != null)
            {
                return View((PatrocinadorViewModel)_entity);
            }
            return RedirectToAction("Create");
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id, CreatePatrocinadorViewModel model)
        {
            model.PK_IdPatrocinador = id;
            if (ModelState.IsValid)
            {
                var _entity = (Patrocinador)model;
                var _original = (Patrocinador)_db.Patrocinador
                .Where(p => p.PK_IdPatrocinador == _entity.PK_IdPatrocinador).FirstOrDefault();
                // -----------------------------------
                try
                {
                    _db.Entry(_original).CurrentValues.SetValues(_entity);
                    _db.SaveChanges();
                    return Json(new
                    {
                        PatrocinadorId = _entity.PK_IdPatrocinador,
                        Url = string.Format("/Patrocinador/Detail/{0}", _entity.PK_IdPatrocinador)
                    });
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    StringBuilder _builderInnerException = new StringBuilder();
                    foreach (var validationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _err = string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            _builderInnerException.AppendFormat("{0} ", _err);
                            System.Diagnostics.Debug.WriteLine(_err);
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    System.Diagnostics.Debug.WriteLine(e.Data);
                    return new BadRequest(e.InnerException.Message);
                }
            }
            return new BadRequest(ModelState);
        }

        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public ActionResult Create(CreatePatrocinadorViewModel model)
        {
            if (ModelState.IsValid)
            {
                var _entity = (Patrocinador)model;
                _db.Patrocinador.Add(_entity);
                // -----------------------------------
                try
                {
                    _db.SaveChanges();
                    return Json(new
                    {
                        PatrocinadorId = _entity.PK_IdPatrocinador,
                        Url = string.Format("/Patrocinador/Detail/{0}", _entity.PK_IdPatrocinador)
                    });
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    StringBuilder _builderInnerException = new StringBuilder();
                    foreach (var validationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            string _err = string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            _builderInnerException.AppendFormat("{0} ", _err);
                            System.Diagnostics.Debug.WriteLine(_err);
                        }
                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    System.Diagnostics.Debug.WriteLine(e.Data);
                    return new BadRequest(e.InnerException.Message);
                }
            }
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(model))
            {
                string name = descriptor.Name;
                object value = descriptor.GetValue(model);
                System.Diagnostics.Debug.WriteLine("{0}={1}", name, value);
            }
            return new BadRequest("Cualquier Cosa");
        }
        */
    }
}
﻿
using ECO._2015.Bussiness;
using ECO._2015.Models.App;
using ECO._2015.Models.ViewsModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Models
{

    public class SelectListViewModel
    {
        public static implicit operator SelectListViewModel(AppCategoria _entity)
        {
            return new SelectListViewModel
            {
                Id = _entity.Id,
                Descripcion = string.Format("{0} | {1}", _entity.Categoria_ES, _entity.Categoria_EN)
            };
        }


        //public SelectListViewModel(AppCategoria _entity)
        //{ 
        //    Id = _entity.Id;
        //    Descripcion = string.Format("{0} | {1}", _entity.Categoria_ES, _entity.Categoria_EN);
        //}

        public SelectListViewModel()
        {

        }
        public int Id { get; set; }

        public string Descripcion { get; set; }
    }

    public class ShortFilmsController : ShortFilmsBussiness
    {
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Index
        [Authorize(Roles = "participante")]
        public ActionResult Index()
        {
            if (UserActive != null)
            {
                ActionResult _ExistEntrevista = ValidationExistEntrevista();
                if (_ExistEntrevista != null)
                {
                    return _ExistEntrevista;
                }

                ActionResult _ValidationExistShortFilm =  ValidationExistShortFilm();
                if (_ValidationExistShortFilm != null)
                {
                    return _ValidationExistShortFilm;
                }

                if (CountListShortFilm <= 0)
                {
                    return RedirectToAction("Create", "ShortFilms");
                }
                
                ViewBag.IsCloseConvocatoria = IsCloseConvocatoria;
                return View(ListShortFilm);
                
            }

            return HttpNotFound();
        }


        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Entrevista
        [Authorize(Roles = "participante")]
        public ActionResult Entrevista(string userId)
        {
            return View(new AppEntrevista() { UserId = userId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "participante")]
        public ActionResult Entrevista(AppEntrevista entrevista)
        {
            if (UserActive != null)
            {
                if (ModelState.IsValid)
                {
                    entrevista.Edicion = EdicionFestival;
                    db.AppEntrevista.Add(entrevista);
                    db.SaveChanges();
                    return RedirectToAction("Index", "ShortFilms");
                }
                else
                {
                    return View(entrevista);
                }
            }

            return HttpNotFound();
        }


        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Create

        

        // GET: ShortFilms/Create
        [Authorize(Roles = "participante")]
        public ActionResult Create()
        {
            if (UserActive != null)
            {
                ActionResult _CloseConvocatoria = ValidationCloseConvocatoria();
                if (_CloseConvocatoria != null)
                {
                    return _CloseConvocatoria;
                }

                ActionResult _ExistEntrevista = ValidationExistEntrevista();
                if (_ExistEntrevista != null)
                {
                    return _ExistEntrevista;
                }

                ActionResult _ValidationExistShortFilm = ValidationExistShortFilm();
                if (_ValidationExistShortFilm != null)
                {
                    return _ValidationExistShortFilm;
                }
                // ----------------------------------
                ViewBag.PaisId = new SelectList(db.AppPais.OrderBy(p => p.Id), "Id", "Pais_ES");
                ViewBag.EstadoId = new SelectList(db.AppEstado.Where(e => e.PaisId == 1), "Id", "Estado_ES");
                ViewBag.MunicipioId = Enumerable.Empty<SelectListItem>();
                //
                ViewBag.AnnoId = new SelectList(db.AppAnno.Where(e =>e .CT_LIVE).OrderByDescending(a => a.Descripcion), "Id", "Descripcion");
                ViewBag.CategoriaId = new SelectList(db.AppCategoria.ToList().Select<AppCategoria, SelectListViewModel>(e => e).ToList<SelectListViewModel>(), "Id", "Descripcion");
                ViewBag.FormatoAudioId = new SelectList(db.AppFormatoAudio, "Id", "Formato_ES");
                ViewBag.FormatoVideoId = new SelectList(db.AppFormatoVideo, "Id", "Formato_ES");
                ViewBag.SubtituloId = new SelectList(db.AppSubtitulo, "Id", "Subtitulo_ES");
                // ----------------------------------
                ViewBag.Etapa = 0;
                // ----------------------------------
                var _model = new ShortFilmViewModel()
                {
                    // ---------------------
                    Etapa = 1,
                    // ---------------------
                    UserId = UserActive.Id,
                    Contacto = new ECO._2015.Models.ViewsModels.ContactoViewModel(),
                    Director = new DirectorViewModel(),
                    Productor = new ProductorViewModel(),
                    Fotografia = new FotografiaViewModel(),
                    Guion = new GuionViewModel(),
                    Edicion = new ECO._2015.Models.ViewsModels.EdicionViewModel(),
                    DisenoSonoro = new DisenoSonoroViewModel(),
                    Sonido = new SonidoViewModel(),
                    Musica = new MusicaViewModel(),
                };
                
                return View(_model);
            }

            return HttpNotFound();

        }

        // POST: ShortFilms/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "participante")]
        public ActionResult Create(ShortFilmViewModel _model)
        {
            if (UserActive != null)
            {
                ActionResult _CloseConvocatoria = ValidationCloseConvocatoria();
                if (_CloseConvocatoria != null)
                {
                    return _CloseConvocatoria;
                }

                ActionResult _ExistEntrevista = ValidationExistEntrevista();
                if (_ExistEntrevista != null)
                {
                    return _ExistEntrevista;
                }

                ActionResult _ValidationExistShortFilm = ValidationExistShortFilm();
                if (_ValidationExistShortFilm != null)
                {
                    return _ValidationExistShortFilm;
                }

                if (ModelState.IsValid)
                {
                    // ------------------------------------------
                    var _shortFilm = _model.CreateShortFilm(UserActive.Id, DateTime.Now, true, EdicionFestival, ConfImagesAutor, ConfImagesStills);
                    db.AppShortFilm.Add(_shortFilm);
                    db.SaveChanges();
                    // ------------------------------------------
                    return RedirectToAction("CreateStill", new { Id = _shortFilm.Id });
                    // ------------------------------------------
                }
                else
                {
                    AppEstado Estado = db.AppEstado.Find(_model.EstadoId);
                    // ----------------------------------
                    ViewBag.PaisId = new SelectList(db.AppPais, "Id", "Pais_ES", Estado.PaisId);
                    ViewBag.EstadoId = new SelectList(db.AppEstado.Where(e => e.PaisId == Estado.PaisId), "Id", "Estado_ES", _model.EstadoId);
                    ViewBag.MunicipioId = new SelectList(db.Municipio.Where(e => e.FK_IdEstado == _model.EstadoId), "PK_IdMunicipio", "NOM_MUN", _model.MunicipioId);
                    ViewBag.AnnoId = new SelectList(db.AppAnno.Where(e => e.CT_LIVE), "Id", "Descripcion", _model.AnnoId);
                    ViewBag.CategoriaId = new SelectList(db.AppCategoria.ToList().Select<AppCategoria, SelectListViewModel>(e => e).ToList<SelectListViewModel>(), "Id", "Descripcion", _model.CategoriaId);
                    ViewBag.FormatoAudioId = new SelectList(db.AppFormatoAudio, "Id", "Formato_ES", _model.FormatoAudioId);
                    ViewBag.FormatoVideoId = new SelectList(db.AppFormatoVideo, "Id", "Formato_ES", _model.FormatoVideoId);
                    ViewBag.SubtituloId = new SelectList(db.AppSubtitulo, "Id", "Subtitulo_ES", _model.SubtituloId);
                    ViewBag.UserId = UserActive.Id;
                    ViewBag.Etapa = 0;
                    // ----------------------------------
                    return View(_model);
                }
            }

            return HttpNotFound();
        }
        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Imagen
        [HttpGet]
        [Authorize(Roles = "participante")]
        public ActionResult CreateStill(int? id)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);

                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                ViewBag.NoStills = _ShortFilm.AppImagenStill.Where(i => !string.IsNullOrEmpty(i.Path)).ToList().Count;
                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                return View(_ShortFilm);
            }
            return HttpNotFound();

        }

        [HttpPost]
        [Authorize(Roles = "participante")]
        public ActionResult CreateStill(int? id, string Msg)
        {

            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                var _count = _ShortFilm.AppImagenStill.Where(i => string.IsNullOrEmpty(i.Path)).ToList().Count;

                if (_count > 0)
                {
                    ViewBag.NoStills = _ShortFilm.AppImagenStill.Where(i => !string.IsNullOrEmpty(i.Path)).ToList().Count;
                    ViewBag.Etapa = _ShortFilm.Etapa;
                    ViewBag.ShortFilmId = _ShortFilm.Id;
                    return View(_ShortFilm);
                }

                _ShortFilm.Etapa = 2;
                db.SaveChanges();
                return RedirectToAction("CreateAutor", "ShortFilms", new { id = id });
            }
            return HttpNotFound();

        }

        [Authorize(Roles = "participante")]
        public ActionResult CreateAutor(int? id)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);

                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                ViewBag.NoAutor = _ShortFilm.AppImagenAutor.Where(i => !string.IsNullOrEmpty(i.Path)).ToList().Count;
                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                return View(_ShortFilm);
            }
            return HttpNotFound();
        }

        [HttpPost]
        [Authorize(Roles = "participante")]
        public ActionResult CreateAutor(int? id, string Msg)
        {

            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                var _count = _ShortFilm.AppImagenAutor.Where(i => string.IsNullOrEmpty(i.Path)).ToList().Count;

                if (_count > 0)
                {
                    ViewBag.NoAutor = _ShortFilm.AppImagenAutor.Where(i => !string.IsNullOrEmpty(i.Path)).ToList().Count;
                    ViewBag.Etapa = _ShortFilm.Etapa;
                    ViewBag.ShortFilmId = _ShortFilm.Id;
                    return View(_ShortFilm);
                }

                _ShortFilm.Etapa = 3;
                db.SaveChanges();
                return RedirectToAction("CreateVideo", "ShortFilms", new { id = id });
            }
            return HttpNotFound();

        }

        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region CreateVideo
        [Authorize(Roles = "participante")]
        public ActionResult CreateVideo(int? id)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();

                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                return View(_ShortFilm);
            }
            return HttpNotFound();

        }
        [HttpPost]
        [Authorize(Roles = "participante")]
        public ActionResult CreateVideo(int? id, bool Envio_Correo_Video)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                if (Envio_Correo_Video)
                {
                    _ShortFilm.Close_Corto = true;
                    _ShortFilm.Envio_Correo_Video = Envio_Correo_Video;
                    _ShortFilm.Etapa = 5;
                    db.SaveChanges();
                    return RedirectToAction("CloseVideo", "ShortFilms", new { id = _ShortFilm.Id });
                }
                else if (!string.IsNullOrEmpty(_ShortFilm.Path) && !string.IsNullOrEmpty(_ShortFilm.Video))
                {
                    _ShortFilm.Close_Corto = true;
                    _ShortFilm.Envio_Correo_Video = Envio_Correo_Video;
                    _ShortFilm.Etapa = 5;
                    db.SaveChanges();
                    return RedirectToAction("CloseVideo", "ShortFilms", new { id = _ShortFilm.Id });
                }
                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                return View(_ShortFilm);
            }
            return HttpNotFound();

        }
        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Close Video
        [Authorize(Roles = "participante")]
        public ActionResult CloseVideo(int? id)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, false, false);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }
                
                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                ViewBag.IsCloseConvocatoria = IsCloseConvocatoria;
                return  View(_ShortFilm);
            }

            return HttpNotFound();
        }
        [HttpPost]
        [Authorize(Roles = "participante")]
        public ActionResult CloseVideo(int? id, string Msg)
        {

            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                _ShortFilm.Close_Corto = true;
                _ShortFilm.Etapa = 5;
                db.SaveChanges();

                _MailSenderBusiness.SendMail(
                    this, 
                    templateNotifiacion, 
                    RegistroMailSubject, 
                    RegistroMailMsgEsp ,
                    RegistroMailMsgIng ,
                    _ShortFilm , 
                    EdicionFestival,
                    UserAdminMMail
                    );

                return RedirectToAction("Index", "ShortFilms");

            }
            return HttpNotFound();
        }
        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Edit
        // GET: ShortFilms/Edit/5
        [Authorize(Roles = "participante")]
        public ActionResult Edit(int? id)
        {
            if (UserActive != null)
            {
                // ------------------------------------------
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }
                // ------------------------------------------
                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                // ------------------------------------------
                ViewBag.PaisId = new SelectList(db.AppPais, "Id", "Pais_ES", _ShortFilm.AppEstado.PaisId);
                ViewBag.EstadoId = new SelectList(db.AppEstado.Where(e => e.PaisId == _ShortFilm.AppEstado.PaisId), "Id", "Estado_ES", _ShortFilm.EstadoId);
                ViewBag.MunicipioId = new SelectList(db.Municipio.Where(e => e.FK_IdEstado == _ShortFilm.AppEstado.Id), "PK_IdMunicipio", "NOM_MUN", _ShortFilm.MunicipioId);
                ViewBag.AnnoId = new SelectList(db.AppAnno.Where(e => e.CT_LIVE), "Id", "Descripcion", _ShortFilm.AnnoId);
                ViewBag.CategoriaId = new SelectList(db.AppCategoria.ToList().Select<AppCategoria, SelectListViewModel>(e => e).ToList<SelectListViewModel>(), "Id", "Descripcion", _ShortFilm.CategoriaId);
                ViewBag.FormatoAudioId = new SelectList(db.AppFormatoAudio, "Id", "Formato_ES", _ShortFilm.FormatoAudioId);
                ViewBag.FormatoVideoId = new SelectList(db.AppFormatoVideo, "Id", "Formato_ES", _ShortFilm.FormatoVideoId);
                ViewBag.SubtituloId = new SelectList(db.AppSubtitulo, "Id", "Subtitulo_ES", _ShortFilm.SubtituloId);
                // ------------------------------------------
                return (_ActionResult != null) ? _ActionResult : View(new ShortFilmViewModel(_ShortFilm));
                // ------------------------------------------
            }
            return HttpNotFound();
        }

        // POST: ShortFilms/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "participante")]
        public ActionResult Edit(ShortFilmViewModel _model)
        {
            if (UserActive != null)
            {
                // ------------------------------------------
                int? id = _model.Id;
                // ------------------------------------------
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, true);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                if (ModelState.IsValid)
                {
                    _model.EditShortFilm(ref _ShortFilm);
                    db.SaveChanges();
                    return RedirectToAction("Index", "ShortFilms");
                }
                else
                {
                    // ------------------------------------------
                    ViewBag.Etapa = _ShortFilm.Etapa;
                    ViewBag.ShortFilmId = _ShortFilm.Id;
                    // ------------------------------------------
                    ViewBag.PaisId = new SelectList(db.AppPais, "Id", "Pais_ES", _model.PaisId);
                    ViewBag.EstadoId = new SelectList(db.AppEstado.Where(e => e.PaisId == _model.PaisId), "Id", "Estado_ES", _model.EstadoId);
                    ViewBag.MunicipioId = new SelectList(db.Municipio.Where(e => e.FK_IdEstado == _ShortFilm.AppEstado.Id), "PK_IdMunicipio", "NOM_MUN", _ShortFilm.MunicipioId);
                    ViewBag.AnnoId = new SelectList(db.AppAnno.Where(e => e.CT_LIVE), "Id", "Descripcion", _model.AnnoId);
                    ViewBag.CategoriaId = new SelectList(db.AppCategoria.ToList().Select<AppCategoria, SelectListViewModel>(e => e).ToList<SelectListViewModel>(), "Id", "Descripcion", _model.CategoriaId);
                    ViewBag.FormatoAudioId = new SelectList(db.AppFormatoAudio, "Id", "Formato_ES", _model.FormatoAudioId);
                    ViewBag.FormatoVideoId = new SelectList(db.AppFormatoVideo, "Id", "Formato_ES", _model.FormatoVideoId);
                    ViewBag.SubtituloId = new SelectList(db.AppSubtitulo, "Id", "Subtitulo_ES", _model.SubtituloId);
                    // ------------------------------------------
                    return (_ActionResult != null) ? _ActionResult : View(_model);
                }
            }

            return HttpNotFound();


        }
        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Delete
        /*
        // GET: ShortFilms/Delete/5
        [Authorize(Roles = "participante")]
        public ActionResult Delete(int? id)
        {
            ShortFilm _ShortFilm = new ShortFilm();
            ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm);
            return (_ActionResult != null) ? _ActionResult : View(_ShortFilm);
        }

        // POST: ShortFilms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "participante")]
        public ActionResult DeleteConfirmed(int? id)
        {
            ShortFilm _ShortFilm = new ShortFilm();
            ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm);

            if (_ActionResult == null)
            {
                db.ShortFilm.Remove(_ShortFilm);
                db.SaveChanges();
            }

            return (_ActionResult != null) ? _ActionResult : RedirectToAction("Index");
        }
        */
        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region RegisterClose
        public ActionResult RegisterClose()
        {
            //-------------------------------------
            return View();
        }

        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Pais
        public JsonResult DetailsPais(int id)
        {
            List<EstadoViewModel> _ListEstadoView = new List<EstadoViewModel>();
            var r = from e in db.AppEstado where e.PaisId == id select e;
            r.ToList().ForEach(e => _ListEstadoView.Add(new EstadoViewModel() { Id = e.Id, Estado = e.Estado_ES }));
            return Json(_ListEstadoView);
        }
        #endregion

        #region Estado
        public JsonResult DetailsEstado(int id)
        {
            List<MunicipioViewModel> _ListMunicipioView = new List<MunicipioViewModel>();

            db.Municipio
                .Where(e => e.FK_IdEstado == id)
                .ToList()
                .ForEach(e => _ListMunicipioView.Add(new MunicipioViewModel() { Id = e.PK_IdMunicipio, Municipio = e.NOM_MUN }));
            return Json(_ListMunicipioView);
        }
        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
    }
}
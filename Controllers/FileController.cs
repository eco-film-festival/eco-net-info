﻿using ECO._2015.Bussiness;
using ECO._2015.Models;
using ECO._2015.Models.App;
using ECO._2015.Models.ViewsModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Controllers
{
    public class FileController : ControllerBussines
    {
        #region Actions Upload Image

        [Authorize(Roles = "participante")]
        [HttpPost]
        public virtual ActionResult UploadFileStill(FileUploadViewModel _FileUpload)
        {
            if (UserActive != null)
            {
                string path = "/Uploads/Still/";
                bool isUploaded = false;
                string message = "File upload failed";
                string file = "";
                // -------------------------------

                int? IdShortFilm = _FileUpload.IdShortFilm; 
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref IdShortFilm, ref _ShortFilm, true, !User.IsInRole("admin"));

                if (_ActionResult == null && _ShortFilm != null)
                {
                    AppImagenStill _Still = db.AppImagenStill.Find(_FileUpload.Id);
                    if (_Still != null)
                    {
                        isUploaded = UploadFile(ref message, ref file, path, _FileUpload.File);

                        if (isUploaded)
                        {
                            _Still.Path = path;
                            _Still.File = file;
                            _Still.AppImageStillResize.Clear();

                            db.AppImageResizeConf
                                .Where(e => e.IsStill)
                                .ToList()
                                .ForEach(_AppImageResizeConf => {
                                var _path = string.Format("{0}{1}" , path, _AppImageResizeConf.Path);
                                string _file;
                                if (UploadFileResize(out _file, _path, _AppImageResizeConf.width, _AppImageResizeConf.height, _AppImageResizeConf.fixWidht, _FileUpload.File))
                                {
                                    _Still.AppImageStillResize.Add(new AppImageStillResize
                                    {
                                        AppImageResizeConf = _AppImageResizeConf , 
                                        Path = _path,
                                        File = _file,
                                    });
                                }
                            });

                            db.SaveChanges();

                            return Json(
                                new { isUploaded = isUploaded, message = message, pathfile = GetHostUrl(file, path ,Request) }
                                , "text/html");
                        }
                        else
                        {
                            message += " | File Error";
                        }
                    }
                    else
                    {
                        message += " | Still Error";
                    }
                }
                else
                {
                    message += " | Security Error";
                }
                return Json(new { isUploaded = isUploaded, message = message }, "text/html");
            }
            return HttpNotFound();
        }

        private bool UploadFileResize(out string _file, string path, int width, int? height, bool fixWidht, HttpPostedFileBase _File)
        {
            _file = "";
            if (_File != null && _File.ContentLength != 0)
            {
                var _streamUpload = _File.InputStream;
                var _imageUpload = Image.FromStream(_streamUpload);
                var _resizeImage = (fixWidht) ? ResizeImage(_imageUpload, width) : ResizeImage(_imageUpload, width, (int) height);

                string pathForSaving = Server.MapPath("~" + path);
                if (CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        _file = string.Format("{0}.png", Guid.NewGuid().ToString());
                        _resizeImage.Save(Path.Combine(pathForSaving, _file), ImageFormat.Png);
                        return true;
                    }
                    catch
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        private static Image ResizeImage(Image imgToResize, int width)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = ((float)width / (float)sourceWidth);

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage(b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return b;
        }

        private static Image ResizeImage(Image imgToResize, int width , int height)
        {
            double? widthScale = width / (double) imgToResize.Width;
            double? heightScale = height / (double) imgToResize.Height;
            double scale = Math.Min((double)(widthScale ?? heightScale), (double)(heightScale ?? widthScale));
            var _size = new Size((int)Math.Floor(imgToResize.Width * scale), (int)Math.Ceiling(imgToResize.Height * scale));
            Bitmap b = new Bitmap(_size.Width, _size.Height);
            Graphics g = Graphics.FromImage(b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.DrawImage(imgToResize, 0, 0, _size.Width, _size.Height);
            g.Dispose();
            return b;
        }


        [Authorize(Roles = "participante")]
        [HttpPost]
        public virtual ActionResult UploadFileAutor(FileUploadViewModel _FileUpload)
        {
            if (UserActive != null)
            {
                string path = "/Uploads/Autor/";
                bool isUploaded = false;
                string message = "File upload failed";
                string file = "";
                // -------------------------------

                int? IdShortFilm = _FileUpload.IdShortFilm;
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref IdShortFilm, ref _ShortFilm , true);

                if (_ActionResult == null && _ShortFilm != null)
                {
                    AppImagenAutor _ImageAutor = db.AppImagenAutor.Find(_FileUpload.Id);
                    if (_ImageAutor != null)
                    {
                        isUploaded = UploadFile(ref message, ref file, path, _FileUpload.File);

                        if (isUploaded)
                        {
                            _ImageAutor.Path = path;
                            _ImageAutor.File = file;
                            _ImageAutor.AppImageAutorResize.Clear();

                            db.AppImageResizeConf
                                .Where(e => e.IsAutor)
                                .ToList()
                                .ForEach(_AppImageResizeConf => {
                                    var _path = string.Format("{0}{1}", path, _AppImageResizeConf.Path);
                                    string _file;
                                    if (UploadFileResize(out _file, _path, _AppImageResizeConf.width, _AppImageResizeConf.height, _AppImageResizeConf.fixWidht, _FileUpload.File))
                                    {
                                        _ImageAutor.AppImageAutorResize.Add(new AppImageAutorResize
                                        {
                                            AppImageResizeConf = _AppImageResizeConf,
                                            Path = _path,
                                            File = _file,
                                        });
                                    }
                                });

                            db.SaveChanges();

                            return Json(
                                new { isUploaded = isUploaded, message = message, pathfile = GetHostUrl(file, path, Request) }
                                , "text/html");
                        }
                        else
                        {
                            message += " | File Error";
                        }
                    }
                    else
                    {
                        message += " | Still Error";
                    }
                }
                else
                {
                    message += " | Security Error";
                }
                return Json(new { isUploaded = isUploaded, message = message }, "text/html");
            }
            return HttpNotFound();

            
        }
        #endregion


        #region Upload Video
        [Authorize(Roles = "participante  , admin")]
        [HttpPost]
        public virtual ActionResult UploadFileShortFilm(FileUploadViewModel _FileUpload)
        {
            if (UserActive != null)
            {
                string path = "/Uploads/Video/";
                bool isUploaded = false;
                string message = "File upload failed";
                string file = "";
                // -------------------------------
                int? IdShortFilm = _FileUpload.IdShortFilm;
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref IdShortFilm, ref _ShortFilm, true, !User.IsInRole("admin"));

                if ((_ActionResult == null && _ShortFilm != null) || User.IsInRole("admin"))
                {
                    isUploaded = UploadFile(ref message, ref file, path, _FileUpload.File);

                    if (isUploaded)
                    {
                        _ShortFilm.Path = path;
                        _ShortFilm.Video = file;

                        if (User.IsInRole("participante") || User.IsInRole("admin"))
                        {
                            _ShortFilm.Envio_Correo_Video = false;
                        }
                        
                        db.SaveChanges();

                        return Json(
                            new { isUploaded = isUploaded, message = message, pathfile = GetHostUrl(file, path, Request) }
                            , "text/html");
                    }
                }
                else
                {
                    message += " | Security Error";
                }
                // -------------------------------
                return Json(new { isUploaded = isUploaded, message = message }, "text/html");
            }
            return HttpNotFound();

            
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public virtual ActionResult UploadFileImage(FileUploadConfigViewModel _FileUpload)
        {
            if (UserActive != null)
            {
                string path = "/Uploads/EcoPrograma/";
                bool isUploaded = false;
                string message = "File upload failed";
                string file = "";
                // -------------------------------

                if (User.IsInRole("admin"))
                {
                    isUploaded = UploadFile(ref message, ref file, path, _FileUpload.File);

                    if (isUploaded)
                    {
                        return Json(
                            new { isUploaded = isUploaded, message = message, pathfile = GetHostUrl(file, path, Request) }
                            , "text/html");
                    }
                }
                else
                {
                    message += " | Security Error";
                }
                // -------------------------------
                return Json(new { isUploaded = isUploaded, message = message }, "text/html");
            }
            return HttpNotFound();
        }


        #endregion


        #region Private Methods

        private bool UploadFile(ref string message, ref string file, string path, HttpPostedFileBase _File)
        {
            if (_File != null && _File.ContentLength != 0)
            {
                string pathForSaving = Server.MapPath("~" + path);

                if (CreateFolderIfNeeded(pathForSaving))
                {
                    try
                    {
                        string extension = Path.GetExtension(_File.FileName);
                        file = Guid.NewGuid().ToString() + extension;
                        _File.SaveAs(Path.Combine(pathForSaving, file));
                        message = "File uploaded successfully!";
                        return true;
                    }
                    catch (Exception ex)
                    {
                        message = string.Format("File upload failed: {0}", ex.Message);
                    }
                }
            }
            return false;
        }
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        private string GetHostUrl(string file, string path, HttpRequestBase Request)
        {
            return string.Format("{0}{1}/{2}", GetHost(Request), path, file);
        }

        private static string GetHost(HttpRequestBase Request)
        {
            return
                    Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
                    (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
        }
        #endregion
    }
}
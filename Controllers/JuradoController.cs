﻿using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ECO._2015.Models;
using ECO._2015.Bussiness;
using ECO._2015.Models.App;


namespace ECO._2015.Controllers
{
    public class JuradoController : JuradoBussines
    {
        #region Index
        // GET: Jurado
        [HttpGet]
        [Authorize(Roles = "jurado")]
        public ActionResult Index()
        {
            var _model = GetListShortFilmByJurado();

            if (_model != null)
            {
                //if (_model.Termino) 
                //{
                //    return RedirectToAction("Encuesta");
                //}

                return View(_model);
            }

            return HttpNotFound();
        }
        #endregion

        #region Detail
        [HttpGet]
        [Authorize(Roles = "jurado")]
        public ActionResult Detail(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AppShortFilm _ShortFilm = db.AppShortFilm.Find(id);

            if (_ShortFilm == null)
            {
                return HttpNotFound();
            }

            AppJurado Jurado = GetJuradoByIdUser();
            if (Jurado != null)
            {
                return View(new JuradoDetailViewModel(_ShortFilm, Jurado, EdicionFestival));
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "jurado")]
        public ActionResult Detail(int? id, CriterioCalificarValueViewModel[] ListCriterio)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AppShortFilm _ShortFilm = db.AppShortFilm.Find(id);

            if (_ShortFilm == null)
            {
                return HttpNotFound();
            }

            AppJurado Jurado = GetJuradoByIdUser();
            if (Jurado != null)
            {
                db.AppCalificacion
                    .Where(c => c.ShortFilm_Id == _ShortFilm.Id)
                    .Where(c => c.Jurado_Id == Jurado.Id)
                    .ToList()
                    .ForEach(c => db.AppCalificacion.Remove(c))
                    ;

                db.SaveChanges();


                foreach (var c in ListCriterio)
                {
                    db.AppCalificacion.Add(new AppCalificacion()
                    {
                        Jurado_Id = Jurado.Id,
                        CriterioCalificar_Id = c.Id,
                        ShortFilm_Id = _ShortFilm.Id,
                        Valor = c.Valor,
                        FechaCaptura = DateTime.Now
                    });
                }

                db.SaveChanges();

                return RedirectToAction("Index", "Jurado");
            }

            return HttpNotFound();
            
        }

        #endregion

        #region Encuesta
        //[HttpGet]
        //[Authorize(Roles = "jurado")]
        //public ActionResult Encuesta()
        //{
        //    AppJurado Jurado = GetJuradoByIdUser();

        //    if (Jurado != null)
        //    {
        //        return View(new EncuestaViewModelAdmin(Jurado, EdicionFestival));                   
        //    }
        //    return HttpNotFound();
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "jurado")]
        public ActionResult Encuesta(RespuestaValueViewModel[] ListRespueta)
        {

            AppJurado Jurado = GetJuradoByIdUser();
            if (Jurado != null)
            {

                Jurado.AppRespuestaJurado.Where(e => e.AppPreguntaJurado.Edicion == EdicionFestival)
                    .ToList().ForEach(e => db.AppRespuestaJurado.Remove(e));

                ListRespueta.ToList().ForEach(e => db.AppRespuestaJurado.Add(new AppRespuestaJurado
                {
                    AppJurado = Jurado,
                    PreguntaJurado_Id =  e.Id,
                    Respuesta = e.Valor
                }));

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return HttpNotFound();
        }
        #endregion

        #region Admin
        [Authorize(Roles = "admin")]
        public ActionResult Admin()
        {
            return View();
        }
        #endregion


    }
}
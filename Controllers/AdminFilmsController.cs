﻿using ECO._2015.Bussiness;
using ECO._2015.Models.App;
using ECO._2015.Models.ViewsModels;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;


namespace ECO._2015.Models
{
    public class AdminFilmsController : AdminFilmsBusssines
    {
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Index
        // GET: AdminFilms
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var _filter = new AdminFilmsFilterViewModel()
            {
                Activo = true ,
                Preseleccion = false,
                Sel_Oficial = false,
                Ganador = false,
                Etapa = EtapaRegistroFin ,
                Envio = -1 ,
                Edicion = EdicionFestival ,
                CategoriaId = -1 ,
                PaisId = -1 ,
                NacionalidadId = -1,
            };
            // ----------------------------------
            var _model = GetModelShortFilmFilter(_filter, ListShortFilm);
            // ----------------------------------
            SetViewBagIndex(_filter);
            // ----------------------------------
            return View(_model);
            // ----------------------------------
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AdminFilmsFilterConfViewModel _filterConf)
        {
            var _filter = new AdminFilmsFilterViewModel()
            {
                Activo = _filterConf.Filter.Activo,
                Preseleccion = _filterConf.Filter.Preseleccion,
                Sel_Oficial = _filterConf.Filter.Sel_Oficial,
                Ganador = _filterConf.Filter.Ganador,
                Etapa = _filterConf.Etapa,
                Envio = _filterConf.Envio,
                Edicion = _filterConf.Edicion,
                CategoriaId = _filterConf.CategoriaId,
                PaisId = _filterConf.PaisId,
                NacionalidadId = _filterConf.NacionalidadId,
            };
            // ----------------------------------
            //var _model = GetModelShortFilmFilter(_filter, GetListShortFilmFilter(_filter, ListShortFilm));
            var _model = GetModelShortFilmFilter(_filter, ListShortFilm);
            // ----------------------------------
            SetViewBagIndex(_filter);
            // ----------------------------------
            return View(_model);
            // ----------------------------------
        }
        #endregion

        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Detail

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Detail( int? id )
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, false, false);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                ViewBag.IsCloseConvocatoria = IsCloseConvocatoria;

                ViewBag.Operations = new SelectList(ListOperations, "Id", "Operation", GetEstatusShortFilm(_ShortFilm));
                ViewBag.EtapaEdit = new SelectList(ListEtapaEdit, "Id", "Etapa", _ShortFilm.Etapa);
                return View(_ShortFilm);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Detail(AdminFilmsOperationViewModel _filterConf)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = db.AppShortFilm.Find(_filterConf.Id);

                if (_ShortFilm != null && ModelState.IsValid)
                {
                    _ShortFilm.Preseleccion = false;
                    _ShortFilm.Sel_Oficial = false;
                    _ShortFilm.Ganador = false;
                    _ShortFilm.Activo = false;

                    switch (_filterConf.Operations)
                    {
                        case 0:  //Preselección
                            _ShortFilm.Activo = true;
                            break;
                        case 1:  //Preselección
                            _ShortFilm.Activo = true;
                            _ShortFilm.Preseleccion = true;
                            break;
                        case 2: // Selección Oficial
                            _ShortFilm.Activo = true;
                            _ShortFilm.Preseleccion = true;
                            _ShortFilm.Sel_Oficial = true;
                            break;

                        case 3: // Selección Oficial
                            _ShortFilm.Activo = true;
                            _ShortFilm.Preseleccion = false;
                            _ShortFilm.Sel_Oficial = true;
                            break;
                        //case 3: // Ganador
                        //    _ShortFilm.Activo = true;
                        //    _ShortFilm.Preseleccion = true;
                        //    _ShortFilm.Sel_Oficial = true;
                        //    _ShortFilm.Ganador = true;
                        //    break;
                        case 4: // Eliminar
                            _ShortFilm.Activo = false;
                            break;                   
                    }

                    _ShortFilm.Etapa = _filterConf.EtapaEdit;

                    db.SaveChanges();

                    if (_filterConf.ActiveNotification)
                    {
                        switch (_filterConf.Operations)
                        {
                            case 1:  //Preselección
                                SendMailPreseleccion(_ShortFilm);
                                break;
                            case 2: // Selección Oficial
                                SendMailSeleccionOficial(_ShortFilm);
                                break;
                        }
                    }
                }

                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                ViewBag.IsCloseConvocatoria = IsCloseConvocatoria;

                ViewBag.Operations = new SelectList(ListOperations, "Id", "Operation", GetEstatusShortFilm(_ShortFilm));
                ViewBag.EtapaEdit = new SelectList(ListEtapaEdit, "Id", "Etapa", _ShortFilm.Etapa);
                return View(_ShortFilm);
            }

            return HttpNotFound();
        }
        #endregion

        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region CreateVideo
        [Authorize(Roles = "admin")]
        public ActionResult CreateVideo(int? id)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();

                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, false, false);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                return View(_ShortFilm);
            }
            return HttpNotFound();

        }
        #endregion

        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region Result
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Result()
        {
            var _filter = new AdminFilmsFilterViewModel
            {
                Activo = true,
                Preseleccion = true,
                Sel_Oficial = false,
                Ganador = false,
                Etapa = EtapaRegistroFin,
                Envio = -1,
                Edicion = EdicionFestival,
                CategoriaId = -1,
                PaisId = -1,
                NacionalidadId = -1,
            };
            // ----------------------------------
            var _model = GetModelShortFilmJuradoFilter(_filter, ListShortFilmSeleccionOficial);
            // ----------------------------------
            SetViewBagIndex(_filter);
            // ----------------------------------
            return View(_model);
            // ----------------------------------
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Result(AdminFilmsFilterConfViewModel _filterConf)
        {
            var _filter = new AdminFilmsFilterViewModel
            {
                Activo = true,
                Preseleccion = true,
                Sel_Oficial = _filterConf.Filter.Sel_Oficial,
                Ganador = _filterConf.Filter.Ganador,
                Etapa = EtapaRegistroFin,
                Envio = -1,
                Edicion = EdicionFestival,
                CategoriaId = _filterConf.CategoriaId,
                PaisId = -1,
                NacionalidadId = _filterConf.NacionalidadId,

            };
            // ----------------------------------
            var _model = GetModelShortFilmJuradoFilter(_filter, ListShortFilmSeleccionOficial);
            // ----------------------------------
            SetViewBagIndex(_filter);
            // ----------------------------------
            return View(_model);
            // ----------------------------------
        }


        //[HttpPost]
        //[Authorize(Roles = "admin")]
        //[ValidateAntiForgeryToken]
        //public ActionResult Result(AdminFilmsFilterConfViewModel _filterConf)
        //{
        //    var _filter = new AdminFilmsFilterViewModel()
        //    {
        //        Activo = _filterConf.Filter.Activo,
        //        Preseleccion = _filterConf.Filter.Preseleccion,
        //        Sel_Oficial = _filterConf.Filter.Sel_Oficial,
        //        Ganador = _filterConf.Filter.Ganador,
        //        Etapa = _filterConf.Etapa,
        //        Envio = _filterConf.Envio,
        //        Edicion = _filterConf.Edicion,
        //        CategoriaId = _filterConf.CategoriaId,
        //        PaisId = _filterConf.PaisId,
        //    };
        //    // ----------------------------------
        //    var _model = GetModelShortFilmFilter(_filter, ListShortFilmPreseleccion);
        //    // ----------------------------------
        //    SetViewBagIndex(_filter);
        //    // ----------------------------------
        //    return View(_model);
        //    // ----------------------------------
        //}

        #endregion

        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region DeailtResult

        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult DeailtResult(int? id)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = new AppShortFilm();
                ActionResult _ActionResult = ValidAction(ref id, ref _ShortFilm, false, false);
                if (_ActionResult != null)
                {
                    return _ActionResult;
                }

                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                ViewBag.IsCloseConvocatoria = IsCloseConvocatoria;

                ViewBag.Operations = new SelectList(ListOperations, "Id", "Operation", GetEstatusShortFilm(_ShortFilm));
                ViewBag.EtapaEdit = new SelectList(ListEtapaEdit, "Id", "Etapa", _ShortFilm.Etapa);
                return View(new AppShortFilmsResultViewModel(_ShortFilm));
            }

            return HttpNotFound();
        }


        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeailtResult(AdminFilmsOperationViewModel _filterConf)
        {
            if (UserActive != null)
            {
                AppShortFilm _ShortFilm = db.AppShortFilm.Find(_filterConf.Id);

                if (_ShortFilm != null && ModelState.IsValid)
                {
                    _ShortFilm.Preseleccion = false;
                    _ShortFilm.Sel_Oficial = false;
                    _ShortFilm.Ganador = false;
                    _ShortFilm.Activo = false;

                    switch (_filterConf.Operations)
                    {
                        case 0:  //Preselección
                            _ShortFilm.Activo = true;
                            break;
                        case 1:  //Preselección
                            _ShortFilm.Activo = true;
                            _ShortFilm.Preseleccion = true;
                            break;
                        case 2: // Selección Oficial
                            _ShortFilm.Activo = true;
                            _ShortFilm.Preseleccion = true;
                            _ShortFilm.Sel_Oficial = true;
                            break;
                        case 3: // Ganador
                            _ShortFilm.Activo = true;
                            _ShortFilm.Preseleccion = true;
                            _ShortFilm.Sel_Oficial = true;
                            _ShortFilm.Ganador = true;
                            break;
                        case 4: // Eliminar
                            _ShortFilm.Activo = false;
                            break;
                    }

                    _ShortFilm.Etapa = _filterConf.EtapaEdit;

                    db.SaveChanges();

                    if (_filterConf.ActiveNotification)
                    {
                        switch (_filterConf.Operations)
                        {
                            case 1:  //Preselección
                                SendMailPreseleccion(_ShortFilm);
                                break;
                            case 2: // Selección Oficial
                                SendMailSeleccionOficial(_ShortFilm);
                                break;
                        }
                    }
                }

                ViewBag.Etapa = _ShortFilm.Etapa;
                ViewBag.ShortFilmId = _ShortFilm.Id;
                ViewBag.IsCloseConvocatoria = IsCloseConvocatoria;

                ViewBag.Operations = new SelectList(ListOperations, "Id", "Operation", GetEstatusShortFilm(_ShortFilm));
                ViewBag.EtapaEdit = new SelectList(ListEtapaEdit, "Id", "Etapa", _ShortFilm.Etapa);
                return View(_ShortFilm);
            }

            return HttpNotFound();
        }
        #endregion

        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        #region View Films

        [HttpGet]
        [Authorize(Roles = "view_films")]
        public ActionResult ViewFilms()
        {
            var _userDb = UserActiveDb;

            if (_userDb != null)
            { 
                var _model = new List<AppViewFilmsViewModel>();
                _userDb.AppViewFilms.Where(e => e.Edicion == EdicionFestival).ToList().ForEach(e => _model.Add(new AppViewFilmsViewModel(e)));
                return View(_model);
            }

            return HttpNotFound();
        }
        #endregion

        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
    }

    

}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ECO._2015.Models.Prensa;
using System.Threading.Tasks;
using ECO._2015.Bussiness;
using ECO._2015.Models;
using ECO._2015.Controllers.Services;
using ECO._2015.Bussiness.SendMailService;
using Microsoft.AspNet.Identity;
using System.Text;
using Microsoft.AspNet.Identity.Owin;


namespace ECO._2015.Controllers
{
    [Authorize]
    public class MediosController : UserBussines
    {
        private DBContextPrensa db = new DBContextPrensa();


        [Authorize(Roles = "medio")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "medio")]
        public ActionResult Detail(string id)
        {
            var _entity = db.AspNetUsers.Find(id);
            if (_entity != null)
            {
                return View((DisplayMedioVidewModel)_entity.Medio.FirstOrDefault());
            }
            return RedirectToAction("Register");
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterMedioViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, EmailConfirmed = true };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    IdentityResult result_rol = await UserManager.AddToRoleAsync(user.Id, "medio");
                    if (result_rol.Succeeded && RegisterMedio(model, user.Id))
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return Json(new
                        {
                            UserId = user.Id , 
                            Url = string.Format("/Medios/Detail/{0}", user.Id)
                        });
                    }
                    else
                    {
                        AddErrors(result_rol);
                    }
                }
                else
                {
                    AddErrors(result);
                }
            }
            return new BadRequest(ModelState);
        }

        private bool RegisterMedio(RegisterMedioViewModel model, string UserId)
        {
            var _entity = (Medio)model;
            // -----------------------------------
            _entity.FK_IdEdicion__SCH = EdicionFestival;
            _entity.UserId = UserId;
            // -----------------------------------
            if (model.MedioTipoMedioDetail != null)
                model.MedioTipoMedioDetail.ToList().ForEach(e => _entity.TipoMedioDetail.Add(db.TipoMedioDetail.Find(e)));
            if (model.MedioCobertura != null)
                model.MedioCobertura.ToList().ForEach(e => _entity.Cobertura.Add(db.Cobertura.Find(e)));
            if (model.MedioPerfil != null)
                model.MedioPerfil.ToList().ForEach(e => _entity.Perfil.Add(db.Perfil.Find(e)));
            if (model.MedioPeriodicidad != null)
                model.MedioPeriodicidad.ToList().ForEach(e => _entity.Periodicidad.Add(db.Periodicidad.Find(e)));
            if (model.MedioDiaTranmision != null)
                model.MedioDiaTranmision.ToList().ForEach(e => _entity.DiaTranmision.Add(db.DiaTranmision.Find(e)));
            db.Medio.Add(_entity);
            // -----------------------------------
            try {
                db.SaveChanges();
                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                StringBuilder _builderInnerException = new StringBuilder();
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string _err = string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        _builderInnerException.AppendFormat("{0} ", _err);
                        System.Diagnostics.Debug.WriteLine(_err);
                    }
                }
            }            
            catch(Exception e) {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return false;
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Register");
            }
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var UserId  = SignInManager.UserManager.FindByEmail(model.Email).Id;
                    return RedirectToAction("Detail", new { id = UserId });
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View("Register");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

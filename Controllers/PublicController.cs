﻿using ECO._2015.Bussiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Controllers
{
    public class PublicController : MsgBussines
    {
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult Report()
        {
            ViewBag.Service = "/api/PublicService/Report";
            ViewBag.Title = "Estadísticas  | Statistics ";
            return View();
        }

        public ActionResult Report2014()
        {
            ViewBag.Service = "/api/PublicService/Report2014";
            ViewBag.Title = "Estadísticas 2014 | Statistics 2014";
            return View("Report");
        }

        public ActionResult Report2015()
        {
            ViewBag.Service = "/api/PublicService/Report2015";
            ViewBag.Title = "Estadísticas 2015 | Statistics 2015";
            return View("Report");
        }
    }
}
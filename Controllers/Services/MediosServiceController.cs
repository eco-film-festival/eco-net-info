﻿using ECO._2015.Models;
using ECO._2015.Models.App;
using ECO._2015.Models.Prensa;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ECO._2015.Controllers.Services
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MediosServiceController : ApiController
    {
        #region DB
        private DBContextPrensa _db = new DBContextPrensa();
        #endregion

        #region Catalogos
        [HttpGet]
        [ActionName("TipoMedio")]
        public IHttpActionResult GetTipoMedio()
        {
            var _list = new List<SelectViewModel>();
            _db.TipoMedio.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }

        [HttpGet]
        [ActionName("Cobertura")]
        public IHttpActionResult GetCobertura(int id)
        {
            var _entity = _db.TipoMedio.Find(id);
            if (_entity != null)
            {
                var _list = new List<SelectViewModel>();
                _entity.Cobertura.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("DiaTranmision")]
        public IHttpActionResult GetDiaTranmision(int id)
        {
            var _entity = _db.TipoMedio.Find(id);
            if (_entity != null)
            {
                var _list = new List<SelectViewModel>();
                _entity.DiaTranmision.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("TipoMedioDetail")]
        public IHttpActionResult GetTipoMedioDetail(int id)
        {
            var _entity = _db.TipoMedio.Find(id);
            if (_entity != null)
            {
                var _list = new List<SelectViewModel>();
                _entity.TipoMedioDetail.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("Periodicidad")]
        public IHttpActionResult GetPeriodicidad(int id)
        {
            var _entity = _db.TipoMedio.Find(id);
            if (_entity != null)
            {
                var _list = new List<SelectViewModel>();
                _entity.Periodicidad.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("Perfil")]
        public IHttpActionResult GetPerfil(int id)
        {
            var _entity = _db.TipoMedio.Find(id);
            if (_entity != null)
            {
                var _list = new List<SelectViewModel>();
                _entity.Perfil.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("Pais")]
        public IHttpActionResult GetPais()
        {
            using (var _dbApp = new DBContextApp())
            {
                var _list = new List<SelectViewModel>();
                _dbApp.AppPais.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
        }

        [HttpGet]
        [ActionName("Estado")]
        public IHttpActionResult GetEstado(int id)
        {
            using (var _dbApp = new DBContextApp())
            {
                var _entity = _dbApp.AppPais.Find(id);
                if (_entity != null)
                {
                    var _list = new List<SelectViewModel>();
                    _entity.AppEstado.ToList().ForEach(e => _list.Add(e));
                    return Ok(_list);
                }
                return NotFound();
            }
        }

        [HttpGet]
        [ActionName("Prensa")]
        public IHttpActionResult GetPrensa()
        {
            var _list = new List<EdicionPrensaViewModel>();
            _db.Edicion.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }

        #endregion
    }

    #region ViewNodels

    public class DisplayMedioVidewModel
    {
        public static implicit operator DisplayMedioVidewModel(ECO._2015.Models.Prensa.Medio _entity)
        {
            var _result = new DisplayMedioVidewModel
            {
                Id = _entity.PK_IdMedio,
                Edicion = _entity.FK_IdEdicion__SCH,
                Email = _entity.Email,
                Nombre = _entity.Nombre,
                Apellidos = _entity.Apellidos,
                Medio = _entity.Medio1,
                Telefono = _entity.Telefono,
                TipoTelefono = _entity.TipoTelefono,
                WebSite = _entity.WebSite,
                Facebook = _entity.Facebook,
                Twitter = _entity.Twitter,
                Youtube = _entity.Youtube,
                Vimeo = _entity.Vimeo,
                Nacionalidad = _entity.AppPais.AppNacionalidad.Nacionalidad_ES,
                Pais = _entity.AppPais.Pais_ES,
                Estado = _entity.AppEstado.Estado_ES,
                TipoMedio = _entity.TipoMedio.Descripcion,
                TipoMedioOtro = _entity.TipoMedioOtro,
                TestigoUrl = _entity.TestigoUrl,
            };
            return _result;
        }

        #region Attr
        public int Id { get; set; }
        public int Edicion { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Cargo { get; set; }
        public string Medio { get; set; }
        public string Telefono { get; set; }
        public string TipoTelefono { get; set; }
        public string WebSite { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Youtube { get; set; }
        public string Vimeo { get; set; }
        public string Nacionalidad { get; set; }
        public string Pais { get; set; }
        public string Estado { get; set; }
        public string TipoMedio { get; set; }
        public string TipoMedioOtro { get; set; }
        public string TipoMedioDetailOtro { get; set; }
        public string TestigoUrl { get; set; }

        //public virtual MedioTVRadio MedioTVRadio { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Cobertura> Cobertura { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<DiaTranmision> DiaTranmision { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Perfil> Perfil { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Periodicidad> Periodicidad { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<TipoMedioDetail> TipoMedioDetail { get; set; }
        #endregion
    }

    public class RegisterMedioViewModel
    {
        public static implicit operator ECO._2015.Models.Prensa.Medio(RegisterMedioViewModel _entity)
        {
            return new ECO._2015.Models.Prensa.Medio()
            {
                AceptoTerminos = (bool)_entity.aceptoterminos,
                Email = _entity.Email,
                Nombre = _entity.Nombre,
                Apellidos = _entity.Apellidos,
                Medio1 = _entity.Medio,
                Cargo = _entity.Cargo,
                Telefono = _entity.Telefono,
                TipoTelefono = _entity.TipoTelefono,
                WebSite = _entity.WebSite,
                Facebook = _entity.Facebook,
                Vimeo = _entity.Vimeo,
                FK__AppPais = (int)_entity.AppPais,
                FK__AppEstado = (int)_entity.AppEstado,
                FK_IdTipoMedio__SCH = (int)_entity.TipoMedio,
                TestigoUrl = _entity.TestigoUrl,
                TipoMedioOtro = _entity.TipoMedioOtro,
                TipoMedioDetailOtro = _entity.TipoMedioDetailOtro,
            };
        }


        public int[] MedioTipoMedioDetail { get; set; }
        public int[] MedioCobertura { get; set; }
        public int[] MedioPerfil { get; set; }
        public int[] MedioPeriodicidad { get; set; }
        public int[] MedioDiaTranmision { get; set; }

        #region Attr
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Apellidos { get; set; }
        [Required]
        public string Medio { get; set; }
        [Required]
        public string Cargo { get; set; }
        [Required]
        public string TipoTelefono { get; set; }
        [Required]
        public string WebSite { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Vimeo { get; set; }
        [Required(ErrorMessage = "Selecciona un País")]
        public int? AppPais { get; set; }
        [Required(ErrorMessage = "Selecciona un Estado")]
        public int? AppEstado { get; set; }
        [Required(ErrorMessage = "Selecciona un Tipo de Medio")]
        public int? TipoMedio { get; set; }
        [Required]
        [Display(Name = "Link Testigo")]
        public string TestigoUrl { get; set; }
        [Required]
        [Display(Name = "Acepto Terminos y Condiciones")]
        public bool? aceptoterminos { get; set; }

        public string Telefono { get; set; }
        public string TipoMedioOtro { get; set; }
        public string TipoMedioDetailOtro { get; set; }
        #endregion
    }

    public class SelectViewModel
    {
        public static implicit operator SelectViewModel(ECO._2015.Models.App.AppPais _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.Id,
                Descripcion = _entity.Pais_ES,
                Otro = false,
            };
        }
        public static implicit operator SelectViewModel(ECO._2015.Models.App.AppEstado _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.Id,
                Descripcion = _entity.Estado_ES,
                Otro = false,
            };
        }
        public static implicit operator SelectViewModel(ECO._2015.Models.Prensa.Perfil _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdPerfil,
                Descripcion = _entity.Descripcion,
                Otro = false,
            };
        }
        public static implicit operator SelectViewModel(ECO._2015.Models.Prensa.Periodicidad _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdPeriodicidad,
                Descripcion = _entity.Descripcion,
                Otro = false,
            };
        }
        public static implicit operator SelectViewModel(ECO._2015.Models.Prensa.TipoMedioDetail _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdTipoMedioDetail,
                Descripcion = _entity.Descripcion,
                Otro = _entity.Otro,

            };
        }
        public static implicit operator SelectViewModel(ECO._2015.Models.Prensa.DiaTranmision _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdDiaTranmision,
                Descripcion = _entity.Descripcion,
                Otro = false,

            };
        }
        public static implicit operator SelectViewModel(ECO._2015.Models.Prensa.Cobertura _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdCobertura,
                Descripcion = _entity.Descripcion,
                Otro = false,

            };
        }
        public static implicit operator SelectViewModel(ECO._2015.Models.Prensa.TipoMedio _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdTipoMedio,
                Descripcion = _entity.Descripcion,
                Otro = _entity.Otro,
            };
        }

        public static implicit operator SelectViewModel(ECO._2015.Models.Config.Edicion _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdEdicion,
                Descripcion = _entity.Nombre_ES,
                Otro = false,
            };
        }

        public static implicit operator SelectViewModel(ECO._2015.Models.App.TipoPatrocinador _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdTipoPatrocinador,
                Descripcion = _entity.Descripcion_ES,
                Otro = false,
            };
        }

        public static implicit operator SelectViewModel(ECO._2015.Models.Config.Proyeccion _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdProyeccion,
                Descripcion = _entity.Proyeccion_ES,
                Otro = false,
            };
        }

        public static implicit operator SelectViewModel(ECO._2015.Models.App.TipoEvento _entity)
        {
            return new SelectViewModel
            {
                Id = _entity.PK_IdTipoEvento,
                Descripcion = _entity.Tipo_ES,
                Otro = false,
            };
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool Otro { get; set; }
    }

    public class EdicionPrensaViewModel
    {
        public EdicionPrensaViewModel()
        {
            Eventos = new List<EventoFestivalViewModel>();
            Boletin = new List<BoletinViewModel>();
        }
        public static implicit operator EdicionPrensaViewModel(ECO._2015.Models.Prensa.Edicion _entity)
        {
            var _result = new EdicionPrensaViewModel { 
                Edicion = _entity.PK_IdEdicion
            };

            _entity.Boletin.ToList().ForEach(e => _result.Boletin.Add(e));
            _entity.EventoFestival.ToList().ForEach(e => _result.Eventos.Add(e));
            return _result;

        }
        #region Attr 
        public int Edicion { get; set; }

        public List<EventoFestivalViewModel> Eventos { get; set; }
        public List<BoletinViewModel> Boletin { get; set; }
        #endregion

    }

    public class BoletinViewModel
    {
        public static implicit operator BoletinViewModel(ECO._2015.Models.Prensa.Boletin _entity)
        {
            var _result = new BoletinViewModel
            {
                Id = _entity.PK_IdBoletin,
                Boletin_ES = _entity.Boletin_ES,
                Boletin_EN = _entity.Boletin_EN,
                Titulo_ES = _entity.Titulo_ES,
                Titulo_EN = _entity.Titulo_EN,
                Fecha = _entity.Fecha,
            };
            return _result;
        }

        #region Attr
        public int Id { get; set; }
        public string Boletin_ES { get; set; }
        public string Boletin_EN { get; set; }
        public string Titulo_ES { get; set; }
        public string Titulo_EN { get; set; }
        public DateTime? Fecha { get; set; }
        #endregion
    }
    public class EventoFestivalViewModel
    {
        public EventoFestivalViewModel()
        {
            Notas = new List<EventoNotasViewModel>();
            Galeria = new List<ImagenViewModel>();
        }
        public static implicit operator EventoFestivalViewModel(ECO._2015.Models.Prensa.EventoFestival _entity)
        {
            var _result = new EventoFestivalViewModel
            {
                Id = _entity.PK_IdEventoFestival,
                Edicion = _entity.FK_IdEdicion__SCH,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Fecha = _entity.Fecha,
            };

            _entity.EventoNotas.ToList().ForEach(e => _result.Notas.Add(e));
            _entity.EventosImagen.ToList().ForEach(e => _result.Galeria.Add(e));
            return _result;
        }
        #region Attr
        public int Id { get; set; }
        public int Edicion { get; set; }
        public string Nombre_ES { get; set; }
        public string Nombre_EN { get; set; }
        public DateTime? Fecha { get; set; }
        public List<EventoNotasViewModel> Notas { get; set; }
        public List<ImagenViewModel> Galeria { get; set; }
        #endregion
    }

    public class ImagenViewModel
    {
        public static implicit operator ImagenViewModel(ECO._2015.Models.Prensa.EventosImagen _entity)
        {
            return new ImagenViewModel
            {
                Id = _entity.PK_IdEventosImagen,
                Path = _entity.Path,
                Image = _entity.Image,
                Ext = _entity.Ext,
                Name = _entity.Name,
                Portada = _entity.Portada,
            };
        }

        #region Attr
        public int Id { get; set; }
        public string Path { get; set; }

        public string Image { get; set; }

        public string Ext { get; set; }

        public string Name { get; set; }

        public bool Portada { get; set; }

        #endregion
    }

    public class EventoNotasViewModel
    {
        public static implicit operator EventoNotasViewModel(ECO._2015.Models.Prensa.EventoNotas _entity)
        {
            return new EventoNotasViewModel { 
                Id = _entity.PK_IdEventoNotas,
                Nota = _entity.Nota,
            };
        }
        #region Attr
        public int Id { get; set; }
        public string Nota { get; set; }
        #endregion
    }

    #endregion
}
﻿using ECO._2015.Bussiness;
using ECO._2015.Models.App;
using ECO._2015.Models.His;
using ECO._2015.Models.Prensa;
using ECO._2015.Models.Vis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ECO._2015.Controllers.Services
{
    [EnableCors(origins:"*", headers: "*", methods: "*")]
    public class PublicServiceController : PublicServiceBussines
    {

        #region Test

        //[HttpGet]
        //[ActionName("Test2016ZO")]
        //public IHttpActionResult GetTest()
        //{
        //    var _listGanador = _db.AppGanador
        //        .Where(e => e.Edicion == EdicionFestivalApp)
        //        .OrderBy(e => e.Orden)
        //        //.Select(e => e.AppShortFilm)                
        //        .ToList();

        //    var _listCategoria = _db.AppCategoria.ToList();

        //    var _listNacionalidad = _db.AppNacionalidad.ToList();

        //    var _entity = new WinnersViewModel(_listGanador, _listCategoria, _listNacionalidad, true);
        //    return Ok(_entity);
        //}
        #endregion

        #region Flickr

        [HttpGet]
        [ActionName("FlickrEcoEdition")]
        public IHttpActionResult GetFlickrEcoEdition()
        {
            var _result = new List<FlickrNet.Photo>();
            var _entity = _db.Edicion.Find(EdicionFestival);
            if (_entity != null) {
                var _listPhotos = FlickrBussines.PhotosetsGetPhotos(_entity.FlickrGaleria.Select(e => e.photosetId).ToArray());
                _listPhotos.ToList().ForEach(e => _result.Add(e));
                return Ok(_result);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("FlickrEco")]
        public IHttpActionResult GetFlickrEco()
        {
            return Ok(FlickrBussines.GetFeed());
        }

        [HttpGet]
        [ActionName("FlickrAlmbunEco")]
        public IHttpActionResult GetFlickrEco(string id)
        {
            return Ok(FlickrBussines.PhotosetsGetPhotos(id));
        }

        #endregion




        #region DB
        private DBContextApp _db = new DBContextApp();
        private DBContextVis _dbVis = new DBContextVis();
        private DBContextHis _dbHis = new DBContextHis();
        private DBContextPrensa _dbPres = new DBContextPrensa();
        #endregion

        #region Estadisticas
        [HttpGet]
        [ActionName("Report")]
        public IHttpActionResult GetReport()
        {
            return Ok(new ChartBussiness(null).ListReport);
        }

        [HttpGet]
        [ActionName("Report2014")]
        public IHttpActionResult Report2014()
        {
            return Ok(new ChartBussiness(2014).ListReport);
        }

        [HttpGet]
        [ActionName("Report2015")]
        public IHttpActionResult Report2015()
        {
            return Ok(new ChartBussiness(2015).ListReport);
        }
        #endregion

        #region Actions

        #region App

        [HttpGet]
        [ActionName("ListEdition")]
        public IHttpActionResult ListEdition()
        {
            var _result = new List<Models.ViewsModels.EditionViewModel>();
            _db.Edicion.OrderByDescending(e => e.PK_IdEdicion).ToList().ForEach(e => _result.Add(e));
            return Ok(_result);
        }

        [HttpGet]
        [ActionName("Schedule")]
        public IHttpActionResult GetSchedule(int id)
        {
            // var _listPrograma = _db.Programa.Where(e => e.Edicion == EdicionFestivalApp);
            IQueryable<Programa> _listPrograma;
            if (EdicionFestivalApp == id)
            {
                _listPrograma = _db.Programa.Where(e => e.Edicion == id && DateTime.Now >= VerProgramaFechaApp);
            }
            else
            {
                _listPrograma = _db.Programa.Where(e => e.Edicion == id);
            }
            var _listTipoEvento = _db.TipoEvento;
            var _entity = new ScheduleViewModel(_listPrograma, _listTipoEvento);
            return Ok(_entity);
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("App")]
        public IHttpActionResult GetApp()
        {
            var _listPrograma = _db.Programa.Where(e => e.Edicion == EdicionFestivalApp);
            var _listPatrocinado = _db.Patrocinador.Where(e => e.Edicion == EdicionFestivalApp).Select(e => e.TipoPatrocinador).Distinct();
            var _listTipoEvento = _db.TipoEvento;
            var _Feed = new List<PostViewModel>();
            try
            {
                _Feed = new FeedBussines().GetFeed().Feed;
            }
            catch
            {
               
            }
            var _entity = new AppViewModel(_listPrograma, _listPatrocinado, _listTipoEvento, _Feed);
            return Ok(_entity);
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Event")]
        public IHttpActionResult GetEvent(int id)
        {
            var _entity = _db.Evento.Find(id);
            if (_entity != null)
            {
                return Ok(new EventoViewModel(_entity));
            }

            return NotFound();
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Projection")]
        public IHttpActionResult GetProjection(int id)
        {
            var _entity = _db.Proyeccion.Find(id);
            if (_entity != null)
            {
                return Ok(new ProjectionViewModel(_entity));
            }

            return NotFound();
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("ShortFilm")]
        public IHttpActionResult GetShortFilm(int id)
        {
            var _entity = _db.AppShortFilm.Find(id);
            //if (_entity != null && _entity.Activo && _entity.Sel_Oficial)
            if(_entity != null)
            {
                var _shortFilm = new ShortFilmViewModel(_entity);
                if (EdicionFestivalApp == _entity.Edicion && DateTime.Now <= VerProgramaFechaApp)
                {
                    _shortFilm.ListEvento = new List<EventoViewModel>();
                }
                return Ok(_shortFilm);
            }

            return NotFound();
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("OfficialSelection")]
        public IHttpActionResult GetOfficialSelection()
        {
            var _listSeleccion = _db.AppShortFilm
                .Where(e => e.Edicion == EdicionFestivalApp)
                .Where(e => e.Sel_Oficial)
                .ToList();

            var _listCategoria = _db.AppCategoria.ToList();

            var _listNacionalidad = _db.AppNacionalidad.ToList();

            var _entity = new OfficialSelectionViewModel(_listSeleccion, _listCategoria, _listNacionalidad);
            return Ok(_entity);
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("PreSeleccion")]
        public IHttpActionResult GetPreSeleccion()
        {
            var _listSeleccion = _db.AppShortFilm
                .Where(e => e.Edicion == EdicionFestivalApp)
                .Where(e => e.Preseleccion)
                .ToList();

            var _listCategoria = _db.AppCategoria.ToList();

            var _listNacionalidad = _db.AppNacionalidad.ToList();

            var _entity = new OfficialSelectionViewModel(_listSeleccion, _listCategoria, _listNacionalidad);
            return Ok(_entity);
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Jury")]
        public IEnumerable<JuradoViewModel> GetJury()
        {
            var _list = new List<JuradoViewModel>();

            _db.AppJurado.Where(e => e.Jurado_Activo).Where(e => e.Edicion == EdicionFestivalApp)
                .ToList().ForEach(e =>_list.Add(new JuradoViewModel(e)));

            return _list;
        }

        [HttpGet]
        [ActionName("Places")]
        public IEnumerable<PlaceViewModel> GetPlaces()
        {
            var _listPrograma = _db.Programa.Where(e => e.Edicion == EdicionFestivalApp);
            var _list =  new List<PlaceViewModel>();
            _listPrograma.Select(e => e.Sede).Distinct().ToList().ForEach(e => _list.Add(new PlaceViewModel(e)));
            return _list;
        }
        
        #endregion

        #region Prensa
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Prensa")]
        public IHttpActionResult GetPrensa()
        {
            return GetPrensaByEdition(EdicionFestival);
        }

        [HttpGet]
        [ActionName("PrensaByEdition")]
        public IHttpActionResult GetPrensaByEdition(int id)
        {
            var _entity = _dbPres.Edicion.Find(id);
            if (_entity != null)
            {
                return Ok((EdicionPrensaViewModel)_entity);
            }
            return NotFound();
        }

        #endregion

        #region Feed

        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Feed")]
        public IHttpActionResult GetFeedCustom(int id)
        {
            var _Feed = new List<PostViewModel>();
            try
            {
                _Feed = new FeedBussines().GetFeed(id).Feed;
                return Ok(_Feed);

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Feed")]
        public IHttpActionResult GetFeed()

        {
            var _Feed = new List<PostViewModel>();
            try
            {
                _Feed = new FeedBussines().GetFeed().Feed;
                return Ok(_Feed);

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }
        // -----------------------------
        // -----------------------------
        [HttpPost]
        [ActionName("Feed")]
        public IHttpActionResult GetFeed(FilterFeedViewModel _model)
        {
            try
            {
                return Ok(new FeedBussines().GetFeed(_model));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return BadRequest(e.Message);
            }
        }
        // -----------------------------
        // -----------------------------
        #endregion

        #region Ganadores
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Winners")] 
        public IHttpActionResult GetWinners()
        {
            var _listGanador = _db.AppGanador
                .Where(e => e.Edicion == EdicionFestivalApp)
                .OrderBy(e => e.Orden)
                //.Select(e => e.AppShortFilm)                
                .ToList();

            var _listCategoria = _db.AppCategoria.ToList();

            var _listNacionalidad = _db.AppNacionalidad.ToList();

            var _entity = new WinnersViewModel(_listGanador, _listCategoria, _listNacionalidad);
            return Ok(_entity);
        }
        // -----------------------------
        // -----------------------------
        #endregion

        #region Patrocinadores
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("Sponsor")]
        public IHttpActionResult GetSponsor()
        {
            return GetSponsorByEdition(EdicionFestival);
        }

        [HttpGet]
        [ActionName("SponsorHome")]
        public IHttpActionResult GetSponsorHome()
        {
            var _listPatrocinado = _db.Patrocinador
                .Where(e => e.Edicion == EdicionFestival)
                .Where(e => e.FK_IdTipoPatrocinador__SCH == PatrocinadorAupisiadores || e.FK_IdTipoPatrocinador__SCH == PatrocinadorOficial)
                .Select(e => e.TipoPatrocinador).Distinct();
            var ListSponsor = new List<TipoPatrocinadorViewModel>();
            _listPatrocinado.OrderBy(e => e.Orden).ToList().ForEach(e => ListSponsor.Add(new TipoPatrocinadorViewModel(e, EdicionFestival)));
            return Ok(ListSponsor);
        }


        [HttpGet]
        [ActionName("SponsorByEdition")]
        public IHttpActionResult GetSponsorByEdition(int id)
        {
            var _listPatrocinado = _db.Patrocinador.Where(e => e.Edicion == id).Select(e => e.TipoPatrocinador).Distinct();
            var ListSponsor = new List<TipoPatrocinadorViewModel>();
            _listPatrocinado.OrderBy(e => e.Orden).ToList().ForEach(e => ListSponsor.Add(new TipoPatrocinadorViewModel(e , id)));
            return Ok(ListSponsor);
        }
        // -----------------------------
        // -----------------------------
        #endregion

        #region Video Galeria
        // -----------------------------
        // -----------------------------
        [HttpGet]
        [ActionName("VideoDia")]
        public IHttpActionResult GetVideoDia()
        {
            VideoDiaBussines _videoDiaBussines = VideoDiaBussines.Instance;
            return Ok(_videoDiaBussines.getVideo(HttpContext.Current.Timestamp));
        }
        // -----------------------------
        [HttpGet]
        [ActionName("VideoGalery")]
        public IHttpActionResult GetVideoGalery(int? id)
        {
            var _list = (id == null)
                    ? _dbVis.VideoGaleria
                        .OrderByDescending(e => e.Año).ThenBy(e => e.Orden).ToList()
                    : _dbVis.VideoGaleria
                        .Where(e => e.Edicion == id)
                        .OrderByDescending(e => e.Año).ThenBy(e => e.Orden).ToList();
            return Ok(_list.Where(e => ValidWinnerShortFilm(e)));
        }
        // -----------------------------
        [HttpGet]
        [ActionName("VideoGaleryAll")]
        public IHttpActionResult GetVideoGaleryAll()
        {
            var _list = _dbVis.VideoGaleria.OrderByDescending(e => e.Año).ThenBy(e => e.Orden).ToList();
            return Ok(_list.Where(e => ValidWinnerShortFilm(e)));
        }

        private bool ValidWinnerShortFilm(VideoGaleria _VideoGaleria)
        {
            if (_VideoGaleria.Edicion == EdicionFestivalApp)
            {
                return isWinnersOpen;
            }
            return true;
        }
        // -----------------------------
        [HttpGet]
        [ActionName("VideoGaleryCSV")]
        public HttpResponseMessage GetVideoGaleryCSV()
        {
            var _list = _dbVis.VideoGaleria.OrderByDescending(e => e.Año).ThenBy(e => e.Orden).ToList();
            return CreateCSVHttpResponse(_list.Where(e => ValidWinnerShortFilm(e)).ToList());
        }
        // -----------------------------
        #endregion

        #region Ediciones Anteriores

        [HttpGet]
        [ActionName("EditionSections")]
        public IHttpActionResult GetEditionSections()
        {
            var _list = new List<EditionViewModel>();
            var _query = _db.Edicion.ToList();
            foreach (var _entity in _query) {
                var _count = _entity.AppShortFilm.Where(e => e.Sel_Oficial).Count() > 0;
                if (_count || _entity.PK_IdEdicion == EdicionFestival)
                {
                    _list.Add((EditionViewModel)_entity);
                }
                else 
                {
                    _list.Add((EditionViewModel)_dbHis.Edicion.Find(_entity.PK_IdEdicion));
                }
            }
            return Ok(_list);
        }

        [HttpGet]
        [ActionName("Edition")]
        public IHttpActionResult GetEdition()
        {
            return GetEditionByEdition(EdicionFestival);
        }

        [HttpGet]
        [ActionName("EditionByEdition")]
        public IHttpActionResult GetEditionByEdition(int id)
        {
            var _count = (_db.AppShortFilm.Where(e => e.Edicion == id).Where(e => e.Sel_Oficial).Count() > 0);

            if (_count || id == EdicionFestival)
            {
                return Ok((EditionHisViewModel)_db.Edicion.Find(id));
            }
            else 
            {
                var _entity = _dbHis.Edicion.Find(id);


                return Ok((EditionHisViewModel)_dbHis.Edicion.Find(id));
            }
        }

        #endregion

        #region Sedes
        [HttpGet]
        [ActionName("SedeByEdicion")]
        public IHttpActionResult GetSedeByEdicion(int id)
        {
            var _list = new List<SedeViewModel>();
            var _entiy = _db.Edicion.Find(id);
            if (_entiy != null)
            {
                _entiy.Sede.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }
        #endregion
        #endregion

        #region ApiController
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
    }

    #region ViewModel

    #region APP
    public class ScheduleViewModel
    {
        public ScheduleViewModel() { }
        public ScheduleViewModel(IEnumerable<Programa> _ListPrograma, IEnumerable<TipoEvento> _listTipoEvento)
        {
            Edition = PublicServiceBussines.EdicionFestivalApp;
            // -------------------------
            _ListPrograma.ToList().ForEach(e => Add(e));
            // -------------------------
            // -------------------------
            ListTipoEvento = new List<TipoEventoViewModel>();
            _listTipoEvento.ToList().ForEach(e => ListTipoEvento.Add(new TipoEventoViewModel(e)));
            // -------------------------
            // -------------------------
            // -------------------------
            ListDays = new List<DayViewModel>();
            _ListEvent.GroupBy(e => e.Date.DateObj).Select(e => e.Key).OrderBy(e => e.Day).ToList().ForEach(e => ListDays.Add(new DayViewModel(e)));
            // -------------------------
            // -------------------------
            // -------------------------
            ListPlaces = new List<PlaceViewModel>();
            _ListPrograma.Select(e => e.Sede).Distinct().ToList().ForEach(e => ListPlaces.Add(new PlaceViewModel(e)));
        }

        public List<PlaceViewModel> ListPlaces { get; set; }
        public List<TipoEventoViewModel> ListTipoEvento { get; private set; }
        public List<EventoViewModel> ListEvent { get { return _ListEvent.OrderBy(e => e.Date.DateObj).ThenBy(e => e.Start).ToList(); } }
        public List<DayViewModel> ListDays { get; private set; }
        
        public int Edition { get; set; }
        #region _ListEvent
        private void Add(Programa _Programa)
        {
            /*
            _Programa.Evento.Where(e => e.Programa.Edicion == PublicServiceBussines.EdicionFestivalApp).ToList().ForEach(e => _ListEvent.Add(new EventoViewModel(e)));
            _Programa.Proyeccion.Where(e => e.Programa.Edicion == PublicServiceBussines.EdicionFestivalApp).ToList().ForEach(e => _ListEvent.Add(new EventoViewModel(e)));
            */
            _Programa.Evento.ToList().ForEach(e => _ListEvent.Add(new EventoViewModel(e)));
            _Programa.Proyeccion.ToList().ForEach(e => _ListEvent.Add(new EventoViewModel(e)));
        }
        protected List<EventoViewModel> _ListEvent = new List<EventoViewModel>();
        #endregion



        
    }
    public class AppViewModel : ScheduleViewModel
    {
        public AppViewModel(IEnumerable<Programa> _ListPrograma, IEnumerable<TipoPatrocinador> _ListPatrocinado, IEnumerable<TipoEvento> _listTipoEvento, List<PostViewModel> _Feed)
            :base(_ListPrograma , _listTipoEvento)
        {
            // -------------------------
            ListPlaces = new List<PlaceViewModel>();
            _ListPrograma.Select(e => e.Sede).Distinct().ToList().ForEach(e => ListPlaces.Add(new PlaceViewModel(e)));
            // -------------------------
            ListSponsor = new List<TipoPatrocinadorViewModel>();
            _ListPatrocinado.OrderBy(e => e.Orden).ToList().ForEach(e => ListSponsor.Add(new TipoPatrocinadorViewModel(e, PublicServiceBussines.EdicionFestival)));
            // -------------------------
            Feed = _Feed;
            // -------------------------
        }

        
        //public List<PlaceViewModel> ListPlaces { get; private set; }
        public List<TipoPatrocinadorViewModel> ListSponsor { get; private set; }
        public List<PostViewModel> Feed { get; private set; }

    }
    public class TipoPatrocinadorViewModel
    {
        public TipoPatrocinadorViewModel()
        {
            ListSponsor = new List<SponsorViewModel>();
        }
        public TipoPatrocinadorViewModel(TipoPatrocinador _TipoPatrocinador , int Edicion)
        {
            Descripcion_ES = _TipoPatrocinador.Descripcion_ES;
            Descripcion_EN = _TipoPatrocinador.Descripcion_EN;
            ListSponsor = new List<SponsorViewModel>();
            _TipoPatrocinador.Patrocinador.Where(e  => e.Edicion == Edicion).ToList().ForEach(e => ListSponsor.Add(new SponsorViewModel(e)));
        }

        public string Descripcion_ES { get; set; }
        public string Descripcion_EN { get; set; }
        public List<SponsorViewModel> ListSponsor { get; set; }
    }
    public class SponsorViewModel
    {
        public SponsorViewModel(Patrocinador _Patrocinador)
        {
            Title_ES = _Patrocinador.Descripcion_ES;
            Title_EN = _Patrocinador.Descripcion_EN;
            Image = _Patrocinador.Image;
            WebSite = _Patrocinador.WebSite;
        }

        public string Title_ES { get; set; }
        public string Title_EN { get; set; }
        public string Image { get; set; }
        public string WebSite { get; set; }
        
    }
    public class PlaceViewModel
    {
        public PlaceViewModel(Sede _Sede)
        {
            Id = _Sede.PK_IdSede;
            Title_ES = _Sede.Sede_ES;
            Title_EN = _Sede.Sede_EN;
            Address = _Sede.Direccion;
            Url = _Sede.url;
            Image = _Sede.Image;
        }

        public PlaceViewModel(PlaceViewModel _Sede)
        {
            Id = _Sede.Id;
            Title_ES = _Sede.Title_ES;
            Title_EN = _Sede.Title_EN;
            Address = _Sede.Address;
            Url = _Sede.Url;
            Image = _Sede.Image;
        }
        
        public int Id { get; set; }

        public string Title_ES { get; set; }

        public string Title_EN { get; set; }

        public string Address { get; set; }

        public string Url { get; set; }

        public string Image { get; set; }
    }
    public class EventoViewModel
    {
        #region EventoViewModel
        public EventoViewModel(Evento _Evento)
        {
            Id = _Evento.PK_IdEvento;
            UrlEvent = _Evento.url;
            Start = (_Evento.start != null) ? ((TimeSpan)_Evento.start).ToString(@"hh\:mm") : "";
            End = (_Evento.end != null) ? ((TimeSpan )_Evento.end).ToString(@"hh\:mm") : "";
            //------------------------
            //------------------------
            //------------------------
            Site_ES = _Evento.Lugar_ES;
            Site_EN = _Evento.Lugar_EN;
            Description_EN = _Evento.Descripcion_EN;
            Description_ES = _Evento.Descripcion_ES;
            Event_EN = _Evento.Evento_EN;
            Event_ES = _Evento.Evento_ES;
            ImageEvent = _Evento.Image;
            ImageWeb = _Evento.ImageWeb;
            //------------------------
            //------------------------
            //------------------------
            FK_IdTipoEvento__SCH = _Evento.FK_IdTipoEvento__SCH;
            Type_ES = _Evento.TipoEvento.Tipo_ES;
            Type_EN = _Evento.TipoEvento.Tipo_EN;
            IconType = _Evento.TipoEvento.icon;
            ClassNameType = _Evento.TipoEvento.className;
            DetailUrl = _Evento.TipoEvento.DetailUrl;
            //------------------------
            //------------------------
            //------------------------
            Place = new PlaceViewModel(_Evento.Programa.Sede);
            Date = new DayViewModel(_Evento);
            //------------------------
            //------------------------
            //------------------------
            DateFilter = _Evento.Programa.Date;
            PlaceFilter = _Evento.Programa.Sede.PK_IdSede;
            TypeFilter = _Evento.TipoEvento.PK_IdTipoEvento;

        }

        public EventoViewModel(Proyeccion _Proyeccion)
        {
            Id = _Proyeccion.PK_IdProyeccion;
            UrlEvent = _Proyeccion.url;
            Start = (_Proyeccion.start != null) ? ((TimeSpan)_Proyeccion.start).ToString(@"hh\:mm") : "";
            End = (_Proyeccion.end != null) ? ((TimeSpan)_Proyeccion.end).ToString(@"hh\:mm") : "";
            //------------------------
            //------------------------
            //------------------------
            Site_ES = _Proyeccion.Lugar_ES;
            Site_EN = _Proyeccion.Lugar_EN;
            Description_EN = _Proyeccion.Descripcion_EN;
            Description_ES = _Proyeccion.Descripcion_ES;
            Event_EN = _Proyeccion.Proyeccion_EN;
            Event_ES = _Proyeccion.Proyeccion_ES;
            ImageEvent = _Proyeccion.Image;
            ImageWeb = _Proyeccion.ImageWeb;
            //------------------------
            //------------------------
            //------------------------
            FK_IdTipoEvento__SCH = _Proyeccion.FK_IdTipoEvento__SCH;
            Type_ES = _Proyeccion.TipoEvento.Tipo_ES;
            Type_EN = _Proyeccion.TipoEvento.Tipo_EN;
            IconType = _Proyeccion.TipoEvento.icon;
            ClassNameType = _Proyeccion.TipoEvento.className;
            DetailUrl = _Proyeccion.TipoEvento.DetailUrl;
            //------------------------
            //------------------------
            //------------------------
            Place = new PlaceViewModel(_Proyeccion.Programa.Sede);
            Date = new DayViewModel(_Proyeccion);
            //------------------------
            //------------------------
            //------------------------
            DateFilter = _Proyeccion.Programa.Date;
            PlaceFilter = _Proyeccion.Programa.Sede.PK_IdSede;
            TypeFilter = _Proyeccion.TipoEvento.PK_IdTipoEvento;

        }
        #endregion

        #region Attr
        public int Id { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public string UrlEvent { get; set; }
        public int FK_IdTipoEvento__SCH { get; set; }
        public string Type_ES { get; set; }
        public string Type_EN { get; set; }
        public string IconType { get; set; }
        public string ClassNameType { get; set; }
        public string Site_ES { get; set; }
        public string Site_EN { get; set; }
        public string Description_EN { get; set; }
        public string Description_ES { get; set; }
        public string Event_EN { get; set; }
        public string Event_ES { get; set; }
        public string ImageEvent { get; set; }
        public string DetailUrl { get; set; }
        public PlaceViewModel Place { get; set; }
        public DayViewModel Date { get; set; }
        public DateTime DateFilter { get; set; }
        public int PlaceFilter { get; set; }
        public int TypeFilter { get; set; }

        #endregion


        public string ImageWeb { get; set; }
    }
    public class DayViewModel
    {
        public DayViewModel(Evento _Evento)
        {
            DateObj = _Evento.Programa.Date;
            Day = _Evento.Programa.Date.ToString("dd");
            DateString = string.Format("{0} {1}", _Evento.Programa.Date.ToString("dd"), getMonthString(_Evento.Programa.Date));
            MonthString = getMonthString(_Evento.Programa.Date);
        }

        public DayViewModel(Proyeccion _Proyeccion)
        {
            DateObj = _Proyeccion.Programa.Date;
            Day = _Proyeccion.Programa.Date.ToString("dd");
            DateString = string.Format("{0} {1}", _Proyeccion.Programa.Date.ToString("dd"), getMonthString(_Proyeccion.Programa.Date));
            MonthString = getMonthString(_Proyeccion.Programa.Date);
        }
        public DayViewModel(DateTime _Date)
        {
            DateObj = _Date;
            Day = DateObj.ToString("dd");
            DateString = string.Format("{0} {1}", DateObj.ToString("dd"), getMonthString(_Date));
            MonthString = getMonthString(_Date);
        }
        public DateTime DateObj { get; set; }
        public string Day { get; set; }
        public string DateString { get; set; }
        private string getMonthString(DateTime _day)
        {
            switch (_day.Month)
            {
                case 1: return "ENE";
                case 2: return "FEB";
                case 3: return "MAR";
                case 4: return "ABR";
                case 5: return "MAY";
                case 6: return "JUN";
                case 7: return "JUL";
                case 8: return "AGO";
                case 9: return "SEP";
                case 10: return "OCT";
                case 11: return "NOV";
                case 12: return "DIC"; 
            }
            return string.Empty;
        }
        public string MonthString { get; set; }
    }
    public class ProjectionViewModel : EventoViewModel
    {
        public ProjectionViewModel(Proyeccion _Proyeccion)
            : base(_Proyeccion)
        {
            ListShortFilms = _Proyeccion.AppShortFilm.Select(e => new ShortFilmViewModel(e)).ToList();
            ListShortFilmsMuestra = _Proyeccion.Muestra.Select(e => new ShortFilmMuestra(e)).ToList();
        }
        public List<ShortFilmViewModel> ListShortFilms { get; set; }
        public List<ShortFilmMuestra> ListShortFilmsMuestra { get; set; }
    }
    public class ShortFilmMuestra
    {
        public ShortFilmMuestra(Muestra _Muestra)
        {
            PK_IdMuestra = _Muestra.PK_IdMuestra;
            Titulo_Original = _Muestra.Titulo_Original;
            Titulo_Espanol = _Muestra.Titulo_Espanol;
            Sinopsis_Espanol = _Muestra.Sinopsis_Espanol;
            Sinopsis_Ingles = _Muestra.Sinopsis_Ingles;
            Duracion = _Muestra.Duracion;
            Director = _Muestra.Director;
            Pais = _Muestra.Pais;
            Edicion = _Muestra.Edicion;
            ListImagenStill = new List<ImageViewModel>();
            ListImagenStill.Add(new ImageViewModel {
                Host = PublicServiceBussines.HostImageMuestra,
                Path = _Muestra.Path,
                File = _Muestra.File
            });
        }
        public int PK_IdMuestra { get; set; }
        public string Titulo_Original { get; set; }
        public string Titulo_Espanol { get; set; }
        public string Titulo_Ingles { get; set; }
        public string Sinopsis_Espanol { get; set; }
        public string Sinopsis_Ingles { get; set; }
        public string Duracion { get; set; }
        public string Director { get; set; }
        public string Pais { get; set; }
        public int Edicion { get; set; }
        public List<ImageViewModel> ListImagenStill { get; set; }
    }
    public class ShortFilmGanadorViewModel : ShortFilmSimpleViewModel
    {

        public ShortFilmGanadorViewModel(ECO._2015.Models.App.AppShortFilm _AppShortFilm, string Descripcion_ES, string Descipcion_EN, string Premio)
            : base(_AppShortFilm)
        {
            //------------------------------
            Descripcion_Ganador_ES = Descripcion_ES;
            Descripcion_Ganador_EN = Descipcion_EN;
            Premio_Ganador = Premio;
            Path = _AppShortFilm.Path;
            Video = _AppShortFilm.Video;
            Host = "http://ecofilmfestival.info"; 
            //------------------------------
            var _still = _AppShortFilm.AppImagenStill.FirstOrDefault();
            if (_still != null)
            {
                Still = new ImageViewModel(_still, PublicServiceBussines.HostImageStill);
            }
        }
        public string Descripcion_Ganador_ES { get; set; }

        public string Descripcion_Ganador_EN { get; set; }
        public ImageViewModel Still { get; set; }

        public string Premio_Ganador { get; set; }

        public string Path { get; set; }

        public string Video { get; set; }

        public string Host { get; set; }
    }
    public class ShortFilmSimpleViewModel
    {
        public ShortFilmSimpleViewModel(ECO._2015.Models.App.AppShortFilm _AppShortFilm)
        {
            Id = _AppShortFilm.Id;
            Categoria = new CategoriaViewModel(_AppShortFilm.AppCategoria);
            Nacionalidad = new NacionalidadViewModel(_AppShortFilm.AppEstado.AppPais.AppNacionalidad);
            Titulo_Original = _AppShortFilm.Titulo_Original;
            CategoriaId = _AppShortFilm.CategoriaId;
            NacionalidadId = _AppShortFilm.AppEstado.AppPais.AppNacionalidad.Id;
            var Director = _AppShortFilm.AppEquipo.Where(e => e.AppTipoEquipo.Id == 1).FirstOrDefault();
            if(Director != null)
            {
                DirectorCorto = Director.Nombre;
            }

        }
        public int Id { get; set; }
        public string Titulo_Original { get; set; }
        public CategoriaViewModel Categoria { get; set; }
        public NacionalidadViewModel Nacionalidad { get; set; }
        public string DirectorCorto { get; set; }
        public int NacionalidadId { get; set; }
        public int CategoriaId { get; set; }
    }
    public class ShortFilmViewModel : ShortFilmSimpleViewModel
     {
        public ShortFilmViewModel(ECO._2015.Models.App.AppShortFilm _AppShortFilm)
            : base(_AppShortFilm)
        {
            var _dirEntity = _AppShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == PublicServiceBussines.Director).FirstOrDefault();
            var _directorName = (_dirEntity != null) ? _dirEntity.Nombre : "";
            var _infoES = string.Format("{0} / {1} / {2}", _directorName, _AppShortFilm.AppCategoria.Categoria_ES, _AppShortFilm.AppEstado.AppPais.Pais_ES);
            var _infoEN = string.Format("{0} / {1} / {2}", _directorName, _AppShortFilm.AppCategoria.Categoria_EN, _AppShortFilm.AppEstado.AppPais.Pais_EN);

            Titulo_ES = _AppShortFilm.Titulo_Espanol;
            Titulo_EN = _AppShortFilm.Titulo_Ingles;
            Info_ES = _infoES;
            Info_EN = _infoEN;
            Edicion = _AppShortFilm.Edicion;
            Categoria_ES = _AppShortFilm.AppCategoria.Categoria_ES;
            Categoria_EN = _AppShortFilm.AppCategoria.Categoria_EN;
            DirectorCorto_ES = _directorName;
            DirectorCorto_EN = _directorName;
            Nacionalidad_ES = _AppShortFilm.AppEstado.AppPais.AppNacionalidad.Nacionalidad_ES;
            Nacionalidad_EN = _AppShortFilm.AppEstado.AppPais.AppNacionalidad.Nacionalidad_EN;

            FormatoVideoId = _AppShortFilm.FormatoVideoId;
            Formato_ES = _AppShortFilm.AppFormatoVideo.Formato_ES;
            Formato_EN = _AppShortFilm.AppFormatoVideo.Formato_EN;
            FormatoAudioId = _AppShortFilm.FormatoAudioId;
            FormatoAudio_ES = _AppShortFilm.AppFormatoAudio.Formato_ES;
            FormatoAudio_EN = _AppShortFilm.AppFormatoAudio.Formato_EN;
            EstadoId = _AppShortFilm.EstadoId;
            Estado_ES = _AppShortFilm.AppEstado.Estado_ES;
            Estado_EN = _AppShortFilm.AppEstado.Estado_EN;
            PaisId = _AppShortFilm.AppEstado.AppPais.Id;
            Pais_ES = _AppShortFilm.AppEstado.AppPais.Pais_ES;
            Pais_EN = _AppShortFilm.AppEstado.AppPais.Pais_EN;
            AnnioId = _AppShortFilm.AnnoId;
            Annio = _AppShortFilm.AppAnno.Descripcion;
            SubtituloId = _AppShortFilm.SubtituloId;
            Subtitulo_ES = _AppShortFilm.AppSubtitulo.Subtitulo_ES;
            Subtitulo_EN = _AppShortFilm.AppSubtitulo.Subtitulo_EN;
            Duracion_Minutos = _AppShortFilm.Duracion_Minutos;
            Duracion_Segundos = _AppShortFilm.Duracion_Segundos;
            Sinopsis_Espanol = _AppShortFilm.Sinopsis_Espanol;
            Sinopsis_Ingles = _AppShortFilm.Sinopsis_Ingles;
            Titulo_Espanol = _AppShortFilm.Titulo_Espanol;
            Titulo_Ingles = _AppShortFilm.Titulo_Ingles;
            Idioma = _AppShortFilm.Idioma;
            OtroFormatoVideo = _AppShortFilm.OtroFormatoVideo;
            OtroFormatoAudio = _AppShortFilm.OtroFormatoAudio;
            FechaRegistro = _AppShortFilm.FechaRegistro;
            ListImagenAutor = _AppShortFilm.AppImagenAutor.Select(e => new ImageViewModel(e, PublicServiceBussines.HostImageAutor)).ToList();
            ListImagenStill = _AppShortFilm.AppImagenStill.Select(e => new ImageViewModel(e, PublicServiceBussines.HostImageStill)).ToList();
            ListEquipo = _AppShortFilm.AppEquipo.Select(e => new EquipoViewModel(e)).ToList();

            ListEvento = new List<EventoViewModel>();
            if (_AppShortFilm.Edicion == PublicServiceBussines.EdicionFestivalApp && DateTime.Now >= PublicServiceBussines.VerProgramaFechaApp)
            {
                _AppShortFilm.Proyeccion.ToList().ForEach(e => ListEvento.Add(new EventoViewModel(e)));
            }
            else if(_AppShortFilm.Edicion != PublicServiceBussines.EdicionFestivalApp)
            {
                _AppShortFilm.Proyeccion.ToList().ForEach(e => ListEvento.Add(new EventoViewModel(e)));
            }
        }
        #region Attrs
        public string Titulo_ES { get; set; }
        public string Titulo_EN { get; set; }
        public string Info_ES { get; set; }
        public string Info_EN { get; set; }
        public int Edicion { get; set; }
        public string Nacionalidad_ES { get; set; }
        public string Categoria_ES { get; set; }
        public string Nacionalidad_EN { get; set; }
        public string Categoria_EN { get; set; }
        public string DirectorCorto_ES { get; set; }
        public string DirectorCorto_EN { get; set; }
        public int FormatoVideoId { get; set; }
        public string Formato_ES { get; set; }
        public string Formato_EN { get; set; }
        public int FormatoAudioId { get; set; }
        public string FormatoAudio_ES { get; set; }
        public string FormatoAudio_EN { get; set; }
        public int EstadoId { get; set; }
        public string Estado_ES { get; set; }
        public string Estado_EN { get; set; }
        public int AnnioId { get; set; }
        public string Annio { get; set; }
        public int SubtituloId { get; set; }
        public string Subtitulo_ES { get; set; }
        public string Subtitulo_EN { get; set; }
        public int Duracion_Minutos { get; set; }
        public int Duracion_Segundos { get; set; }
        
        public string Titulo_Espanol { get; set; }
        public string Titulo_Ingles { get; set; }
        public string Sinopsis_Espanol { get; set; }
        public string Sinopsis_Ingles { get; set; }
        public string Idioma { get; set; }
        public string OtroFormatoVideo { get; set; }
        public string OtroFormatoAudio { get; set; }
        public DateTime FechaRegistro { get; set; }
        public int PaisId { get; set; }
        public string Pais_ES { get; set; }
        public string Pais_EN { get; set; }
        public List<ImageViewModel> ListImagenAutor { get; set; }
        public List<ImageViewModel> ListImagenStill { get; set; }
        public List<EquipoViewModel> ListEquipo { get; set; }

        #endregion

        public List<EventoViewModel> ListEvento { get; set; }

     }
    public class ImageViewModel
    {
        public ImageViewModel(){ }

        public ImageViewModel(AppImagenAutor _AppImagenAutor, string HostImage)
        {
            Host = HostImage;
            Path = _AppImagenAutor.Path;
            File = _AppImagenAutor.File; ;
        }

        public ImageViewModel(AppImagenStill _AppImagenStill, string HostImage)
        {
            Host = HostImage;
            Path = _AppImagenStill.Path;
            File = _AppImagenStill.File;
        }

        public string Host { get; set; }
        public string Path { get; set; }
        public string File { get; set; }

    }
    public class EquipoViewModel
    {
        public EquipoViewModel(AppEquipo _AppEquipo)
        {
            Id = _AppEquipo.Id;
            Nombre = _AppEquipo.Nombre;
            Estudios = _AppEquipo.Estudios;
            Email = _AppEquipo.Email;
            Estudiante = _AppEquipo.Estudiante;
            Profecional = _AppEquipo.Profecional;
            Ciudad_Estudio = _AppEquipo.Ciudad_Estudio;
            Mexicano = _AppEquipo.Mexicano;
            Nacionalidad = _AppEquipo.Nacionalidad;
            Biografia = _AppEquipo.Biografia;
            IdTipoEquipo = _AppEquipo.IdTipoEquipo;
            TipoEquipo_ES = _AppEquipo.AppTipoEquipo.TipoEquipo_ES;
            TipoEquipo_EN = _AppEquipo.AppTipoEquipo.TipoEquipo_EN;
            ShortFilm_Id = _AppEquipo.ShortFilm_Id;
            Facebook = _AppEquipo.Facebook;
            Twiiter = _AppEquipo.Twiiter;
            Ocupacion = _AppEquipo.Ocupacion;
            Casa_Productora = _AppEquipo.Casa_Productora;
            Web_Site_Casa_Productora = _AppEquipo.Web_Site_Casa_Productora;
        }

        #region Attrs
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Estudios { get; set; }
        public string Email { get; set; }
        public bool Estudiante { get; set; }
        public bool Profecional { get; set; }
        public string Ciudad_Estudio { get; set; }
        public bool Mexicano { get; set; }
        public string Nacionalidad { get; set; }
        public string Biografia { get; set; }
        public int IdTipoEquipo { get; set; }
        public string TipoEquipo_ES { get; set; }
        public string TipoEquipo_EN { get; set; }
        public int? ShortFilm_Id { get; set; }
        public string Direccion { get; set; }
        public string Facebook { get; set; }
        public string Twiiter { get; set; }
        public string Ocupacion { get; set; }
        public bool Casa_Productora { get; set; }
        public string Web_Site_Casa_Productora { get; set; }
        #endregion
    }
    public class JuradoViewModel 
    {
        public JuradoViewModel(ECO._2015.Models.App.AppJurado _Jurado)
        {
            Id = _Jurado.Id;
            Descripcion = _Jurado.Descripcion;
            TipoJurado_Id = _Jurado.TipoJurado_Id;
            TipoJurado = _Jurado.AppTipoJurado.Descripcion;
            Nacionalidad_ES = _Jurado.AppTipoJurado.AppNacionalidad.Nacionalidad_ES;
            Nacionalidad_EN = _Jurado.AppTipoJurado.AppNacionalidad.Nacionalidad_EN;
            Image = new ImageViewModel { File = _Jurado.Image };
            Edicion = _Jurado.Edicion;
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int TipoJurado_Id { get; set; }
        public string UserId { get; set; }
        public ImageViewModel Image { get; set; }
        public bool Jurado_Activo { get; set; }
        public int Edicion { get; set; }

        public string TipoJurado { get; set; }

        public string Nacionalidad_ES { get; set; }

        public string Nacionalidad_EN { get; set; }
    }
    public class WinnersViewModel : OfficialSelectionViewModel
    {

        public WinnersViewModel(List<AppGanador> _listGanador, List<AppCategoria> _listCategoria, List<ECO._2015.Models.App.AppNacionalidad> _listNacionalidad , bool Active = false)
        {
            ListShortFilmWinners = new List<ShortFilmGanadorViewModel>();

            if (PublicServiceBussines.isWinnersOpen || Active)
            {
                _listGanador.ForEach(g => ListShortFilmWinners.Add(new ShortFilmGanadorViewModel(g.AppShortFilm, g.Descripcion_ES, g.Descipcion_EN, g.Premio)));
            }
            //---------------------
            ListCategoria = new List<CategoriaViewModel>();
            _listCategoria.ForEach(e => ListCategoria.Add(new CategoriaViewModel(e)));
            //---------------------
            ListNacionalidad = new List<NacionalidadViewModel>();
            _listNacionalidad.ForEach(e => ListNacionalidad.Add(new NacionalidadViewModel(e)));
        }
        public string WinnersMsg
        {
            get
            {
                return (PublicServiceBussines.isWinnersOpen) ? ConfigurationManager.AppSettings["WinnersMsgOpen"] : ConfigurationManager.AppSettings["WinnersMsgClose"];
            }
        }

        

        public List<ShortFilmGanadorViewModel> ListShortFilmWinners { get; set; }
    }
    public class OfficialSelectionViewModel
    {
        public OfficialSelectionViewModel() { }
        public OfficialSelectionViewModel(List<ECO._2015.Models.App.AppShortFilm> _listSeleccion, List<AppCategoria> _listCategoria, List<ECO._2015.Models.App.AppNacionalidad> _listNacionalidad)
        {
            ListShortFilm = new List<ShortFilmSimpleViewModel>();
            _listSeleccion.ForEach(e => ListShortFilm.Add(new ShortFilmSimpleViewModel(e)));
            //---------------------
            ListCategoria = new List<CategoriaViewModel>();
            _listCategoria.ForEach(e => ListCategoria.Add(new CategoriaViewModel(e)));
            //---------------------
            ListNacionalidad = new List<NacionalidadViewModel>();
            _listNacionalidad.ForEach(e => ListNacionalidad.Add(new NacionalidadViewModel(e)));
        }


        public List<ShortFilmSimpleViewModel> ListShortFilm { get; set; }
        public List<CategoriaViewModel> ListCategoria { get; set; }
        public List<NacionalidadViewModel> ListNacionalidad { get; set; }

    }
    public class ShortFilmDetailViewModel : ShortFilmViewModel
    {
        public ShortFilmDetailViewModel(ECO._2015.Models.App.AppShortFilm _AppShortFilm)
            : base(_AppShortFilm)
        { 
            ListEventoViewModel = new List<EventoViewModel>();
            _AppShortFilm.Proyeccion.ToList().ForEach(e => ListEventoViewModel.Add(new EventoViewModel(e)));
        }

        public List<EventoViewModel> ListEventoViewModel { get; set; }
    }
    public class CategoriaViewModel
    {
        public CategoriaViewModel(AppCategoria _AppCategoria)
        {
            Id = _AppCategoria.Id;
            Title_ES = _AppCategoria.Categoria_ES;
            Title_EN = _AppCategoria.Categoria_EN;
            Icon = _AppCategoria.Icon;
        }
        public int Id { get; set; }
        public string Title_ES { get; set; }
        public string Title_EN { get; set; }
        public string Icon { get; set; }
    }
    public class NacionalidadViewModel
    {
        public NacionalidadViewModel(ECO._2015.Models.App.AppNacionalidad _AppNacionalidad)
        {
            Id = _AppNacionalidad.Id;
            Title_ES = _AppNacionalidad.Nacionalidad_ES;
            Title_EN = _AppNacionalidad.Nacionalidad_EN;
        }

        public int Id { get; set; }
        public string Title_ES { get; set; }
        public string Title_EN { get; set; }
    }
    public class TipoEventoViewModel
    {
        public TipoEventoViewModel(TipoEvento _TipoEvento)
        {
            icon = _TipoEvento.icon;
            className = _TipoEvento.className;
            Tipo_ES = _TipoEvento.Tipo_ES;
            Tipo_EN = _TipoEvento.Tipo_EN;
            PK_IdTipoEvento = _TipoEvento.PK_IdTipoEvento;
        }


        public string icon { get; set; }

        public string className { get; set; }

        public string Tipo_ES { get; set; }

        public string Tipo_EN { get; set; }

        public int PK_IdTipoEvento { get; set; }
    }

    #endregion

    #region ViewModel His

    public class EditionViewModel
    {
        public static implicit operator EditionViewModel(ECO._2015.Models.App.Edicion _entity)
        {
            return new EditionViewModel {
                Id = _entity.PK_IdEdicion ,
                year = _entity.PK_IdEdicion,
                Nombre_ES = string.Format("{0} {1}",_entity.Nombre_ES,_entity.Tema_ES) ,
                Nombre_EN = string.Format("{0} {1}", _entity.Nombre_EN, _entity.Tema_EN),
                festival = true,
                selOf =  _entity.AppShortFilm.Where(e => e.Sel_Oficial).Count() > 0,
                jury = _entity.AppJurado.Where(e => e.Jurado_Activo).Count() > 0,
                schedule = (_entity.Programa.Count > 0 ), // && PublicServiceBussines.EdicionFestival == _entity.PK_IdEdicion),
                winners = (PublicServiceBussines.EdicionFestival == _entity.PK_IdEdicion) ? PublicServiceBussines.isWinnersOpen : true,
                actual = PublicServiceBussines.EdicionFestival == _entity.PK_IdEdicion
            };
        }

        public static implicit operator EditionViewModel(ECO._2015.Models.His.Edicion _entity)
        {
            return new EditionViewModel
            {
                Id = _entity.PK_IdEdicion,
                year = _entity.PK_IdEdicion,
                Nombre_ES = string.Format("{0} {1}", _entity.Nombre_ES, _entity.Tema_ES),
                Nombre_EN = string.Format("{0} {1}", _entity.Nombre_EN, _entity.Tema_EN),
                festival = true,
                selOf = _entity.SeleccionOficial.Count() > 0,
                jury = _entity.AppJurado.Count() > 0,
                schedule = false,
                winners = true,
                actual = PublicServiceBussines.EdicionFestival == _entity.PK_IdEdicion
            };
        }

        #region Attr
        public int Id { get; set; }
        public string Nombre_ES { get; set; }
        public string Nombre_EN { get; set; }
        public int year { get; set; }
        public bool festival { get; set; }
        public bool selOf { get; set; }
        public bool jury { get; set; }
        public bool schedule { get; set; }
        public bool winners { get; set; }
        public bool actual { get; set; }
        #endregion
    }

    public class EditionHisViewModel : EditionViewModel
    {
        public EditionHisViewModel()
        {
            Jurado = new List<JuradoHisViewModel>();
            Galeria = new List<GaleriaHisViewModel>();
            SeleccionOficial = new List<SeleccionOficialHisViewModel>();
            Ganadores = new List<VideoGaleria>();
        }
        public static implicit operator EditionHisViewModel(ECO._2015.Models.App.Edicion _entity)
        {

            var _result = new EditionHisViewModel
            {
                Id = _entity.PK_IdEdicion,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Tema_ES = _entity.Tema_ES,
                Tema_EN = _entity.Tema_EN,
                Info_ES = _entity.Info_ES,
                Info_EN = _entity.Info_EN,
                BasesConvocatoria_ES = _entity.BasesConvocatoria_ES,
                BasesConvocatoria_EN = _entity.BasesConvocatoria_EN,
                Cartel_ES = _entity.Cartel_ES,
                Cartel_EN = _entity.Cartel_EN,
                Logotipo_ES = _entity.Logotipo_ES,
                Logotipo_EN = _entity.Logotipo_EN,
                Teaser_ES = _entity.Teaser_ES,
                Teaser_EN = _entity.Teaser_EN,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN,
                Direccion_ES = _entity.Direccion_ES,
                Direccion_EN = _entity.Direccion_EN,
                MapaContato = _entity.MapaContato,
                ManualGrafico_ES = _entity.ManualGrafico_ES,
                ManualGrafico_EN = _entity.ManualGrafico_EN,
                Catalogo_ES = _entity.Catalogo_ES,
                Catalogo_EN = _entity.Catalogo_EN,
                Boletin_EN = _entity.Boletin_EN,
                Boletin_ES = _entity.Boletin_ES,
            };

            using (var _dbHis = new DBContextHis())
            {
                var _entHis = _dbHis.Edicion.Find(_entity.PK_IdEdicion);
                if (_entity != null)
                {
                    _entHis.Galeria.ToList().ForEach(e => _result.Galeria.Add(e));
                }
            }

            try
            {
                var _listPhotos = FlickrBussines.PhotosetsGetPhotos(_entity.FlickrGaleria.Select(e => e.photosetId).ToArray());
                _listPhotos.Select(e => new GaleriaHisViewModel
                {
                    Id = 0,
                    Edicion = _entity.PK_IdEdicion,
                    Image = e.MediumUrl,
                }).ToList().ForEach(e => _result.Galeria.Add(e));
            }
            catch
            {
                _result.Galeria = new List<GaleriaHisViewModel>();
            }
            
            
            
            _entity.AppJurado.Where(e => e.Jurado_Activo).ToList().ForEach(e => _result.Jurado.Add(e));

            if (_entity.PK_IdEdicion == PublicServiceBussines.EdicionFestival)
            {
                if(PublicServiceBussines.isOficialSelectionOpen)
                    _entity.AppShortFilm.Where(e => e.Sel_Oficial).ToList().ForEach(e => _result.SeleccionOficial.Add(e));
            }
            else
            {
                _entity.AppShortFilm.Where(e => e.Sel_Oficial).ToList().ForEach(e => _result.SeleccionOficial.Add(e));
            }
            

            // ------------------------------------
            using (var _dbVis = new DBContextVis())
            {
                var _list = _dbVis.VideoGaleria.Where(e => e.Edicion == _entity.PK_IdEdicion).OrderByDescending(e => e.Año).ThenBy(e => e.Orden).ToList();
                _result.Ganadores = _list.Where(e => (e.Edicion == PublicServiceBussines.EdicionFestivalApp) ? PublicServiceBussines.isWinnersOpen : true).ToList();
            }
            // ------------------------------------
            return _result;
        }


        public static implicit operator EditionHisViewModel(ECO._2015.Models.His.Edicion _entity)
        {
            var _result = new EditionHisViewModel
            {
                Id = _entity.PK_IdEdicion,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Tema_ES = _entity.Tema_ES,
                Tema_EN = _entity.Tema_EN,
                Info_ES = _entity.Info_ES,
                Info_EN = _entity.Info_EN,
                BasesConvocatoria_ES = _entity.BasesConvocatoria_ES,
                BasesConvocatoria_EN = _entity.BasesConvocatoria_EN,
                Cartel_ES = _entity.Cartel_ES,
                Cartel_EN = _entity.Cartel_EN,
                Logotipo_ES = _entity.Logotipo_ES,
                Logotipo_EN = _entity.Logotipo_EN,
                Teaser_ES = _entity.Teaser_ES,
                Teaser_EN = _entity.Teaser_EN,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN,
                Direccion_ES = _entity.Direccion_ES,
                Direccion_EN = _entity.Direccion_EN,
                MapaContato = _entity.MapaContato,
                ManualGrafico_ES = _entity.ManualGrafico_ES,
                ManualGrafico_EN = _entity.ManualGrafico_EN,
                Catalogo_ES = _entity.Catalogo_ES,
                Catalogo_EN = _entity.Catalogo_EN,
            };

            _entity.Galeria.ToList().ForEach(e => _result.Galeria.Add(e));
            _entity.AppJurado.ToList().ForEach(e => _result.Jurado.Add(e));
            _entity.SeleccionOficial.ToList().ForEach(e => _result.SeleccionOficial.Add(e));
            // ------------------------------------
            using (var _dbVis = new DBContextVis())
            {
                _result.Ganadores = _dbVis.VideoGaleria.Where(e => e.Edicion == _entity.PK_IdEdicion).OrderByDescending(e => e.Año).ThenBy(e => e.Orden).ToList();
            }
            // ------------------------------------
            return _result;
        }

        #region Attr


        public string Tema_ES { get; set; }

        public string Tema_EN { get; set; }

        public string Info_ES { get; set; }

        public string Info_EN { get; set; }

        public string BasesConvocatoria_ES { get; set; }

        public string BasesConvocatoria_EN { get; set; }

        public string Cartel_ES { get; set; }

        public string Cartel_EN { get; set; }

        public string Logotipo_ES { get; set; }

        public string Logotipo_EN { get; set; }

        public string Teaser_ES { get; set; }

        public string Teaser_EN { get; set; }

        public string Informacion_ES { get; set; }

        public string Informacion_EN { get; set; }

        public string Direccion_ES { get; set; }

        public string Direccion_EN { get; set; }

        public string MapaContato { get; set; }

        public string ManualGrafico_ES { get; set; }

        public string ManualGrafico_EN { get; set; }

        public string Catalogo_ES { get; set; }

        public string Catalogo_EN { get; set; }

        public string Boletin_EN { get; set; }

        public string Boletin_ES { get; set; }
        #endregion

        public List<JuradoHisViewModel> Jurado { get; set; }
        public List<GaleriaHisViewModel> Galeria { get; set; }
        public List<SeleccionOficialHisViewModel> SeleccionOficial { get; set; }
        public List<VideoGaleria> Ganadores { get; set; }
    }

    public class JuradoHisViewModel
    {
        public static implicit operator JuradoHisViewModel(ECO._2015.Models.App.AppJurado _entity)
        {
            return new JuradoHisViewModel
            {
                Id = _entity.Id,
                Descripcion = _entity.Descripcion,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN,
                Image = _entity.Image,
                Edicion = _entity.Edicion,
                TipoJurado = _entity.AppTipoJurado.Descripcion,
            };
        }

        public static implicit operator JuradoHisViewModel(ECO._2015.Models.His.AppJurado _entity)
        { 
            return new JuradoHisViewModel
            {
                Id = _entity.Id,
                Descripcion = _entity.Descripcion,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN,
                Image = _entity.Image,
                Edicion = _entity.Edicion,
                TipoJurado = "",
            };
        }

        #region Attr
        public int Id { get; set; }

        public string Descripcion { get; set; }

        public string Informacion_ES { get; set; }

        public string Informacion_EN { get; set; }

        public string Image { get; set; }

        public int Edicion { get; set; }
        public string TipoJurado { get; set; }
        #endregion 
    }

    public class GaleriaHisViewModel
    {
        public static implicit operator GaleriaHisViewModel(ECO._2015.Models.His.Galeria _entity)
        {
            return new GaleriaHisViewModel 
            { 
                Id = _entity.Id,
                Image = _entity.Image,
                Edicion = _entity.Edicion,
            };
        }

        #region Attr
        public int Id { get; set; }

        public string Image { get; set; }

        public int Edicion { get; set; }
        #endregion
    }

    public class SeleccionOficialHisViewModel
    {
        public static implicit operator SeleccionOficialHisViewModel(ECO._2015.Models.App.AppShortFilm _entity)
        {
            var _dirEntity = _entity.AppEquipo.Where(e => e.IdTipoEquipo == PublicServiceBussines.Director).FirstOrDefault();
            var _directorName = (_dirEntity != null) ? _dirEntity.Nombre : "";
            var _infoES = string.Format("{0} / {1} / {2}", _directorName, _entity.AppCategoria.Categoria_ES, _entity.AppEstado.AppPais.Pais_ES);
            var _infoEN = string.Format("{0} / {1} / {2}", _directorName, _entity.AppCategoria.Categoria_EN, _entity.AppEstado.AppPais.Pais_EN);

            return new SeleccionOficialHisViewModel
            {
                Id = _entity.Id,
                Titulo_ES = _entity.Titulo_Espanol,
                Titulo_EN = _entity.Titulo_Ingles,
                Info_ES = _infoES,
                Info_EN = _infoEN,
                Edicion = _entity.Edicion,
                Categoria_ES = _entity.AppCategoria.Categoria_ES,
                Categoria_EN = _entity.AppCategoria.Categoria_EN,
                DirectorCorto_ES = _directorName,
                DirectorCorto_EN = _directorName,
                Nacionalidad_ES = _entity.AppEstado.AppPais.AppNacionalidad.Nacionalidad_ES ,
                Nacionalidad_EN = _entity.AppEstado.AppPais.AppNacionalidad.Nacionalidad_EN
            };
        }
        public static implicit operator SeleccionOficialHisViewModel(ECO._2015.Models.His.SeleccionOficial _entity)
        {
            return new SeleccionOficialHisViewModel { 
                Id = _entity.Id,
                Titulo_ES = _entity.Titulo_ES,
                Titulo_EN = _entity.Titulo_EN,
                Info_ES = _entity.Info_ES,
                Info_EN = _entity.Info_EN,
                Edicion = _entity.Edicion,
                Categoria_ES = _entity.Categoria_ES,
                Categoria_EN = _entity.Categoria_EN,
                DirectorCorto_ES = _entity.DirectorCorto,
                DirectorCorto_EN = _entity.DirectorCorto,
                Nacionalidad_ES = _entity.Nacionalidad_ES,
                Nacionalidad_EN = _entity.Nacionalidad_EN
            };
        }

        #region Attr
        public int Id { get; set; }

        public string Titulo_ES { get; set; }

        public string Titulo_EN { get; set; }

        public string Info_ES { get; set; }

        public string Info_EN { get; set; }

        public int Edicion { get; set; }
        public string Nacionalidad_ES { get; set; }
        public string Categoria_ES { get; set; }
        public string Nacionalidad_EN { get; set; }
        public string Categoria_EN { get; set; }
        public string DirectorCorto_ES { get; set; }
        public string DirectorCorto_EN { get; set; }
        #endregion
    }
    #endregion

    #endregion



}
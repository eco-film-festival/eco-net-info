﻿using ECO._2015.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ECO._2015.Controllers.Services
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ConfigServiceController : ApiController
    {
        #region DB
        private DBContextApp _db = new DBContextApp();
        #endregion

        #region Boletin
        [HttpGet]
        [ActionName("Boletin")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetBoletin()
        {
            var _list = new List<DisplayBoletinViewModel>();
            _db.Boletin.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Boletin")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetBoletin(int id)
        {
            var _entity = _db.Boletin.Find(id);
            if (_entity != null)
            {
                return Ok((DisplayBoletinViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Boletin")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostBoletin(DisplayBoletinViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            if (_edition != null)
            {
                var _entity = (Boletin)_model;
                _edition.Boletin.Add(_entity);
                _db.SaveChanges();
                return Ok((DisplayBoletinViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("Boletin")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutBoletin(int id , DisplayBoletinViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Boletin.Find(id);
            if (_entity != null)
            {
                _model.ToEntityBoletin(ref _entity);
                _db.SaveChanges();
                return Ok((DisplayBoletinViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Boletin")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteBoletin(int id)
        {
            var _entity = _db.Boletin.Find(id);
            if (_entity != null)
            {
                _db.Boletin.Remove(_entity);
                _db.SaveChanges();
                return Ok((DisplayBoletinViewModel)_entity);
            }
            return NotFound();
        }
        #endregion

        #region Contacto

        [HttpGet]
        [ActionName("Contacto")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetContacto()
        {
            var _list = new List<ContactoViewModel>();
            _db.Contacto.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Contacto")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetContacto(int id)
        {
            var _entity = _db.Contacto.Find(id);
            if (_entity != null)
            {
                return Ok((ContactoViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Contacto")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostContacto(ContactoViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            if (_edition != null)
            {
                var _entity = (Contacto)_model;
                _edition.Contacto.Add(_entity);
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((ContactoViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("Contacto")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutContacto(int id, ContactoViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Contacto.Find(_model.Id);
            if (_entity != null)
            {
                _model.ToEntityContacto(ref _entity);
                try { 
                _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((ContactoViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Contacto")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteContacto(int id)
        {
            var _entity = _db.Contacto.Find(id);
            if (_entity != null)
            {
                _db.Contacto.Remove(_entity);
                _db.SaveChanges();
                return Ok((ContactoViewModel)_entity);
            }
            return NotFound();
        }
        #endregion

        #region Edicion
        [HttpGet]
        [ActionName("Edicion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetEdicion()
        {
            var _list = new List<EdicionSimpleWithParent>();
            
            _db.Edicion
                .ToList()
                .ForEach(
                    e => _list.Add(e)
                );
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Edicion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetEdicion(int id)
        {
            var _entity = _db.Edicion.Find(id);
            if (_entity != null)
            {
                return Ok((EdicionSimpleWithParent)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Edicion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostEdicion(EdicionViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = (Edicion)_model;
            if(_model.FK_IdEdicionAnterior__SCH != null)
            {
                var _edicionAnterior = _db.Edicion.Find(_model.FK_IdEdicionAnterior__SCH);
                _edicionAnterior.AppCriterio.ToList().ForEach(e => {
                    _entity.AppCriterio.Add(
                        new AppCriterio {
                            Descripcion = e.Descripcion,
                            DescripcionCompleta = e.DescripcionCompleta,
                            Explicacion = e.Explicacion,
                            DescripcionCompleta_EN = e.DescripcionCompleta_EN,
                            Explicacion_EN = e.Explicacion_EN
                        }
                    );
                });
                _edicionAnterior.AppPreguntaJurado.ToList().ForEach(e => {
                    var _pregunta = new AppPreguntaJurado
                    {
                        Pregunta_ES = e.Pregunta_ES,
                        Pregunta_EN = e.Pregunta_EN,
                    };
                    e.AppTipoJurado.ToList().ForEach(tj =>
                    {
                        _pregunta.AppTipoJurado.Add(tj);
                    });
                    
                    _entity.AppPreguntaJurado.Add(
                        _pregunta
                    );
                });
                _edicionAnterior.Patrocinador.ToList().ForEach(e =>
                {
                    _entity.Patrocinador.Add(
                        new Patrocinador
                        {
                            FK_IdTipoPatrocinador__SCH = e.FK_IdTipoPatrocinador__SCH,
                            Descripcion_ES = e.Descripcion_ES,
                            Descripcion_EN = e.Descripcion_EN,
                            Image = e.Image,
                            WebSite = e.WebSite,
                            Home = e.Home
                        }
                    );
                });
            }
            _db.Edicion.Add(_entity);
            _db.SaveChanges();
            return Ok((EdicionSimpleWithParent)_entity);
        }

        [HttpPut]
        [ActionName("Edicion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutEdicion(int id, EdicionViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Edicion.Find(id);
            if (_entity != null)
            {
                _model.ToEntityEdicion(ref _entity);
                _db.SaveChanges();
                return Ok((EdicionSimpleWithParent)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Edicion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteEdicion(int id)
        {
            var _entity = _db.Edicion.Find(id);
            if (_entity != null)
            {
                _db.Edicion.Remove(_entity);
                _db.SaveChanges();
                return Ok((EdicionSimpleWithParent)_entity);
            }
            return NotFound();
        }

        #endregion

        #region EventoFestival
        [HttpGet]
        [ActionName("EventoFestival")]
        public IHttpActionResult GetEventoFestival()
        {
            var _list = new List<DisplayEventoFestivalViewModel>();
            _db.EventoFestival.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("EventoFestival")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetEventoFestival(int id)
        {
            var _entity = _db.EventoFestival.Find(id);
            if (_entity != null)
            {
                return Ok((DisplayEventoFestivalViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("EventoFestival")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostEventoFestival(DisplayEventoFestivalViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            if (_edition != null)
            {
                var _entity = (EventoFestival)_model;
                _edition.EventoFestival.Add(_entity);
                _db.SaveChanges();
                return Ok((DisplayEventoFestivalViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("EventoFestival")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutEventoFestival(int id, DisplayEventoFestivalViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.EventoFestival.Find(id);
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            if (_entity != null && _edition != null)
            {
                _model.ToEntityEventoFestival(ref _entity);
                _entity.Edicion = _edition;
                _db.SaveChanges();
                return Ok((DisplayEventoFestivalViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("EventoFestival")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteEventoFestival(int id)
        {
            var _entity = _db.EventoFestival.Find(id);
            if (_entity != null)
            {
                _db.EventoFestival.Remove(_entity);
                _db.SaveChanges();
                return Ok((DisplayEventoFestivalViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("EventoFestivalNota")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostEventoFestivalNota(int id, DisplayEventoNotasViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.EventoFestival.Find(id);
            if (_entity != null)
            {
                _entity.EventoNotas.Add((EventoNotas)_model);
                _db.SaveChanges();
                return Ok((DisplayEventoFestivalViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPut]
        [ActionName("EventoFestivalNota")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutEventoFestivalNota(int id, DisplayEventoNotasViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.EventoNotas.Find(_model.Id);
            if (_entity != null)
            {
                _model.ToEntityEventoNotas(ref _entity);
                _db.SaveChanges();
                var _eventoFestival = _db.EventoFestival.Find(id);
                return Ok((DisplayEventoFestivalViewModel)_eventoFestival);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("EventoFestivalImagen")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostEventoFestivalImagen(int id, DisplayImagenViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.EventoFestival.Find(id);
            if (_entity != null)
            {
                _entity.EventosImagen.Add((EventosImagen)_model);
                _db.SaveChanges();
                return Ok((DisplayEventoFestivalViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPut]
        [ActionName("EventoFestivalImagen")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutEventoFestivalImagen(int id, DisplayImagenViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.EventosImagen.Find(_model.Id);
            if (_entity != null)
            {
                _model.ToEntityEventoImagen(ref _entity);
                _db.SaveChanges();
                var _eventoFestival = _db.EventoFestival.Find(id);
                return Ok((DisplayEventoFestivalViewModel)_eventoFestival);
            }
            return NotFound();
        }
        #endregion

        #region Patrocinador
        [HttpGet]
        [ActionName("Patrocinador")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetPatrocinador()
        {
            var _list = new List<PatrocinadorViewModel>();
            _db.Patrocinador.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Patrocinador")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetPatrocinador(int id)
        {
            var _entity = _db.Patrocinador.Find(id);
            if (_entity != null)
            {
                return Ok((PatrocinadorViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Patrocinador")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostPatrocinador(PatrocinadorViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            var _tipoPatrocinador = _db.TipoPatrocinador.Find(_model.FK_IdTipoPatrocinador__SCH);
            if (_edition != null && _tipoPatrocinador != null)
            {
                var _entity = (Patrocinador)_model;
                _entity.TipoPatrocinador = _tipoPatrocinador;
                _edition.Patrocinador.Add(_entity);
                _db.SaveChanges();
                return Ok((PatrocinadorViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("Patrocinador")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutPatrocinador(int id, PatrocinadorViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Patrocinador.Find(_model.Id);
            if (_entity != null)
            {
                _model.ToEntityPatrocinador(ref _entity);
                _db.SaveChanges();
                return Ok((PatrocinadorViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Patrocinador")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeletePatrocinador(int id)
        {
            var _entity = _db.Patrocinador.Find(id);
            if (_entity != null)
            {
                _db.Patrocinador.Remove(_entity);
                _db.SaveChanges();
                return Ok((PatrocinadorViewModel)_entity);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("SelectTipoPatrocinador")]
        public IHttpActionResult GetTipoPatrocinador()
        {
            var _list = new List<SelectViewModel>();
            _db.TipoPatrocinador.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        #endregion

        #region Sede

        [HttpGet]
        [ActionName("SedeByEdicion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetSedeByEdicion(int id)
        {
            var _list = new List<SedeViewModel>();
            var _entiy = _db.Edicion.Find(id);
            if (_entiy != null) {
                _entiy.Sede.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }


        [HttpGet]
        [ActionName("Sede")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetSede()
        {
            var _list = new List<SedeViewModel>();
            _db.Sede.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Sede")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetSede(int id)
        {
            var _entity = _db.Sede.Find(id);
            if (_entity != null)
            {
                return Ok((SedeViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Sede")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostSede(SedeViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            if (_edition != null)
            {
                var _entity = (Sede)_model;
                _edition.Sede.Add(_entity);
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((SedeViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("Sede")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutSede(int id, SedeViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Sede.Find(_model.Id);
            if (_entity != null)
            {
                _model.ToEntitySede(ref _entity);
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((SedeViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Sede")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteSede(int id)
        {
            var _entity = _db.Sede.Find(id);
            if (_entity != null)
            {
                _db.Sede.Remove(_entity);
                _db.SaveChanges();
                return Ok((SedeViewModel)_entity);
            }
            return NotFound();
        }
        #endregion

        #region Muestra
        [HttpGet]
        [ActionName("Muestra")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetMuestra()
        {
            var _list = new List<MuestraViewModel>();
            _db.Muestra.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Muestra")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetMuestra(int id)
        {
            var _entity = _db.Muestra.Find(id);
            if (_entity != null)
            {
                return Ok((MuestraViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Muestra")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostMuestra(MuestraViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            if (_edition != null)
            {
                var _entity = (Muestra)_model;
                _edition.Muestra.Add(_entity);
                _model.Proyecciones.ForEach(e => _entity.Proyeccion.Add(_db.Proyeccion.Find(e)));
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((MuestraViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("Muestra")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutMuestra(int id, MuestraViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Muestra.Find(id);
            if (_entity != null)
            {
                _model.ToEntityMuestra(ref _entity);
                _entity.Proyeccion.Clear();
                _model.Proyecciones.ForEach(e => _entity.Proyeccion.Add(_db.Proyeccion.Find(e)));

                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((MuestraViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Muestra")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteMuestra(int id)
        {
            var _entity = _db.Muestra.Find(id);
            if (_entity != null)
            {
                _db.Muestra.Remove(_entity);
                _db.SaveChanges();
                return Ok((MuestraViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("MuestraProyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostMuestraProyeccion(int id, ProyeccionViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Muestra.Find(id);
            var _proyeccion = _db.Proyeccion.Find(_model.Id);
            if (_entity != null && _proyeccion  != null)
            {
                _entity.Proyeccion.Add(_proyeccion);
                _db.SaveChanges();
                return Ok((MuestraViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPut]
        [ActionName("MuestraProyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteMuestraProyeccion(int id, ProyeccionViewModel _model)
        {
            var _entity = _db.Muestra.Find(id);
            var _proyeccion = _db.Proyeccion.Find(_model.Id);
            if (_entity != null && _proyeccion != null)
            {
                _entity.Proyeccion.Remove(_proyeccion);
                _db.SaveChanges();
                return Ok((MuestraViewModel)_entity);
            }
            return NotFound();
        }
        #endregion

        #region Programa
        [HttpGet]
        [ActionName("ProgramaBySede")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetProgramaBySede(int id)
        {
            var _list = new List<ProgramaSelectViewModel>();
            var _entity = _db.Sede.Find(id);
            if (_entity != null)
            {
                _entity.Programa.ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }


        [HttpGet]
        [ActionName("Programa")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetPrograma()
        {
            var _list = new List<ProgramaViewModel>();
            _db.Programa.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Programa")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetPrograma(int id)
        {
            var _entity = _db.Programa.Find(id);
            if (_entity != null)
            {
                return Ok((ProgramaViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Programa")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostPrograma(ProgramaViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.FK_IdEdicion__SCH);
            var _sede = _db.Sede.Find(_model.FK_IdSede__SCH);
            if (_edition != null && _sede != null)
            {
                var _entity = (Programa)_model;
                _entity.Sede = _sede;
                _edition.Programa.Add(_entity);
                _db.SaveChanges();
                return Ok((ProgramaViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("Programa")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutPrograma(int id, ProgramaViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Programa.Find(_model.Id);
            var _sede = _db.Sede.Find(_model.FK_IdSede__SCH);
            if (_entity != null && _sede != null)
            {
                _model.ToEntityPrograma(ref _entity);
                _entity.Sede = _sede;
                _db.SaveChanges();
                return Ok((ProgramaViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Programa")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeletePrograma(int id)
        {
            var _entity = _db.Programa.Find(id);
            if (_entity != null)
            {
                _db.Programa.Remove(_entity);
                _db.SaveChanges();
                return Ok((ProgramaViewModel)_entity);
            }
            return NotFound();
        }
        [HttpGet]
        [ActionName("TipoEventoProyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetTipoEventoProyeccion()
        {
            var _list = new List<SelectViewModel>();
            _db.TipoEvento.Where(e => e.PK_IdTipoEvento ==  1).ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }

        [HttpGet]
        [ActionName("TipoEventoEvento")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetTipoEventoEvento()
        {
            var _list = new List<SelectViewModel>();
            _db.TipoEvento.Where(e => e.PK_IdTipoEvento != 1).ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }

        #endregion

        #region Proyeccion

        [HttpGet]
        [ActionName("ProyeccionByEdition")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetProyeccionByEdition(int id)
        {
            var _list = new List<ProyeccionSelectViewModel>();
            var _entity = _db.Edicion.Find(id);
            if (_entity != null)
            {
                _entity.Programa.SelectMany(e => e.Proyeccion).ToList().ForEach(e => _list.Add(e));
                return Ok(_list);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("Proyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetProyeccion()
        {
            var _list = new List<ProyeccionViewModel>();
            _db.Proyeccion.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }

        [HttpGet]
        [ActionName("Proyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetProyeccion(int id)
        {
            var _entity = _db.Proyeccion.Find(id);
            if (_entity != null)
            {
                return Ok((ProyeccionViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Proyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostProyeccion(ProyeccionViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _programa = _db.Programa.Find(_model.FK_IdPrograma__SCH);
            var _tipoEvento = _db.TipoEvento.Find(_model.FK_IdTipoEvento__SCH);
            if (_programa != null && _tipoEvento != null)
            {
                var _entity = (Proyeccion)_model;
                _entity.TipoEvento = _tipoEvento;
                _programa.Proyeccion.Add(_entity);
                _db.SaveChanges();
                return Ok((ProyeccionViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPut]
        [ActionName("Proyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutProyeccion(int id, ProyeccionViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Proyeccion.Find(id);
            var _tipoEvento = _db.TipoEvento.Find(_model.FK_IdTipoEvento__SCH);
            if (_entity != null && _tipoEvento != null)
            {
                _entity.TipoEvento = _tipoEvento;
                _model.ToEntityProyeccion(ref _entity);
                _db.SaveChanges();
                return Ok((ProyeccionViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Proyeccion")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteProyeccion(int id)
        {
            var _entity = _db.Proyeccion.Find(id);
            if (_entity != null)
            {
                _db.Proyeccion.Remove(_entity);
                _db.SaveChanges();
                return Ok((ProyeccionViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("ProyeccionFilm")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostProyeccionFilm(int id, DisplayShortFilmViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Proyeccion.Find(id);
            var _film = _db.AppShortFilm.Find(_model.Id);
            if (_entity != null && _film != null)
            {
                _entity.AppShortFilm.Add(_film);
                _db.SaveChanges();
                return Ok((ProyeccionViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPut]
        [ActionName("ProyeccionFilm")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteProyeccionFilm(int id, DisplayShortFilmViewModel _model)
        {
            var _entity = _db.Proyeccion.Find(id);
            var _film = _db.AppShortFilm.Find(_model.Id);
            if (_entity != null && _film != null)
            {
                _entity.AppShortFilm.Remove(_film);
                _db.SaveChanges();
                return Ok((ProyeccionViewModel)_entity);
            }
            return NotFound();
        }

        [HttpGet]
        [ActionName("SelectShortFilmByEdition")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetShortFilm(int id)
        {
            var _list = new List<DisplayShortFilmViewModel>();
            var _entity = _db.Edicion.Find(id);
            if (_entity != null)
            {
                _entity.AppShortFilm.Where(e => e.Sel_Oficial).ToList().ForEach(e => _list.Add(e));
                return Ok(_list);                
            }
            return NotFound();
        }
        #endregion

        #region Evento
        [HttpGet]
        [ActionName("Evento")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetEvento()
        {
            var _list = new List<DisplayEventoViewModel>();
            _db.Evento.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Evento")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetEvento(int id)
        {
            var _entity = _db.Evento.Find(id);
            if (_entity != null)
            {
                return Ok((DisplayEventoViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Evento")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostEvento(DisplayEventoViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var _programa = _db.Programa.Find(_model.FK_IdPrograma__SCH);
            var _tipoEvento = _db.TipoEvento.Find(_model.FK_IdTipoEvento__SCH);
            if (_programa != null && _tipoEvento != null)
            {
                var _entity = (Evento)_model;
                _entity.TipoEvento = _tipoEvento;
                _programa.Evento.Add(_entity);
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((DisplayEventoViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPut]
        [ActionName("Evento")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutEvento(int id, DisplayEventoViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.Evento.Find(id);
            var _tipoEvento = _db.TipoEvento.Find(_model.FK_IdTipoEvento__SCH);
            if (_entity != null && _tipoEvento != null)
            {
                _entity.TipoEvento = _tipoEvento;
                _model.ToEntityEvento(ref _entity);
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((DisplayEventoViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Evento")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteEvento(int id)
        {
            var _entity = _db.Evento.Find(id);
            if (_entity != null)
            {
                _db.Evento.Remove(_entity);
                _db.SaveChanges();
                return Ok((DisplayEventoViewModel)_entity);
            }
            return NotFound();
        }

        #endregion

        #region Jurado

        [HttpGet]
        [ActionName("Jurado")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetJurado()
        {
            var _list = new List<AppJuradoViewModel>();
            _db.AppJurado.ToList().ForEach(e => _list.Add(e));
            return Ok(_list);
        }
        [HttpGet]
        [ActionName("Jurado")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult GetJurado(int id)
        {
            var _entity = _db.AppJurado.Find(id);
            if (_entity != null)
            {
                return Ok((AppJuradoViewModel)_entity);
            }
            return NotFound();
        }

        [HttpPost]
        [ActionName("Jurado")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PostJurado(AppJuradoViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _edition = _db.Edicion.Find(_model.Edicion);
            if (_edition != null)
            {
                var _entity = (AppJurado)_model;
                _edition.AppJurado.Add(_entity);
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((AppJuradoViewModel)_entity);
            }

            return NotFound();
        }

        [HttpPut]
        [ActionName("Jurado")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult PutJurado(int id, AppJuradoViewModel _model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var _entity = _db.AppJurado.Find(_model.Id);
            if (_entity != null)
            {
                _model.ToEntityAppJurado(ref _entity);
                try
                {
                    _db.SaveChanges();
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return BadRequest("error");
                }
                return Ok((AppJuradoViewModel)_entity);
            }
            return NotFound();
        }

        [HttpDelete]
        [ActionName("Jurado")]
        [Authorize(Roles = "admin")]
        public IHttpActionResult DeleteJurado(int id)
        {
            var _entity = _db.AppJurado.Find(id);
            if (_entity != null)
            {
                _db.AppJurado.Remove(_entity);
                _db.SaveChanges();
                return Ok((AppJuradoViewModel)_entity);
            }
            return NotFound();
        }
        #endregion
    }

    #region Boletin
    // Reutiliza view model !! 
    public class DisplayBoletinViewModel : BoletinViewModel
    {
        public static implicit operator DisplayBoletinViewModel(Boletin _entity)
        {
            var _result = new DisplayBoletinViewModel
            {
                Id = _entity.PK_IdBoletin,
                Boletin_ES = _entity.Boletin_ES,
                Boletin_EN = _entity.Boletin_EN,
                Titulo_ES = _entity.Titulo_ES,
                Titulo_EN = _entity.Titulo_EN,
                Fecha = _entity.Fecha,
                FK_IdEdicion__SCH = _entity.FK_IdEdicion__SCH,
                descripcionEdicion = _entity.Edicion.Nombre_ES
            };
            return _result;
        }

        public static implicit operator Boletin(DisplayBoletinViewModel _model) {
            return new Boletin
            {
                Boletin_ES = _model.Boletin_ES,
                Boletin_EN = _model.Boletin_EN,
                Titulo_ES = _model.Titulo_ES,
                Titulo_EN = _model.Titulo_EN,
                Fecha = _model.Fecha,
                PK_IdBoletin = _model.Id,
            };
        }

        #region Attr
        public int FK_IdEdicion__SCH { get; set; }
        public string descripcionEdicion { get; set; }
        #endregion

        public void ToEntityBoletin(ref Boletin _entity)
        {
            _entity.Boletin_EN = Boletin_EN;
            _entity.Boletin_ES = Boletin_ES;
            _entity.Titulo_ES = Titulo_ES;
            _entity.Titulo_EN = Titulo_EN;
            _entity.Fecha = Fecha;
            _entity.FK_IdEdicion__SCH = FK_IdEdicion__SCH;
        }
    }
    #endregion

    #region Contacto

    public class ContactoViewModel
    {
        public static implicit operator ContactoViewModel(Contacto _entity)
        {
            var _result = new ContactoViewModel
            {
                Id = _entity.PK_IdContacto,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Email = _entity.Email,
                FK_IdEdicion__SCH = (int)_entity.FK_IdEdicion__SCH,
                descripcionEdicion = _entity.Edicion.Nombre_ES
            };
            return _result;
        }

        public static implicit operator Contacto(ContactoViewModel _entity)
        {
            return new Contacto()
            {
                PK_IdContacto = _entity.Id,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Email = _entity.Email,
                FK_IdEdicion__SCH = (int)_entity.FK_IdEdicion__SCH,
            };
        }

        #region Attr
        public int Id { get; set; }
        public string descripcionEdicion { get; set; }
        [Required]
        [Display(Name = "Tema en Español")]
        public string Nombre_ES { get; set; }
        [Required]
        [Display(Name = "Tema en Inglés")]
        public string Nombre_EN { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Edicion del Festival")]
        public int FK_IdEdicion__SCH { get; set; }
        #endregion

        public void ToEntityContacto(ref Contacto _entity)
        {
            _entity.Nombre_ES = Nombre_ES;
            _entity.Nombre_EN = Nombre_EN;
            _entity.Email = Email;
        }

    }
    #endregion

    #region EventoFestival
    public class DisplayEventoFestivalViewModel : EventoFestivalViewModel
    {
        public DisplayEventoFestivalViewModel()
        {
            Notas = new List<DisplayEventoNotasViewModel>();
            Galeria = new List<DisplayImagenViewModel>();
        }
        public static implicit operator DisplayEventoFestivalViewModel(EventoFestival _entity)
        {
            var _result = new DisplayEventoFestivalViewModel
            {
                Id = _entity.PK_IdEventoFestival,
                FK_IdEdicion__SCH = _entity.FK_IdEdicion__SCH,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Fecha = _entity.Fecha,
                descripcionEdicion = _entity.Edicion.Nombre_ES
            };

            _entity.EventoNotas.ToList().ForEach(e => _result.Notas.Add(e));
            _entity.EventosImagen.ToList().ForEach(e => _result.Galeria.Add(e));
            return _result;
        }

        public static implicit operator EventoFestival(DisplayEventoFestivalViewModel _entity)
        {
            return new EventoFestival()
            {
                PK_IdEventoFestival = (int)_entity.Id,
                FK_IdEdicion__SCH = (int)_entity.FK_IdEdicion__SCH,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Fecha = _entity.Fecha
            };
        }
        #region Attr
        public int FK_IdEdicion__SCH { get; set; }
        public string descripcionEdicion { get; set; }
        public new List<DisplayEventoNotasViewModel> Notas { get; set; }
        public new List<DisplayImagenViewModel> Galeria { get; set; }

        internal void ToEntityEventoFestival(ref EventoFestival _entity)
        {
            _entity.Nombre_ES = Nombre_ES;
            _entity.Nombre_EN = _entity.Nombre_EN;
            _entity.Fecha = _entity.Fecha;
        }
        #endregion
    }

    public class DisplayImagenViewModel
    {
        public static implicit operator DisplayImagenViewModel(EventosImagen _entity)
        {
            return new DisplayImagenViewModel
            {
                Id = _entity.PK_IdEventosImagen,
                FK_IdEventoFestival__SCH = _entity.FK_IdEventoFestival__SCH,
                Path = _entity.Path,
                Image = _entity.Image,
                Ext = _entity.Ext,
                Name = _entity.Name,
                Portada = _entity.Portada,
            };
        }

        public static implicit operator EventosImagen(DisplayImagenViewModel _entity)
        {
            return new EventosImagen()
            {
                Path = _entity.Path,
                Image = _entity.Image,
                Ext = _entity.Ext,
                Name = _entity.Name,
                Portada = _entity.Portada
            };
        }

        #region Attr
        public int Id { get; set; }
        public int FK_IdEventoFestival__SCH { get; set; }
        public string Path { get; set; }
        public string Image { get; set; }
        public string Ext { get; set; }
        public string Name { get; set; }
        public bool Portada { get; set; }

        internal void ToEntityEventoImagen(ref EventosImagen _entity)
        {
            _entity.Path = Path;
            _entity.Image = Image;
            _entity.Ext = Ext;
            _entity.Name = Name;
            _entity.Portada = _entity.Portada;
        }
        #endregion
    }

    public class DisplayEventoNotasViewModel
    {
        public static implicit operator DisplayEventoNotasViewModel(EventoNotas _entity)
        {
            return new DisplayEventoNotasViewModel
            {
                Id = _entity.PK_IdEventoNotas,
                Nota = _entity.Nota,
            };
        }

        public static implicit operator EventoNotas(DisplayEventoNotasViewModel _entity)
        {
            return new EventoNotas()
            {
                Nota = _entity.Nota
            };
        }
        #region Attr
        public int Id { get; set; }
        public string Nota { get; set; }
        #endregion

        internal void ToEntityEventoNotas(ref EventoNotas _entity)
        {
            _entity.Nota = Nota;
        }
    }
    #endregion

    #region EdicionFestival
    public class EdicionSimpleWithParent : EdicionViewModel
    {

        public static implicit operator EdicionSimpleWithParent(Edicion _entity)
        {
            var _result = new EdicionSimpleWithParent
            {
                Id = _entity.PK_IdEdicion,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Tema_ES = _entity.Tema_ES,
                Tema_EN = _entity.Tema_EN,
                Info_ES = _entity.Info_ES,
                Info_EN = _entity.Info_EN,
                BasesConvocatoria_ES = _entity.BasesConvocatoria_ES,
                BasesConvocatoria_EN = _entity.BasesConvocatoria_EN,
                Cartel_ES = _entity.Cartel_ES,
                Cartel_EN = _entity.Cartel_EN,
                Logotipo_ES = _entity.Logotipo_ES,
                Logotipo_EN = _entity.Logotipo_EN,
                Teaser_ES = _entity.Teaser_ES,
                Teaser_EN = _entity.Teaser_EN,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN,
                Direccion_ES = _entity.Direccion_ES,
                Direccion_EN = _entity.Direccion_EN,
                MapaContacto = _entity.MapaContato,
                ManualGrafico_ES = _entity.ManualGrafico_ES,
                ManualGrafico_EN = _entity.ManualGrafico_EN,
                Catalogo_ES = _entity.Catalogo_ES,
                Catalogo_EN = _entity.Catalogo_EN,
                FK_IdEdicionAnterior__SCH = _entity.FK_IdEdicionAnterior__SCH
            };

            if(_entity.EdicionAnterior != null)
            {
                _result.EdicionAnterior = _entity.EdicionAnterior;
            }
            return _result;
        }
        #region Attr
        public EdicionViewModel EdicionAnterior { get; set; }
        #endregion
    }
    public class EdicionViewModel
    {
        public static implicit operator Edicion(EdicionViewModel _entity)
        {
            return new Edicion()
            {
                PK_IdEdicion = _entity.Id,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Tema_ES = _entity.Tema_ES,
                Tema_EN = _entity.Tema_EN,
                Info_ES = _entity.Info_ES,
                Info_EN = _entity.Info_EN,
                BasesConvocatoria_ES = _entity.BasesConvocatoria_ES,
                BasesConvocatoria_EN = _entity.BasesConvocatoria_EN,
                Cartel_ES = _entity.Cartel_ES,
                Cartel_EN = _entity.Cartel_EN,
                Logotipo_ES = _entity.Logotipo_ES,
                Logotipo_EN = _entity.Logotipo_EN,
                Teaser_ES = _entity.Teaser_ES,
                Teaser_EN = _entity.Teaser_EN,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN,
                Direccion_ES = _entity.Direccion_ES,
                Direccion_EN = _entity.Direccion_EN,
                MapaContato = _entity.MapaContacto,
                ManualGrafico_ES = _entity.ManualGrafico_ES,
                ManualGrafico_EN = _entity.ManualGrafico_EN,
                Catalogo_ES = _entity.Catalogo_ES,
                Catalogo_EN = _entity.Catalogo_EN,
                FK_IdEdicionAnterior__SCH = _entity.FK_IdEdicionAnterior__SCH
            };
        }

        public static implicit operator EdicionViewModel(Edicion _entity)
        {
            var _result = new EdicionViewModel
            {
                Id = _entity.PK_IdEdicion,
                Nombre_ES = _entity.Nombre_ES,
                Nombre_EN = _entity.Nombre_EN,
                Tema_ES = _entity.Tema_ES,
                Tema_EN = _entity.Tema_EN,
                Info_ES = _entity.Info_ES,
                Info_EN = _entity.Info_EN,
                BasesConvocatoria_ES = _entity.BasesConvocatoria_ES,
                BasesConvocatoria_EN = _entity.BasesConvocatoria_EN,
                Cartel_ES = _entity.Cartel_ES,
                Cartel_EN = _entity.Cartel_EN,
                Logotipo_ES = _entity.Logotipo_ES,
                Logotipo_EN = _entity.Logotipo_EN,
                Teaser_ES = _entity.Teaser_ES,
                Teaser_EN = _entity.Teaser_EN,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN,
                Direccion_ES = _entity.Direccion_ES,
                Direccion_EN = _entity.Direccion_EN,
                MapaContacto = _entity.MapaContato,
                ManualGrafico_ES = _entity.ManualGrafico_ES,
                ManualGrafico_EN = _entity.ManualGrafico_EN,
                Catalogo_ES = _entity.Catalogo_ES,
                Catalogo_EN = _entity.Catalogo_EN,
                FK_IdEdicionAnterior__SCH = _entity.FK_IdEdicionAnterior__SCH
            };
            return _result;
        }

        #region Attr
        [Required]
        [Display(Name = "Año")]
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nombre en Español")]
        public string Nombre_ES { get; set; }
        [Required]
        [Display(Name = "Nombre en Inglés")]
        public string Nombre_EN { get; set; }
        [Required]
        [Display(Name = "Tema en Español")]
        public string Tema_ES { get; set; }
        [Required]
        [Display(Name = "Tema en Inglés")]
        public string Tema_EN { get; set; }
        [Required]
        [Display(Name = "Info en Español")]
        public string Info_ES { get; set; }
        [Required]
        [Display(Name = "Info en Inglés")]
        public string Info_EN { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Bases de la Convocatoria en Español")]
        public string BasesConvocatoria_ES { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Bases de la Convocatoria en Inglés")]
        public string BasesConvocatoria_EN { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Cartel en Español")]
        public string Cartel_ES { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Cartel en Inglés")]
        public string Cartel_EN { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Logotipo en Español")]
        public string Logotipo_ES { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Logotipo en Inglés")]
        public string Logotipo_EN { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Teaser en Español")]
        public string Teaser_ES { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Teaser en Inglés")]
        public string Teaser_EN { get; set; }
        //[Required]
        [Display(Name = "Informacion en Español")]
        public string Informacion_ES { get; set; }
        //[Required]
        [Display(Name = "Informacion en Inglés")]
        public string Informacion_EN { get; set; }
        [Required]
        [Display(Name = "Direccion en Español")]
        public string Direccion_ES { get; set; }
        [Required]
        [Display(Name = "Direccion en Inglés")]
        public string Direccion_EN { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Map de Contacto")]
        public string MapaContacto { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Manual Grafico en Español")]
        public string ManualGrafico_ES { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Manual Grafico en Inglés")]
        public string ManualGrafico_EN { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Catalogo en Español")]
        public string Catalogo_ES { get; set; }
        //[Required]
        [DataType(DataType.Url)]
        [Display(Name = "Catalogo en Inglés")]
        public string Catalogo_EN { get; set; }
        [Required]
        public int? FK_IdEdicionAnterior__SCH { get; set; }
        #endregion

        public void ToEntityEdicion(ref Edicion _entity)
        {
            _entity.Nombre_ES = Nombre_ES;
            _entity.Nombre_EN = Nombre_EN;
            _entity.Tema_ES = Tema_ES;
            _entity.Tema_EN = Tema_EN;
            _entity.Info_ES = Info_ES;
            _entity.Info_EN = Info_EN;
            _entity.BasesConvocatoria_ES = BasesConvocatoria_ES;
            _entity.BasesConvocatoria_EN = BasesConvocatoria_EN;
            _entity.Cartel_ES = Cartel_ES;
            _entity.Cartel_EN = Cartel_EN;
            _entity.Logotipo_ES = Logotipo_ES;
            _entity.Logotipo_EN = Logotipo_EN;
            _entity.Teaser_ES = Teaser_ES;
            _entity.Teaser_EN = Teaser_EN;
            _entity.Informacion_ES = Informacion_ES;
            _entity.Informacion_EN = Informacion_EN;
            _entity.Direccion_ES = Direccion_ES;
            _entity.Direccion_EN = Direccion_EN;
            _entity.MapaContato = MapaContacto;
            _entity.ManualGrafico_ES = ManualGrafico_ES;
            _entity.ManualGrafico_EN = ManualGrafico_EN;
            _entity.Catalogo_ES = Catalogo_ES;
            _entity.Catalogo_EN = Catalogo_EN;
        }
    }
    #endregion

    #region Patrocinador
    public class PatrocinadorViewModel
    {
        public static implicit operator PatrocinadorViewModel(Patrocinador _entity)
        {
            var _result = new PatrocinadorViewModel
            {
                Id = _entity.PK_IdPatrocinador,
                FK_IdTipoPatrocinador__SCH = _entity.FK_IdTipoPatrocinador__SCH,
                Descripcion_ES = _entity.Descripcion_ES,
                Descripcion_EN = _entity.Descripcion_EN,
                Image = _entity.Image,
                WebSite = _entity.WebSite,
                FK_IdEdicion__SCH = (int)_entity.Edicion,
                Orden = _entity.Orden,
                Home = _entity.Home,
                tipoPatrocinador = _entity.TipoPatrocinador.Descripcion_ES,
                descripcionEdicion = _entity.Edicion1.Nombre_ES
            };
            return _result;
        }

        public static implicit operator Patrocinador(PatrocinadorViewModel _entity)
        {
            return new Patrocinador()
            {
                PK_IdPatrocinador = _entity.Id,
                FK_IdTipoPatrocinador__SCH = (int)_entity.FK_IdTipoPatrocinador__SCH,
                Descripcion_ES = _entity.Descripcion_ES,
                Descripcion_EN = _entity.Descripcion_EN,
                Image = _entity.Image,
                WebSite = _entity.WebSite,
                Edicion = (int)_entity.FK_IdEdicion__SCH,
                Orden = _entity.Orden,
                Home = _entity.Home
            };
        }

        #region Attr
        public int Id { get; set; }
        public string tipoPatrocinador { get; set; }
        public string descripcionEdicion { get; set; }
        [Required]
        [Display(Name = "Tipo de Patrocinador")]
        public int FK_IdTipoPatrocinador__SCH { get; set; }
        [Required]
        [Display(Name = "Tema en Español")]
        public string Descripcion_ES { get; set; }
        [Required]
        [Display(Name = "Tema en Inglés")]
        public string Descripcion_EN { get; set; }
        [Display(Name = "Imagen")]
        public string Image { get; set; }
        [Display(Name = "Sitio Web")]
        [DataType(DataType.Url)]
        public string WebSite { get; set; }
        [Display(Name = "Edicion del Festival")]
        public int FK_IdEdicion__SCH { get; set; }
        public int Orden { get; set; }
        [Required]
        [Display(Name = "Home")]
        public bool Home { get; set; }
        #endregion

        public void ToEntityPatrocinador(ref Patrocinador _entity)
        {
            _entity.FK_IdTipoPatrocinador__SCH = FK_IdTipoPatrocinador__SCH;
            _entity.Descripcion_ES = Descripcion_ES;
            _entity.Descripcion_EN = Descripcion_EN;
            _entity.Image = Image;
            _entity.WebSite = WebSite;
            _entity.Home = Home;
        }

    }
    #endregion

    #region Sede
    public class SedeViewModel
    {
        public static implicit operator SedeViewModel(Sede _entity)
        {
            var _result = new SedeViewModel
            {
                Id = _entity.PK_IdSede,
                Sede_ES = _entity.Sede_ES,
                Sede_EN = _entity.Sede_EN,
                Direccion = _entity.Direccion,
                Image = _entity.Image,
                url = _entity.url,
                WebSite = _entity.WebSite,
                FK_IdEdicion__SCH = (int)_entity.Edicion,
                descripcionEdicion = _entity.Edicion1.Nombre_ES
            };
            return _result;
        }

        public static implicit operator Sede(SedeViewModel _entity)
        {
            return new Sede()
            {
                PK_IdSede = _entity.Id,
                Sede_ES = _entity.Sede_ES,
                Sede_EN = _entity.Sede_EN,
                Direccion = _entity.Direccion,
                Image = _entity.Image,
                url = _entity.url,
                WebSite = _entity.WebSite,
                Edicion = (int)_entity.FK_IdEdicion__SCH
            };
        }

        #region Attr
        public int Id { get; set; }
        public string descripcionEdicion { get; set; }
        [Required]
        [Display(Name = "Sede en Español")]
        public string Sede_ES { get; set; }
        [Required]
        [Display(Name = "Sede en Inglés")]
        public string Sede_EN { get; set; }
        [Required]
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }
        [Display(Name = "Url")]
        [DataType(DataType.Url)]
        public string url { get; set; }
        [Display(Name = "Imagen")]
        public string Image { get; set; }
        [Display(Name = "Sitio Web")]
        [DataType(DataType.Url)]
        public string WebSite { get; set; }
        [Display(Name = "Edicion del Festival")]
        public int FK_IdEdicion__SCH { get; set; }
        #endregion

        public void ToEntitySede(ref Sede _entity)
        {
            _entity.Sede_ES = _entity.Sede_ES;
            _entity.Sede_EN = _entity.Sede_EN;
            _entity.Direccion = _entity.Direccion;
            _entity.Image = _entity.Image;
            _entity.url = _entity.url;
            _entity.WebSite = _entity.WebSite;
        }

    }
    #endregion

    #region Muestra
    public class MuestraViewModel
    {
        public MuestraViewModel()
        {
            Proyecciones = new List<int>();
            Path = "";
        }
        public static implicit operator MuestraViewModel(Muestra _entity)
        {
            var _result = new MuestraViewModel
            {
                Id = _entity.PK_IdMuestra,
                FK_IdEdicion__SCH = _entity.Edicion,
                Titulo_Original = _entity.Titulo_Original,
                Titulo_ES = _entity.Titulo_Espanol,
                Titulo_EN = _entity.Titulo_Ingles,
                Sinopsis_ES = _entity.Sinopsis_Espanol,
                Sinopsis_EN = _entity.Sinopsis_Ingles,
                Duracion = _entity.Duracion,
                Director = _entity.Director,
                Pais = _entity.Pais,
                File = _entity.File,
                Path = _entity.Path,
                descripcionEdicion = (_entity.Edicion1 != null) ? _entity.Edicion1.Nombre_ES : string.Empty,
             };
            _entity.Proyeccion.ToList().ForEach(e => _result.Proyecciones.Add(e.PK_IdProyeccion));
            return _result;
        }

        public static implicit operator Muestra(MuestraViewModel _entity)
        {
            return new Muestra()
            {
                Titulo_Original = _entity.Titulo_Original,
                Titulo_Espanol = _entity.Titulo_ES,
                Titulo_Ingles = _entity.Titulo_EN,
                Sinopsis_Espanol = _entity.Sinopsis_ES,
                Sinopsis_Ingles = _entity.Sinopsis_EN,
                Duracion = _entity.Duracion,
                Director = _entity.Director,
                Pais = _entity.Pais,
                File = _entity.File
            };
        }

        #region Attr
        public int Id { get; set; }
        public int FK_IdEdicion__SCH { get; set; }
        public string descripcionEdicion { get; set; }
        public string Titulo_Original { get; set; }
        public string Titulo_ES { get; set; }
        public string Titulo_EN { get; set; }
        public string Sinopsis_ES { get; set; }
        public string Sinopsis_EN { get; set; }
        public string Duracion { get; set; }
        public string Director { get; set; }
        public string Pais { get; set; }
        public string File { get; set; }
        public string Path { get; set; }

        public List<int> Proyecciones { get; set; }
        #endregion

        public void ToEntityMuestra(ref Muestra _entity)
        {
            _entity.Titulo_Original = Titulo_Original;
            _entity.Titulo_Espanol = Titulo_ES;
            _entity.Titulo_Ingles = Titulo_EN;
            _entity.Sinopsis_Espanol = Sinopsis_ES;
            _entity.Sinopsis_Ingles = Sinopsis_EN;
            _entity.Duracion = Duracion;
            _entity.Director = Director;
            _entity.Pais = Pais;
            _entity.File = File;
        }
    }
    public class ProyeccionSelectViewModel {
        public static implicit operator ProyeccionSelectViewModel(Proyeccion _entity)
        {
            string _descripcionPrograma = 
                string.Format("{0} | {1} | {2} | {3} - {4}", 
                    _entity.Programa.Sede.Sede_ES, 
                    _entity.Programa.Date.ToString("dd/MM/yyyy"), 
                    _entity.Proyeccion_ES, 
                    ((TimeSpan)_entity.start).ToString(@"hh\:mm"), 
                    ((TimeSpan)_entity.end).ToString(@"hh\:mm")
                );
            var _result = new ProyeccionSelectViewModel
            {
                Id = _entity.PK_IdProyeccion,
                FK_IdEdicion__SCH = _entity.Programa.Edicion,
                FK_IdSede__SCH = _entity.Programa.Sede.PK_IdSede,
                FK_IdPrograma__SCH = _entity.FK_IdPrograma__SCH,
                start = _entity.start,
                end = _entity.end,
                allDay = _entity.allDay,
                url = _entity.url,
                FK_IdTipoEvento__SCH = _entity.FK_IdTipoEvento__SCH,
                Lugar_ES = _entity.Lugar_ES,
                Lugar_EN = _entity.Lugar_EN,
                Descripcion_ES = _entity.Descripcion_ES,
                Descripcion_EN = _entity.Descripcion_EN,
                Proyeccion_ES = _entity.Proyeccion_ES,
                Proyeccion_EN = _entity.Proyeccion_EN,
                Image = _entity.Image,
                ImageWeb = _entity.ImageWeb,
                diaPrograma = _entity.Programa.Date,
                descripcionPrograma = _descripcionPrograma ,
                descripcionTipoEvento = _entity.TipoEvento.Tipo_ES
            };
            return _result;
        }
        #region Attr
        public int Id { get; set; }
        public int FK_IdPrograma__SCH { get; set; }
        public int FK_IdEdicion__SCH { get; set; }
        public int FK_IdSede__SCH { get; set; }
        public TimeSpan? start { get; set; }
        public TimeSpan? end { get; set; }
        public bool allDay { get; set; }
        public string url { get; set; }
        public int FK_IdTipoEvento__SCH { get; set; }
        public string Lugar_ES { get; set; }
        public string Lugar_EN { get; set; }
        public string Descripcion_ES { get; set; }
        public string Descripcion_EN { get; set; }
        public string Proyeccion_ES { get; set; }
        public string Proyeccion_EN { get; set; }
        public string Image { get; set; }
        public string ImageWeb { get; set; }
        public string descripcionPrograma { get; set; }
        public string descripcionTipoEvento { get; set; }
        public DateTime diaPrograma { get; set; }

        #endregion
    }
    public class ProyeccionViewModel : ProyeccionSelectViewModel
    {
        public ProyeccionViewModel()
        {
            ShortFilms = new List<DisplayShortFilmViewModel>();
        }
        public static implicit operator ProyeccionViewModel(Proyeccion _entity)
        {
            string _descripcionPrograma =
                string.Format("{0} | {1} | {2} | {3} - {4}",
                    _entity.Programa.Sede.Sede_ES,
                    _entity.Programa.Date.ToString("dd/MM/yyyy"),
                    _entity.Proyeccion_ES,
                    ((TimeSpan)_entity.start).ToString(@"hh\:mm"),
                    ((TimeSpan)_entity.end).ToString(@"hh\:mm")
                );
            var _result = new ProyeccionViewModel
            {
                Id = _entity.PK_IdProyeccion,
                FK_IdEdicion__SCH = _entity.Programa.Edicion ,
                FK_IdSede__SCH = _entity.Programa.Sede.PK_IdSede ,
                FK_IdPrograma__SCH = _entity.FK_IdPrograma__SCH,
                start = _entity.start,
                end = _entity.end,
                allDay = _entity.allDay,
                url = _entity.url,
                FK_IdTipoEvento__SCH = _entity.FK_IdTipoEvento__SCH,
                Lugar_ES = _entity.Lugar_ES,
                Lugar_EN = _entity.Lugar_EN,
                Descripcion_ES = _entity.Descripcion_ES,
                Descripcion_EN = _entity.Descripcion_EN,
                Proyeccion_ES = _entity.Proyeccion_ES,
                Proyeccion_EN = _entity.Proyeccion_EN,
                Image = _entity.Image,
                ImageWeb = _entity.ImageWeb,
                diaPrograma = _entity.Programa.Date,
                descripcionPrograma = _descripcionPrograma,
                descripcionTipoEvento = _entity.TipoEvento.Tipo_ES
            };
            _entity.AppShortFilm.ToList().ForEach(e => _result.ShortFilms.Add(e));
            return _result;
        }

        public static implicit operator Proyeccion(ProyeccionViewModel _model)
        {
            return new Proyeccion()
            {
                start = _model.start,
                end = _model.end,
                allDay = _model.allDay,
                url = _model.url,
                Lugar_ES = _model.Lugar_ES,
                Lugar_EN = _model.Lugar_EN,
                Descripcion_ES = _model.Descripcion_ES,
                Descripcion_EN = _model.Descripcion_EN,
                Proyeccion_ES = _model.Proyeccion_ES,
                Proyeccion_EN = _model.Proyeccion_EN,
                Image = _model.Image,
                ImageWeb = _model.ImageWeb
            };
        }

        

        public void ToEntityProyeccion(ref Proyeccion _entity)
        {
            _entity.start = start;
            _entity.end = end;
            _entity.allDay = allDay;
            _entity.url = url;
            _entity.Lugar_ES = Lugar_ES;
            _entity.Lugar_EN = Lugar_EN;
            _entity.Descripcion_ES = Descripcion_ES;
            _entity.Descripcion_EN = Descripcion_EN;
            _entity.Proyeccion_ES = Proyeccion_ES;
            _entity.Proyeccion_EN = Proyeccion_EN;
            _entity.Image = Image;
            _entity.ImageWeb = ImageWeb;
        }
        public List<DisplayShortFilmViewModel> ShortFilms { get; set; }


        
    }

    public class DisplayShortFilmViewModel
    {
        public static implicit operator DisplayShortFilmViewModel(AppShortFilm _entity)
        {
            var _result = new DisplayShortFilmViewModel
            {
                Id = _entity.Id,
                UserId = _entity.UserId,
                CategoriaId = _entity.CategoriaId,
                AnnoId = _entity.AnnoId,
                Duracion_Minutos = _entity.Duracion_Minutos,
                Duracion_Segundos = _entity.Duracion_Segundos,
                SubtituloId = _entity.SubtituloId,
                Idioma = _entity.Idioma,
                FormatoVideoId = _entity.FormatoVideoId,
                OtroFormatoVideo = _entity.OtroFormatoVideo,
                FormatoAudioId = _entity.FormatoAudioId,
                OtroFormatoAudio = _entity.OtroFormatoAudio,
                Titulo_Original = _entity.Titulo_Original,
                Titulo_Espanol = _entity.Titulo_Espanol,
                Titulo_Ingles = _entity.Titulo_Ingles,
                Sinopsis_Espanol = _entity.Sinopsis_Espanol,
                Sinopsis_Ingles = _entity.Sinopsis_Ingles,
                Etapa = _entity.Etapa
            };
             return _result;
        }

        public static implicit operator AppShortFilm(DisplayShortFilmViewModel _entity)
        {
            return new AppShortFilm()
            {
                Id = _entity.Id,
                UserId = _entity.UserId,
                CategoriaId = _entity.CategoriaId,
                AnnoId = _entity.AnnoId,
                Duracion_Minutos = _entity.Duracion_Minutos,
                Duracion_Segundos = _entity.Duracion_Segundos,
                SubtituloId = _entity.SubtituloId,
                Idioma = _entity.Idioma,
                FormatoVideoId = _entity.FormatoVideoId,
                OtroFormatoVideo = _entity.OtroFormatoVideo,
                FormatoAudioId = _entity.FormatoAudioId,
                OtroFormatoAudio = _entity.OtroFormatoAudio,
                Titulo_Original = _entity.Titulo_Original,
                Titulo_Espanol = _entity.Titulo_Espanol,
                Titulo_Ingles = _entity.Titulo_Ingles,
                Sinopsis_Espanol = _entity.Sinopsis_Espanol,
                Sinopsis_Ingles = _entity.Sinopsis_Ingles,
                Etapa = _entity.Etapa
            };
        }

        public int Etapa { get; set; }
        public int Id { get; set; }

        [Display(Name = "User")]
        public string UserId { get; set; }

        [Display(Name = "GÉNERO EN LA QUE SE PARTICIPA")]
        public int CategoriaId { get; set; }

        [Display(Name = "Año | Year")]
        public int AnnoId { get; set; }
        [Display(Name = "Minutos | Minutes")]
        public int Duracion_Minutos { get; set; }
        [Display(Name = "Segundos | Segundos")]
        public int Duracion_Segundos { get; set; }

        [Display(Name = "Subtitulo")]
        public int SubtituloId { get; set; }
        [Display(Name = "Idioma | Lenguage")]
        public string Idioma { get; set; }

        [Display(Name = "Formato Video")]
        public int FormatoVideoId { get; set; }

        [Display(Name = "Otro | Other")]
        public string OtroFormatoVideo { get; set; }

        [Display(Name = "Formato Audio")]
        public int FormatoAudioId { get; set; }

        [Display(Name = "Otro | Other")]
        public string OtroFormatoAudio { get; set; }

        [Display(Name = "Título original | Original title")]
        public string Titulo_Original { get; set; }
        [Display(Name = "Título en español | Spanish title")]
        public string Titulo_Espanol { get; set; }
        [Display(Name = "Título en inglés | English title")]
        public string Titulo_Ingles { get; set; }
        [Display(Name = "Sinopsis en Español | Synopsis in Spanish")]
        public string Sinopsis_Espanol { get; set; }

        [Display(Name = "Sinopsis en Inglés | Synopsis in English")]
        public string Sinopsis_Ingles { get; set; }
    }

    #endregion

    #region Programa

    public class ProgramaSelectViewModel
    {
        public static implicit operator ProgramaSelectViewModel(Programa _entity)
        {
            var _result = new ProgramaViewModel
            {
                Id = _entity.PK_IdPrograma,
                FK_IdEdicion__SCH = _entity.Edicion,
                FK_IdSede__SCH = _entity.FK_IdSede__SCH,
                Date = _entity.Date,
                descripcionEdicion = _entity.Edicion1.Nombre_ES,
                descipcionSede = _entity.Sede.Sede_ES,
                descripcionPrograma = string.Format("{0} - {1} - {2}", _entity.Date.ToString("dd/MM/yyyy") , _entity.Edicion1.Nombre_ES ,_entity.Sede.Sede_ES),// + " - " + 
            };
            return _result;
        }
        #region Attr
        public int Id { get; set; }
        public int FK_IdEdicion__SCH { get; set; }
        public string descripcionEdicion { get; set; }
        public string descipcionSede { get; set; }
        public string descripcionPrograma { get; set; }
        [Required]
        [Display(Name = "Sede de Programa")]
        public int? FK_IdSede__SCH { get; set; }

        [Required]
        [Display(Name = "Fecha")]
        public DateTime Date { get; set; }
        
        #endregion

    }
    public class ProgramaViewModel : ProgramaSelectViewModel
    {
        public ProgramaViewModel()
        {
            Eventos = new List<DisplayEventoViewModel>();
            Proyecciones = new List<ProyeccionViewModel>();
        }
        public static implicit operator ProgramaViewModel(Programa _entity)
        {
            var _result = new ProgramaViewModel
            {
                Id = _entity.PK_IdPrograma,
                FK_IdEdicion__SCH = _entity.Edicion,
                FK_IdSede__SCH = _entity.FK_IdSede__SCH,
                Date = _entity.Date,
                descripcionEdicion = _entity.Edicion1.Nombre_ES,
                descipcionSede = _entity.Sede.Sede_ES,
                descripcionPrograma = _entity.Sede.Sede_ES + " - " + _entity.Edicion1.Nombre_ES
            };
            _entity.Proyeccion.ToList().ForEach(e => _result.Proyecciones.Add(e));
            _entity.Evento.ToList().ForEach(e => _result.Eventos.Add(e));
            return _result;
        }

        public static implicit operator Programa(ProgramaViewModel _entity)
        {
            return new Programa()
            {
                Date = _entity.Date
            };
        }

        #region Attr
        public List<ProyeccionViewModel> Proyecciones { get; set; }
        public List<DisplayEventoViewModel> Eventos { get; set; }
        #endregion

        public void ToEntityPrograma(ref Programa _entity)
        {
            _entity.Date = Date;
        }

    }

    public class DisplayEventoViewModel
    {
        public static implicit operator DisplayEventoViewModel(Evento _entity)
        {
            var _result = new DisplayEventoViewModel
            {
                Id = _entity.PK_IdEvento,
                FK_IdEdicion__SCH = _entity.Programa.Edicion,
                FK_IdSede__SCH = _entity.Programa.Sede.PK_IdSede,
                FK_IdPrograma__SCH = (int)_entity.FK_IdPrograma__SCH,
                FK_IdTipoEvento__SCH = (int)_entity.FK_IdTipoEvento__SCH,
                start = _entity.start,
                end = _entity.end,
                allDay = _entity.allDay,
                url = _entity.url,
                Lugar_ES = _entity.Lugar_ES,
                Lugar_EN = _entity.Lugar_EN,
                Descripcion_EN = _entity.Descripcion_EN,
                Descripcion_ES = _entity.Descripcion_ES,
                Evento_EN = _entity.Evento_EN,
                Evento_ES = _entity.Evento_ES,
                Image = _entity.Image,
                ImageWeb = _entity.ImageWeb,
                descripcionPrograma = _entity.Programa.Sede.Sede_ES + " - " + _entity.Programa.Edicion1.Nombre_ES,
                descripcionTipoEvento = _entity.TipoEvento.Tipo_ES
            };
            return _result;
        }

        public static implicit operator Evento(DisplayEventoViewModel _model)
        {
            return new Evento()
            {
                PK_IdEvento = _model.Id,
                FK_IdTipoEvento__SCH = (int)_model.FK_IdTipoEvento__SCH,
                start = _model.start,
                end = _model.end,
                allDay = _model.allDay,
                url = _model.url,
                Lugar_ES = _model.Lugar_ES,
                Lugar_EN = _model.Lugar_EN,
                Descripcion_EN = _model.Descripcion_EN,
                Descripcion_ES = _model.Descripcion_ES,
                Evento_EN = _model.Evento_EN,
                Evento_ES = _model.Evento_ES,
                Image = _model.Image,
                ImageWeb = _model.ImageWeb
            };
        }

        #region attr
            public int Id { get; set; }
            public string descripcionPrograma { get; set; }
            public string descripcionTipoEvento { get; set; }
            public int FK_IdPrograma__SCH { get; set; }
            public TimeSpan? start { get; set; }
            public TimeSpan? end { get; set; }
            public bool allDay { get; set; }
            public string url { get; set; }
            public int FK_IdTipoEvento__SCH { get; set; }
            public string Lugar_ES { get; set; }
            public string Lugar_EN { get; set; }
            public string Descripcion_EN { get; set; }
            public string Descripcion_ES { get; set; }
            public string Evento_EN { get; set; }
            public string Evento_ES { get; set; }
            public string Image { get; set; }
            public string ImageWeb { get; set; }
        #endregion

        public void ToEntityEvento(ref Evento _entity)
        {
            _entity.start = start;
            _entity.end = end;
            _entity.allDay = allDay;
            _entity.url = url;
            _entity.Lugar_ES = Lugar_ES;
            _entity.Lugar_EN = Lugar_EN;
            _entity.Descripcion_EN = Descripcion_EN;
            _entity.Descripcion_ES = Descripcion_ES;
            _entity.Evento_EN = Evento_EN;
            _entity.Evento_ES = Evento_ES;
            _entity.Image = Image;
            _entity.ImageWeb = ImageWeb;
        }

        public int FK_IdEdicion__SCH { get; set; }

        public int FK_IdSede__SCH { get; set; }
    }
    #endregion

    #region Jurado
    public class AppJuradoViewModel
    {
        public static implicit operator AppJuradoViewModel(AppJurado _entity)
        {
            var _result = new AppJuradoViewModel
            {
                Id = _entity.Id,
                Descripcion = _entity.Descripcion,
                TipoJurado_Id = _entity.TipoJurado_Id,
                UserId = _entity.UserId,
                Image = _entity.Image,
                Jurado_Activo = _entity.Jurado_Activo,
                Edicion = _entity.Edicion,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN
            };
            return _result;
        }

        public static implicit operator AppJurado(AppJuradoViewModel _entity)
        {
            return new AppJurado()
            {
                Id = _entity.Id,
                Descripcion = _entity.Descripcion,
                TipoJurado_Id = _entity.TipoJurado_Id,
                UserId = _entity.UserId,
                Image = _entity.Image,
                Jurado_Activo = _entity.Jurado_Activo,
                Edicion = _entity.Edicion,
                Informacion_ES = _entity.Informacion_ES,
                Informacion_EN = _entity.Informacion_EN
            };
        }

        #region Attr
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int TipoJurado_Id { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        public string Image { get; set; }

        public bool Jurado_Activo { get; set; }

        public int Edicion { get; set; }

        public string Informacion_ES { get; set; }

        public string Informacion_EN { get; set; }
        #endregion

        public void ToEntityAppJurado(ref AppJurado _entity)
        {
            Descripcion = _entity.Descripcion;
            TipoJurado_Id = _entity.TipoJurado_Id;
            UserId = _entity.UserId;
            Image = _entity.Image;
            Jurado_Activo = _entity.Jurado_Activo;
            Edicion = _entity.Edicion;
            Informacion_ES = _entity.Informacion_ES;
            Informacion_EN = _entity.Informacion_EN;
        }

    }
    #endregion
}

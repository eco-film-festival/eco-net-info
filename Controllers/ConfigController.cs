﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Controllers
{
    public class ConfigController : Controller
    {
        [Authorize(Roles = "admin")]
        public ActionResult Boletin()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Contacto()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Edicion()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Evento()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult EventoFestival()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Muestra()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Patrocinador()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Programa()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Proyeccion()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Sede()
        {
            return View();
        }

        [Authorize(Roles = "admin")]
        public ActionResult Jurado()
        {
            return View();
        }
    }
}
namespace ECO._2015.Models.Estadistica
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_Estadisitica_Festival_2015
    {
        [Key]
        public long? Id { get; set; }

        public string Key { get; set; }

        public int? Total { get; set; }

        //[Key]
        //[Column(Order = 0)]
        //[StringLength(33)]
        public string Type { get; set; }

        //[Key]
        //[Column(Order = 1)]
        //[StringLength(33)]
        public string Group { get; set; }

        //[Key]
        //[Column(Order = 2)]
        //[StringLength(4)]
        public string TypeChart { get; set; }

        //[Key]
        //[Column(Order = 3)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Orden { get; set; }

        //[Key]
        //[Column(Order = 4)]
        //[StringLength(8)]
        public string Unidad { get; set; }

        //[Key]
        //[Column(Order = 5)]
        //[StringLength(12)]
        public string Titulo { get; set; }
    }
}

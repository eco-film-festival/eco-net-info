namespace ECO._2015.Models.Estadistica
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContextEstadistica : DbContext
    {
        public DBContextEstadistica()
            : base("name=DBContextEstadistica")
        {
        }

        public virtual DbSet<v_Estadisitica_Entrevista> v_Estadisitica_Entrevista { get; set; }
        public virtual DbSet<v_Estadisitica_Festival_2014> v_Estadisitica_Festival_2014 { get; set; }
        public virtual DbSet<v_Estadisitica_Festival_2015> v_Estadisitica_Festival_2015 { get; set; }


        public virtual DbSet<v_Estadisitica_Festival> v_Estadisitica_Festival { get; set; }
    }
}

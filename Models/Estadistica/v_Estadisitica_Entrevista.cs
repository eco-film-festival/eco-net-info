namespace ECO._2015.Models.Estadistica
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class v_Estadisitica_Entrevista
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int? Universidad { get; set; }

        public int? Red_social_facebook { get; set; }

        public int? Red_social_otro { get; set; }

        public int? Red_social_twitter { get; set; }

        public int? oficial_web_site { get; set; }

        public int? revista { get; set; }

        public int? periodico { get; set; }

        public int? tv { get; set; }

        public int? radio { get; set; }

        public int? parabus { get; set; }

        public int? metro_bus { get; set; }

        public int? auto_bus { get; set; }

        public int? metro { get; set; }

        public int? muestra { get; set; }
    }
}

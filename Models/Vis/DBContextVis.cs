namespace ECO._2015.Models.Vis
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContextVis : DbContext
    {
        public DBContextVis()
            : base("name=DBContextVis")
        {
        }

        public virtual DbSet<VideoGaleria> VideoGaleria { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}

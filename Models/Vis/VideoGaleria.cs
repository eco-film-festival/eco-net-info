namespace ECO._2015.Models.Vis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("VIS.VideoGaleria")]
    public partial class VideoGaleria
    {
        public string Nombre { get; set; }

        public string Nobre_EN { get; set; }

        public string Lugar { get; set; }

        public string Lugar_Descripcion { get; set; }

        public string Lugar_Descripcion_EN { get; set; }

        public string Director { get; set; }

        public string Web_site { get; set; }

        public string Nacionalidad { get; set; }

        public string Nacionalidad_EN { get; set; }

        public string Categoria { get; set; }

        public string Categoria_EN { get; set; }

        [Column("class")]
        public string _class { get; set; }

        public string Duracion { get; set; }

        public int? Año { get; set; }

        public string Youtube { get; set; }

        public string Youtube_EN { get; set; }

        public string url_video_youtube { get; set; }

        public string url_video_youtube_en { get; set; }

        public string Vimeo { get; set; }

        public string host_img { get; set; }

        public string directory_imagen { get; set; }

        public string Poster { get; set; }

        public string Sinopsis { get; set; }

        public string Sinopsis_EN { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int active_sinopsis { get; set; }

        public string host { get; set; }

        public string directory { get; set; }

        public string mp4_hd { get; set; }

        public string mp4_sd { get; set; }

        public string ogv { get; set; }

        public string webm { get; set; }

        public string mp4_hd_en { get; set; }

        public string mp4_sd_en { get; set; }

        public string ogv_en { get; set; }

        public string webm_en { get; set; }

        public int? id_corto { get; set; }

        public string Premio { get; set; }

        public int? Edicion { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Orden { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ID { get; set; }

        public bool display { get; set; }
    }
}

namespace ECO._2015.Models.Config
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Sede")]
    public partial class Sede
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Sede()
        {
            Programa = new HashSet<Programa>();
        }

        [Key]
        public int PK_IdSede { get; set; }

        [Required]
        [StringLength(50)]
        public string Sede_ES { get; set; }

        [Required]
        [StringLength(50)]
        public string Sede_EN { get; set; }

        [Required]
        [StringLength(250)]
        public string Direccion { get; set; }

        public string url { get; set; }

        public string Image { get; set; }

        public string WebSite { get; set; }

        public int Edicion { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Programa> Programa { get; set; }
    }
}

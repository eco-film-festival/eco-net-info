namespace ECO._2015.Models.Config
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.TipoPatrocinador")]
    public partial class TipoPatrocinador
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoPatrocinador()
        {
            Patrocinador = new HashSet<Patrocinador>();
        }

        [Key]
        public int PK_IdTipoPatrocinador { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion_ES { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion_EN { get; set; }

        public int Orden { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Patrocinador> Patrocinador { get; set; }
    }
}

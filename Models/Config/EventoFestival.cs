namespace ECO._2015.Models.Config
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRE.EventoFestival")]
    public partial class EventoFestival
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EventoFestival()
        {
            EventoNotas = new HashSet<EventoNotas>();
            EventosImagen = new HashSet<EventosImagen>();
        }

        [Key]
        public int PK_IdEventoFestival { get; set; }

        public int FK_IdEdicion__SCH { get; set; }

        [StringLength(50)]
        public string Nombre_ES { get; set; }

        [StringLength(50)]
        public string Nombre_EN { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }

        public virtual Edicion Edicion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventoNotas> EventoNotas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventosImagen> EventosImagen { get; set; }
    }
}

namespace ECO._2015.Models.Config
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Evento")]
    public partial class Evento
    {
        [Key]
        public int PK_IdEvento { get; set; }

        public int FK_IdPrograma__SCH { get; set; }

        public TimeSpan? start { get; set; }

        public TimeSpan? end { get; set; }

        public bool allDay { get; set; }

        public string url { get; set; }

        public int FK_IdTipoEvento__SCH { get; set; }

        public string Lugar_ES { get; set; }

        public string Lugar_EN { get; set; }

        public string Descripcion_EN { get; set; }

        public string Descripcion_ES { get; set; }

        public string Evento_EN { get; set; }

        public string Evento_ES { get; set; }

        public string Image { get; set; }

        public string ImageWeb { get; set; }

        public virtual Programa Programa { get; set; }

        public virtual TipoEvento TipoEvento { get; set; }
    }
}

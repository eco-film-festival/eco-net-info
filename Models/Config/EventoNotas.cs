namespace ECO._2015.Models.Config
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRE.EventoNotas")]
    public partial class EventoNotas
    {
        [Key]
        public int PK_IdEventoNotas { get; set; }

        public int FK_IdEventoFestival__SCH { get; set; }

        [Required]
        public string Nota { get; set; }

        public virtual EventoFestival EventoFestival { get; set; }
    }
}

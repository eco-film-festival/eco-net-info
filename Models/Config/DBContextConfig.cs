﻿namespace ECO._2015.Models.Config
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    public class DBContextConfig : DbContext
    {
        public DBContextConfig()
            : base("name=DBContextConfig")
        {
        }

        public virtual DbSet<AppShortFilm> AppShortFilm { get; set; }
        public virtual DbSet<Boletin> Boletin { get; set; }
        public virtual DbSet<Contacto> Contacto { get; set; }
        public virtual DbSet<Edicion> Edicion { get; set; }
        public virtual DbSet<Evento> Evento { get; set; }
        public virtual DbSet<EventoFestival> EventoFestival { get; set; }
        public virtual DbSet<EventoNotas> EventoNotas { get; set; }
        public virtual DbSet<EventosImagen> EventosImagen { get; set; }
        public virtual DbSet<Muestra> Muestra { get; set; }
        public virtual DbSet<Patrocinador> Patrocinador { get; set; }
        public virtual DbSet<Programa> Programa { get; set; }
        public virtual DbSet<Proyeccion> Proyeccion { get; set; }
        public virtual DbSet<Sede> Sede { get; set; }
        public virtual DbSet<TipoEvento> TipoEvento { get; set; }
        public virtual DbSet<TipoPatrocinador> TipoPatrocinador { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.Proyeccion)
                .WithMany(e => e.AppShortFilm)
                .Map(m => m.ToTable("ProyeccionShortFilm", "SCH").MapLeftKey("FK_IdShortFilm__dbo").MapRightKey("FK_IdProyeccion__SCH"));

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Boletin)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Contacto)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.EventoFestival)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Muestra)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.EdicionId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Patrocinador)
                .WithOptional(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Programa)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Sede)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EventoFestival>()
                .HasMany(e => e.EventoNotas)
                .WithRequired(e => e.EventoFestival)
                .HasForeignKey(e => e.FK_IdEventoFestival__SCH);

            modelBuilder.Entity<EventoFestival>()
                .HasMany(e => e.EventosImagen)
                .WithRequired(e => e.EventoFestival)
                .HasForeignKey(e => e.FK_IdEventoFestival__SCH);

            modelBuilder.Entity<Muestra>()
                .HasMany(e => e.Proyeccion)
                .WithMany(e => e.Muestra)
                .Map(m => m.ToTable("ProyeccionMuestra", "SCH").MapLeftKey("FK_IdMuestra__SCH").MapRightKey("FK_IdProyeccion__SCH"));

            modelBuilder.Entity<Programa>()
                .HasMany(e => e.Evento)
                .WithRequired(e => e.Programa)
                .HasForeignKey(e => e.FK_IdPrograma__SCH);

            modelBuilder.Entity<Programa>()
                .HasMany(e => e.Proyeccion)
                .WithRequired(e => e.Programa)
                .HasForeignKey(e => e.FK_IdPrograma__SCH);

            modelBuilder.Entity<Sede>()
                .HasMany(e => e.Programa)
                .WithOptional(e => e.Sede)
                .HasForeignKey(e => e.FK_IdSede__SCH)
                .WillCascadeOnDelete();

            modelBuilder.Entity<TipoEvento>()
                .HasMany(e => e.Evento)
                .WithRequired(e => e.TipoEvento)
                .HasForeignKey(e => e.FK_IdTipoEvento__SCH);

            modelBuilder.Entity<TipoEvento>()
                .HasMany(e => e.Proyeccion)
                .WithRequired(e => e.TipoEvento)
                .HasForeignKey(e => e.FK_IdTipoEvento__SCH);

            modelBuilder.Entity<TipoPatrocinador>()
                .HasMany(e => e.Patrocinador)
                .WithRequired(e => e.TipoPatrocinador)
                .HasForeignKey(e => e.FK_IdTipoPatrocinador__SCH);
        }
    }
}
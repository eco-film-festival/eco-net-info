namespace ECO._2015.Models.Config
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRE.Boletin")]
    public partial class Boletin
    {
        [Key]
        public int PK_IdBoletin { get; set; }

        public int FK_IdEdicion__SCH { get; set; }

        public string Boletin_ES { get; set; }

        public string Boletin_EN { get; set; }

        public string Titulo_ES { get; set; }

        public string Titulo_EN { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Fecha { get; set; }

        public virtual Edicion Edicion { get; set; }
    }
}

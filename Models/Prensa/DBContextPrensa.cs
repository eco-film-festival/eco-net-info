namespace ECO._2015.Models.Prensa
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContextPrensa : DbContext
    {
        public DBContextPrensa()
            : base("name=DBContextPrensa")
        {
        }

        public virtual DbSet<AppEstado> AppEstado { get; set; }
        public virtual DbSet<AppNacionalidad> AppNacionalidad { get; set; }
        public virtual DbSet<AppPais> AppPais { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Boletin> Boletin { get; set; }
        public virtual DbSet<Cobertura> Cobertura { get; set; }
        public virtual DbSet<DiaTranmision> DiaTranmision { get; set; }
        public virtual DbSet<EventoFestival> EventoFestival { get; set; }
        public virtual DbSet<EventoNotas> EventoNotas { get; set; }
        public virtual DbSet<EventosImagen> EventosImagen { get; set; }
        public virtual DbSet<Medio> Medio { get; set; }
        public virtual DbSet<MedioTVRadio> MedioTVRadio { get; set; }
        public virtual DbSet<Perfil> Perfil { get; set; }
        public virtual DbSet<Periodicidad> Periodicidad { get; set; }
        public virtual DbSet<TipoMedio> TipoMedio { get; set; }
        public virtual DbSet<TipoMedioDetail> TipoMedioDetail { get; set; }
        public virtual DbSet<Edicion> Edicion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppEstado>()
                .HasMany(e => e.Medio)
                .WithOptional(e => e.AppEstado)
                .HasForeignKey(e => e.FK__AppEstado);

            modelBuilder.Entity<AppNacionalidad>()
                .HasMany(e => e.AppPais)
                .WithRequired(e => e.AppNacionalidad)
                .HasForeignKey(e => e.Nacionalidad_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppNacionalidad>()
                .HasMany(e => e.AppPais1)
                .WithMany(e => e.AppNacionalidad1)
                .Map(m => m.ToTable("PaisNacionalidads").MapLeftKey("Nacionalidad_Id").MapRightKey("Pais_Id"));

            modelBuilder.Entity<AppPais>()
                .HasMany(e => e.AppEstado)
                .WithRequired(e => e.AppPais)
                .HasForeignKey(e => e.PaisId);

            modelBuilder.Entity<AppPais>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.AppPais)
                .HasForeignKey(e => e.FK__AppPais)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cobertura>()
                .HasMany(e => e.Medio)
                .WithMany(e => e.Cobertura)
                .Map(m => m.ToTable("MedioCobertura", "PRE").MapLeftKey("FK_IdCobertura__SCH").MapRightKey("FK_IdMedio__SCH"));

            modelBuilder.Entity<DiaTranmision>()
                .HasMany(e => e.Medio)
                .WithMany(e => e.DiaTranmision)
                .Map(m => m.ToTable("MedioDiaTranmision", "PRE").MapLeftKey("FK_IdDiaTranmision__SCH").MapRightKey("FK_IdMedio__SCH"));

            modelBuilder.Entity<EventoFestival>()
                .HasMany(e => e.EventoNotas)
                .WithRequired(e => e.EventoFestival)
                .HasForeignKey(e => e.FK_IdEventoFestival__SCH);

            modelBuilder.Entity<EventoFestival>()
                .HasMany(e => e.EventosImagen)
                .WithRequired(e => e.EventoFestival)
                .HasForeignKey(e => e.FK_IdEventoFestival__SCH);

            modelBuilder.Entity<Medio>()
                .HasOptional(e => e.MedioTVRadio)
                .WithRequired(e => e.Medio)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Medio>()
                .HasMany(e => e.Perfil)
                .WithMany(e => e.Medio)
                .Map(m => m.ToTable("MedioPerfil", "PRE").MapLeftKey("FK_IdMedio__SCH").MapRightKey("FK_IdPerfil__SCH"));

            modelBuilder.Entity<Medio>()
                .HasMany(e => e.Periodicidad)
                .WithMany(e => e.Medio)
                .Map(m => m.ToTable("MedioPeriodicidad", "PRE").MapLeftKey("FK_IdMedio__SCH").MapRightKey("FK_IdPeriodicidad__SCH"));

            modelBuilder.Entity<Medio>()
                .HasMany(e => e.TipoMedioDetail)
                .WithMany(e => e.Medio)
                .Map(m => m.ToTable("MedioTipoMedioDetail", "PRE").MapLeftKey("FK_IdMedio__SCH").MapRightKey("FK_IdTipoMedioDetail__SCH"));

            modelBuilder.Entity<MedioTVRadio>()
                .Property(e => e.Frecuencia)
                .IsFixedLength();

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Cobertura)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.DiaTranmision)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Perfil)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Periodicidad)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.TipoMedioDetail)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Boletin)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.EventoFestival)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);
        }
    }
}

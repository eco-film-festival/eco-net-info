namespace ECO._2015.Models.Prensa
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRE.EventosImagen")]
    public partial class EventosImagen
    {
        [Key]
        public int PK_IdEventosImagen { get; set; }

        public int FK_IdEventoFestival__SCH { get; set; }

        [Required]
        [StringLength(50)]
        public string Path { get; set; }

        [Required]
        [StringLength(50)]
        public string Image { get; set; }

        [Required]
        [StringLength(50)]
        public string Ext { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public bool Portada { get; set; }

        public virtual EventoFestival EventoFestival { get; set; }
    }
}

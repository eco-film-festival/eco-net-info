namespace ECO._2015.Models.Prensa
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRE.TipoMedio")]
    public partial class TipoMedio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoMedio()
        {
            Cobertura = new HashSet<Cobertura>();
            DiaTranmision = new HashSet<DiaTranmision>();
            Medio = new HashSet<Medio>();
            Perfil = new HashSet<Perfil>();
            Periodicidad = new HashSet<Periodicidad>();
            TipoMedioDetail = new HashSet<TipoMedioDetail>();
        }

        [Key]
        public int PK_IdTipoMedio { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion { get; set; }

        public bool Otro { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cobertura> Cobertura { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiaTranmision> DiaTranmision { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medio> Medio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Perfil> Perfil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Periodicidad> Periodicidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipoMedioDetail> TipoMedioDetail { get; set; }
    }
}

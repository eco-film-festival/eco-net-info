namespace ECO._2015.Models.Prensa
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PRE.Medio")]
    public partial class Medio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Medio()
        {
            Cobertura = new HashSet<Cobertura>();
            DiaTranmision = new HashSet<DiaTranmision>();
            Perfil = new HashSet<Perfil>();
            Periodicidad = new HashSet<Periodicidad>();
            TipoMedioDetail = new HashSet<TipoMedioDetail>();
        }

        [Key]
        public int PK_IdMedio { get; set; }

        public int FK_IdEdicion__SCH { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public string Apellidos { get; set; }

        public string Cargo { get; set; }

        [Column("Medio")]
        [Required]
        public string Medio1 { get; set; }

        [Required]
        public string Telefono { get; set; }

        public string TipoTelefono { get; set; }

        public string WebSite { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Youtube { get; set; }

        public string Vimeo { get; set; }

        public int FK__AppPais { get; set; }

        public int? FK__AppEstado { get; set; }

        public int FK_IdTipoMedio__SCH { get; set; }

        public string TipoMedioOtro { get; set; }

        public string TipoMedioDetailOtro { get; set; }

        public bool AceptoTerminos { get; set; }

        public string TestigoUrl { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public virtual AppEstado AppEstado { get; set; }

        public virtual AppPais AppPais { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        public virtual Edicion Edicion { get; set; }

        public virtual TipoMedio TipoMedio { get; set; }

        public virtual MedioTVRadio MedioTVRadio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Cobertura> Cobertura { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DiaTranmision> DiaTranmision { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Perfil> Perfil { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Periodicidad> Periodicidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipoMedioDetail> TipoMedioDetail { get; set; }
    }
}

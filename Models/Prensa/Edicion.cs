namespace ECO._2015.Models.Prensa
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Edicion")]
    public partial class Edicion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Edicion()
        {
            Boletin = new HashSet<Boletin>();
            EventoFestival = new HashSet<EventoFestival>();
            Medio = new HashSet<Medio>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PK_IdEdicion { get; set; }

        public string Nombre_ES { get; set; }

        public string Nombre_EN { get; set; }

        public string Tema_ES { get; set; }

        public string Tema_EN { get; set; }

        public string Info_ES { get; set; }

        public string Info_EN { get; set; }

        public string BasesConvocatoria_ES { get; set; }

        public string BasesConvocatoria_EN { get; set; }

        public string Cartel_ES { get; set; }

        public string Cartel_EN { get; set; }

        public string Logotipo_ES { get; set; }

        public string Logotipo_EN { get; set; }

        public string Teaser_ES { get; set; }

        public string Teaser_EN { get; set; }

        public string Informacion_ES { get; set; }

        public string Informacion_EN { get; set; }

        public string Direccion_ES { get; set; }

        public string Direccion_EN { get; set; }

        public string MapaContato { get; set; }

        public string ManualGrafico_ES { get; set; }

        public string ManualGrafico_EN { get; set; }

        public string Catalogo_ES { get; set; }

        public string Catalogo_EN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Boletin> Boletin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventoFestival> EventoFestival { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medio> Medio { get; set; }
    }
}

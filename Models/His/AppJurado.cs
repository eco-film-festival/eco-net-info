namespace ECO._2015.Models.His
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HIS.AppJurado")]
    public partial class AppJurado
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion { get; set; }

        public string Informacion_ES { get; set; }

        public string Informacion_EN { get; set; }

        public string Image { get; set; }

        public int Edicion { get; set; }

        public virtual Edicion EdicionEntity { get; set; }
    }
}

namespace ECO._2015.Models.His
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HIS.Galeria")]
    public partial class Galeria
    {
        public int Id { get; set; }

        public string Image { get; set; }

        public int Edicion { get; set; }

        public virtual Edicion EdicionEntity { get; set; }
    }
}

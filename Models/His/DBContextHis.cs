namespace ECO._2015.Models.His
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContextHis : DbContext
    {
        public DBContextHis()
            : base("name=DBContextHis")
        {
        }

        public virtual DbSet<AppJurado> AppJurado { get; set; }
        public virtual DbSet<Galeria> Galeria { get; set; }
        public virtual DbSet<Edicion> Edicion { get; set; }
        public virtual DbSet<SeleccionOficial> SeleccionOficial { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppJurado)
                .WithRequired(e => e.EdicionEntity)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Galeria)
                .WithRequired(e => e.EdicionEntity)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.SeleccionOficial)
                .WithRequired(e => e.EdicionEntity)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);
        }
    }
}

﻿namespace ECO._2015.Models.His
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HIS.SeleccionOficial")]
    public class SeleccionOficial
    {
        public int Id { get; set; }

        public string Titulo_ES { get; set; }

        public string Titulo_EN { get; set; }

        public string Info_ES { get; set; }

        public string Info_EN { get; set; }
        public string Categoria_ES { get; set; }
        public string DirectorCorto { get; set; }

        public int Edicion { get; set; }

        public virtual Edicion EdicionEntity { get; set; }

        public string Nacionalidad_ES { get; set; }

        public string Categoria_EN { get; set; }

        public string Nacionalidad_EN { get; set; }
    }
}
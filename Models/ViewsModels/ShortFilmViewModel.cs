﻿using ECO._2015.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Models.ViewsModels
{
    #region ShortFilmViewModel

    public class ShortFilmViewModel
    {
        #region Constructor

        public ShortFilmViewModel()
        {
        }

        public ShortFilmViewModel(AppShortFilm _ShortFilm)
        {
            Id = _ShortFilm.Id;
            UserId = _ShortFilm.UserId;
            PaisId = _ShortFilm.AppEstado.AppPais.Id;
            EstadoId = _ShortFilm.AppEstado.Id;
            MunicipioId = _ShortFilm.MunicipioId;
            CategoriaId = _ShortFilm.CategoriaId;
            AnnoId = _ShortFilm.AnnoId;
            Duracion_Minutos = _ShortFilm.Duracion_Minutos;
            Duracion_Segundos = _ShortFilm.Duracion_Segundos;
            SubtituloId = _ShortFilm.SubtituloId;
            Idioma = _ShortFilm.Idioma;
            FormatoVideoId = _ShortFilm.FormatoVideoId;
            OtroFormatoVideo = _ShortFilm.OtroFormatoVideo;
            FormatoAudioId = _ShortFilm.FormatoAudioId;
            OtroFormatoAudio = _ShortFilm.OtroFormatoAudio;
            Titulo_Original = _ShortFilm.Titulo_Original;
            Titulo_Espanol = _ShortFilm.Titulo_Espanol;
            Titulo_Ingles = _ShortFilm.Titulo_Ingles;
            Sinopsis_Espanol = _ShortFilm.Sinopsis_Espanol;
            Sinopsis_Ingles = _ShortFilm.Sinopsis_Ingles;
            Acepto_Terminos_Condiciones = _ShortFilm.Acepto_Terminos_Condiciones;
            Etapa = _ShortFilm.Etapa;
            Titulo_Ingles = _ShortFilm.Titulo_Ingles;
            Titulo_Ingles = _ShortFilm.Titulo_Ingles;
            Contacto = new ContactoViewModel(_ShortFilm);
            Director = new DirectorViewModel(_ShortFilm);
            Productor = new ProductorViewModel(_ShortFilm);
            Fotografia = new FotografiaViewModel(_ShortFilm);
            Guion = new GuionViewModel(_ShortFilm);
            Edicion = new EdicionViewModel(_ShortFilm);
            DisenoSonoro = new DisenoSonoroViewModel(_ShortFilm);
            Sonido = new SonidoViewModel(_ShortFilm);
            Musica = new MusicaViewModel(_ShortFilm);
            //--------------------------------------------
        }

        #endregion Constructor

        #region ShortFilm

        [Display(Name = "User")]
        public string UserId { get; set; }

        [Display(Name = "Pais")]
        public int PaisId { get; set; }

        [Display(Name = "Estado")]
        public int EstadoId { get; set; }

        [Display(Name = "GÉNERO EN LA QUE SE PARTICIPA | SECTION TO PARTICIPATE")]
        public int CategoriaId { get; set; }

        [Display(Name = "Año | Year")]
        public int AnnoId { get; set; }

        [Required]
        [Range(0, 59)]
        [Display(Name = "Minutos | Minutes")]
        public int Duracion_Minutos { get; set; }

        [Required]
        [Range(0, 59)]
        [Display(Name = "Segundos | Segundos")]
        public int Duracion_Segundos { get; set; }

        [Display(Name = "Subtitulo")]
        public int SubtituloId { get; set; }

        [Required]
        [Display(Name = "Idioma | Lenguage")]
        public string Idioma { get; set; }

        [Display(Name = "Formato Video")]
        public int FormatoVideoId { get; set; }

        [Display(Name = "Otro | Other")]
        public string OtroFormatoVideo { get; set; }

        [Display(Name = "Formato Audio")]
        public int FormatoAudioId { get; set; }

        [Display(Name = "Otro | Other")]
        public string OtroFormatoAudio { get; set; }

        [Required]
        [Display(Name = "Título original | Original title")]
        public string Titulo_Original { get; set; }

        [Required]
        [Display(Name = "Título en español | Spanish title")]
        public string Titulo_Espanol { get; set; }

        [Required]
        [Display(Name = "Título en inglés | English title")]
        public string Titulo_Ingles { get; set; }

        [Required]
        [Display(Name = "Sinopsis en Español | Synopsis in Spanish")]
        public string Sinopsis_Espanol { get; set; }

        [Required]
        [Display(Name = "Sinopsis en Inglés | Synopsis in English")]
        public string Sinopsis_Ingles { get; set; }

        #endregion ShortFilm

        #region Acepto_Terminos_Condiciones

        [Required]
        [Display(Name = "Acepto Términos y Condiciones | Accept Terms and Conditions")]
        [DefaultValue(false)]
        public bool Acepto_Terminos_Condiciones { get; set; }

        #endregion Acepto_Terminos_Condiciones

        #region Operacion

        public AppShortFilm CreateShortFilm(
            string UserId,
            DateTime FechaRegistro, bool Activo, int EdicionFestival,
            int ConfImagesAutor, int ConfImagesStills
            )
        {
            #region _shortFilm
            var _shortFilm = new AppShortFilm()
            {
                UserId = UserId,
                Etapa = Etapa,
                // -----------------------------
                FechaRegistro = FechaRegistro,
                Activo = Activo,
                Edicion = EdicionFestival,
                // -----------------------------
                CategoriaId = CategoriaId,
                EstadoId = EstadoId,
                MunicipioId = MunicipioId,
                FormatoVideoId = FormatoVideoId,
                OtroFormatoVideo = OtroFormatoVideo,
                FormatoAudioId = FormatoAudioId,
                OtroFormatoAudio = OtroFormatoAudio,
                AnnoId = AnnoId,
                SubtituloId = SubtituloId,
                Duracion_Minutos = Duracion_Minutos,
                Duracion_Segundos = Duracion_Segundos,
                Idioma = Idioma,
                Titulo_Original = Titulo_Original,
                Titulo_Espanol = Titulo_Espanol,
                Titulo_Ingles = Titulo_Ingles,
                Sinopsis_Espanol = Sinopsis_Espanol,
                Sinopsis_Ingles = Sinopsis_Ingles,
                // -------------------------
                Acepto_Terminos_Condiciones = Acepto_Terminos_Condiciones,
                // -------------------------
                AppEquipo = new List<AppEquipo>(),
                AppImagenStill = new List<AppImagenStill>(),
                AppImagenAutor = new List<AppImagenAutor>(),
                // -------------------------
            };
            #endregion _shortFilm

            #region ListImageAutor ListStill
            for (int i = 0; i < ConfImagesAutor; i++)
            {
                _shortFilm.AppImagenAutor.Add(new AppImagenAutor() { AppShortFilm = _shortFilm });
            }

            for (int i = 0; i < ConfImagesStills; i++)
            {
                _shortFilm.AppImagenStill.Add(new AppImagenStill() { AppShortFilm = _shortFilm });
            }
            #endregion ListImageAutor ListStill

            #region ListEquipo
            _shortFilm.AppEquipo.Add(Contacto.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(Director.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(Productor.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(Fotografia.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(Guion.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(DisenoSonoro.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(Edicion.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(Sonido.CreateEquipo(_shortFilm));
            _shortFilm.AppEquipo.Add(Musica.CreateEquipo(_shortFilm));
            #endregion ListEquipo
            return _shortFilm;
        }

        public void EditShortFilm(ref AppShortFilm _ShortFilm)
        {
            // -----------------------------
            _ShortFilm.CategoriaId = CategoriaId;
            _ShortFilm.EstadoId = EstadoId;
            _ShortFilm.MunicipioId = MunicipioId;
            _ShortFilm.FormatoVideoId = FormatoVideoId;
            _ShortFilm.OtroFormatoVideo = OtroFormatoVideo;
            _ShortFilm.FormatoAudioId = FormatoAudioId;
            _ShortFilm.OtroFormatoAudio = OtroFormatoAudio;
            _ShortFilm.AnnoId = AnnoId;
            _ShortFilm.SubtituloId = SubtituloId;
            _ShortFilm.Duracion_Minutos = Duracion_Minutos;
            _ShortFilm.Duracion_Segundos = Duracion_Segundos;
            _ShortFilm.Idioma = Idioma;
            _ShortFilm.Titulo_Original = Titulo_Original;
            _ShortFilm.Titulo_Espanol = Titulo_Espanol;
            _ShortFilm.Titulo_Ingles = Titulo_Ingles;
            _ShortFilm.Sinopsis_Espanol = Sinopsis_Espanol;
            _ShortFilm.Sinopsis_Ingles = Sinopsis_Ingles;            
            // -------------------------
            Contacto.EditEquipo(ref _ShortFilm);
            // -------------------------
            Director.EditEquipo(ref _ShortFilm);
            // -------------------------
            Productor.EditEquipo(ref _ShortFilm);
            // -------------------------
            Fotografia.EditEquipo(ref _ShortFilm);
            // -------------------------
            Guion.EditEquipo(ref _ShortFilm);
            // -------------------------
            Edicion.EditEquipo(ref _ShortFilm);
            // -------------------------
            DisenoSonoro.EditEquipo(ref _ShortFilm);
            // -------------------------
            Sonido.EditEquipo(ref _ShortFilm);
            // -------------------------
            Musica.EditEquipo(ref _ShortFilm);
            // -------------------------
        }

        #endregion Operacion

        #region Equipo

        public ContactoViewModel Contacto { get; set; }

        public DirectorViewModel Director { get; set; }

        public ProductorViewModel Productor { get; set; }

        public FotografiaViewModel Fotografia { get; set; }

        public GuionViewModel Guion { get; set; }

        public EdicionViewModel Edicion { get; set; }

        public DisenoSonoroViewModel DisenoSonoro { get; set; }

        public SonidoViewModel Sonido { get; set; }

        public MusicaViewModel Musica { get; set; }

        #endregion Equipo

        #region

        public int Etapa { get; set; }

        public int Id { get; set; }
        [Display(Name = "Municipio")]
        public int? MunicipioId { get; set; }

        #endregion ShortFilmViewModel
    }

    #endregion

    #region Equipo

    public class MusicaViewModel : OtroViewModel
    {
        public MusicaViewModel()
        {
        }

        public MusicaViewModel(AppShortFilm _ShortFilm)
            : base(_ShortFilm)
        {
        }

        [Display(Name = "Música Original | Original music")]
        public override int IdTipoEquipo { get { return 8; } }

        public override AppEquipo CreateEquipo(AppShortFilm _shortFilm)
        {
            var _equipo = base.CreateEquipo(_shortFilm);
            _equipo.IdTipoEquipo = IdTipoEquipo;
            return _equipo;
        }
    }

    public class SonidoViewModel : OtroViewModel
    {
        public SonidoViewModel()
        {
        }

        public SonidoViewModel(AppShortFilm _ShortFilm)
            : base(_ShortFilm)
        {
        }

        [Display(Name = "Sonido | Sound")]
        public override int IdTipoEquipo { get { return 6; } }

        public override AppEquipo CreateEquipo(AppShortFilm _shortFilm)
        {
            var _equipo = base.CreateEquipo(_shortFilm);
            _equipo.IdTipoEquipo = IdTipoEquipo;
            return _equipo;
        }
    }

    public class FotografiaViewModel : OtroViewModel
    {
        public FotografiaViewModel()
        {
        }

        public FotografiaViewModel(AppShortFilm _ShortFilm)
            : base(_ShortFilm)
        {
        }

        [Display(Name = "Fotografía | Photographer")]
        public override int IdTipoEquipo { get { return 4; } }

        public override AppEquipo CreateEquipo(AppShortFilm _shortFilm)
        {
            var _equipo = base.CreateEquipo(_shortFilm);
            _equipo.IdTipoEquipo = IdTipoEquipo;
            return _equipo;
        }
    }

    public class GuionViewModel : OtroViewModel
    {
        public GuionViewModel()
        {
        }

        public GuionViewModel(AppShortFilm _ShortFilm)
            : base(_ShortFilm)
        {
        }

        [Display(Name = "Guión | Screenplay")]
        public override int IdTipoEquipo { get { return 3; } }

        public override AppEquipo CreateEquipo(AppShortFilm _shortFilm)
        {
            var _equipo = base.CreateEquipo(_shortFilm);
            _equipo.IdTipoEquipo = IdTipoEquipo;
            return _equipo;
        }
    }

    public class EdicionViewModel : OtroViewModel
    {
        public EdicionViewModel()
        {
        }

        public EdicionViewModel(AppShortFilm _ShortFilm)
            : base(_ShortFilm)
        {
        }

        [Display(Name = "Edición | Editingy")]
        public override int IdTipoEquipo { get { return 5; } }

        public override AppEquipo CreateEquipo(AppShortFilm _shortFilm)
        {
            var _equipo = base.CreateEquipo(_shortFilm);
            _equipo.IdTipoEquipo = IdTipoEquipo;
            return _equipo;
        }
    }

    public class DisenoSonoroViewModel : OtroViewModel
    {
        public DisenoSonoroViewModel()
        {
        }

        public DisenoSonoroViewModel(AppShortFilm _ShortFilm)
            : base(_ShortFilm)
        {
        }

        [Display(Name = "Diseño Sonoro | Sound Design")]
        public override int IdTipoEquipo { get { return 7; } }

        public override AppEquipo CreateEquipo(AppShortFilm _shortFilm)
        {
            var _equipo = base.CreateEquipo(_shortFilm);
            _equipo.IdTipoEquipo = IdTipoEquipo;
            return _equipo;
        }
    }

    public class ContactoViewModel
    {
        public ContactoViewModel()
        {
        }

        public ContactoViewModel(AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                var _equipo = _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo).FirstOrDefault();
                if (_equipo != null)
                {
                    Id = _equipo.Id;
                    IdShortFilm = _ShortFilm.Id;
                    Nombre = _equipo.Nombre;
                    Direccion = _equipo.Direccion;
                    Email = _equipo.Email;
                    Telefono = _equipo.Telefono;
                    Local_Cel = _equipo.Local_Cel;
                    Facebook = _equipo.Facebook;
                    Twiiter = _equipo.Twiiter;
                    Casa_Productora = _equipo.Casa_Productora;
                    Web_Site_Casa_Productora = _equipo.Web_Site_Casa_Productora;
                    Sexo = _equipo.Sexo;
                    Fecha_Nac = _equipo.Fecha_Nac;
                    Residencia = _equipo.Residencia;
                    Annio_Nac = _equipo.Annio_Nac;
                }
            }
        }

        #region

        [Required]
        public int IdShortFilm { get; set; }

        [Required]
        [Display(Name = "CONTACTO | CONTACT INFO")]
        public int IdTipoEquipo { get { return 89; } }

        [Required]
        [Display(Name = "Nombre | Name :")]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "Dirección | Adress :")]
        public string Direccion { get; set; }

        [Required]
        [Display(Name = "E-mail | E-mail:")]
        public string Email { get; set; }

        //[Required]
        [Display(Name = "Fecha Nacimiento | Birth date:")]
        public string Fecha_Nac { get; set; }

        [Required]
        [Display(Name = "Año de Nacimiento | Year of Birth:")]
        public int Annio_Nac { get; set; }

        [Required]
        [Display(Name = "Sexo | Gender:")]
        public string Sexo { get; set; }

        //[Required]
        [Display(Name = "Residencia | Residence:")]
        public string Residencia { get; set; }


        [Required]
        [Display(Name = "Teléfono (con lada) | Phone number (with dial codes):")]
        public string Telefono { get; set; }

        [Required]
        [Display(Name = "Es Celular | Is Cell phone?:")]
        public bool Local_Cel { get; set; }

        //[Required]
        [Display(Name = "Perfil Facebook | Facebook Profile: ")]
        public string Facebook { get; set; }

        //[Required]
        [Display(Name = "Twitter:")]
        public string Twiiter { get; set; }

        [Display(Name = "Casa productora | Production company:")]
        public bool Casa_Productora { get; set; }

        [Display(Name = "Web Site Casa productora | Web Site Production company:")]
        public string Web_Site_Casa_Productora { get; set; }

        #endregion

        public AppEquipo CreateEquipo(AppShortFilm _ShortFilm)
        {
            return new AppEquipo()
            {
                IdTipoEquipo = IdTipoEquipo,
                AppShortFilm = _ShortFilm,
                Nombre = Nombre,
                Direccion = Direccion,
                Email = Email,
                Telefono = Telefono,
                Local_Cel = Local_Cel,
                Facebook = Facebook,
                Twiiter = Twiiter,
                Casa_Productora = Casa_Productora,
                Web_Site_Casa_Productora = Web_Site_Casa_Productora,
                Sexo = Sexo,
                Fecha_Nac = Fecha_Nac,
                Residencia = Residencia,
                Annio_Nac = Annio_Nac,
            };
        }

        public int Id { get; set; }
        public List<SelectListItem> List_Annio_Nac {
            get {
                return SelectTool.GetListAnnioNacimiento();
            }
        }

        public void EditEquipo(ref AppEquipo _equipo)
        {
            if (_equipo != null)
            {
                _equipo.Nombre = Nombre;
                _equipo.Direccion = Direccion;
                _equipo.Email = Email;
                _equipo.Telefono = Telefono;
                _equipo.Local_Cel = Local_Cel;
                _equipo.Facebook = Facebook;
                _equipo.Twiiter = Twiiter;
                _equipo.Casa_Productora = Casa_Productora;
                _equipo.Web_Site_Casa_Productora = Web_Site_Casa_Productora;
                _equipo.Sexo = Sexo;
                _equipo.Fecha_Nac = Fecha_Nac;
                _equipo.Residencia = Residencia;
                _equipo.Annio_Nac = Annio_Nac;
            }
        }

        public void EditEquipo(ref AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo)
                    .ToList().ForEach(e => EditEquipo(ref e));
            }
        }
    }

    public class DirectorViewModel
    {
        public DirectorViewModel()
        {
        }

        public DirectorViewModel(AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                var _equipo = _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo).FirstOrDefault();
                if (_equipo != null)
                {
                    Id = _equipo.Id;
                    IdShortFilm = _ShortFilm.Id;
                    Nombre = _equipo.Nombre;
                    Email = _equipo.Email;
                    Telefono = _equipo.Telefono;
                    Local_Cel = _equipo.Local_Cel;
                    Ocupacion = _equipo.Ocupacion;
                    Estudiante = _equipo.Estudiante;
                    Profecional = _equipo.Profecional;
                    Ciudad_Estudio = _equipo.Ciudad_Estudio;
                    Nacionalidad = _equipo.Nacionalidad;
                    Biografia = _equipo.Biografia;
                    Sexo = _equipo.Sexo;
                    Fecha_Nac = _equipo.Fecha_Nac;
                    Residencia = _equipo.Residencia;
                    Edad = _equipo.Annio_Nac;
                    Annio_Nac = _equipo.Annio_Nac;
                    Nacionalidad_Id = _equipo.Nacionalidad_Id;
                }
            }
        }

        #region

        public int Id { get; set; }

        [Required]
        public int IdShortFilm { get; set; }

        [Required]
        [Display(Name = "DIRECTOR | DIRECTOR")]
        public int IdTipoEquipo { get { return 1; } }

        [Required]
        [Display(Name = "Nombre | Name :")]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "E-mail | E-mail :")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Teléfono (con lada) | Phone number (with dial codes) :")]
        public string Telefono { get; set; }

        [Required]
        [Display(Name = "Es Celular | Is Cell phone?:")]
        public bool Local_Cel { get; set; }

        [Required]
        [Display(Name = "Ocupación | Occupation:")]
        public string Ocupacion { get; set; }

        [Required]
        [Display(Name = "Estudiante | Student:")]
        public bool Estudiante { get; set; }

        [Required]
        [Display(Name = "Profesional | Professional:")]
        public bool Profecional { get; set; }

        [Required]
        [Display(Name = "Nombre y ciudad de estudios | Name and city of education:")]
        public string Ciudad_Estudio { get; set; }

        //[Required]
        [Display(Name = "Nacionalidad | Nationality")]
        public string Nacionalidad { get; set; }

        [Required]
        [Display(Name = "Biografía | Biography")]
        public string Biografia { get; set; }

        //[Required]
        [Display(Name = "Fecha Nacimiento | Birth date:")]
        public string Fecha_Nac { get; set; }

        [Required]
        [Display(Name = "Sexo | Gender:")]
        public string Sexo { get; set; }

        //[Required]
        [Display(Name = "Residencia | Residence:")]
        public string Residencia { get; set; }

        // public ShortFilm ShortFilm { get; set; }

        [Required]
        [Display(Name = "Edad | Age:")]
        public int Edad { get; set; }

        [Required]
        [Display(Name = "Año de Nacimiento | Year of Birth:")]
        public int Annio_Nac { get; set; }

        public List<SelectListItem> List_Annio_Nac
        {
            get
            {
                return SelectTool.GetListAnnioNacimiento();
            }
        }

        [Required]
        [Display(Name = "Nacionalidad | Nationality")]
        public int? Nacionalidad_Id { get; set; }


        //public List<SelectListItem> List_Nacionalidad
        //{
        //    get
        //    {
        //        return SelectTool.GetListNacionalidad();
        //    }
        //}
        #endregion

        public AppEquipo CreateEquipo(AppShortFilm _ShortFilm)
        {
            return new AppEquipo()
                    {
                        IdTipoEquipo = IdTipoEquipo,
                        AppShortFilm = _ShortFilm,
                        Nombre = Nombre,
                        Email = Email,
                        Telefono = Telefono,
                        Local_Cel = Local_Cel,
                        Ocupacion = Ocupacion,
                        Estudiante = Estudiante,
                        Profecional = Profecional,
                        Ciudad_Estudio = Ciudad_Estudio,
                        Nacionalidad = Nacionalidad,
                        Biografia = Biografia,
                        Sexo = Sexo,
                        Fecha_Nac = Fecha_Nac,
                        Residencia = Residencia,
                        Annio_Nac = Annio_Nac,
            };
        }

        public void EditEquipo(ref AppEquipo _equipo)
        {
            if (_equipo != null)
            {
                _equipo.Nombre = Nombre;
                _equipo.Email = Email;
                _equipo.Telefono = Telefono;
                _equipo.Local_Cel = Local_Cel;
                _equipo.Ocupacion = Ocupacion;
                _equipo.Estudiante = Estudiante;
                _equipo.Profecional = Profecional;
                _equipo.Ciudad_Estudio = Ciudad_Estudio;
                _equipo.Nacionalidad = Nacionalidad;
                _equipo.Biografia = Biografia;
                _equipo.Sexo = Sexo;
                _equipo.Fecha_Nac = Fecha_Nac;
                _equipo.Residencia = Residencia;
                _equipo.Annio_Nac = Annio_Nac;
                _equipo.Nacionalidad_Id = Nacionalidad_Id;
            }
        }

        public void EditEquipo(ref AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo)
                    .ToList().ForEach(e => EditEquipo(ref e));
            }
        }
    }

    public class ProductorViewModel
    {
        public ProductorViewModel()
        {
        }

        public ProductorViewModel(AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                var _equipo = _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo).FirstOrDefault();
                if (_equipo != null)
                {
                    Id = _equipo.Id;
                    IdShortFilm = _ShortFilm.Id;
                    Nombre = _equipo.Nombre;
                    Email = _equipo.Email;
                    Telefono = _equipo.Telefono;
                    Local_Cel = _equipo.Local_Cel;
                    Ocupacion = _equipo.Ocupacion;
                    Estudiante = _equipo.Estudiante;
                    Profecional = _equipo.Profecional;
                    Ciudad_Estudio = _equipo.Ciudad_Estudio;
                    Nacionalidad = _equipo.Nacionalidad;
                    Biografia = _equipo.Biografia;
                    Sexo = _equipo.Sexo;
                    Fecha_Nac = _equipo.Fecha_Nac;
                    Residencia = _equipo.Residencia;
                    Annio_Nac = _equipo.Annio_Nac;
                    Nacionalidad_Id = _equipo.Nacionalidad_Id;
                }
            }
        }

        #region

        public int Id { get; set; }

        [Required]
        public int IdShortFilm { get; set; }

        [Required]
        [Display(Name = "PRODUCTOR | PRODUCER")]
        public int IdTipoEquipo { get { return 2; } }

        [Required]
        [Display(Name = "Nombre | Name :")]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "E-mail | E-mail:")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Teléfono (con lada) | Phone number (with dial codes):")]
        public string Telefono { get; set; }

        [Required]
        [Display(Name = "Es Celular | Is Cell phone?:")]
        public bool Local_Cel { get; set; }

        [Required]
        [Display(Name = "Ocupación | Occupation:")]
        public string Ocupacion { get; set; }

        [Required]
        [Display(Name = "Estudiante | Student:")]
        public bool Estudiante { get; set; }

        [Required]
        [Display(Name = "Profesional | Professional:")]
        public bool Profecional { get; set; }

        [Required]
        [Display(Name = "Nombre y ciudad de estudios | Name and city of education:")]
        public string Ciudad_Estudio { get; set; }

        //[Required]
        [Display(Name = "Nacionalidad | Nationality:")]
        public string Nacionalidad { get; set; }

        [Required]
        [Display(Name = "Biografía | Biography")]
        public string Biografia { get; set; }

        //[Required]
        [Display(Name = "Fecha Nacimiento | Birth date:")]
        public string Fecha_Nac { get; set; }

        [Required]
        [Display(Name = "Sexo | Gender:")]
        public string Sexo { get; set; }

        //[Required]
        [Display(Name = "Residencia | Residence:")]
        public string Residencia { get; set; }

        [Required]
        [Display(Name = "Año de Nacimiento | Year of Birth:")]
        public int Annio_Nac { get; set; }

        [Required]
        [Display(Name = "Nacionalidad | Nationality")]
        public int? Nacionalidad_Id { get; set; }

        public List<SelectListItem> List_Annio_Nac
        {
            get
            {
                return SelectTool.GetListAnnioNacimiento();
            }
        }
        #endregion

        public AppEquipo CreateEquipo(AppShortFilm _ShortFilm)
        {
            return new AppEquipo()
                    {
                        IdTipoEquipo = IdTipoEquipo,
                        AppShortFilm = _ShortFilm,
                        Nombre = Nombre,
                        Email = Email,
                        Telefono = Telefono,
                        Local_Cel = Local_Cel,
                        Ocupacion = Ocupacion,
                        Estudiante = Estudiante,
                        Profecional = Profecional,
                        Ciudad_Estudio = Ciudad_Estudio,
                        Nacionalidad = Nacionalidad,
                        Biografia = Biografia,
                        Sexo = Sexo,
                        Fecha_Nac = Fecha_Nac,
                        Residencia = Residencia,
                        Annio_Nac = Annio_Nac,
            };
        }

        public void EditEquipo(ref AppEquipo _equipo)
        {
            if (_equipo != null)
            {
                _equipo.Nombre = Nombre;
                _equipo.Email = Email;
                _equipo.Telefono = Telefono;
                _equipo.Local_Cel = Local_Cel;
                _equipo.Ocupacion = Ocupacion;
                _equipo.Estudiante = Estudiante;
                _equipo.Profecional = Profecional;
                _equipo.Ciudad_Estudio = Ciudad_Estudio;
                _equipo.Nacionalidad = Nacionalidad;
                _equipo.Biografia = Biografia;
                _equipo.Sexo = Sexo;
                _equipo.Fecha_Nac = Fecha_Nac;
                _equipo.Residencia = Residencia;
                _equipo.Annio_Nac = Annio_Nac;
                _equipo.Nacionalidad_Id = Nacionalidad_Id;
            }
        }

        public void EditEquipo(ref AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo)
                    .ToList().ForEach(e => EditEquipo(ref e));
            }
        }
    }

    public class OtroViewModel
    {
        public OtroViewModel()
        {
        }

        public OtroViewModel(AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                var _equipo = _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo).FirstOrDefault();
                if (_equipo != null)
                {
                    Id = _equipo.Id;
                    IdShortFilm = _ShortFilm.Id;
                    Nombre = _equipo.Nombre;
                    Email = _equipo.Email;
                    Telefono = _equipo.Telefono;
                    Local_Cel = _equipo.Local_Cel;
                    Estudios = _equipo.Estudios;
                }
            }
        }

        #region

        public int Id { get; set; }

        [Required]
        public int IdShortFilm { get; set; }

        [Required]
        [Display(Name = "Nombre | Name:")]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "Estudios | Education:")]
        public string Estudios { get; set; }

        [Required]
        [Display(Name = "E-mail | E-mail:")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Teléfono (con lada) | Phone number (with dial codes):")]
        public string Telefono { get; set; }

        [Required]
        [Display(Name = "Es Celular | Is Cell phone?:")]
        public bool Local_Cel { get; set; }

        #endregion

        public virtual AppEquipo CreateEquipo(AppShortFilm _ShortFilm)
        {
            return new AppEquipo()
            {
                AppShortFilm = _ShortFilm,
                Nombre = Nombre,
                Estudios = Estudios,
                Email = Email,
                Telefono = Telefono,
                Local_Cel = Local_Cel,
            };
        }

        public void EditEquipo(ref AppEquipo _Equipo)
        {
            if (_Equipo != null)
            {
                _Equipo.Nombre = Nombre;
                _Equipo.Estudios = Estudios;
                _Equipo.Email = Email;
                _Equipo.Telefono = Telefono;
                _Equipo.Local_Cel = Local_Cel;
            }
        }

        public void EditEquipo(ref AppShortFilm _ShortFilm)
        {
            if (_ShortFilm != null)
            {
                _ShortFilm.AppEquipo.Where(e => e.IdTipoEquipo == IdTipoEquipo)
                    .ToList().ForEach(e => EditEquipo(ref e));
            }
        }

        public virtual int IdTipoEquipo { get { return 0; } }
    }

    #endregion

    #region Año de Nacimiento
    public static class SelectTool
    {
        public static List<SelectListItem> GetListAnnioNacimiento()
        {
            var _result = new List<SelectListItem>();
            var _year_top = DateTime.Now.Year - AnnioNacMayoresDe;
            var _year_bottom = _year_top - AnnioNacMenorDe;
            var _list = Enumerable.Range(_year_bottom, AnnioNacMenorDe + 1).ToList();
            _list.ForEach(e => _result.Add(new SelectListItem { Text = e.ToString(), Value = e.ToString() }));
            return _result;
        }

        //public static List<SelectListItem> GetListNacionalidad()
        //{
        //    var _result = new List<SelectListItem>();
        //    var _year_top = DateTime.Now.Year - AnnioNacMayoresDe;
        //    var _year_bottom = _year_top - AnnioNacMenorDe;
        //    var _list = Enumerable.Range(_year_bottom, AnnioNacMenorDe + 1).ToList();
        //    _list.ForEach(e => _result.Add(new SelectListItem { Text = e.ToString(), Value = e.ToString() }));
        //    return _result;
        //}

        #region Año de Nacimiento
        public static int AnnioNacMayoresDe
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["AnnioNacMayoresDe"]);
            }
        }

        public static int AnnioNacMenorDe
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["AnnioNacMenorDe"]);
            }
        }
        #endregion

    }

    #endregion

    #region Estado
    public class EstadoViewModel
    {
        public int Id { get; set; }

        public string Estado { get; set; }
    }
    #endregion

    #region Estado
    public class MunicipioViewModel
    {
        public int Id { get; set; }

        public string Municipio { get; set; }
    }
    #endregion

    #region Upload
    public class FileUploadViewModel
    {
        public int IdShortFilm { get; set; }

        public int Id { get; set; }

        public string title { get; set; }

        public HttpPostedFileBase File { get; set; }

        public string PathFile { get; set; }

        public string Url { get; set; }
    }

    public class FileUploadConfigViewModel
    {
        public string Id { get; set; }

        public string title { get; set; }

        public HttpPostedFileBase File { get; set; }

        public string PathFile { get; set; }

        public string Url { get; set; }
    }
    #endregion

    #region AdminFilmsBusssines

    public class AdminFilmsFilterConfViewModel 
    {
        public int CategoriaId { get; set; }
        public int NacionalidadId { get; set; }
        public int PaisId { get; set; }
        public int Etapa { get; set; }
        public int Edicion { get; set; }
        public int Envio { get; set; }
        public AdminFilmsFilterConfEstatusViewModel Filter { get; set; }
    }

    public class AdminFilmsFilterConfEstatusViewModel
    {
        public bool Activo { get; set; }
        public bool Preseleccion { get; set; }
        public bool Sel_Oficial { get; set; }
        public bool Ganador { get; set; }
    }

    public class AdminFilmsResultViewModel
    {

        public int CountListShortFilm { get; set; }

        public int CountListShortFilmResult { get; set; }

        public AdminFilmsFilterViewModel Filter { get; set; }

        public int CountListShortFilmEdicion { get; set; }
    }

    public class AdminFilmsResultSimpleViewModel : AdminFilmsResultViewModel
    {
        public List<AppShortFilm> ListShortFilmsResutl { get; set; }
    }

    public class AdminFilmsResultJuradoViewModel : AdminFilmsResultViewModel
    {
        public List<AppShortFilmsResultViewModel> ListShortFilmsResutl { get; set; }
    }

    public class AdminFilmsFilterViewModel
    {
        [Display(Name = "Activo")]
        public bool Activo { get; set; }

        [Display(Name = "Etapa")]
        public int Etapa { get; set; }

        //[Display(Name = "Envio Correo")]
        //public bool Envio_Correo_Video { get; set; }
        
        [Display(Name = "Preselección")]
        public bool Preseleccion { get; set; }

         [Display(Name = "Selección Oficial")]
        public bool Sel_Oficial { get; set; }
        
        [Display(Name = "Ganador")]
        public bool Ganador { get; set; }

        [Display(Name = "Edicion")]
        public int Edicion { get; set; }

        [Display(Name="Categoría")]
        public int CategoriaId { get; set; }

        [Display(Name = "País")]
        public int PaisId { get; set; }
        [Display(Name = "Envio por Correo")]
        public int Envio { get; set; }
        [Display(Name = "Nacionalidad")]
        public int NacionalidadId { get; set; }

    }


    //public class AdminFilmsFilterResultViewModel : AdminFilmsFilterViewModel
    //{ 
    //    public List<AppShortFilmsResultViewModel> ListResult {get; set;}
    //}
    public class AdminFilmsOperationViewModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int Operations { get; set; }
        [Required]
        public bool ActiveNotification { get; set; }
        [Required]
        public int EtapaEdit { get; set; }
    }


    #endregion

    #region AppViewFilms
    public class AppViewFilmsViewModel
    {
        public AppViewFilmsViewModel(AppViewFilms _AppViewFilms)
        {
            Id = _AppViewFilms.Id;
            Descripcion_ES = _AppViewFilms.Descripcion_ES;
            Descripcion_EN = _AppViewFilms.Descripcion_EN;
            // ---------------------------
            ListShortFilmsResult = new List<AppShortFilm>();
            _AppViewFilms.AppShortFilm.ToList().ForEach(e => ListShortFilmsResult.Add(e));
            // ---------------------------
        }

        public int Id { get; set; }
        public string Descripcion_ES { get; set; }
        public string Descripcion_EN { get; set; }
        public List<AppShortFilm> ListShortFilmsResult { get; set; }
    }
    #endregion
}
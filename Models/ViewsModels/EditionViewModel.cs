﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO._2015.Models.ViewsModels
{
    public class EditionViewModel
    {
        public string edicion { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string logotipo { get; set; }
        public static implicit operator EditionViewModel(App.Edicion _entity)
        {
            return new EditionViewModel {
                id = _entity.PK_IdEdicion,
                title = _entity.Tema_ES,
                edicion = _entity.Nombre_ES,
                logotipo = _entity.Logotipo_ES,
            };
        }

    }
}
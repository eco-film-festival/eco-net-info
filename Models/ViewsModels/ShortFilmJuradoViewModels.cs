﻿using ECO._2015.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ECO._2015.Models
{
    

    #region Calculo Calficacion
    
    public class TipoJuradoResult
    {
        public string Descripcion { get; set; }

        public string Nacionalidad { get; set; }

        public ResultCalificarTotalByTipoJurado ResultCalificarTotalByTipoJurado { get; set; }
        
        public int Id { get; set; }
        public int Ponderado { get; set; }
    }

    public class ResultCriterioCalificarByJurado
    {
        public AppJurado Jurado { get; set; }

        public decimal Calificacion { get; set; }

        public ICollection<AppCalificacion> ListCalificacion { get; set; }

        public bool Califico {
            get { 
                return (ListCalificacion != null)
                    ? (ListCalificacion.Count > 0)
                    : false
                    ;
            }
        }
    }
    public class ResultCalificarTotalByTipoJurado
    {
        [Display(Name = "Calificación | Ratings")]
        [RegularExpression(@"^\d+.\d{0,3}$")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Total
        {
            get
            {
                int _count = ListResultCriterioCalificarByJurado.Count();

                decimal _sum = ListResultCriterioCalificarByJurado.Sum(c => c.Calificacion);

                return (_count > 0) ? _sum / _count : 0;
            }
        }

        [Display(Name = "Jurado(s) | Jury")]
        public int NoJurado {
            get {
                return (ListResultCriterioCalificarByJurado != null)
                ? ListResultCriterioCalificarByJurado.Count()
                : 0;
            }
        }

        [Display(Name = "Jurado(s) Restante(s) | Remaining  Jury")]
        public int NoJuradoFaltanCalificar {
            get {
                return ListResultCriterioCalificarByJurado.Where(c => c.Califico == false).Count();
            }
        }

        public IEnumerable<ResultCriterioCalificarByJurado> ListResultCriterioCalificarByJurado { get; set; }
    }
    public class ResultCalificarTotalView 
    {
        private List<TipoJuradoResult> _ListTipoJuradoResult = new List<TipoJuradoResult>();

        public void Add(TipoJuradoResult _TipoJuradoResult)
        {
            _ListTipoJuradoResult.Add(_TipoJuradoResult);
        }
        
        public List<TipoJuradoResult> ListTipoJuradoResult {
            get {
                return _ListTipoJuradoResult;
            }
        }

        [Display(Name = "Calificación | Ratings")]
        [RegularExpression(@"^\d+.\d{0,3}$")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public decimal Total
        {
            get
            {
             int _count = _ListTipoJuradoResult.Count;

                //foreach (var _TipoJuradoResult in _ListTipoJuradoResult)
                //{
                //    Debug.WriteLine("------------------------");
                //    Debug.WriteLine("------------------------");
                //    Debug.WriteLine(string.Format("Nacionalidad - {0}", _TipoJuradoResult.Nacionalidad));
                //    Debug.WriteLine(string.Format("Ponderado - {0}", _TipoJuradoResult.Ponderado));
                //    Debug.WriteLine(string.Format("Descripcion - {0}", _TipoJuradoResult.Descripcion));
                //    Debug.WriteLine("------------------------");

                //    Debug.WriteLine(string.Format("_TipoJuradoResult.ResultCalificarTotalByTipoJurado.Total - {0}", _TipoJuradoResult.ResultCalificarTotalByTipoJurado.Total));

                //    Debug.WriteLine(
                //        string.Format(
                //            "_TipoJuradoResult.ResultCalificarTotalByTipoJurado.Total - {0} | TipoJuradoResult.Ponderado /100.0 {1} | {2}"
                //            , _TipoJuradoResult.ResultCalificarTotalByTipoJurado.Total 
                //            , (decimal)(_TipoJuradoResult.Ponderado / 100.0)
                //            , (_TipoJuradoResult.ResultCalificarTotalByTipoJurado.Total) * ((decimal)(_TipoJuradoResult.Ponderado / 100.0)
                //            )
                //            )
                //    );

                //    foreach (var _ResultCriterioCalificarByJurado in _TipoJuradoResult.ResultCalificarTotalByTipoJurado.ListResultCriterioCalificarByJurado)
                //    {
                //        Debug.WriteLine(
                //            string.Format(
                //            "_ResultCriterioCalificarByJurado.Jurado.Id - {0} | _ResultCriterioCalificarByJurado.Calificacion - {1}"
                //            , _ResultCriterioCalificarByJurado.Jurado.Id
                //            , _ResultCriterioCalificarByJurado.Calificacion
                //            )
                //        );
                //    }
                //}


                decimal _sum = (_count > 0)
                    ? _ListTipoJuradoResult.Sum(c =>
                        ( c.ResultCalificarTotalByTipoJurado.Total > 0 )
                            ? c.ResultCalificarTotalByTipoJurado.Total * ((decimal)(c.Ponderado / 100.0))
                            : 0
                            )
                    : 0
                    ;

                return _sum;
            }
        }

        [Display(Name = "Jurado(s) | Jury")]
        public int NoJurado
        {
            get
            {
                return _ListTipoJuradoResult.Sum(c => c.ResultCalificarTotalByTipoJurado.NoJurado);
            }
        }

        [Display(Name = "Jurado(s) Restante(s) | Remaining  Jury")]
        public int NoJuradoFaltanCalificar
        {
            get
            {
                return _ListTipoJuradoResult.Sum(c => c.ResultCalificarTotalByTipoJurado.NoJuradoFaltanCalificar);
            }
        }

        
    }

    #endregion  
}
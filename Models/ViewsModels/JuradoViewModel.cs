﻿using ECO._2015.Models.App;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ECO._2015.Models
{
    #region Jurado
    public class JuradoViewModel
    {

        public JuradoViewModel()
        {

        }

        public JuradoViewModel(AppJurado Jurado)
        {
            id = Jurado.Id;
            TipoJurado_Id = Jurado.AppTipoJurado.Id;
            JuradoNacional = (Jurado.AppTipoJurado.AppNacionalidad.Nacionalidad_ES == "Nacional");
            Nacionalidad_ES = Jurado.AppTipoJurado.AppNacionalidad.Nacionalidad_ES;
            Nacionalidad_EN = Jurado.AppTipoJurado.AppNacionalidad.Nacionalidad_EN;
            Nombre = Jurado.Descripcion;
            Tipo = Jurado.AppTipoJurado.Descripcion;
            Image = Jurado.Image;
        }


        #region Jurado
        public int id { get; private set; }
        [Required]
        [Display(Name = "Tipo Jurado")]
        public int TipoJurado_Id { get; set; }
        [Required]
        [Display(Name = "Jurado Nacional")]
        public bool JuradoNacional { get; private set; }
        public string Nacionalidad_ES { get; private set; }
        public string Nacionalidad_EN { get; private set; }
        [Required]
        [Display(Name = "Nombre")]
        public string Nombre { get; private set; }
        public string Tipo { get; private set; }
        public string Image { get; private set; }
        #endregion
    }

    public class JuradoListShortFilmViewModel : JuradoViewModel
    {
        public JuradoListShortFilmViewModel(AppJurado Jurado, int EdicionFestival)
            : base(Jurado)
        {
            ListShortFilmJurado = new List<ShortFilmJuradoViewModel>();
            Encuesta = new EncuestaViewModelAdmin(Jurado, EdicionFestival);
        }
        public List<ShortFilmJuradoViewModel> ListShortFilmJurado { get; set; }

        #region Contadores
        public int Total
        {
            get
            {
                return ListShortFilmJurado.Count;
            }
        }

        private int? _Calificados = null;
        public int Calificados
        {
            get
            {

                if (_Calificados == null)
                {
                    _Calificados = ListShortFilmJurado.Where(s => s.IsRated).ToList().Count;
                }

                return (int)_Calificados;
            }
        }

        public int Pendientes
        {
            get
            {
                return Total - Calificados;
            }
        }

        public bool Termino
        {
            get
            {
                return (Total == Calificados);
            }
        }
        #endregion

        #region Encuesta
        public EncuestaViewModelAdmin Encuesta { get; private set; }
        #endregion
    }

    public class ShortFilmJuradoViewModel
    {
        public ShortFilmJuradoViewModel(AppShortFilm ShortFilm, AppJurado Jurado)
        {
            var _listCalificacion = ShortFilm.AppCalificacion
                .Where(c => c.Jurado_Id == Jurado.Id)
                .ToList();

            IsRated = (_listCalificacion.Count > 0);
            // --------------------------------------
            Calificacion = (_listCalificacion.Count > 0)
                ? _listCalificacion.Sum(c => c.Valor) / _listCalificacion.Count
                : 0;
            // --------------------------------------
            Id = ShortFilm.Id;
            Titulo_Original = ShortFilm.Titulo_Original;
            Categoria_ES = ShortFilm.AppCategoria.Categoria_ES;
            Categoria_EN = ShortFilm.AppCategoria.Categoria_EN;
            Pais_ES = ShortFilm.AppEstado.AppPais.Pais_ES;
            Pais_EN = ShortFilm.AppEstado.AppPais.Pais_EN;
            Subtitulo_ES = ShortFilm.AppSubtitulo.Subtitulo_ES;
            Subtitulo_EN = ShortFilm.AppSubtitulo.Subtitulo_EN;
            Idioma = ShortFilm.Idioma;
            Tiempo = string.Format("{0} : {1}", ShortFilm.Duracion_Minutos, ShortFilm.Duracion_Segundos);
            // --------------------------------------
        }

        #region Contadores
        public bool IsRated { get; set; }
        public decimal Calificacion { get; set; }
        public decimal CalificacionRound
        {
            get
            {
                return decimal.Round(Calificacion, 2);
            }
        }
        #endregion

        #region ShortFilm
        public int Id { get; private set; }
        public string Titulo_Original { get; private set; }
        public string Categoria_ES { get; private set; }
        public string Categoria_EN { get; private set; }
        public string Pais_ES { get; private set; }
        public string Pais_EN { get; private set; }
        public string Subtitulo_ES { get; private set; }
        public string Subtitulo_EN { get; private set; }
        public string Idioma { get; private set; }
        public string Tiempo { get; private set; }
        #endregion
    }

    public class JuradoDetailViewModel : JuradoViewModel
    {
        public JuradoDetailViewModel(AppShortFilm _ShortFilm, AppJurado Jurado, int Edicion)
            : base(Jurado)
        {
            ShortFilm = _ShortFilm;

            var _listCalificacion = ShortFilm.AppCalificacion
                .Where(c => c.Jurado_Id == Jurado.Id)
                .ToList();


            //var _list = _ShortFilm.AppCategoria.AppCriterioCalificar
            //    // .Where(c => c.Edicion == Edicion)
            //    .Where(c => c.Categoria_Id == ShortFilm.CategoriaId)
            //    //.Where(c => c.AppTipoJurado.Nacionalidad_Id == _ShortFilm.AppEstado.AppPais.Nacionalidad_Id)
            //    .Where(c => c.AppTipoJurado.Id == Jurado.AppTipoJurado.Id)
            //    .Where(c => c.AppCriterio.Edicion == Edicion)
            //    .ToList();



            _ListCriterioCalificar =
                _ShortFilm.AppCategoria.AppCriterioCalificar
               // .Where(c => c.Edicion == Edicion)
                .Where(c => c.Categoria_Id == ShortFilm.CategoriaId)
                .Where(c => c.AppTipoJurado.Nacionalidad_Id == _ShortFilm.AppEstado.AppPais.Nacionalidad_Id)
                .Where(c => c.AppTipoJurado.Id == Jurado.AppTipoJurado.Id)
                .Where(c => c.AppCriterio.Edicion == Edicion)
                .GroupJoin(
                    _listCalificacion,
                    cr => cr.Id,
                    cl => cl.CriterioCalificar_Id,
                    (cr, cl) => new { CriterioCalificar = cr, Calificacion = cl }
                ).SelectMany(
                    crcl => crcl.Calificacion.DefaultIfEmpty(),
                    (cr, cl) => new { cr = cr, cl = cl }
                ).Select(
                    r => new CriterioCalificarViewModel()
                    {
                        Descripcion = r.cr.CriterioCalificar.AppCriterio.Descripcion,
                        DescripcionCompleta = r.cr.CriterioCalificar.AppCriterio.DescripcionCompleta,
                        Explicacion = r.cr.CriterioCalificar.AppCriterio.Explicacion,
                        DescripcionCompleta_EN = r.cr.CriterioCalificar.AppCriterio.DescripcionCompleta_EN,
                        Explicacion_EN = r.cr.CriterioCalificar.AppCriterio.Explicacion_EN,
                        Id = r.cr.CriterioCalificar.Id,
                        Value = (r.cl != null) ? r.cl.Valor : 0
                    }
                )
                .ToList()
                ;
        }

        public AppShortFilm ShortFilm { get; set; }

        private List<CriterioCalificarViewModel> _ListCriterioCalificar = new List<CriterioCalificarViewModel>();

        public List<CriterioCalificarViewModel> ListCriterioCalificarByJurado
        {
            get
            {
                return _ListCriterioCalificar;
            }
        }
    }
    #endregion

    #region Criterios a Calificar

    public class CriterioCalificarViewModel
    {        
        public string Descripcion { get; set; }
        public int Id { get; set; }
        public int Value { get; set; }

        public string DescripcionCompleta { get; set; }

        public string Explicacion { get; set; }
        public string DescripcionCompleta_EN { get; set; }

        public string Explicacion_EN { get; set; }

    }
    public class CriterioCalificarValueViewModel
    {
        public int Id { get; set; }
        public int Valor { get; set; }
    }


    #endregion

    #region JuradoAdmin

    public class JuradoViewModelAdmin : JuradoViewModel
    {
        public JuradoViewModelAdmin()
        {

        }
        public JuradoViewModelAdmin(AppJurado Jurado)
            : base(Jurado)
        {
            Email = Jurado.AspNetUsers.UserName;
            Activo = Jurado.Jurado_Activo;
        }

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        public bool Activo { get; set; }
    }

    public class TipoJuradoViewModel
    {
        public TipoJuradoViewModel()
        { 
        
        }
        public TipoJuradoViewModel(AppTipoJurado TipoJurado)
        {
            Id = TipoJurado.Id;
            Descripcion = TipoJurado.Descripcion;
            Nacionalidad_Id = TipoJurado.Nacionalidad_Id;
            Nacionalidad_ES = TipoJurado.AppNacionalidad.Nacionalidad_ES;
            Nacionalidad_EN = TipoJurado.AppNacionalidad.Nacionalidad_EN;
        }
        public int Id { get; set; }
        [Required]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }
        [Required]
        [Display(Name = "Nacionalidad")]
        public int Nacionalidad_Id { get; set; }
        public string Nacionalidad_ES { get; set; }
        public string Nacionalidad_EN { get; set; }
    }

    public class CriterioViewModel
    {
        public CriterioViewModel()
        { 
        
        }
        public CriterioViewModel(AppCriterio Criterio)
        {
            Id = Criterio.Id;
            Descripcion = Criterio.Descripcion;
            DescripcionCompleta = Criterio.DescripcionCompleta;
            Explicacion = Criterio.Explicacion;
            DescripcionCompleta_EN = Criterio.DescripcionCompleta_EN;
            Explicacion_EN = Criterio.Explicacion_EN;
        }

        public int Id { get; set; }
        [Required]
        public string Descripcion { get; set; }
        [Required]
        public string DescripcionCompleta { get; set; }
        [Required]
        public string Explicacion { get; set; }
        [Required]
        public string DescripcionCompleta_EN { get; set; }
        [Required]
        public string Explicacion_EN { get; set; }
    }

    public class QualifyViewModel
    {
        public QualifyViewModel()
        { 
        
        }

        public QualifyViewModel(AppCriterioCalificar c)
        {
            Id = c.Id;
            TipoJurado_Id = c.TipoJurado_Id;
            Criterio_Id = c.Criterio_Id;
            Categoria_Id = c.Categoria_Id;
            //---------------------------
            DescripcionTipoJurado = c.AppTipoJurado.Descripcion;
            DescripcionCriterio = c.AppCriterio.Descripcion;
            DescripcionCategoria = c.AppCategoria.Categoria_ES;
            //---------------------------
        }

        public int Id { get; set; }
        public int TipoJurado_Id { get; set; }
        public int Criterio_Id { get; set; }
        public int Categoria_Id { get; set; }
        public string DescripcionTipoJurado { get; set; }
        public string DescripcionCriterio{ get; set; }
        public string DescripcionCategoria { get; set; }
    }


    #endregion

    #region Encuesta 

    public class RespuestaValueViewModel
    {
        public int Id { get; set; }
        public string Valor { get; set; }
    }

    public class EncuestaViewModelAdmin : JuradoViewModel
    {
        public EncuestaViewModelAdmin(AppJurado Jurado, int EdicionFestival)
            :base(Jurado)
        {
            ListRespueta =
                    Jurado.AppTipoJurado.AppPreguntaJurado
                    .Where(e => e.Edicion == EdicionFestival)
                    .GroupJoin(
                        Jurado.AppRespuestaJurado,
                        tlp => tlp.Id,
                        lrj => lrj.AppPreguntaJurado.Id,
                        (tlp, lrj) => new { PreguntaJurado = tlp, RespuestaJurado = lrj.Select(s => s) }
                    )
                    .SelectMany(
                        s => s.RespuestaJurado.DefaultIfEmpty(),
                        (tlp, lrj) => new { PreguntaJurado = tlp, RespuestaJurado = lrj }
                    ).Select(
                        s => new RespuestaJuradoView
                        {
                            PreguntaJurado = s.PreguntaJurado.PreguntaJurado,
                            RespuestaJurado = (s.RespuestaJurado != null)
                                ? s.RespuestaJurado
                                : new AppRespuestaJurado
                                {
                                    AppJurado = Jurado,
                                    Jurado_Id = Jurado.Id,
                                    AppPreguntaJurado = s.PreguntaJurado.PreguntaJurado,
                                    PreguntaJurado_Id = s.PreguntaJurado.PreguntaJurado.Id
                                }
                        }
                    )
                    ;
        }


        public IEnumerable<RespuestaJuradoView> ListRespueta { get; set; }
    }

    public class RespuestaJuradoView
    {
        public AppPreguntaJurado PreguntaJurado { get; set; }
        public AppRespuestaJurado RespuestaJurado { get; set; }
    }
    
    #endregion

    #region Calificaciones

    public class AppShortFilmsResultViewModel 
    {
        public AppShortFilmsResultViewModel(AppShortFilm _AppShortFilm)
        {
            AppShortFilm = _AppShortFilm;

        }

        public AppShortFilm AppShortFilm { get; private set; }

        public List<AppTipoJurado> ListTipoJurado
        {
            get
            {
                return AppShortFilm.AppCategoria.AppCriterioCalificar
                .Where(c => c.Categoria_Id == AppShortFilm.CategoriaId)
                .Where(c => c.AppTipoJurado.Nacionalidad_Id == AppShortFilm.AppEstado.AppPais.Nacionalidad_Id)
                //.Where(c => c.AppTipoJurado.)
                .GroupBy(c => c.AppTipoJurado)
                .Select(g => g.Key)
                //.Where(e=> e.)
                .ToList<AppTipoJurado>()
                ;
            }
        }

        public ResultCalificarTotalView ResultCalificar
        {
            get
            {

                ResultCalificarTotalView _result = new ResultCalificarTotalView();

                ListTipoJurado.ForEach(
                     t => _result.Add(
                            new TipoJuradoResult()
                            {
                                Id = t.Id,
                                Ponderado = t.Ponderado,
                                Descripcion = t.Descripcion,
                                Nacionalidad = t.AppNacionalidad.Nacionalidad_ES,
                                ResultCalificarTotalByTipoJurado = getResultCalificarTotal(t)
                            }
                         )
                    );
                return _result;
            }
        }

        private ResultCalificarTotalByTipoJurado getResultCalificarTotal(AppTipoJurado _TipoJurado)
        {
            ResultCalificarTotalByTipoJurado _result = new ResultCalificarTotalByTipoJurado()
            {
                ListResultCriterioCalificarByJurado =
                _TipoJurado.AppJurado
                    .Where(c => c.Jurado_Activo == true)
                    .Where(c => c.Edicion == EdicionFestival) // zowar !!! 
                    .Select(c => new ResultCriterioCalificarByJurado()
                    {
                        Jurado = c,
                        ListCalificacion = ListCalificacionByJurado(c),
                        Calificacion = CalificacionByJurado(c)
                    })
            };

            return _result;
        }

        private int EdicionFestival
        {
            get
            {
                try
                {
                    return int.Parse(System.Configuration.ConfigurationManager.AppSettings["EdicionFestival"]);
                }
                catch
                {

                }

                return 2015;
            }
        }

        public ICollection<AppCalificacion> ListCalificacionByJurado(AppJurado Jurado)
        {
            return AppShortFilm.AppCalificacion
                .Where(c => c.Jurado_Id == Jurado.Id).ToList();
        }

        public decimal CalificacionByJurado(AppJurado Jurado)
        {
            int _count = ListCalificacionByJuradoCount(Jurado);

            decimal _sum = ListCalificacionByJurado(Jurado).Sum(c => c.Valor);

            return (_count > 0) ? _sum / _count : 0;

        }

        public int ListCalificacionByJuradoCount(AppJurado Jurado)
        {
            return ListCalificacionByJurado(Jurado).Count;
        }

        public List<AppJurado> GetJuradoByTipoJurado(AppTipoJurado _TipoJurado)
        {
            return _TipoJurado.AppJurado.Where(e => e.Edicion == EdicionFestival).Where(e => e.Jurado_Activo).ToList();
        }
    }
    #endregion
}
namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppTipoJurado")]
    public partial class AppTipoJurado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppTipoJurado()
        {
            AppCriterioCalificar = new HashSet<AppCriterioCalificar>();
            AppJurado = new HashSet<AppJurado>();
            AppPreguntaJurado = new HashSet<AppPreguntaJurado>();
        }

        public int Id { get; set; }

        public string Descripcion { get; set; }

        public int Nacionalidad_Id { get; set; }

        public int Ponderado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppCriterioCalificar> AppCriterioCalificar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppJurado> AppJurado { get; set; }

        public virtual AppNacionalidad AppNacionalidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppPreguntaJurado> AppPreguntaJurado { get; set; }
    }
}

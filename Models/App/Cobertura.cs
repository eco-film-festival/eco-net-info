namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Cobertura")]
    public partial class Cobertura
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cobertura()
        {
            Medio = new HashSet<Medio>();
        }

        [Key]
        public int PK_IdCobertura { get; set; }

        public int FK_IdTipoMedio__SCH { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion { get; set; }

        public virtual TipoMedio TipoMedio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medio> Medio { get; set; }
    }
}

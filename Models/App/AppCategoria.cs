namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppCategoria")]
    public partial class AppCategoria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppCategoria()
        {
            AppCriterioCalificar = new HashSet<AppCriterioCalificar>();
            AppShortFilm = new HashSet<AppShortFilm>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Categoria_ES { get; set; }

        [Required]
        [StringLength(50)]
        public string Categoria_EN { get; set; }

        [Required]
        [StringLength(50)]
        public string Icon { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppCriterioCalificar> AppCriterioCalificar { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AppPais
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppPais()
        {
            AppEstado = new HashSet<AppEstado>();
            Medio = new HashSet<Medio>();
            AppNacionalidad1 = new HashSet<AppNacionalidad>();
            AppEquipo = new HashSet<AppEquipo>();
        }

        public int Id { get; set; }

        [Required]
        public string Pais_ES { get; set; }

        [Required]
        public string Pais_EN { get; set; }

        public int Nacionalidad_Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppEstado> AppEstado { get; set; }

        public virtual AppNacionalidad AppNacionalidad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medio> Medio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppNacionalidad> AppNacionalidad1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppEquipo> AppEquipo { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppPreguntaJurado")]
    public partial class AppPreguntaJurado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppPreguntaJurado()
        {
            AppRespuestaJurado = new HashSet<AppRespuestaJurado>();
            AppTipoJurado = new HashSet<AppTipoJurado>();
        }

        public int Id { get; set; }

        public string Pregunta_ES { get; set; }

        public string Pregunta_EN { get; set; }

        public int Edicion { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppRespuestaJurado> AppRespuestaJurado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppTipoJurado> AppTipoJurado { get; set; }
    }
}

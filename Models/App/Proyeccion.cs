namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Proyeccion")]
    public partial class Proyeccion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Proyeccion()
        {
            Muestra = new HashSet<Muestra>();
            AppShortFilm = new HashSet<AppShortFilm>();
        }

        [Key]
        public int PK_IdProyeccion { get; set; }

        public int FK_IdPrograma__SCH { get; set; }

        public TimeSpan? start { get; set; }

        public TimeSpan? end { get; set; }

        public bool allDay { get; set; }

        [StringLength(50)]
        public string url { get; set; }

        public int FK_IdTipoEvento__SCH { get; set; }

        public string Lugar_ES { get; set; }

        public string Lugar_EN { get; set; }

        public string Descripcion_EN { get; set; }

        public string Descripcion_ES { get; set; }

        public string Proyeccion_EN { get; set; }

        public string Proyeccion_ES { get; set; }

        public string Image { get; set; }

        public string ImageWeb { get; set; }

        public virtual Programa Programa { get; set; }

        public virtual TipoEvento TipoEvento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Muestra> Muestra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.TipoEvento")]
    public partial class TipoEvento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TipoEvento()
        {
            Evento = new HashSet<Evento>();
            Proyeccion = new HashSet<Proyeccion>();
        }

        [Key]
        public int PK_IdTipoEvento { get; set; }

        [Required]
        [StringLength(50)]
        public string Tipo_ES { get; set; }

        [Required]
        [StringLength(50)]
        public string Tipo_EN { get; set; }

        [StringLength(50)]
        public string icon { get; set; }

        [StringLength(50)]
        public string className { get; set; }

        public string DetailUrl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Evento> Evento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proyeccion> Proyeccion { get; set; }
    }
}

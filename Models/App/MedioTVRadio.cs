namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.MedioTVRadio")]
    public partial class MedioTVRadio
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PK_IdMedio { get; set; }

        public string Nombre { get; set; }

        public string Canal { get; set; }

        public string Conductor { get; set; }

        public TimeSpan? Inicio { get; set; }

        public TimeSpan? Fin { get; set; }

        public string Estacion { get; set; }

        [StringLength(10)]
        public string Frecuencia { get; set; }

        public virtual Medio Medio { get; set; }
    }
}

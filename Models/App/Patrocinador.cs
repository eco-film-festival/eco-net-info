namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Patrocinador")]
    public partial class Patrocinador
    {
        [Key]
        public int PK_IdPatrocinador { get; set; }

        public int FK_IdTipoPatrocinador__SCH { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion_ES { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion_EN { get; set; }

        public string Image { get; set; }

        public string WebSite { get; set; }

        public int? Edicion { get; set; }

        public int Orden { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        public virtual TipoPatrocinador TipoPatrocinador { get; set; }

        public bool Home { get; set; }
    }
}

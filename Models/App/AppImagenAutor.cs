namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppImagenAutor")]
    public partial class AppImagenAutor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppImagenAutor()
        {
            AppImageAutorResize = new HashSet<AppImageAutorResize>();
        }
        public int Id { get; set; }

        public string Path { get; set; }

        public string File { get; set; }

        public int ShortFilmId { get; set; }

        public virtual AppShortFilm AppShortFilm { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppImageAutorResize> AppImageAutorResize { get; set; }
    }
}

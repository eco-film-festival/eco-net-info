namespace ECO._2015.Models.App
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBContextApp : DbContext
    {
        public DBContextApp()
            : base("name=DBContextApp")
        {
        }
        #region App 2016
        public virtual DbSet<AppAnno> AppAnno { get; set; }
        public virtual DbSet<AppCalificacion> AppCalificacion { get; set; }
        public virtual DbSet<AppCategoria> AppCategoria { get; set; }
        public virtual DbSet<AppCriterio> AppCriterio { get; set; }
        public virtual DbSet<AppCriterioCalificar> AppCriterioCalificar { get; set; }
        public virtual DbSet<AppEntrevista> AppEntrevista { get; set; }
        public virtual DbSet<AppEquipo> AppEquipo { get; set; }
        public virtual DbSet<AppEstado> AppEstado { get; set; }
        public virtual DbSet<AppFormatoAudio> AppFormatoAudio { get; set; }
        public virtual DbSet<AppFormatoVideo> AppFormatoVideo { get; set; }
        public virtual DbSet<AppGanador> AppGanador { get; set; }
        public virtual DbSet<AppImagenAutor> AppImagenAutor { get; set; }
        public virtual DbSet<AppImagenStill> AppImagenStill { get; set; }
        public virtual DbSet<AppJurado> AppJurado { get; set; }
        public virtual DbSet<AppNacionalidad> AppNacionalidad { get; set; }
        public virtual DbSet<AppOpenUserRegister> AppOpenUserRegister { get; set; }
        public virtual DbSet<AppPais> AppPais { get; set; }
        public virtual DbSet<AppPreguntaJurado> AppPreguntaJurado { get; set; }
        public virtual DbSet<AppRespuestaJurado> AppRespuestaJurado { get; set; }
        public virtual DbSet<AppShortFilm> AppShortFilm { get; set; }
        public virtual DbSet<AppSubtitulo> AppSubtitulo { get; set; }
        public virtual DbSet<AppTipoEquipo> AppTipoEquipo { get; set; }
        public virtual DbSet<AppTipoJurado> AppTipoJurado { get; set; }
        public virtual DbSet<AppViewFilms> AppViewFilms { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Boletin> Boletin { get; set; }
        public virtual DbSet<Cobertura> Cobertura { get; set; }
        public virtual DbSet<Contacto> Contacto { get; set; }
        public virtual DbSet<DiaTranmision> DiaTranmision { get; set; }
        public virtual DbSet<Edicion> Edicion { get; set; }
        public virtual DbSet<Evento> Evento { get; set; }
        public virtual DbSet<EventoFestival> EventoFestival { get; set; }
        public virtual DbSet<EventoNotas> EventoNotas { get; set; }
        public virtual DbSet<EventosImagen> EventosImagen { get; set; }
        public virtual DbSet<Medio> Medio { get; set; }
        public virtual DbSet<MedioTVRadio> MedioTVRadio { get; set; }
        public virtual DbSet<Muestra> Muestra { get; set; }
        public virtual DbSet<Patrocinador> Patrocinador { get; set; }
        public virtual DbSet<Perfil> Perfil { get; set; }
        public virtual DbSet<Periodicidad> Periodicidad { get; set; }
        public virtual DbSet<Programa> Programa { get; set; }
        public virtual DbSet<Proyeccion> Proyeccion { get; set; }
        public virtual DbSet<RedesSociales> RedesSociales { get; set; }
        public virtual DbSet<Sede> Sede { get; set; }
        public virtual DbSet<Telefono> Telefono { get; set; }
        public virtual DbSet<TipoEvento> TipoEvento { get; set; }
        public virtual DbSet<TipoMedio> TipoMedio { get; set; }
        public virtual DbSet<TipoMedioDetail> TipoMedioDetail { get; set; }
        public virtual DbSet<TipoPatrocinador> TipoPatrocinador { get; set; }
        public virtual DbSet<AppImageStillResize> AppImageResize { get; set; }
        public virtual DbSet<AppImageResizeConf> AppImageResizeConf { get; set; }
        public virtual DbSet<AppImageAutorResize> AppImageAutorResize { get; set; }
        #endregion
        #region LOC 2017
        public virtual DbSet<Estado> Estado { get; set; }
        public virtual DbSet<Municipio> Municipio { get; set; }
        #endregion

        #region

        #endregion
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Eco 2016
            modelBuilder.Entity<AppAnno>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.AppAnno)
                .HasForeignKey(e => e.AnnoId);

            modelBuilder.Entity<AppCategoria>()
                .HasMany(e => e.AppCriterioCalificar)
                .WithRequired(e => e.AppCategoria)
                .HasForeignKey(e => e.Categoria_Id);

            modelBuilder.Entity<AppCategoria>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.AppCategoria)
                .HasForeignKey(e => e.CategoriaId);

            modelBuilder.Entity<AppCriterio>()
                .HasMany(e => e.AppCriterioCalificar)
                .WithRequired(e => e.AppCriterio)
                .HasForeignKey(e => e.Criterio_Id);

            modelBuilder.Entity<AppCriterioCalificar>()
                .HasMany(e => e.AppCalificacion)
                .WithRequired(e => e.AppCriterioCalificar)
                .HasForeignKey(e => e.CriterioCalificar_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppEquipo>()
                .HasMany(e => e.AppShortFilm1)
                .WithMany(e => e.AppEquipo1)
                .Map(m => m.ToTable("AppShortFilmAppEquipo").MapLeftKey("AppEquipoId").MapRightKey("AppShortFilmId"));

            modelBuilder.Entity<AppEstado>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.AppEstado)
                .HasForeignKey(e => e.EstadoId);

            modelBuilder.Entity<AppEstado>()
                .HasMany(e => e.Medio)
                .WithOptional(e => e.AppEstado)
                .HasForeignKey(e => e.FK__AppEstado);

            modelBuilder.Entity<AppFormatoAudio>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.AppFormatoAudio)
                .HasForeignKey(e => e.FormatoAudioId);

            modelBuilder.Entity<AppFormatoVideo>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.AppFormatoVideo)
                .HasForeignKey(e => e.FormatoVideoId);

            modelBuilder.Entity<AppJurado>()
                .HasMany(e => e.AppCalificacion)
                .WithRequired(e => e.AppJurado)
                .HasForeignKey(e => e.Jurado_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppJurado>()
                .HasMany(e => e.AppRespuestaJurado)
                .WithRequired(e => e.AppJurado)
                .HasForeignKey(e => e.Jurado_Id);

            modelBuilder.Entity<AppNacionalidad>()
                .HasMany(e => e.AppPais)
                .WithRequired(e => e.AppNacionalidad)
                .HasForeignKey(e => e.Nacionalidad_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppNacionalidad>()
                .HasMany(e => e.AppTipoJurado)
                .WithRequired(e => e.AppNacionalidad)
                .HasForeignKey(e => e.Nacionalidad_Id);

            modelBuilder.Entity<AppNacionalidad>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.AppNacionalidad)
                .HasForeignKey(e => e.FK__AppNacionalidad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppNacionalidad>()
                .HasMany(e => e.AppPais1)
                .WithMany(e => e.AppNacionalidad1)
                .Map(m => m.ToTable("PaisNacionalidads").MapLeftKey("Nacionalidad_Id").MapRightKey("Pais_Id"));

            modelBuilder.Entity<AppPais>()
                .HasMany(e => e.AppEstado)
                .WithRequired(e => e.AppPais)
                .HasForeignKey(e => e.PaisId);

            modelBuilder.Entity<AppPais>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.AppPais)
                .HasForeignKey(e => e.FK__AppPais)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppPreguntaJurado>()
                .HasMany(e => e.AppRespuestaJurado)
                .WithRequired(e => e.AppPreguntaJurado)
                .HasForeignKey(e => e.PreguntaJurado_Id);

            modelBuilder.Entity<AppPreguntaJurado>()
                .HasMany(e => e.AppTipoJurado)
                .WithMany(e => e.AppPreguntaJurado)
                .Map(m => m.ToTable("PreguntaJuradoTipoJuradoes").MapLeftKey("PreguntaJurado_Id").MapRightKey("TipoJurado_Id"));

            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.AppCalificacion)
                .WithRequired(e => e.AppShortFilm)
                .HasForeignKey(e => e.ShortFilm_Id);

            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.AppEquipo)
                .WithOptional(e => e.AppShortFilm)
                .HasForeignKey(e => e.ShortFilm_Id);

            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.AppGanador)
                .WithRequired(e => e.AppShortFilm)
                .HasForeignKey(e => e.ShortFilmId);

            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.AppImagenAutor)
                .WithRequired(e => e.AppShortFilm)
                .HasForeignKey(e => e.ShortFilmId);

            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.AppImagenStill)
                .WithRequired(e => e.AppShortFilm)
                .HasForeignKey(e => e.ShortFilmId);

            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.AppViewFilms)
                .WithMany(e => e.AppShortFilm)
                .Map(m => m.ToTable("AppViewFilmsShortFilm").MapLeftKey("AppShortFilmId").MapRightKey("ViewFilmId"));

            modelBuilder.Entity<AppShortFilm>()
                .HasMany(e => e.Proyeccion)
                .WithMany(e => e.AppShortFilm)
                .Map(m => m.ToTable("ProyeccionShortFilm", "SCH").MapLeftKey("FK_IdShortFilm__dbo").MapRightKey("FK_IdProyeccion__SCH"));

            modelBuilder.Entity<AppSubtitulo>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.AppSubtitulo)
                .HasForeignKey(e => e.SubtituloId);

            modelBuilder.Entity<AppTipoEquipo>()
                .HasMany(e => e.AppEquipo)
                .WithRequired(e => e.AppTipoEquipo)
                .HasForeignKey(e => e.IdTipoEquipo);

            modelBuilder.Entity<AppTipoJurado>()
                .HasMany(e => e.AppCriterioCalificar)
                .WithRequired(e => e.AppTipoJurado)
                .HasForeignKey(e => e.TipoJurado_Id);

            modelBuilder.Entity<AppTipoJurado>()
                .HasMany(e => e.AppJurado)
                .WithRequired(e => e.AppTipoJurado)
                .HasForeignKey(e => e.TipoJurado_Id);

            modelBuilder.Entity<AppViewFilms>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AppViewFilms)
                .Map(m => m.ToTable("AspNetUserAppViewFilms").MapLeftKey("ViewFilmId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AppEntrevista)
                .WithOptional(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AppJurado)
                .WithOptional(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Cobertura>()
                .HasMany(e => e.Medio)
                .WithMany(e => e.Cobertura)
                .Map(m => m.ToTable("MedioCobertura", "SCH").MapLeftKey("FK_IdCobertura__SCH").MapRightKey("FK_IdMedio__SCH"));

            modelBuilder.Entity<DiaTranmision>()
                .HasMany(e => e.Medio)
                .WithMany(e => e.DiaTranmision)
                .Map(m => m.ToTable("MedioDiaTranmision", "SCH").MapLeftKey("FK_IdDiaTranmision__SCH").MapRightKey("FK_IdMedio__SCH"));

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppCriterio)
                .WithOptional(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppEntrevista)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppGanador)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppJurado)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppPreguntaJurado)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppShortFilm)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Boletin)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Contacto)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.EventoFestival)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Muestra)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Patrocinador)
                .WithOptional(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Programa)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasOptional(e => e.RedesSociales)
                .WithRequired(e => e.Edicion)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Sede)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.Telefono)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.EdicionHijas)
                .WithOptional(e => e.EdicionAnterior)
                .HasForeignKey(e => e.FK_IdEdicionAnterior__SCH);

            modelBuilder.Entity<EventoFestival>()
                .HasMany(e => e.EventoNotas)
                .WithRequired(e => e.EventoFestival)
                .HasForeignKey(e => e.FK_IdEventoFestival__SCH);

            modelBuilder.Entity<EventoFestival>()
                .HasMany(e => e.EventosImagen)
                .WithRequired(e => e.EventoFestival)
                .HasForeignKey(e => e.FK_IdEventoFestival__SCH);

            modelBuilder.Entity<Medio>()
                .Property(e => e.Cargo)
                .IsFixedLength();

            modelBuilder.Entity<Medio>()
                .Property(e => e.TipoTelefono)
                .IsFixedLength();

            modelBuilder.Entity<Medio>()
                .HasOptional(e => e.MedioTVRadio)
                .WithRequired(e => e.Medio)
                .WillCascadeOnDelete();

            modelBuilder.Entity<Medio>()
                .HasMany(e => e.Perfil)
                .WithMany(e => e.Medio)
                .Map(m => m.ToTable("MedioPerfil", "SCH").MapLeftKey("FK_IdMedio__SCH").MapRightKey("FK_IdPerfil__SCH"));

            modelBuilder.Entity<Medio>()
                .HasMany(e => e.Periodicidad)
                .WithMany(e => e.Medio)
                .Map(m => m.ToTable("MedioPeriodicidad", "SCH").MapLeftKey("FK_IdMedio__SCH").MapRightKey("FK_IdPeriodicidad__SCH"));

            modelBuilder.Entity<MedioTVRadio>()
                .Property(e => e.Frecuencia)
                .IsFixedLength();

            modelBuilder.Entity<Muestra>()
                .HasMany(e => e.Proyeccion)
                .WithMany(e => e.Muestra)
                .Map(m => m.ToTable("ProyeccionMuestra", "SCH").MapLeftKey("FK_IdMuestra__SCH").MapRightKey("FK_IdProyeccion__SCH"));

            modelBuilder.Entity<Programa>()
                .HasMany(e => e.Evento)
                .WithRequired(e => e.Programa)
                .HasForeignKey(e => e.FK_IdPrograma__SCH);

            modelBuilder.Entity<Programa>()
                .HasMany(e => e.Proyeccion)
                .WithRequired(e => e.Programa)
                .HasForeignKey(e => e.FK_IdPrograma__SCH);

            modelBuilder.Entity<Sede>()
                .HasMany(e => e.Programa)
                .WithOptional(e => e.Sede)
                .HasForeignKey(e => e.FK_IdSede__SCH)
                .WillCascadeOnDelete();

            modelBuilder.Entity<TipoEvento>()
                .HasMany(e => e.Evento)
                .WithRequired(e => e.TipoEvento)
                .HasForeignKey(e => e.FK_IdTipoEvento__SCH);

            modelBuilder.Entity<TipoEvento>()
                .HasMany(e => e.Proyeccion)
                .WithRequired(e => e.TipoEvento)
                .HasForeignKey(e => e.FK_IdTipoEvento__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Cobertura)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.DiaTranmision)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Medio)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Perfil)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.Periodicidad)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH);

            modelBuilder.Entity<TipoMedio>()
                .HasMany(e => e.TipoMedioDetail)
                .WithRequired(e => e.TipoMedio)
                .HasForeignKey(e => e.FK_IdTipoMedio__SCH)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoPatrocinador>()
                .HasMany(e => e.Patrocinador)
                .WithRequired(e => e.TipoPatrocinador)
                .HasForeignKey(e => e.FK_IdTipoPatrocinador__SCH);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.AppViewFilms)
                .WithRequired(e => e.Edicion1)
                .HasForeignKey(e => e.Edicion);

            modelBuilder.Entity<Edicion>()
                .HasMany(e => e.FlickrGaleria)
                .WithRequired(e => e.Edicion)
                .HasForeignKey(e => e.FK_IdEdicion__SCH);

            modelBuilder.Entity<AppImagenStill>()
                .HasMany(e => e.AppImageStillResize)
                .WithRequired(e => e.AppImagenStill)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppImageResizeConf>()
                .HasMany(e => e.AppImageResizes)
                .WithRequired(e => e.AppImageResizeConf)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppImagenAutor>()
                .HasMany(e => e.AppImageAutorResize)
                .WithRequired(e => e.AppImagenAutor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AppImageResizeConf>()
                .HasMany(e => e.AppImageAutorResize)
                .WithRequired(e => e.AppImageResizeConf)
                .WillCascadeOnDelete(false);
            #endregion
            #region LOC 2017
            modelBuilder.Entity<AppEstado>()
                .HasOptional(e => e.Estado)
                .WithRequired(e => e.AppEstado);

            modelBuilder.Entity<Estado>()
                .HasMany(e => e.Municipio)
                .WithRequired(e => e.Estado)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Municipio>()
                .HasMany(e => e.AppShortFilm)
                .WithOptional(e => e.Municipio)
                .HasForeignKey(e => e.MunicipioId);

            modelBuilder.Entity<AppPais>()
                .HasMany(e => e.AppEquipo)
                .WithOptional(e => e.AppPais)
                .HasForeignKey(e => e.Nacionalidad_Id);
            #endregion
        }
    }
}

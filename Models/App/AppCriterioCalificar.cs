namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppCriterioCalificar")]
    public partial class AppCriterioCalificar
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppCriterioCalificar()
        {
            AppCalificacion = new HashSet<AppCalificacion>();
        }

        public int Id { get; set; }

        public int TipoJurado_Id { get; set; }

        public int Criterio_Id { get; set; }

        public int Categoria_Id { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppCalificacion> AppCalificacion { get; set; }

        public virtual AppCategoria AppCategoria { get; set; }

        public virtual AppCriterio AppCriterio { get; set; }

        public virtual AppTipoJurado AppTipoJurado { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppEquipo")]
    public partial class AppEquipo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppEquipo()
        {
            AppShortFilm1 = new HashSet<AppShortFilm>();
        }

        public int Id { get; set; }

        [Required]
        public string Nombre { get; set; }

        public string Estudios { get; set; }

        [Required]
        public string Email { get; set; }

        public string Telefono { get; set; }

        public bool Local_Cel { get; set; }

        public bool Estudiante { get; set; }

        public bool Profecional { get; set; }

        public string Ciudad_Estudio { get; set; }

        public bool Mexicano { get; set; }

        public string Nacionalidad { get; set; }

        public string Biografia { get; set; }

        public int IdTipoEquipo { get; set; }

        public int? ShortFilm_Id { get; set; }

        public string Direccion { get; set; }

        public string Facebook { get; set; }

        public string Twiiter { get; set; }

        public string Ocupacion { get; set; }

        public bool Casa_Productora { get; set; }

        public string Web_Site_Casa_Productora { get; set; }
        public string Sexo { get; set; }
        public string Fecha_Nac { get; set; }
        public string Residencia { get; set; }

        public int Annio_Nac { get; set; }

        public virtual AppShortFilm AppShortFilm { get; set; }

        public virtual AppTipoEquipo AppTipoEquipo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm1 { get; set; }
        public int? Nacionalidad_Id { get; set; }
        public virtual AppPais AppPais { get; set; }
    }
}

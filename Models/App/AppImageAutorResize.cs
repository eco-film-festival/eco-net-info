namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppImageAutorResize")]
    public partial class AppImageAutorResize
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AppImagenAutorId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AppImageResizeConfId { get; set; }

        [Required]
        public string File { get; set; }

        [Required]
        public string Path { get; set; }

        public virtual AppImagenAutor AppImagenAutor { get; set; }

        public virtual AppImageResizeConf AppImageResizeConf { get; set; }
    }
}

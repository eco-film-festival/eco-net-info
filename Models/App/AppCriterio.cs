namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppCriterio")]
    public partial class AppCriterio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppCriterio()
        {
            AppCriterioCalificar = new HashSet<AppCriterioCalificar>();
        }

        public int Id { get; set; }

        public string Descripcion { get; set; }

        public string DescripcionCompleta { get; set; }

        public string Explicacion { get; set; }

        public string DescripcionCompleta_EN { get; set; }

        public string Explicacion_EN { get; set; }

        public int? Edicion { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppCriterioCalificar> AppCriterioCalificar { get; set; }
    }
}

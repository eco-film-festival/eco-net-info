namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Programa")]
    public partial class Programa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Programa()
        {
            Evento = new HashSet<Evento>();
            Proyeccion = new HashSet<Proyeccion>();
        }

        [Key]
        public int PK_IdPrograma { get; set; }

        public int? FK_IdSede__SCH { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public int Edicion { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Evento> Evento { get; set; }

        public virtual Sede Sede { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proyeccion> Proyeccion { get; set; }
    }
}

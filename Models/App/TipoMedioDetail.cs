namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.TipoMedioDetail")]
    public partial class TipoMedioDetail
    {
        [Key]
        public int PK_IdTipoMedioDetail { get; set; }

        public int FK_IdTipoMedio__SCH { get; set; }

        [Required]
        [StringLength(50)]
        public string Descripcion { get; set; }

        public bool Otro { get; set; }

        public virtual TipoMedio TipoMedio { get; set; }
    }
}

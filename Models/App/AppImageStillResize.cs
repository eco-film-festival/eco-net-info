namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppImageStillResize")]
    public partial class AppImageStillResize
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AppImagenStillId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AppImageResizeConfId { get; set; }

        public string File { get; set; }

        public virtual AppImagenStill AppImagenStill { get; set; }

        public virtual AppImageResizeConf AppImageResizeConf { get; set; }
        public string Path { get; set; }
    }
}

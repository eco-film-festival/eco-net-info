﻿namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppEntrevista")]
    public partial class AppEntrevista
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }
        
        public bool Universidad { get; set; }
        [Display(Name="Facebook")]
        public bool Red_social_facebook { get; set; }
        [Display(Name = "Twitter")]
        public bool Red_social_twitter { get; set; }
        [Display(Name = "Otro")]
        public string Red_social_otro { get; set; }
        [Display(Name = "Web Site")]
        public bool oficial_web_site { get; set; }
        [Display(Name = "Revista")]
        public bool revista { get; set; }
        [Display(Name = "Periodico")]
        public bool periodico { get; set; }
        [Display(Name = "Tv")]
        public bool tv { get; set; }
        [Display(Name = "Radio")]
        public bool radio { get; set; }
        [Display(Name = "Parabus")]
        public bool parabus { get; set; }
        [Display(Name = "Metrobus")]
        public bool metro_bus { get; set; }
        [Display(Name = "Autobus")]
        public bool auto_bus { get; set; }
        [Display(Name = "Metro")]
        public bool metro { get; set; }
        [Display(Name = "Cine")]
        public bool cine { get; set; }
        public int Edicion { get; set; }


        [Display(Name = "​Instagram")]
        public bool Red_social_instagram { get; set; }

        [Display(Name = "​​Youtube")]
        public bool Red_social_youtube { get; set; }

        [Display(Name = "​​Vimeo")]
        public bool Red_social_vimeo{ get; set; }

        [Display(Name = "​​Publicidad Impresa")]
        public bool Publicidad_Impresa { get; set; }

        [Display(Name = "​​Publicidad Digital")]
        public bool Publicidad_Digital { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }
    }
}

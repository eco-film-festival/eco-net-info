namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppShortFilm")]
    public partial class AppShortFilm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppShortFilm()
        {
            AppCalificacion = new HashSet<AppCalificacion>();
            AppEquipo = new HashSet<AppEquipo>();
            AppGanador = new HashSet<AppGanador>();
            AppImagenAutor = new HashSet<AppImagenAutor>();
            AppImagenStill = new HashSet<AppImagenStill>();
            AppEquipo1 = new HashSet<AppEquipo>();
            AppViewFilms = new HashSet<AppViewFilms>();
            Proyeccion = new HashSet<Proyeccion>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }

        public int CategoriaId { get; set; }

        public int FormatoVideoId { get; set; }

        public int FormatoAudioId { get; set; }

        public int EstadoId { get; set; }

        public int AnnoId { get; set; }

        public int SubtituloId { get; set; }

        public int Duracion_Minutos { get; set; }

        public int Duracion_Segundos { get; set; }

        [Required]
        public string Titulo_Original { get; set; }

        [Required]
        public string Titulo_Espanol { get; set; }

        [Required]
        public string Titulo_Ingles { get; set; }

        [Required]
        public string Sinopsis_Espanol { get; set; }

        [Required]
        public string Sinopsis_Ingles { get; set; }

        public string Path { get; set; }

        public string Video { get; set; }

        public bool Acepto_Terminos_Condiciones { get; set; }

        public bool Close_Corto { get; set; }

        public bool Preseleccion { get; set; }

        public bool Sel_Oficial { get; set; }
        [Display(Name = "Env�o en DVD Correo tradicional | DVD submission")]
        public bool Envio_Correo_Video { get; set; }

        [Required]
        public string Idioma { get; set; }

        public string OtroFormatoVideo { get; set; }

        public string OtroFormatoAudio { get; set; }

        public DateTime FechaRegistro { get; set; }

        public bool Activo { get; set; }

        public bool Ganador { get; set; }

        public string Descripcion_Ganador_ES { get; set; }

        public string Descripcion_Ganador_EN { get; set; }

        public string Lugar { get; set; }

        public int Edicion { get; set; }

        public int Etapa { get; set; }

        public virtual AppAnno AppAnno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppCalificacion> AppCalificacion { get; set; }

        public virtual AppCategoria AppCategoria { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppEquipo> AppEquipo { get; set; }

        public virtual AppEstado AppEstado { get; set; }

        public virtual AppFormatoAudio AppFormatoAudio { get; set; }

        public virtual AppFormatoVideo AppFormatoVideo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppGanador> AppGanador { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppImagenAutor> AppImagenAutor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppImagenStill> AppImagenStill { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        public virtual AppSubtitulo AppSubtitulo { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppEquipo> AppEquipo1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppViewFilms> AppViewFilms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proyeccion> Proyeccion { get; set; }

        public virtual Municipio Municipio { get; set; }
        public int? MunicipioId { get; set; }
    }
}

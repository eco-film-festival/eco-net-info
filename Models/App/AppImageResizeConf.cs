namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppImageResizeConf")]
    public partial class AppImageResizeConf
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppImageResizeConf()
        {
            AppImageResizes = new HashSet<AppImageStillResize>();
            AppImageAutorResize = new HashSet<AppImageAutorResize>();
        }

        public int Id { get; set; }

        public string Path { get; set; }

        public int width { get; set; }

        public int? height { get; set; }

        public bool fixWidht { get; set; }

        public bool IsStill { get; set; }

        public bool IsAutor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppImageStillResize> AppImageResizes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppImageAutorResize> AppImageAutorResize { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppAnno")]
    public partial class AppAnno
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppAnno()
        {
            AppShortFilm = new HashSet<AppShortFilm>();
        }

        public int Id { get; set; }

        [Required]
        public string Descripcion { get; set; }

        public bool CT_LIVE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm { get; set; }
    }
}

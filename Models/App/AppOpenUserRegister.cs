namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppOpenUserRegister")]
    public partial class AppOpenUserRegister
    {
        [Key]
        public string UserId { get; set; }

        [Column(TypeName = "date")]
        public DateTime FechaOpenRegistro { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppJurado")]
    public partial class AppJurado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppJurado()
        {
            AppCalificacion = new HashSet<AppCalificacion>();
            AppRespuestaJurado = new HashSet<AppRespuestaJurado>();
        }

        public int Id { get; set; }

        public string Descripcion { get; set; }

        public int TipoJurado_Id { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        public string Image { get; set; }

        public bool Jurado_Activo { get; set; }

        public int Edicion { get; set; }

        public string Informacion_ES { get; set; }

        public string Informacion_EN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppCalificacion> AppCalificacion { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        public virtual AppTipoJurado AppTipoJurado { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppRespuestaJurado> AppRespuestaJurado { get; set; }
    }
}

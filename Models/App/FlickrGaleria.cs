﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ECO._2015.Models.App
{
    [Table("SCH.FlickrGaleria")]
    public class FlickrGaleria
    {
        public FlickrGaleria()
        {
        }
        [Key]
        public int PK_IdFlickrGaleria { get; set; }

        public string photosetId { get; set; }

        public int FK_IdEdicion__SCH { get; set; }

        public virtual Edicion Edicion { get; set; }
    }
}
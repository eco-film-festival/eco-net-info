namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppEstado")]
    public partial class AppEstado
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppEstado()
        {
            AppShortFilm = new HashSet<AppShortFilm>();
            Medio = new HashSet<Medio>();
        }

        public int Id { get; set; }

        [Required]
        public string Estado_ES { get; set; }

        [Required]
        public string Estado_EN { get; set; }

        public int PaisId { get; set; }

        public virtual AppPais AppPais { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medio> Medio { get; set; }
        public virtual Estado Estado { get; set; }
    }
}

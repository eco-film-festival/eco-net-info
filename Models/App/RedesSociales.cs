namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.RedesSociales")]
    public partial class RedesSociales
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PK_IdEdicion { get; set; }

        public string Twitter { get; set; }

        public string Facebook { get; set; }

        public string Vimeo { get; set; }

        public string Google { get; set; }

        public string Instagram { get; set; }

        public string Flickr { get; set; }

        public virtual Edicion Edicion { get; set; }
    }
}

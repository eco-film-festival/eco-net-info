namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Telefono")]
    public partial class Telefono
    {
        [Key]
        public int PK_IdTelefono { get; set; }

        public int FK_IdEdicion__SCH { get; set; }

        [Column("Telefono")]
        public string Telefono1 { get; set; }

        public virtual Edicion Edicion { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Muestra")]
    public partial class Muestra
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Muestra()
        {
            Proyeccion = new HashSet<Proyeccion>();
        }

        [Key]
        public int PK_IdMuestra { get; set; }

        public string Titulo_Original { get; set; }

        public string Titulo_Espanol { get; set; }

        public string Titulo_Ingles { get; set; }

        public string Sinopsis_Espanol { get; set; }

        public string Sinopsis_Ingles { get; set; }

        [StringLength(50)]
        public string Duracion { get; set; }

        public string Director { get; set; }

        [StringLength(50)]
        public string Pais { get; set; }

        public int Edicion { get; set; }

        public string Path { get; set; }

        public string File { get; set; }

        public virtual Edicion Edicion1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Proyeccion> Proyeccion { get; set; }
    }
}

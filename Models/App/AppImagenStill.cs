namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppImagenStill")]
    public partial class AppImagenStill
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppImagenStill()
        {
            AppImageStillResize = new HashSet<AppImageStillResize>();
        }
        public int Id { get; set; }

        public string Path { get; set; }

        public string File { get; set; }

        public int ShortFilmId { get; set; }

        public virtual AppShortFilm AppShortFilm { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppImageStillResize> AppImageStillResize { get; set; }
    }
}

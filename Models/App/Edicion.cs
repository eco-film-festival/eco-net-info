namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Edicion")]
    public partial class Edicion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Edicion()
        {
            AppCriterio = new List<AppCriterio>();
            AppEntrevista = new HashSet<AppEntrevista>();
            AppGanador = new HashSet<AppGanador>();
            AppJurado = new HashSet<AppJurado>();
            AppPreguntaJurado = new HashSet<AppPreguntaJurado>();
            AppShortFilm = new HashSet<AppShortFilm>();
            Boletin = new HashSet<Boletin>();
            Contacto = new HashSet<Contacto>();
            EventoFestival = new HashSet<EventoFestival>();
            Medio = new HashSet<Medio>();
            Muestra = new HashSet<Muestra>();
            Patrocinador = new HashSet<Patrocinador>();
            Programa = new HashSet<Programa>();
            Sede = new HashSet<Sede>();
            Telefono = new HashSet<Telefono>();
            AppViewFilms = new HashSet<AppViewFilms>();
            FlickrGaleria = new HashSet<FlickrGaleria>();
            EdicionHijas = new HashSet<Edicion>();
        }


        public string Info_ES { get; set; }

        public string Info_EN { get; set; }
        public string Catalogo_ES { get; set; }

        public string Catalogo_EN { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PK_IdEdicion { get; set; }

        [StringLength(50)]
        public string Nombre_ES { get; set; }

        [StringLength(50)]
        public string Nombre_EN { get; set; }

        [StringLength(50)]
        public string Tema_ES { get; set; }

        [StringLength(50)]
        public string Tema_EN { get; set; }

        public string BasesConvocatoria_ES { get; set; }

        public string BasesConvocatoria_EN { get; set; }

        public string Cartel_ES { get; set; }

        public string Cartel_EN { get; set; }

        public string Logotipo_ES { get; set; }

        public string Logotipo_EN { get; set; }

        public string Teaser_ES { get; set; }

        public string Teaser_EN { get; set; }

        public string Informacion_ES { get; set; }

        public string Informacion_EN { get; set; }

        public string Direccion_ES { get; set; }

        public string Direccion_EN { get; set; }

        public string MapaContato { get; set; }

        public string ManualGrafico_ES { get; set; }

        public string ManualGrafico_EN { get; set; }

        public string Boletin_EN { get; set; }

        public string Boletin_ES { get; set; }

        public int? FK_IdEdicionAnterior__SCH { get; set; }
        public bool? CT_Activo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppCriterio> AppCriterio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppEntrevista> AppEntrevista { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppGanador> AppGanador { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppJurado> AppJurado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppPreguntaJurado> AppPreguntaJurado { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Boletin> Boletin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Contacto> Contacto { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventoFestival> EventoFestival { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Medio> Medio { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Muestra> Muestra { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Patrocinador> Patrocinador { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Programa> Programa { get; set; }

        public virtual RedesSociales RedesSociales { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sede> Sede { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Telefono> Telefono { get; set; }

        public virtual ICollection<AppViewFilms> AppViewFilms { get; set; }

        public virtual ICollection<FlickrGaleria> FlickrGaleria { get; set; }

        public virtual ICollection<Edicion> EdicionHijas { get; set; }

        public virtual Edicion EdicionAnterior { get; set; }
    }
}

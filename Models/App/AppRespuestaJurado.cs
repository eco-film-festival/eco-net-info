namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppRespuestaJurado")]
    public partial class AppRespuestaJurado
    {
        public int Id { get; set; }

        public int Jurado_Id { get; set; }

        public int PreguntaJurado_Id { get; set; }

        public string Respuesta { get; set; }

        public virtual AppJurado AppJurado { get; set; }

        public virtual AppPreguntaJurado AppPreguntaJurado { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppFormatoVideo")]
    public partial class AppFormatoVideo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppFormatoVideo()
        {
            AppShortFilm = new HashSet<AppShortFilm>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Formato_ES { get; set; }

        [Required]
        [StringLength(50)]
        public string Formato_EN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppTipoEquipo")]
    public partial class AppTipoEquipo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppTipoEquipo()
        {
            AppEquipo = new HashSet<AppEquipo>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string TipoEquipo_ES { get; set; }

        [Required]
        [StringLength(50)]
        public string TipoEquipo_EN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppEquipo> AppEquipo { get; set; }
    }
}

namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppCalificacion")]
    public partial class AppCalificacion
    {
        public int Id { get; set; }

        public int Valor { get; set; }

        public int Jurado_Id { get; set; }

        public int CriterioCalificar_Id { get; set; }

        public int ShortFilm_Id { get; set; }

        public DateTime FechaCaptura { get; set; }

        public virtual AppCriterioCalificar AppCriterioCalificar { get; set; }

        public virtual AppJurado AppJurado { get; set; }

        public virtual AppShortFilm AppShortFilm { get; set; }
    }
}

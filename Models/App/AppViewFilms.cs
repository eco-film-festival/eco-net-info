namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AppViewFilms
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppViewFilms()
        {
            AppShortFilm = new HashSet<AppShortFilm>();
            AspNetUsers = new HashSet<AspNetUsers>();
        }

        public int Id { get; set; }

        [Required]
        public string Descripcion_ES { get; set; }

        [Required]
        public string Descripcion_EN { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppShortFilm> AppShortFilm { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }

        public int Edicion { get; set; }

        public virtual Edicion Edicion1 { get; set; }
    }
}

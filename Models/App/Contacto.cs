namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SCH.Contacto")]
    public partial class Contacto
    {
        [Key]
        public int PK_IdContacto { get; set; }

        public int FK_IdEdicion__SCH { get; set; }

        [Required]
        public string Email { get; set; }

        public string Nombre_ES { get; set; }

        public string Nombre_EN { get; set; }

        public virtual Edicion Edicion { get; set; }
    }
}

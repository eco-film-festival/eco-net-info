namespace ECO._2015.Models.App
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AppGanador")]
    public partial class AppGanador
    {
        public int Id { get; set; }

        public int ShortFilmId { get; set; }

        public int Edicion { get; set; }

        [Required]
        public string Descripcion_ES { get; set; }

        [Required]
        public string Descipcion_EN { get; set; }

        public int Orden { get; set; }

        public string Premio { get; set; }

        public virtual AppShortFilm AppShortFilm { get; set; }

        public virtual Edicion Edicion1 { get; set; }
    }
}

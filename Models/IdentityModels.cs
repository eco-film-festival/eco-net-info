﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace ECO._2015.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
        public virtual ICollection<EntrevistaApplicationUser> Entrevista { get; set; }
        public virtual ICollection<ShortFilmApplicationUser> ShortFilm { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.Entrevista)
                .WithOptional(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete();

            ////modelBuilder.Entity<ApplicationUser>()
            ////    .HasMany(e => e.AppJurado)
            ////    .WithOptional(e => e.AspNetUsers)
            ////    .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(e => e.ShortFilm)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);
            base.OnModelCreating(modelBuilder);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }

    #region Tables

    [Table("AppEntrevista")]
    public partial class EntrevistaApplicationUser
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public int Edicion { get; set; }
        public virtual ApplicationUser AspNetUsers { get; set; }
    }

    [Table("AppShortFilm")]
    public partial class ShortFilmApplicationUser
    {
        public ShortFilmApplicationUser()
        {
        }

        public int Id { get; set; }
        public string UserId { get; set; }

        public bool Activo { get; set; }

        public int Edicion { get; set; }

        public virtual ApplicationUser AspNetUsers { get; set; }

    }
    
    #endregion

    //public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    //{
    //    public ApplicationDbContext()
    //        : base("DefaultConnection", throwIfV1Schema: false)
    //    {
    //    }

    //    public static ApplicationDbContext Create()
    //    {
    //        return new ApplicationDbContext();
    //    }
    //}
}
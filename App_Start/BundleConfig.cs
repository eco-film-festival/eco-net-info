﻿using System.Web;
using System.Web.Optimization;

namespace ECO._2015
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Boostrap Css Content

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/default.css"));

            #endregion

            #region upload files

            bundles.Add(new ScriptBundle("~/bundles/jquery-file-upload-image").Include(
                        "~/Scripts/jQuery-File-Upload/js/vendor/jquery.ui.widget.js",
                        "~/Scripts/jQuery-File-Upload/js/jquery.iframe-transport.js",
                        "~/Scripts/jQuery-File-Upload/js/jquery.fileupload.js",
                        "~/Scripts/App/jquery-file-upload-image.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/jquery-file-upload-image-config").Include(
                        "~/Scripts/jQuery-File-Upload/js/vendor/jquery.ui.widget.js",
                        "~/Scripts/jQuery-File-Upload/js/jquery.iframe-transport.js",
                        "~/Scripts/jQuery-File-Upload/js/jquery.fileupload.js",
                        "~/Scripts/App/jquery-file-upload-image-config.js"
                        ));


            bundles.Add(new StyleBundle("~/css/jquery-file-upload").Include(
                     "~/Scripts/jQuery-File-Upload/css/style.css",
                     "~/Scripts/jQuery-File-Upload/css/jquery.fileupload.css"));


            bundles.Add(new ScriptBundle("~/bundles/jquery-file-upload-video").Include(
                        "~/Scripts/jQuery-File-Upload/js/vendor/jquery.ui.widget.js",
                        "~/Scripts/jQuery-File-Upload/js/jquery.iframe-transport.js",
                        "~/Scripts/jQuery-File-Upload/js/jquery.fileupload.js",
                        "~/Scripts/App/jquery-file-upload-video.js"
                        ));
            #endregion

            #region Video Player
            bundles.Add(new ScriptBundle("~/bundles/player-video").Include(
                "~/Scripts/flowplayer-5.4.6/flowplayer.min.js",
                "~/Scripts/App/jquery-file-view-video.js"
                ));
            //
            bundles.Add(new StyleBundle("~/css/player-video").Include(
                     "~/Scripts/flowplayer-5.4.6/skin/minimalist.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/style-functions").Include(
                "~/Scripts/App/jquery-style-functions.js"
                ));
            #endregion

            #region Video Galeria

            bundles.Add(new ScriptBundle("~/bundles/flowplayer").Include(
                "~/Scripts/flowplayer-5.4.6/flowplayer.min.js"
                ));
            #endregion

            #region Create Video
            bundles.Add(new ScriptBundle("~/bundles/createShortFilm").Include(
                      "~/Scripts/App/jquery-app-create-shortfilm.js"));
            #endregion

            #region Bootstrap Rating Input
            bundles.Add(new ScriptBundle("~/bundles/star-rating").Include(
                      "~/Scripts/App/jquery-star-rating.js"));
            bundles.Add(new StyleBundle("~/css/star-rating").Include(
                     "~/Content/star-rating.css"
            ));
            #endregion

            #region AngularJS
            bundles.Add(new ScriptBundle("~/bundles/angular")
                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/angular-resource.js")
                );
            #endregion

            #region AngularJS-Chart.js
            bundles.Add(new ScriptBundle("~/bundles/angular-chart.css").Include(
                       "~/Scripts/angular-chart.js/angular-chart.css"));
            bundles.Add(new ScriptBundle("~/bundles/angular-chart.js")
                .Include(
                    "~/Scripts/Chart/Chart.js"
                )
                .Include(
                    "~/Scripts/angular-chart.js/angular-chart.js"
                )
            );
            #endregion

            #region AngularJS Report
            bundles.Add(new ScriptBundle("~/bundles/angular/report").Include(
                "~/Scripts/App/angular.app.report.js"
                ));
            #endregion

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = false;
        }
    }
}

﻿using ECO._2015.Bussiness.SendMailService;
using ECO._2015.Models.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Bussiness
{
    public class MailNotificationViewModel
    {
        public string FechaRegistro { get; set; }

        public string Categoria_ES { get; set; }

        public string Categoria_EN { get; set; }

        public string Pais_ES { get; set; }

        public string Pais_EN { get; set; }

        public string Titulo_Original { get; set; }

        public string MsgEsp { get; set; }

        public string MsgIng { get; set; }

        public string Email { get; set; }

        public string Subject { get; set; }

        public int EdicionFestival { get; set; }
    }
    public class MailNotification : IServiceMail
    {
        public List<string> ListMailToMessage { get; set; }
        public string Subject { get; set; }

        public object Model { get; set; }
    }
    public class MailSenderBusiness
    {
        private int IdTipoEquipoContacto = 89;

        public void SendMail(
            Controller Controller,
            string template,
            string Subject,
            string MsgEsp,
            string MsgIng,
            AppShortFilm _ShortFilm,
            int EdicionFestival ,
            string UserAdminMMail = null
            )
        {
            if (_ShortFilm != null)
            {
                var _notificacion = new MailNotification()
                {
                    Subject = Subject,
                    ListMailToMessage = new List<string>(),
                    Model = new MailNotificationViewModel()
                    {
                        FechaRegistro = _ShortFilm.FechaRegistro.ToString("dd/MM/yyyy"),
                        Categoria_ES = _ShortFilm.AppCategoria.Categoria_ES,
                        Categoria_EN = _ShortFilm.AppCategoria.Categoria_EN,
                        Pais_ES = _ShortFilm.AppEstado.AppPais.Pais_ES,
                        Pais_EN = _ShortFilm.AppEstado.AppPais.Pais_EN,
                        Titulo_Original = _ShortFilm.Titulo_Original,
                        MsgEsp = MsgEsp,
                        MsgIng = MsgIng,
                        Email = _ShortFilm.AspNetUsers.Email,
                        Subject = Subject,
                        EdicionFestival = EdicionFestival
                    }
                };

                if (!string.IsNullOrEmpty(UserAdminMMail))
                {
                    _notificacion.ListMailToMessage.Add(UserAdminMMail);
                }

                _notificacion.ListMailToMessage.Add(_ShortFilm.AspNetUsers.Email);

                _ShortFilm.AppEquipo
                        .Where(e => e.IdTipoEquipo == IdTipoEquipoContacto)
                        .Where(e => e.Email != _ShortFilm.AspNetUsers.Email)
                        .ToList()
                        .ForEach(c => _notificacion.ListMailToMessage.Add(c.Email))
                        ;

                try
                {
                    SendMailMVCService SendMailMVCService = new SendMailMVCService();
                    SendMailMVCService.SendListMessage(Controller, _notificacion, template, "");
                }
                catch (Exception e)
                {
                    throw e;
                }
            
            }


            
        }

    }
}
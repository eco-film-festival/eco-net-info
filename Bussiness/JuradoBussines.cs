﻿using ECO._2015.Models;
using ECO._2015.Models.App;
using Microsoft.AspNet.Identity;
using System.Linq;

namespace ECO._2015.Bussiness
{
    public class JuradoBussines : ControllerBussines
    {
        protected JuradoListShortFilmViewModel GetListShortFilmByJurado()
        {
            AppJurado Jurado = GetJuradoByIdUser();
            if (Jurado != null)
            {
                // -------------------------------
                var _model = new JuradoListShortFilmViewModel(Jurado, EdicionFestival);
                // -------------------------------
                db.AppShortFilm
                       .Where(s => s.Close_Corto == true)
                       .Where(s => s.Preseleccion == true)
                       // .Where(s => s.Sel_Oficial == true)
                       .Where(s => s.Activo == true)
                       .Where(s => s.AppEstado.AppPais.Nacionalidad_Id == Jurado.AppTipoJurado.Nacionalidad_Id)
                       .Where(s=> s.Edicion == EdicionFestival)
                       .ToList()
                       .ForEach(s => _model.ListShortFilmJurado.Add(new ShortFilmJuradoViewModel(s , Jurado)))
                       ;

                return _model;
            }

            return null;

        }

        protected AppJurado GetJuradoByIdUser()
        {
            return GetJuradoByIdUser(User.Identity.GetUserId());
        }

        protected AppJurado GetJuradoByIdUser(string userId)
        {
            return db.AppJurado
                .Where(s => s.Edicion == EdicionFestival)
                .Where(s => s.UserId == userId).FirstOrDefault();
        }

    }
}
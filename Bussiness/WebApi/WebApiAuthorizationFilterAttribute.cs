﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Net.Http;

namespace ECO._2015.Bussiness.WebApi
{
    //public abstract class AuthorizationFilterAttributeTest : FilterAttribute, IAuthorizationFilter, IFilter
    //{

    //    public Task<System.Net.Http.HttpResponseMessage> ExecuteAuthorizationFilterAsync(
    //        System.Web.Http.Controllers.HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken, 
    //        Func<System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage>> continuation)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public virtual bool AllowMultiple { 
    //        get { 
    //            return true; 
    //        } 
    //    }
    //    //public bool AllowMultiple { get; }
    //}
    public class WebApiAuthorizationFilterAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
            actionContext.Response = response;
            base.HandleUnauthorizedRequest(actionContext);
        }

        protected override bool IsAuthorized(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var _user = actionContext.RequestContext.Principal;
            Debug.WriteLine(string.Format("{0} - {1}", "WebApiAuthorizationFilterAttribute", _user.Identity.Name));
            return false;
        }

    }
}
﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ECO._2015.Bussiness.WebApi
{
    public class WebApiServiceMiddleware
    {
        private Func<IDictionary<string, object>, Task> _next;
        public WebApiServiceMiddleware(Func<IDictionary<string ,object> , Task> next)
        {
            _next = next;
        }

        public async Task Invoke(IDictionary<string, object> env)
        {
            var context = new OwinContext(env);
            var _user = context.Request.User;
            Debug.WriteLine(string.Format("{0} - {1}", "WebApiServiceMiddleware", _user.Identity.Name));
            
            await _next(env);
        }
    }
}
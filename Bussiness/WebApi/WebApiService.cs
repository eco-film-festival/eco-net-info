﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Http.Filters;
using System.Web.Mvc;

namespace ECO._2015.Bussiness.WebApi
{
    #region RequestVerificationToken

    public static class ApiService
    {
        public const string RequestVerificationToken = "RequestVerificationToken";

        public static void GetTokens(IEnumerable<string> tokenHeaders, out string cookieToken, out string formToken)
        {
            cookieToken = string.Empty;
            formToken = string.Empty;
            string[] tokens = tokenHeaders.First().Split(':');
            if (tokens.Length == 2)
            {
                cookieToken = tokens[0].Trim();
                formToken = tokens[1].Trim();
            }
        }

        public static void GetTokens(string tokenValue, out string cookieToken, out string formToken)
        {
            cookieToken = string.Empty;
            formToken = string.Empty;
            string[] tokens = tokenValue.Split(':');
            if (tokens.Length == 2)
            {
                cookieToken = tokens[0].Trim();
                formToken = tokens[1].Trim();
            }
        }

        public static string GetTokenHeaderValue()
        {
            string cookieToken, formToken;
            System.Web.Helpers.AntiForgery.GetTokens(null, out cookieToken, out formToken);
            return cookieToken + ":" + formToken;
        }
    }

    #region Helper
    public static class AntiForgeryExtension
    {
        public static string RequestVerificationToken(this HtmlHelper helper)
        {
            return String.Format("ncg-request-verification-token={0}", ApiService.GetTokenHeaderValue());
        }
    }
    #endregion

    #region Op Web Api
    public class AntiForgeryValidate : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            string cookieToken = String.Empty;
            string formToken = String.Empty;
            IEnumerable<string> tokenHeaders;

            if (actionContext.Request.Headers.TryGetValues(ApiService.RequestVerificationToken, out tokenHeaders))
            {
                ApiService.GetTokens(tokenHeaders, out cookieToken, out formToken);
            }

            System.Web.Helpers.AntiForgery.Validate(cookieToken, formToken);

            base.OnActionExecuting(actionContext);
        }
    }
    #endregion

    #region Op Web MVC
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ApiServiceValidateAntiForgeryTokenAttribute : System.Web.Mvc.FilterAttribute, System.Web.Mvc.IAuthorizationFilter
    {
        private void ValidateRequestHeader(HttpRequestBase request)
        {
            string cookieToken = String.Empty;
            string formToken = String.Empty;
            string tokenValue = request.Headers[ApiService.RequestVerificationToken];
            if (!String.IsNullOrEmpty(tokenValue))
            {
                ApiService.GetTokens(tokenValue, out cookieToken, out formToken);
            }
            System.Web.Helpers.AntiForgery.Validate(cookieToken, formToken);
        }
        public void OnAuthorization(System.Web.Mvc.AuthorizationContext filterContext)
        {
            try
            {
                var request = filterContext.HttpContext.Request;
                string cookieToken = String.Empty;
                string formToken = String.Empty;
                string tokenValue = request.Headers[ApiService.RequestVerificationToken];
                if (!String.IsNullOrEmpty(tokenValue))
                {
                    ApiService.GetTokens(tokenValue, out cookieToken, out formToken);
                }
                System.Web.Helpers.AntiForgery.Validate(cookieToken, formToken);

            }
            catch (System.Web.Mvc.HttpAntiForgeryException)
            {
                throw;
            }
        }
    }
    #endregion
    
    #endregion




    //[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    //public class AuthorizeAttribute : System.Web.Mvc.FilterAttribute, System.Web.Mvc.IAuthorizationFilter
    //{

    //    public void OnAuthorization(AuthorizationContext filterContext)
    //    {
    //        //filterContext.

    //        throw new NotImplementedException();
    //    }
    //}
}
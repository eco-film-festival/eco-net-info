﻿using ECO._2015.Models.Vis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO._2015.Bussiness
{
    public sealed class VideoDiaBussines
    {
        private static VideoDiaBussines instance = null;
        private static readonly object padlock = new object();
        private DateTime _ultimaFechaVideo;
        private VideoGaleria videoDia;
        private DBContextVis _dbVis = new DBContextVis();
        private static Random rnd = new Random();

        VideoDiaBussines()
        {
            UpdateVideoDia();
        }

        public static VideoDiaBussines Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new VideoDiaBussines();
                    }
                    return instance;
                }
            }
        }

        public VideoGaleria getVideo(DateTime _fecha)
        {
            if (_fecha.Date.ToString("d") != _ultimaFechaVideo.ToString("d"))
            {
                UpdateVideoDia();
            }

            return videoDia;
        }

        private void UpdateVideoDia()
        {
            _ultimaFechaVideo = DateTime.Now.Date;
            var _videoGaleria = _dbVis.VideoGaleria.OrderByDescending(e => e.Año).ThenBy(e => e.Orden).ToList().Where(e => ValidWinnerShortFilm(e)).ToList();
            int r = rnd.Next(_videoGaleria.Count);
            videoDia = _videoGaleria[r];
        }

        private bool ValidWinnerShortFilm(VideoGaleria _VideoGaleria)
        {
            if (_VideoGaleria.Edicion == PublicServiceBussines.EdicionFestivalApp)
            {
                return PublicServiceBussines.isWinnersOpen;
            }
            return true;
        }

    }
}
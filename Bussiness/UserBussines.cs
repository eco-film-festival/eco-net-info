﻿using System.Web;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Mvc;
using System.Linq;
using Microsoft.AspNet.Identity;

namespace ECO._2015.Bussiness
{
    public class UserBussines : MsgBussines 
    {
        protected string NotificationRegister = "_NotificationRegister";
        protected string NotificactionForgotPassword = "_NotificactionForgotPassword";

        protected ApplicationSignInManager _signInManager;
        protected ApplicationUserManager _userManager;

        public UserBussines()
        {
        }

        public UserBussines(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            //  /Account/Login
            return RedirectToAction("Login", "Account");
        }

        protected void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
        public class BadRequest : JsonResult
        {

            public BadRequest()
            {
            }

            public BadRequest(string message)
            {
                this.Data = message;
            }

            public BadRequest(ModelStateDictionary ModelState)
            {
                var data = ModelState.Values.SelectMany(m => m.Errors)
                                 .Select(e => e.ErrorMessage)
                                 .ToList();
                this.Data = data;
            }

            public BadRequest(object data)
            {
                this.Data = data;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                context.RequestContext.HttpContext.Response.StatusCode = 400;
                base.ExecuteResult(context);
            }
        }

        //public class OkRequest : JsonResult
        //{
        //    private string actionName;
        //    private string controllerName;

        //    public OkRequest(string actionName, string controllerName)
        //    {
        //        this.actionName = actionName;
        //        this.controllerName = controllerName;
        //    }


        //    public override void ExecuteResult(ControllerContext context)
        //    {
        //        context.RequestContext.HttpContext.Response.StatusCode = 200;
        //        context.RequestContext.HttpContext.Response.RedirectLocation = string.Format("/{0}/{1}", controllerName, actionName);
        //        base.ExecuteResult(context);
        //    }
        //}


    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Bussiness.SendMailService
{
    public interface IServiceMail
    {
        List<string> ListMailToMessage { get; set; }
        string Subject { get; set; }
        object Model { get; set; }
    }


    public class SendMailMVCService : ServiceMail
    {
        public SendMailMVCService()
            : base()
        {

        }

        public void SendListMessage(Controller controller, IServiceMail _IServiceMail, string viewName, string content)
        {
            try
            {
                var _body = RenderRazorViewToString(controller, _IServiceMail.Model, viewName, content);
                _IServiceMail.ListMailToMessage.ToList().ForEach(to => SendMail(to, _IServiceMail.Subject, _body));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static string RenderRazorViewToString(Controller controller, object model, string viewName, string content)
        {
            controller.ViewData.Model = model;
            controller.ViewBag.Content = content;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        public void SendMail(Microsoft.AspNet.Identity.IdentityMessage message)
        {
            SendMail(message.Destination, message.Subject, message.Body);
        }
    }
}
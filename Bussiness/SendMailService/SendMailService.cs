﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;


namespace ECO._2015.Bussiness.SendMailService
{
    public class ServiceMail
    {
        public ServiceMail()
        {
            HostSite = ConfigurationManager.AppSettings["MailHostSite"];
            ImagesHostSite = ConfigurationManager.AppSettings["MailImagesHostSite"];
            FromSite = ConfigurationManager.AppSettings["MailFrom"];
            ToMailTest = ConfigurationManager.AppSettings["MailToMailTest"];
            ActiveX509Certificate = bool.Parse(ConfigurationManager.AppSettings["MailX509Certificate"]);
            AttachmentFileName = ConfigurationManager.AppSettings["AttachmentFileName"];
        }
        //-----------------------------------------
        public string HostSite { get; private set; }
        public string ImagesHostSite { get; private set; }
        public string FromSite { get; private set; }
        private string AttachmentFileName = string.Empty;
        private string _ToMailTest = string.Empty;
        private bool ActiveX509Certificate = false;
        public string ToMailTest
        {
            get
            {
                return _ToMailTest;
            }
            private set
            {
                _ToMailTest = value;
            }
        }
        //-----------------------------------------

        private SmtpClient GetSmtpClient()
        {
            SmtpClient _SmtpClient = new SmtpClient();
            // -----------------------
            _SmtpClient.Host = ConfigurationManager.AppSettings["MailHost"];
            _SmtpClient.Port = int.Parse(ConfigurationManager.AppSettings["MailPort"]);
            _SmtpClient.EnableSsl = bool.Parse(ConfigurationManager.AppSettings["MailEnableSsl"]);
            _SmtpClient.Timeout = int.Parse(ConfigurationManager.AppSettings["MailTimeout"]);
            _SmtpClient.DeliveryMethod = (SmtpDeliveryMethod)int.Parse(ConfigurationManager.AppSettings["MailSmtpDeliveryMethod"]);
            _SmtpClient.UseDefaultCredentials = bool.Parse(ConfigurationManager.AppSettings["MailUseDefaultCredentials"]); ;
            // -----------------------

            string _MailCredentialsUser = ConfigurationManager.AppSettings["MailCredentialsUser"];
            string _MailCredentialsPassword = ConfigurationManager.AppSettings["MailCredentialsPassword"];

            if (_MailCredentialsUser != "" && _MailCredentialsPassword != "")
            {
                _SmtpClient.UseDefaultCredentials = false;
                _SmtpClient.Credentials = new System.Net.NetworkCredential(_MailCredentialsUser, _MailCredentialsPassword) as ICredentialsByHost;
            }
            // ------------------------------------------------
            _SmtpClient.SendCompleted += new SendCompletedEventHandler(smtpClient_SendCompleted);
            // ------------------------------------------------
            return _SmtpClient;
        }

        public MailMessage GetMailMessage(String ToMail, String Subject, String Body)
        {
            string _ToMail = (string.IsNullOrEmpty(_ToMailTest)) ? ToMail : _ToMailTest;

            MailMessage _MailMessage = new MailMessage(FromSite, _ToMail, Subject, Body);
            _MailMessage.BodyEncoding = UTF8Encoding.UTF8;
            _MailMessage.IsBodyHtml = true;
            _MailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            _MailMessage.BodyTransferEncoding = TransferEncoding.EightBit;

            return _MailMessage;
        }
        //----------------------------------------
        //------------------------------------------

        public void SendMailAsync(String ToMail, String Subject, String Body, MemoryStream _MemoryStreamAttachment = null)
        {
            string _ToMail = (string.IsNullOrEmpty(_ToMailTest)) ? ToMail : _ToMailTest;

            try
            {
                SendMailAsync(GetMailMessage(_ToMail, Subject, Body), _MemoryStreamAttachment);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void SendMailAsync(MailMessage _MailMessage, MemoryStream _MemoryStreamAttachment = null)
        {
            try
            {
                SmtpClient _SmtpClient = GetSmtpClient();
                AddAttachment(ref _MailMessage, _MemoryStreamAttachment);

                if (ActiveX509Certificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                        delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                        { return true; };
                }

                Object state = new Sender(_SmtpClient, _MailMessage);
                _SmtpClient.SendAsync(_MailMessage, state);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
        //-----------------------------------------
        //-----------------------------------------

        public void SendMail(String ToMail, String Subject, String Body, MemoryStream _MemoryStreamAttachment = null)
        {
            string _ToMail = (string.IsNullOrEmpty(_ToMailTest)) ? ToMail : _ToMailTest;
            try
            {
                SendMail(GetMailMessage(_ToMail, Subject, Body), _MemoryStreamAttachment);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        private void SendMail(MailMessage _MailMessage, MemoryStream _MemoryStreamAttachment = null)
        {
            try
            {
                SmtpClient _SmtpClient = GetSmtpClient();
                AddAttachment(ref _MailMessage, _MemoryStreamAttachment);

                if (ActiveX509Certificate)
                {
                    ServicePointManager.ServerCertificateValidationCallback =
                        delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                        { return true; };
                }

                _SmtpClient.Send(_MailMessage);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }
        //-----------------------------------------
        //-----------------------------------------
        public void SendListMessage(List<MailMessage> _ListMailMessage, MemoryStream _MemoryStream = null)
        {
            if (_ListMailMessage != null)
            {
                _ListMailMessage.ForEach(m => SendMailAsync(m, _MemoryStream));
            }
        }
        //-----------------------------------------
        //-----------------------------------------
        private void smtpClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Sender _Sender = (Sender)e.UserState;

            string subject = _Sender.MailMessage.Subject;

            if (e.Cancelled)
            {
                Debug.WriteLine(string.Format("Send canceled for mail with subject -> {0} ", subject));
            }
            if (e.Error != null)
            {
                Debug.WriteLine(string.Format("Error {1} occurred when sending mail -> {0} ", subject, e.Error.ToString()));
            }
            else
            {
                Debug.WriteLine(string.Format("Message -> {0} sent.", subject));
            }
            _Sender.MailMessage.Dispose();
            _Sender.SmtpClient.Dispose();
        }
        //-----------------------------------------
        //-----------------------------------------
        private void AddAttachment(ref MailMessage _MailMessage, MemoryStream _MemoryStreamAttachment)
        {
            if (_MemoryStreamAttachment != null)
            {
                MemoryStream _MemoryStream = new MemoryStream(_MemoryStreamAttachment.ToArray());
                Attachment _Attachment = new Attachment(_MemoryStream, AttachmentFileName, MediaTypeNames.Application.Pdf);
                _MailMessage.Attachments.Add(_Attachment);
            }
        }
        //-----------------------------------------
        //-----------------------------------------


    }

    class Sender
    {

        public Sender(SmtpClient _SmtpClient, MailMessage _MailMessage)
        {
            this.SmtpClient = _SmtpClient;
            this.MailMessage = _MailMessage;
        }


        public MailMessage MailMessage { get; private set; }
        public SmtpClient SmtpClient { get; private set; }
    }
}
﻿using FlickrNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ECO._2015.Bussiness
{
    public class FlickrBussines
    {

        public static Flickr GetInstance()
        {
            var _Flickr = new Flickr(ApiKey, SharedSecret);
            _Flickr.InstanceCacheDisabled = true;
            return _Flickr;
        }

        public static List<Photo> GetFeed()
        {
            PhotoSearchOptions options = new PhotoSearchOptions();
            options.UserId = UserId; 
            options.PerPage = 100;
            options.Extras = PhotoSearchExtras.All;
            PhotoCollection photos = GetInstance().PhotosSearch(options);
            return filterHeightPhoto(photos.ToList());
        }

        public static List<Photo> PhotosetsGetPhotos(string photosetId)
        {
            PhotosetPhotoCollection _PhotosetPhotoCollection = GetInstance().PhotosetsGetPhotos(photosetId, PhotoSearchExtras.All);
            return filterHeightPhoto(_PhotosetPhotoCollection.ToList());
        }

        public static List<Photo> PhotosetsGetPhotos(string[] listPhotosetId)
        {
            var _result = new List<Photo>();
            foreach (var photosetId in listPhotosetId)
            {
                var _listPhoto = PhotosetsGetPhotos(photosetId);
                _result.AddRange(filterHeightPhoto(_listPhoto));
            }
            return _result;
        }

        private static List<Photo> filterHeightPhoto(List<Photo> _list) 
        {
            var _result = new List<Photo>();
            foreach (var _photo in _list) {
                if (_photo.MediumHeight <= MaxHeightPhoto)
                {
                    _result.Add(_photo);
                }
            }
            return _result;
        }

        public static string ApiKey
        {
            get {
                return ConfigurationManager.AppSettings["ApiKeyFlickr"];
            }
        }

        public static string SharedSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["SharedSecretFlickr"];
            }
        }

        public static string UserId
        {
            get
            {
                return ConfigurationManager.AppSettings["UserIdFlickr"];
            }
        }

        public static int MaxHeightPhoto
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["MaxHeightPhotoFlickr"]);
            }
        }
    }
}
﻿using MediaToolkit;
using MediaToolkit.Model;
using MediaToolkit.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace ECO._2015.Bussiness
{
    public class VideoConverterBussines
    {
        public static VideoConverterResult Test()
        {

   

            string path = "/Uploads/Video/";
            string pathForSaving = System.Web.Hosting.HostingEnvironment.MapPath("~" + path);
            string fileOrg = "544eefba-2228-4710-be9c-5178853d6bb2.mp4";
            string fileDest = "544eefba-2228-4710-be9c-5178853d6bb2.webm";
            string inputFilePath = string.Format("{0}{1}", pathForSaving, fileOrg);
            string outputFilePath = string.Format("{0}{1}", pathForSaving, fileDest);


            var inputFile = new MediaFile { Filename = inputFilePath };
            var outputFile = new MediaFile { Filename = outputFilePath };

            var conversionOptions = new ConversionOptions
            {
                MaxVideoDuration = TimeSpan.FromSeconds(30),
                VideoAspectRatio = VideoAspectRatio.R16_9,
                VideoSize = VideoSize.Hd1080,
                AudioSampleRate = AudioSampleRate.Hz44100
            };


            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);

                Debug.WriteLine(inputFile.Metadata.AudioData.BitRateKbs);
                Debug.WriteLine(inputFile.Metadata.AudioData.ChannelOutput);
                Debug.WriteLine(inputFile.Metadata.AudioData.Format);
                Debug.WriteLine(inputFile.Metadata.AudioData.SampleRate);

                Debug.WriteLine(inputFile.Metadata.VideoData.BitRateKbs);
                Debug.WriteLine(inputFile.Metadata.VideoData.ColorModel);
                Debug.WriteLine(inputFile.Metadata.VideoData.Format);
                Debug.WriteLine(inputFile.Metadata.VideoData.Fps);
                Debug.WriteLine(inputFile.Metadata.VideoData.FrameSize);

               


                engine.ConvertProgressEvent += ConvertProgressEvent;
                engine.ConversionCompleteEvent += engine_ConversionCompleteEvent;
                engine.Convert(inputFile, outputFile);
            }


            var _result = new VideoConverterResult();
            return _result;
        }

        private static void ConvertProgressEvent(object sender, ConvertProgressEventArgs e)
        {

            Debug.WriteLine("\n------------\nConverting...\n------------");
            Debug.WriteLine("Bitrate: {0}", e.Bitrate);
            Debug.WriteLine("Fps: {0}", e.Fps);
            Debug.WriteLine("Frame: {0}", e.Frame);
            Debug.WriteLine("ProcessedDuration: {0}", e.ProcessedDuration);
            Debug.WriteLine("SizeKb: {0}", e.SizeKb);
            Debug.WriteLine("TotalDuration: {0}\n", e.TotalDuration);
        }

        private static void engine_ConversionCompleteEvent(object sender, ConversionCompleteEventArgs e)
        {
            Debug.WriteLine("\n------------\nConversion complete!\n------------");
            Debug.WriteLine("Bitrate: {0}", e.Bitrate);
            Debug.WriteLine("Fps: {0}", e.Fps);
            Debug.WriteLine("Frame: {0}", e.Frame);
            Debug.WriteLine("ProcessedDuration: {0}", e.ProcessedDuration);
            Debug.WriteLine("SizeKb: {0}", e.SizeKb);
            Debug.WriteLine("TotalDuration: {0}\n", e.TotalDuration);
        }
    }

    public class VideoConverterResult 
    { 
    
    }
}
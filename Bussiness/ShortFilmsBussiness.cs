﻿using ECO._2015.Models;
using ECO._2015.Models.App;
//using ECO._2015.Models;
//using ECO._2015.Models.App;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Bussiness
{
    public class ShortFilmsBussiness : ControllerBussines
    {

        #region ListShortFilm
        private List<AppShortFilm> _ListShortFilm = null;

        public List<AppShortFilm> ListShortFilm
        {
            get
            {
                if (_ListShortFilm == null)
                {
                    var _list = UserActive.ShortFilm
                        .Where(s => s.Activo)
                        .Where(s => s.Edicion == EdicionFestival)
                        .Select(e =>e.Id).ToArray();

                    _ListShortFilm = db.AppShortFilm.Where(e => _list.Contains(e.Id)).ToList();

                }

                return _ListShortFilm;
            }
        }

        public int CountListShortFilm
        {
            get
            {
                return ListShortFilm.Count;
            }
        }
        #endregion

        #region Validations

        protected ActionResult ValidationExistShortFilm()
        {
            if (! IsCloseConvocatoria)
            {
                var _listShortFilmEd = ListShortFilm.Where(s => s.Etapa != EtapaRegistroFin).ToList();

                foreach (var s in _listShortFilmEd)
                {
                    switch (s.Etapa)
                    {
                        case 1:
                            return RedirectToAction("CreateStill", "ShortFilms", new { id = s.Id });
                        case 2:
                            return RedirectToAction("CreateAutor", "ShortFilms", new { id = s.Id });
                        case 3:
                            return RedirectToAction("CreateVideo", "ShortFilms", new { id = s.Id });
                        case 4:
                            return RedirectToAction("CloseVideo", "ShortFilms", new { id = s.Id });
                    }
                }
            }

            return null;
        }
        #endregion
    }
}
﻿using ECO._2015.Models;
using ECO._2015.Models.App;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Bussiness
{
    public class ControllerBussines : MsgBussines
    {
        protected MailSenderBusiness _MailSenderBusiness = new MailSenderBusiness();
        protected DBContextApp db = new DBContextApp();
        private ApplicationDbContext dbUsers = new ApplicationDbContext();
        #region UserActive

        private UserManager<ApplicationUser> _UserManager = null;
        private ApplicationUser _UserActive = null;
        protected ApplicationUser UserActive 
        {
            get {
                if (_UserManager == null)
                {
                    _UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(dbUsers)); 
                }

                if (_UserActive == null)
                {
                    _UserActive = _UserManager.FindByEmailAsync(User.Identity.Name).Result;
                }

                return _UserActive; 
            }
        }

        protected AspNetUsers UserActiveDb
        {
            get {
                return db.AspNetUsers.Find(UserActive.Id);
            }
        }


        #endregion

        #region Validations

        protected ActionResult ValidationExistEntrevista()
        {
            var _result = 0;

            if (UserActive != null && User.IsInRole("participante"))
            {
                _result = UserActive.Entrevista.Where(e => e.Edicion == EdicionFestival).ToList().Count;
            }

            return (_result <= 0) ? RedirectToAction("Entrevista", "ShortFilms", new { userId = UserActive.Id }) : null;
        }

        public bool IsCloseConvocatoria
        {
            get
            {
                //return !IsValidDate;
                return  IsRegisterEco() || ! IsValidDate;
                //return false;
            }
        }

        protected ActionResult ValidationCloseConvocatoria(
            string Action = "RegisterClose",
            string Controler = "ShortFilms"
            )

        {
            var _IsValidDate = !IsValidDate;
            var _result = db.AppOpenUserRegister.Find(_UserActive.Id);
            return (_result == null && IsCloseConvocatoria) ? RedirectToAction(Action, Controler) : null;
        }

        protected ActionResult ValidAction(
            ref int? id,
            ref AppShortFilm _ShortFilm,
            bool ValidEtapa = true,
            bool ValidConvocatoria = true,
            string Action = "Index",
            string Close = "RegisterClose",
            string Controler = "ShortFilms"
            )
        {
            #region Variables
            if (_ShortFilm == null)
            {
                return RedirectToAction(Action, Controler);
            }

            if (id == null)
            {
                return RedirectToAction(Action, Controler);
            }
            #endregion

            if (ValidConvocatoria)
            {
                ActionResult _CloseConvocatoria = ValidationCloseConvocatoria(Close, Controler);

                if (_CloseConvocatoria != null)
                {
                    return _CloseConvocatoria;
                }

            }
            
            ActionResult _ExistEntrevista = ValidationExistEntrevista();

            if (_ExistEntrevista != null && User.IsInRole("participante"))
            {
                return _ExistEntrevista;
            }

            if (_ShortFilm.Id == 0)
            {
                _ShortFilm = db.AppShortFilm.Find(id);
                if (User.IsInRole("participante") && !User.IsInRole("admin"))
                {
                    if (_ShortFilm.UserId != UserActive.Id)
                    {
                        throw new Exception("OP Invalid UerActive");
                    }
                }
            }

            if (_ShortFilm.Etapa >= EtapaRegistroFin && ValidEtapa)
            {
                return RedirectToAction(Action, Controler);
            }

            

            return null;
        }

        #endregion

        #region IsRegisterEco
        protected bool IsRegisterEco()
        {
            var _result = db.AppOpenUserRegister.Find(_UserActive.Id);
            return (_result == null);
            //return false;

        }
        #endregion

        #region Dispose
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        #endregion
        /*------------------------------------------------*/
        /*------------------------------------------------*/
        /*------------------------------------------------*/
    }
}
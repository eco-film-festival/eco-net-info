﻿using ECO._2015.Models;
using ECO._2015.Models.ViewsModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoreLinq;
using System.Web.Mvc;
using ECO._2015.Models.App;

namespace ECO._2015.Bussiness
{
    public class AdminFilmsBusssines : JuradoBussines
    {
        #region ListShortFilm
        private List<AppShortFilm> _ListShortFilm = null;
        public List<AppShortFilm> ListShortFilm
        {
            get
            {

                if (_ListShortFilm == null)
                {
                    _ListShortFilm = db.AppShortFilm.ToList();
                }

                return _ListShortFilm;
            }
        }

        public List<AppShortFilm> ListShortFilmSeleccionOficial
        {
            get
            {
                return db.AppShortFilm
                    .Where(e => e.Edicion == EdicionFestival)
                    .Where(e => e.Preseleccion)
                    //.Where(e => e.Sel_Oficial)
                    .ToList();
            }
        }


        //public int CountListShortFilmHist
        //{
        //    get
        //    {
        //        return ListShortFilm.Count;
        //    }
        //}

        //public int CountListShortFilmEdicion
        //{
        //    get
        //    {
        //        return ListShortFilm
        //            .Where(s => s.Edicion == EdicionFestival)
        //            .ToList()
        //            .Count;
        //    }
        //}

        #endregion

        #region List public
        public List<object> ListCategoria
        {
            get {

                var _result = new List<object>();

                _result.Add(new { Id = -1, Categoria = "-----------" });

                ListShortFilm.DistinctBy(s => s.CategoriaId).ToList()
                    .ForEach(s => _result.Add(new { Id = s.CategoriaId, Categoria = s.AppCategoria.Categoria_ES }));
                return _result;

            }
        }

        public List<object> ListPais
        {
            get
            {
                var _result = new List<object>();

                _result.Add(new { Id = -1 , Pais = "-----------" });

                ListShortFilm.DistinctBy(s => s.AppEstado.AppPais.Id).ToList()
                    .ForEach(s => _result.Add(new { Id = s.AppEstado.AppPais.Id, Pais = s.AppEstado.AppPais.Pais_ES }));
                return _result;
            }
        }

        public List<object> ListEtapa
        {
            get
            {
                var _result = new List<object>();

                _result.Add(new { Id = -1, Etapa = "-----------" });

                ListShortFilm.DistinctBy(s => s.Etapa).ToList()
                    .ForEach(s => _result.Add(new { Id = s.Etapa, Etapa = s.Etapa }));
                return _result;
            }
        }


        public List<object> ListEtapaEdit
        {
            get
            {
                var _result = new List<object>();

                _result.Add(new { Id = 2, Etapa = "DATOS GENERALES" });
                _result.Add(new { Id = 2, Etapa = "IMAGEN O CARTEL" });
                _result.Add(new { Id = 3, Etapa = "FOTOGRAFÍA DEL DIRECTOR " });
                _result.Add(new { Id = 4, Etapa = "CORTOMETRAJE " });
                _result.Add(new { Id = 5, Etapa = "RESUMEN (CERRADO) " });
                return _result;
            }
        }

        public List<object> ListEdicion
        {
            get
            {
                var _result = new List<object>();

                _result.Add(new { Id = -1, Edicion = "-----------" });

                ListShortFilm.DistinctBy(s => s.Edicion).ToList()
                    .ForEach(s => _result.Add(new { Id = s.Edicion, Edicion = s.Edicion }));
                return _result;
            }
        }

        public List<object> ListEnvio {
            get {
                var _result = new List<object>();

                _result.Add(new { Id = -1, Envio = "-----------" });
                _result.Add(new { Id = 1, Envio = "SI" });
                _result.Add(new { Id = 2, Envio = "NO" });

                return _result;
            }
        }

        public List<object> ListNacionalidad
        {
            get
            {
                var _result = new List<object>();

                _result.Add(new { Id = -1, Envio = "-----------" });
                _result.Add(new { Id = 1, Envio = "Nacional" });
                _result.Add(new { Id = 2, Envio = "Internacional" });

                return _result;
            }
        }

        public List<object> ListOperations
        {
            get {
                var _result = new List<object>();
                _result.Add(new { Id = -1, Operation = "-----------" });
                _result.Add(new { Id = 0, Operation = "Activar" });
                _result.Add(new { Id = 1, Operation = "Preselección" });
                _result.Add(new { Id = 2, Operation = "Selección Oficial y Preselección" });
                _result.Add(new { Id = 3, Operation = "Selección Oficial y SIN Preselección" });
                //_result.Add(new { Id = 3, Operation = "Ganador" });
                _result.Add(new { Id = 4, Operation = "Desactivar (Eliminar)" });
                return _result; 
            }
        }


        #endregion

    
        protected AdminFilmsResultSimpleViewModel GetModelShortFilmFilter(AdminFilmsFilterViewModel _filter, List<AppShortFilm> _list)
        {
            var _result = GetShortFilmFilter(_filter, _list);

            return new AdminFilmsResultSimpleViewModel()
            {
                ListShortFilmsResutl = _result,
                CountListShortFilmResult = _result.Count,
                CountListShortFilm = _list.Count,
                CountListShortFilmEdicion = _list.Where(s => s.Edicion == EdicionFestival).Count(),
                Filter = _filter,
            };
        }
        protected AdminFilmsResultJuradoViewModel GetModelShortFilmJuradoFilter(AdminFilmsFilterViewModel _filter, List<AppShortFilm> _list)
        {
            //------------------------------------
            var _listShortFilm = GetShortFilmFilter(_filter, _list);
            var _result = new List<AppShortFilmsResultViewModel>();
            _listShortFilm.ForEach(e => _result.Add(new AppShortFilmsResultViewModel(e)));
            //------------------------------------
            return new AdminFilmsResultJuradoViewModel()
            {
                ListShortFilmsResutl = _result,
                CountListShortFilmResult = _listShortFilm.Count,
                CountListShortFilm = _list.Count,
                CountListShortFilmEdicion = _list.Where(s => s.Edicion == EdicionFestival).Count(),
                Filter = _filter,
            };
        }
        protected List<AppShortFilm> GetShortFilmFilter(AdminFilmsFilterViewModel _filter, List<AppShortFilm> _list)
        {
            var _result = new List<AppShortFilm>();

            if (_filter != null)
            {
                _result = _list
                   .Where(s => (_filter.CategoriaId != -1) ? s.CategoriaId == _filter.CategoriaId : true)
                    // -----------------------------
                   .Where(s => (_filter.NacionalidadId != -1) ? s.AppEstado.AppPais.Nacionalidad_Id == _filter.NacionalidadId : true)
                    // -----------------------------
                   .Where(s => (_filter.PaisId != -1) ? s.AppEstado.AppPais.Id == _filter.PaisId : true)
                    // -----------------------------
                   .Where(s => (_filter.Etapa != -1) ? s.Etapa == _filter.Etapa : true)
                    // -----------------------------
                   .Where(s => (_filter.Edicion != -1) ? s.Edicion == _filter.Edicion : true)
                    // -----------------------------
                   .Where(s => (_filter.Envio != -1) ? s.Envio_Correo_Video == (_filter.Envio == 1 ? true : false) : true)
                    // -----------------------------
                   //.Where(s => s.Preseleccion == _filter.Preseleccion)
                   //.Where(s => s.Sel_Oficial == _filter.Sel_Oficial)
                   //.Where(s => s.Ganador == _filter.Ganador)
                   .Where(s => s.Activo == _filter.Activo)
                    // -----------------------------
                   .ToList()
                   ;


                if (_filter.Preseleccion)
                {
                    _filter.Sel_Oficial = false;
                    _filter.Ganador = false;
                    _result = _result.Where(e => e.Preseleccion).ToList();
                }

                else if (_filter.Sel_Oficial)
                {
                    _filter.Preseleccion = false;
                    _filter.Ganador = false;
                    _result = _result.Where(e => e.Sel_Oficial).ToList();
                }

                else if (_filter.Ganador)
                {
                    _filter.Preseleccion = false;
                    _filter.Sel_Oficial = false;
                    _result = _result.Where(e => e.AppGanador.Count > 0 ).ToList();
                }

            }
            return _result;
        }

        #region SetViewBagIndex
        protected void SetViewBagIndex(AdminFilmsFilterViewModel _filter)
        {
            ViewBag.CategoriaId = new SelectList(ListCategoria, "Id", "Categoria", _filter.CategoriaId);
            ViewBag.PaisId = new SelectList(ListPais, "Id", "Pais", _filter.PaisId);
            ViewBag.Etapa = new SelectList(ListEtapa, "Id", "Etapa", _filter.Etapa);
            ViewBag.Edicion = new SelectList(ListEdicion, "Id", "Edicion", _filter.Edicion);
            ViewBag.Envio = new SelectList(ListEnvio, "Id", "Envio", _filter.Envio);
            ViewBag.NacionalidadId = new SelectList(ListNacionalidad, "Id", "Envio", _filter.NacionalidadId);

        }
        #endregion 

        #region SendMessage
        protected const string templateNotifiacionPreseleccion = "_NotificationPreseleccion";
        protected const string templateNotifiacionSeleciconOficial = "_NotificationSeleccionOficial";
        protected void SendMailPreseleccion(AppShortFilm _ShortFilm)
        {
            _MailSenderBusiness.SendMail(
                this ,
                templateNotifiacionPreseleccion,
                PreseleccionMailSubject ,
                PreseleccionMailMsgEsp ,
                PreseleccionMailMsgIng ,
                _ShortFilm,
                EdicionFestival
                );
        }

        protected void SendMailSeleccionOficial(AppShortFilm _ShortFilm)
        {
            _MailSenderBusiness.SendMail(
                this,
                templateNotifiacionSeleciconOficial,
                SelOficialMailSubject,
                SelOficialMailMsgEsp,
                SelOficialMailMsgIng,
                _ShortFilm,
                EdicionFestival
                );
        }
        #endregion

        #region Estatus
        protected int GetEstatusShortFilm(AppShortFilm _ShortFilm)
        {
            if (!_ShortFilm.Activo) return 4;
            if (_ShortFilm.Ganador) return 3;
            if (_ShortFilm.Sel_Oficial) return 2;
            if (_ShortFilm.Preseleccion) return 1;
            return 0;
        }
        #endregion

    }
}
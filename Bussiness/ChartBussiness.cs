﻿using ECO._2015.Models.Estadistica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ECO._2015.Bussiness
{
    public class ChartBussiness
    {
        public ChartBussiness(int? Edicion)
        {
            using (var db = new DBContextEstadistica())
            {
                // ---------------------------
                ChartSeriesViewModel _ChartSeries = null;
                ChartSimpleViewModel _ChartSimple = null;
                string Type = string.Empty;
                string Group = string.Empty;
                // ---------------------------
                switch (Edicion)
                {
                    case 2014:
                        db.v_Estadisitica_Festival_2014
                            .OrderBy(e => e.Id)
                            .ToList()
                            .ForEach(e => CreateChart(e, ref _ChartSeries, ref _ChartSimple, ref Type, ref Group, ref _listChartSimple))
                            ;
                        break;
                    case 2015:
                        db.v_Estadisitica_Festival_2015
                            .OrderBy(e => e.Id)
                            .ToList()
                            .ForEach(e => CreateChart(e, ref _ChartSeries, ref _ChartSimple, ref Type, ref Group, ref _listChartSimple))
                            ;
                        break;
                    default:
                        db.v_Estadisitica_Festival
                            .OrderBy(e => e.Id)
                            .ToList()
                            .ForEach(e => CreateChart(e, ref _ChartSeries, ref _ChartSimple, ref Type, ref Group, ref _listChartSimple))
                            ;
                        break;
                }

                if (_ChartSimple != null) _ChartSeries.Add(_ChartSimple);
                if (_ChartSeries != null) _listChartSimple.Add(_ChartSeries);

                int orden = 0;
                ChartGroupViewModel _ChartGroup = null;
                _listChartSimple.ToList().ForEach(c => CreateGroupChart(c, ref _listChartGroup, ref orden, ref _ChartGroup));
                // ---------------------------
            }
        }

        private void CreateGroupChart(ChartSimpleViewModel c, ref List<ChartGroupViewModel> _listChartGroup, ref int orden, ref ChartGroupViewModel _ChartGroup)
        {
            if (orden != c.order)
            {
                orden = c.order;
                _ChartGroup = new ChartGroupViewModel() { Orden = c.order, Titulo = c.Titulo };
                _listChartGroup.Add(_ChartGroup);
            }
            if (_ChartGroup != null)
            {
                _ChartGroup.ListReport.Add(c);
            }
        }

        private void CreateChart(
            EstadisiticaViewModel e,
            ref ChartSeriesViewModel _ChartSeries,
            ref ChartSimpleViewModel _ChartSimple,
            ref string Type,
            ref string Group,
            ref List<ChartSimpleViewModel> _result
            )
        {
            if (!string.Equals(Type, e.Type))
            {
                Type = e.Type;
                if (_ChartSimple != null) _ChartSeries.Add(_ChartSimple);
                _ChartSimple = new ChartSimpleViewModel() { title = e.Type, type = e.TypeChart, group = e.Group, Titulo = e.Titulo };
            }

            if (string.Equals(Type, e.Type))
            {
                _ChartSimple.Add(new ChartValueViewModel() { key = e.Key, value = (double)e.Total });
            }

            if (!string.Equals(Group, e.Group))
            {
                Group = e.Group;
                if (_ChartSeries != null) _result.Add(_ChartSeries);
                _ChartSeries = new ChartSeriesViewModel() { title = e.Group, type = e.TypeChart, group = e.Group, unidad = e.Unidad, order = e.Orden, Titulo = e.Titulo };
            }
        }

        private List<ChartSimpleViewModel> _listChartSimple = new List<ChartSimpleViewModel>();

        private List<ChartGroupViewModel> _listChartGroup = new List<ChartGroupViewModel>();
        public List<ChartGroupViewModel> ListReport { get { return _listChartGroup; } }

    }

    #region ViewModel

    public partial class EstadisiticaViewModel
    {
        public static implicit operator EstadisiticaViewModel(v_Estadisitica_Festival _entity)
        {
            return new EstadisiticaViewModel
            {
                Id = _entity.Id,
                Key = _entity.Key,
                Total = _entity.Total,
                Type = _entity.Type,
                Group = _entity.Group,
                TypeChart = _entity.TypeChart,
                Orden = _entity.Orden,
                Unidad = _entity.Unidad,
                Titulo = _entity.Titulo,
            };
        }
        // ------------------------------
        // ------------------------------
        // ------------------------------
        public static implicit operator EstadisiticaViewModel(v_Estadisitica_Festival_2014 _entity)
        {
            return new EstadisiticaViewModel
            {
                Id = _entity.Id,
                Key = _entity.Key,
                Total = _entity.Total,
                Type = _entity.Type,
                Group = _entity.Group,
                TypeChart = _entity.TypeChart,
                Orden = _entity.Orden,
                Unidad = _entity.Unidad,
                Titulo = _entity.Titulo,
            };
        }
        // ------------------------------
        // ------------------------------
        // ------------------------------
        public static implicit operator EstadisiticaViewModel(v_Estadisitica_Festival_2015 _entity)
        {
            return new EstadisiticaViewModel
            {
                Id = _entity.Id,
                Key = _entity.Key,
                Total = _entity.Total,
                Type = _entity.Type,
                Group = _entity.Group,
                TypeChart = _entity.TypeChart,
                Orden = _entity.Orden,
                Unidad = _entity.Unidad,
                Titulo = _entity.Titulo,
            };
        }
        // ------------------------------
        // ------------------------------
        // ------------------------------
        #region Attr
        public long? Id { get; set; }

        public string Key { get; set; }

        public int? Total { get; set; }

        public string Type { get; set; }

        public string Group { get; set; }

        public string TypeChart { get; set; }

        public int Orden { get; set; }

        public string Unidad { get; set; }

        public string Titulo { get; set; }
        #endregion
    }

    public class ChartValueViewModel
    {
        public string key { get; set; }
        public double value { get; set; }
    }
    public class ChartSimpleViewModel
    {
        public string Titulo { get; set; }
        public string title { get; set; }
        public string unidad { get; set; }
        public List<string> labels { get; set; }
        public string type { get; set; }
        public string group { get; set; }
        public int order { get; set; }
        public ChartSimpleViewModel()
        {
            labels = new List<string>();
        }
        public void Add(ChartValueViewModel r)
        {
            labels.Add(r.key);
            _data.Add(r.value);
        }

        private List<object> _data = new List<object>();
        public virtual List<object> data { get { return _data; } }
        public virtual List<string[]> display
        {
            get
            {
                List<string[]> _r = new List<string[]>();
                _r.Add(new string[2] { "doughnut", "Dona" });
                _r.Add(new string[2] { "pie", "Pay" });
                _r.Add(new string[2] { "polar-area", "Polar" });
                _r.Add(new string[2] { "line", "Lineal" });
                _r.Add(new string[2] { "bar", "Barras" });
                _r.Add(new string[2] { "radar", "Radar" });
                return _r;
            }
        }
        public virtual bool isSimple { get { return true; } }
        public List<string> series { get; protected set; }
        public List<object> dataSeries { get; protected set; }
    }
    public class ChartSeriesViewModel : ChartSimpleViewModel
    {
        public ChartSeriesViewModel()
        {
            labels = new List<string>();
            series = new List<string>();
            dataSeries = new List<object>();
        }
        public override bool isSimple { get { return (series.Count <= 1); } }
        public override List<string[]> display
        {
            get
            {
                if (series.Count <= 1)
                {
                    return base.display;
                }
                else
                {
                    List<string[]> _r = new List<string[]>();
                    _r.Add(new string[2] { "line", "Lineal" });
                    _r.Add(new string[2] { "bar", "Barras" });
                    _r.Add(new string[2] { "radar", "Radar" });
                    return _r;
                }
            }
        }
        public void Add(ChartSimpleViewModel _chart)
        {
            dataSeries.Add(_chart.data);
            series.Add(_chart.title);
            if (labels.Count <= 0)
            {
                _chart.labels.ForEach(l => labels.Add(l));
            }
        }
        public override List<object> data
        {
            get
            {
                if (dataSeries.Count <= 1)
                {
                    return (List<object>)dataSeries.First();
                }
                else
                {
                    return base.data;
                }
            }
        }

    }

    public class ChartGroupViewModel
    {
        public ChartGroupViewModel()
        {
            ListReport = new List<ChartSimpleViewModel>();
        }
        public List<ChartSimpleViewModel> ListReport { get; set; }

        public int Orden { get; set; }

        public string Titulo { get; set; }
    }

    #endregion
}
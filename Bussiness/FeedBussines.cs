﻿using BlackBayou.Vimeo.Api;
using BlackBayou.Vimeo.Api.Albums.Responses;
using BlackBayou.Vimeo.Api.Videos.Responses;
using BlackBayou.Vimeo.Model;
using BlackBayou.Vimeo.OAuth;
using Facebook;
using Skybrud.Social.Facebook;
using Skybrud.Social.Facebook.Fields;
using Skybrud.Social.Facebook.OAuth;
using Skybrud.Social.Facebook.Objects.Posts;
using Skybrud.Social.Facebook.Options.Posts;
using Skybrud.Social.Facebook.Responses.Posts;
using Skybrud.Social.Instagram;
using Skybrud.Social.Instagram.Objects;
using Skybrud.Social.Instagram.Options.Users;
using Skybrud.Social.Instagram.Responses;
using Skybrud.Social.Twitter.Entities;
using Skybrud.Social.Twitter.OAuth;
using Skybrud.Social.Twitter.Objects;
using Skybrud.Social.Twitter.Options;
using Skybrud.Social.Twitter.Responses;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using static BlackBayou.Vimeo.Api.Videos.VideosApiClient;

namespace ECO._2015.Bussiness
{
    public class FeedBussines
    {
        #region private
        //----------------------
        private string FacebookPage = "EcoFilmFestivalMX";
        private string FacebookAppId = "270352256469767";
        private string FacebookAppKey = "b1cf0caf485d2e1e3dfe2bd29649e42d";

        private string TwitterPage = "ecofilmfestival";
        private string TwitterPageConsumerKey = "HqvriTVhvsoMCoC6ORNgw";
        private string TwitterConsumerSecrete = "DHBRd7tMeovm0DCmg89lDyI6Im1QXhMJXFOOvKsG0";
        private string TwitterOAuthToken = "224079331-PcUwJ8GV2lI6aEBvAXuHmahByajfEukkBAjfxMIE";
        private string TwitterOAuthTokenSecret = "VAjE0GyLS65JHZX9FU8vrKMOiixZyoy0uZvzsJehgHU";

        private string ConsumerKey = "ba46d4b1408e61c2c09cea5fa243c3f2ddb30f2c";
        private string ConsumerSecret = "25919b26b63c2bd3180ed23ef5824b08584f70a8";
        private string AccessToken = "30e121693ad9b5ad987e32e4a5fac804";
        private string AccessTokenSecret = "fdd470d660b644a37e469b4d4e2c778d7bb7fd22";
        private string Username = "user4864531";

        //private string InstagramQuery = "193153741";
        //private string InstagramClientId = "dc5f262aee6c4c41873c854901b0bda0";
        private string accessTokenInstagram = "193153741.6ea9401.34080672ac3b4030a40520487423d871";
        #endregion
        // -------------------------------
        // -------------------------------
        // -------------------------------
        public ResultFeed GetFeed()
        {
            return GetFeed(new FilterFeedViewModel
            {
                MaxId = null,
                NextToken = null,
                Until = null,
                MaxIdInstagram = null,
                MaxIdVimeo = null
            });
        }
        // -------------------------------
        // -------------------------------
        // -------------------------------
        public ResultFeed GetFeed(int _maximoFeedItem)
        {
            return GetFeed(new FilterFeedViewModel
            {
                MaxId = null,
                NextToken = null,
                Until = null,
                MaxIdInstagram = null,
                MaxIdVimeo = null
            }, _maximoFeedItem);
        }
        // -------------------------------
        // -------------------------------
        // -------------------------------
        public ResultFeed GetFeed(FilterFeedViewModel _model)
        {
            // --------------------------------
            if (_model == null)
            {
                _model = new FilterFeedViewModel
                {
                    MaxId = null,
                    NextToken = null,
                    Until = null,
                    MaxIdInstagram = null,
                    MaxIdVimeo = null
                };
            }
            // --------------------------------
            var _feed = new List<PostViewModel>();
            // --------------------------------
            var _FeedVimeo = new VimeoHelper(ConsumerKey, ConsumerSecret, AccessToken, AccessTokenSecret, Username);
            var _FeedInstagram = new InstagramHelper(accessTokenInstagram);
            // var _FeedFacebook = new FacebookHelper(FacebookAppId, FacebookAppKey, FacebookPage);
            var _FeedFacebook = new FacebookHelperOld(FacebookAppId, FacebookAppKey, FacebookPage);
            var _FeedTwitter = new TwitterHelper(TwitterPageConsumerKey, TwitterConsumerSecrete, TwitterOAuthToken, TwitterOAuthTokenSecret, TwitterPage);
            // --------------------------------
            var _ResultFacebook = _FeedFacebook.GetFeed(_model.NextToken, _model.Until.ToString());
            var _ResultTwitter = _FeedTwitter.GetFeed(_model.MaxId);
            var _ResultInstagram = _FeedInstagram.GetFeed(_model.MaxIdInstagram);
            var _ResultVimeo = _FeedVimeo.GetFeed(_model.MaxIdVimeo);
            // --------------------------------
            try
            {
                _ResultFacebook.Feed.ForEach(e => {
                    _feed.Add(e);
                    });
            }
            catch (Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            try
            {
                _ResultTwitter.Feed.ForEach(e => _feed.Add(e));
            }
            catch (Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            try
            {
                _ResultInstagram.Feed.ForEach(e => _feed.Add(e));
            }
            catch (Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            try
            {
                _ResultVimeo.Feed.ForEach(e => _feed.Add(e));
            }
            catch (Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            // --------------------------------
            // --------------------------------
            return new ResultFeed
            {
                Feed = ShuffleList(_feed),
                Filter = new FilterFeedViewModel
                {
                    MaxId = _ResultTwitter.MaxId,
                    Until = _ResultFacebook.Until,
                    NextToken = _ResultFacebook.NextToken,
                    MaxIdInstagram = _ResultInstagram.MaxId,
                    MaxIdVimeo = _ResultVimeo.MaxId
                }
            };
            // --------------------------------
            // -------------------------------- 
        }

        // -------------------------------
        // -------------------------------
        public ResultFeed GetFeed(FilterFeedViewModel _model, int _maximoFeedItem)
        {
            // --------------------------------
            if (_model == null)
            {
                _model = new FilterFeedViewModel
                {
                    MaxId = null,
                    NextToken = null,
                    Until = null,
                    MaxIdInstagram = null,
                    MaxIdVimeo = null
                };
            }
            // --------------------------------
            var _feed = new List<PostViewModel>();
            // --------------------------------
            var _FeedVimeo = new VimeoHelper(ConsumerKey, ConsumerSecret, AccessToken, AccessTokenSecret, Username, _maximoFeedItem);
            var _FeedInstagram = new InstagramHelper(accessTokenInstagram, _maximoFeedItem);
            // var _FeedFacebook = new FacebookHelper(FacebookAppId, FacebookAppKey, FacebookPage, _maximoFeedItem);
            var _FeedFacebook = new FacebookHelperOld(FacebookAppId, FacebookAppKey, FacebookPage, _maximoFeedItem);
            var _FeedTwitter = new TwitterHelper(TwitterPageConsumerKey, TwitterConsumerSecrete, TwitterOAuthToken, TwitterOAuthTokenSecret, TwitterPage, _maximoFeedItem);
            // --------------------------------
            var _ResultFacebook = _FeedFacebook.GetFeed(_model.NextToken, _model.Until.ToString());
            var _ResultTwitter = _FeedTwitter.GetFeed(_model.MaxId);
            var _ResultInstagram = _FeedInstagram.GetFeed(_model.MaxIdInstagram);
            var _ResultVimeo = _FeedVimeo.GetFeed(_model.MaxIdVimeo);
            // --------------------------------
            try
            {
                _ResultFacebook.Feed.ForEach(e => _feed.Add(e));
            }catch(Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            try
            {
                _ResultTwitter.Feed.ForEach(e => _feed.Add(e));
            }
            catch (Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            try
            {
                _ResultInstagram.Feed.ForEach(e => _feed.Add(e));
            }
            catch (Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            try
            {
                _ResultVimeo.Feed.ForEach(e => _feed.Add(e));
            }
            catch (Exception ex)
            {
                var excepcion = ex.StackTrace;
            }
            // --------------------------------
            // --------------------------------
            return new ResultFeed
            {
                Feed = ShuffleList(_feed),
                Filter = new FilterFeedViewModel
                {
                    MaxId = _ResultTwitter.MaxId,
                    Until = _ResultFacebook.Until,
                    NextToken = _ResultFacebook.NextToken,
                    MaxIdInstagram = _ResultInstagram.MaxId,
                    MaxIdVimeo = _ResultVimeo.MaxId
                }
            };
            // --------------------------------
            // -------------------------------- 
        }

        private List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            Random r = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList; //return the new random list
        }

    }

    #region FeedHelper
    public class FeedHelper
    {
        protected object getValue(IDictionary<string, object> _postDetail, string key)
        {
            return !(_postDetail.ContainsKey(key)) ? "" : _postDetail[key];
        }

    }
    #endregion

    #region InstagramHelper
    public class InstagramHelper : FeedHelper
    {
        #region attr
        private InstagramService service { get; set; }
        private int maximoFeedItems = int.Parse(ConfigurationManager.AppSettings["MaximoFeedItems"]);
        #endregion

        public InstagramHelper(string accessToken)
        {
            service = InstagramService.CreateFromAccessToken(accessToken);
        }

        public InstagramHelper(string accessToken, int _maximoFeedItem)
        {
            maximoFeedItems = _maximoFeedItem;
            service = InstagramService.CreateFromAccessToken(accessToken);
        }

        public ResultInstagram GetFeed(string _MaxId)
        {
            InstagramUserRecentMediaOptions options = new InstagramUserRecentMediaOptions
            {
                Count = maximoFeedItems,
            };

            if (_MaxId != null)
            {
                options.MaxId = _MaxId;
            }

            InstagramRecentMediaResponse _response = service.Users.GetRecentMedia(options);
            List<InstagramMedia> _instas = new List<InstagramMedia>();
            _instas.AddRange(_response.Body.Data);
            List<PostViewModel> _feed = new List<PostViewModel>();
            foreach (InstagramMedia _insta in _instas)
            {
                _feed.Add(new PostViewModel
                {
                    Message = _insta.CaptionText,
                    Link = _insta.Link,
                    Update = _insta.Date.ToString(),
                    Image = _insta.Standard,
                    TypePost = TypePost.Instagram,
                    Icon = "icon ion-social-instagram calm",
                });
            }

            return new ResultInstagram { Feed = _feed, MaxId = _instas.Last().Id };
        }
    }
    #endregion

    #region TwitterHekper
    public class TwitterHelper : FeedHelper
    {
        #region attr
        private Skybrud.Social.Twitter.TwitterService service { get; set; }
        private int maximoFeedItems = int.Parse(ConfigurationManager.AppSettings["MaximoFeedItems"]);
        private string TwitterPage { get; set; }
        #endregion
        public TwitterHelper(string _TwitterPageConsumerKey, string _TwitterConsumerSecrete, string _TwitterOAuthToken, string _TwitterOAuthTokenSecret, string _TwitterPage)
        {
            try
            {
                TwitterPage = _TwitterPage;
                TwitterOAuthClient TwClient = new TwitterOAuthClient
                {
                    ConsumerKey = _TwitterPageConsumerKey,
                    ConsumerSecret = _TwitterConsumerSecrete,
                    Token = _TwitterOAuthToken,
                    TokenSecret = _TwitterOAuthTokenSecret
                };
                service = Skybrud.Social.Twitter.TwitterService.CreateFromOAuthClient(TwClient);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public TwitterHelper(string _TwitterPageConsumerKey, string _TwitterConsumerSecrete, string _TwitterOAuthToken, string _TwitterOAuthTokenSecret, string _TwitterPage, int _maximoFeedItem)
        {
            try
            {
                TwitterPage = _TwitterPage;
                maximoFeedItems = _maximoFeedItem;
                TwitterOAuthClient TwClient = new TwitterOAuthClient
                {
                    ConsumerKey = _TwitterPageConsumerKey,
                    ConsumerSecret = _TwitterConsumerSecrete,
                    Token = _TwitterOAuthToken,
                    TokenSecret = _TwitterOAuthTokenSecret
                };
                service = Skybrud.Social.Twitter.TwitterService.CreateFromOAuthClient(TwClient);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultTwitter GetFeed(long? _MaxId)
        {
            TwitterUserTimelineOptions options = new TwitterUserTimelineOptions
            {
                Count = maximoFeedItems,
                ScreenName = TwitterPage
            };

            if (_MaxId != null)
            {
                options.MaxId = _MaxId ?? 0;
            }

            TwitterTimelineResponse _response = service.Statuses.GetUserTimeline(options);
            List<TwitterStatusMessage> _tweets = new List<TwitterStatusMessage>();
            _tweets.AddRange(_response.Body);
            List<PostViewModel> _feed = new List<PostViewModel>();
            foreach (TwitterStatusMessage _tweet in _tweets)
            {
                List<TwitterMediaEntity> _mediaList = new List<TwitterMediaEntity>();
                _mediaList.AddRange(_tweet.Entities.Media);
                string _mediaUrl = _mediaList.Count != 0 ? _mediaList.First().MediaUrl : null;
                _feed.Add(new PostViewModel
                {
                    Message = _tweet.Text,
                    Link = string.Format("https://twitter.com/EcofilmFestival/status/{0}", _tweet.Id),
                    Update = _tweet.CreatedAt.ToString(),
                    Image = _mediaUrl,
                    TypePost = TypePost.Twitter,
                    Icon = "icon ion-social-twitter calm",
                });

            }

            return new ResultTwitter { Feed = _feed, MaxId = _tweets.Last().Id };
        }

    }
    #endregion

    #region FacebookHelper
    public class FacebookHelper : FeedHelper
    {
        #region attrs
        public FacebookService service { get; private set; }
        public string FacebookAppId { get; private set; }
        public string FacebookAppKey { get; private set; }
        public string FacebookPage { get; private set; }
        public string AccessToken { get; private set; }

        private int maximoFeedItems = int.Parse(ConfigurationManager.AppSettings["MaximoFeedItems"]);
        #endregion
        public FacebookHelper(string _FacebookAppId, string _FacebookAppKey, string _FacebookPage)
        {
            //------------------------------
            FacebookAppId = _FacebookAppId;
            FacebookAppKey = _FacebookAppKey;
            FacebookPage = _FacebookPage;
            //------------------------------
            FacebookOAuthClient FbClient = new FacebookOAuthClient
            {
                AppId = _FacebookAppId,
                AppSecret = _FacebookAppKey
            };
            //------------------------------
            string accessToken = FbClient.GetAppAccessToken();
            service = FacebookService.CreateFromAccessToken(accessToken);
        }
        
        public FacebookHelper(string _FacebookAppId, string _FacebookAppKey, string _FacebookPage, int _maximoFeedItem)
        {
            //------------------------------
            FacebookAppId = _FacebookAppId;
            FacebookAppKey = _FacebookAppKey;
            FacebookPage = _FacebookPage;
            maximoFeedItems = _maximoFeedItem;
            //------------------------------
            FacebookOAuthClient FbClient = new FacebookOAuthClient
            {
                AppId = _FacebookAppId,
                AppSecret = _FacebookAppKey
            };
            //------------------------------
            service = FacebookService.CreateFromOAuthClient(FbClient);
        }

        public ResultViewModelFacebook GetFeed(string PagingToken, int? Until)
        {
            FacebookPostsResponse _response = service.Posts.GetPosts(getFacebookPostsOptions(PagingToken, Until));
            List<FacebookPost> _posts = new List<FacebookPost>();
            FacebookPostsCollection _postsCollection = _response.Body;
            var _netxlink = _postsCollection.Paging.Next;
            Uri _Uri = new Uri(_netxlink);
            var _netxToken = HttpUtility.ParseQueryString(_Uri.Query).Get("__paging_token");
            var _until = _postsCollection.Paging.Until;
            _posts.AddRange(_postsCollection.Data);
            List<PostViewModel> _feed = new List<PostViewModel>();
            _posts.ForEach(_post =>
            {
                _feed.Add(new PostViewModel
                {
                    Message = _post.Message,
                    Link = _post.Link,
                    Update = _post.UpdatedTime.ToString(),
                    Image = _post.Picture,
                    TypePost = TypePost.Facebook,
                    Icon = "icon ion-social-facebook calm",
                });
            });
            return new ResultViewModelFacebook { Feed = _feed, NextToken = _netxToken, NextLink = _netxlink, Until = _until };
        }
        private FacebookGetPostsOptions getFacebookPostsOptions(string PagingToken, int? Until)
        {
            FacebookFieldsCollection fields = new FacebookFieldsCollection();
            fields.Add(new FacebookField("message"));
            fields.Add(new FacebookField("picture"));
            fields.Add(new FacebookField("link"));
            fields.Add(new FacebookField("updated_time"));
            if (string.IsNullOrEmpty(PagingToken))
            {
                return new FacebookGetPostsOptions(FacebookPage)
                {
                    Limit = maximoFeedItems,
                    Fields = fields,
                };
            }

            else if (Until == null)
            {
                return new FacebookGetPostsOptions(FacebookPage)
                {
                    Limit = maximoFeedItems,
                    Fields = fields,
                };
            }
            else
            {
                return new FacebookGetPostsOptions(FacebookPage)
                {
                    Limit = maximoFeedItems,
                    Fields = fields,
                    Until = Until ?? 0,
                };
            }

        }
    }
    #endregion

    #region VimeoHelper
    public class VimeoHelper : FeedHelper
    {
        #region attr
        private VimeoPlusApi service { get; set; }
        private string CuentaUsuario { get; set; }

        private int maximoFeedItems = int.Parse(ConfigurationManager.AppSettings["MaximoFeedItems"]);
        #endregion
        public VimeoHelper(string _ConsumerKey, string _ConsumerSecret, string _AccessToken, string _AccessTokenSecret, string _Username)
        {
            CuentaUsuario = _Username;
            InMemoryTokenManager tokenManager = new InMemoryTokenManager(
                _ConsumerKey,
                _ConsumerSecret,
                new Dictionary<string, string> {
                    {
                        _AccessToken,
                        _AccessTokenSecret
                    }
                }
            );
            service = new VimeoPlusApi(tokenManager, _AccessToken);
        }

        public VimeoHelper(string _ConsumerKey, string _ConsumerSecret, string _AccessToken, string _AccessTokenSecret, string _Username, int _maximoFeedItem)
        {
            maximoFeedItems = _maximoFeedItem;
            CuentaUsuario = _Username;
            InMemoryTokenManager tokenManager = new InMemoryTokenManager(
                _ConsumerKey,
                _ConsumerSecret,
                new Dictionary<string, string> {
                    {
                        _AccessToken,
                        _AccessTokenSecret
                    }
                }
            );
            service = new VimeoPlusApi(tokenManager, _AccessToken);
        }

        public ResultVimeo GetFeed(int? _MaxId)
        {

            int MaxId = _MaxId ?? 1;
            GetAlbumsResponse _responseX = service.Albums.GetAlbums(CuentaUsuario);
            GetAllVideosResponse _response = service.Videos.GetAllVideos(CuentaUsuario, GetVideosSortOrderEnum.Newest, MaxId, maximoFeedItems, GetVideosResponseTypeEnum.Full);
            List<Video> _videos = new List<Video>();
            if(_response.Videos != null)
            {
                _videos.AddRange(_response.Videos.Items);
                List<PostViewModel> _feed = new List<PostViewModel>();
                _videos.ForEach(_video =>
                {
                    string _media = null;
                    foreach (Thumbnail image in _video.Thumbnails.Items)
                    {
                        if (image.Height == 360 && image.Width == 640)
                        {
                            _media = image.Url;
                            break;
                        }
                    };

                    _feed.Add(new PostViewModel
                    {
                        Message = _video.Title,
                        Link = _video.Urls.Items.First().Content,
                        Update = _video.ModifiedDate.ToString(),
                        Image = _media,
                        TypePost = TypePost.Vimeo,
                        Icon = "icon ion-social-vimeo calm",
                    });
                });
                return new ResultVimeo { Feed = _feed, MaxId = MaxId + 1 };
            }

            return new ResultVimeo();
            
            //foreach (InstagramMedia _insta in _instas)
            //{
            //    _feed.Add(new PostViewModel
            //    {
            //        Message = _insta.CaptionText,
            //        Link = _insta.Link,
            //        Update = _insta.Date.ToString(),
            //        Image = _insta.Standard,
            //        TypePost = TypePost.Instagram,
            //        Icon = "icon ion-social-instagram calm",
            //    });
            //}
        }
    }
    #endregion

    #region FacebookHelper
    public class FacebookHelperOld : FeedHelper
    {
        #region attrs
        public FacebookClient FbClient { get; private set; }
        public string FacebookAppId { get; private set; }
        public string FacebookAppKey { get; private set; }
        public string FacebookPage { get; private set; }
        public string AccessToken { get; private set; }
        private int maximoFeedItems = int.Parse(ConfigurationManager.AppSettings["MaximoFeedItems"]);
        #endregion
        public FacebookHelperOld(string _FacebookAppId, string _FacebookAppKey, string _FacebookPage)
        {
            //------------------------------
            FacebookAppId = _FacebookAppId;
            FacebookAppKey = _FacebookAppKey;
            FacebookPage = _FacebookPage;
            //------------------------------
            FbClient = new FacebookClient
            {
                AppId = _FacebookAppId,
                AppSecret = _FacebookAppKey
            };
            //------------------------------
            GetAccessToken();
        }

        public FacebookHelperOld(string _FacebookAppId, string _FacebookAppKey, string _FacebookPage, int _maximoFeedItem)
        {
            //------------------------------
            FacebookAppId = _FacebookAppId;
            FacebookAppKey = _FacebookAppKey;
            FacebookPage = _FacebookPage;
            maximoFeedItems = _maximoFeedItem;
            //------------------------------
            FbClient = new FacebookClient
            {
                AppId = _FacebookAppId,
                AppSecret = _FacebookAppKey
            };
            //------------------------------
            GetAccessToken();
        }

        private void GetAccessToken()
        {
            try
            {
                dynamic result = FbClient.Get("/oauth/access_token", new
                {
                    grant_type = "client_credentials",
                    client_id = FacebookAppId,
                    client_secret = FacebookAppKey,
                });

                AccessToken = result.access_token;
                FbClient.AccessToken = AccessToken;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ResultViewModelFacebook GetFeed(string PagingToken, string Until)
        {
            try
            {
                var _response = new List<PostViewModel>();
                var _netxToken = string.Empty;
                var _netxlink = string.Empty;
                var _until = string.Empty;
                //---------------------------------
                var _query = string.Format("{0}/feed", FacebookPage);
                dynamic _queryResult;

                _queryResult = FbClient.Get(_query, getParamsQuery(PagingToken, Until));

                var _result = (IDictionary<string, object>)_queryResult;
                var _listPost = (IEnumerable<object>)_result["data"];

                foreach (var _post in _listPost)
                {
                    var _postDetail = (IDictionary<string, object>)_post;
                    _response.Add(new PostViewModel
                    {
                        Image = (string)getValue(_postDetail, "picture"),
                        Link = (string)getValue(_postDetail, "link"),
                        Message = (string)getValue(_postDetail, "message"),
                        Update = GetStringFormatDateTime((string)getValue(_postDetail, "updated_time")),
                        TypePost = TypePost.Facebook,
                        Icon = "icon ion-social-facebook positive",
                    });
                }

                var paging = (IDictionary<string, object>)_result["paging"];
                if (paging.ContainsKey("next"))
                {
                    _netxlink = (string)paging["next"];
                    Uri _Uri = new Uri(_netxlink);
                    _netxToken = HttpUtility.ParseQueryString(_Uri.Query).Get("__paging_token");
                    _until = HttpUtility.ParseQueryString(_Uri.Query).Get("until");
                }


                return new ResultViewModelFacebook { Feed = _response, NextToken = _netxToken, NextLink = _netxlink, Until = int.Parse(_until) };
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private string GetStringFormatDateTime(string updated_time)
        {
            DateTime dateParsed = DateTime.Now;
            if (DateTime.TryParseExact(updated_time, "yyyy-MM-dd'T'HH:mm:ssK",
                    System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AdjustToUniversal, out dateParsed))
            {
                return dateParsed.ToString(@"dd/MM/yyyy hh:mm:ss tt", new System.Globalization.CultureInfo("en-US"));
            }

            return "";
        }

        private object getParamsQuery(string PagingToken, string Until)
        {

            if (string.IsNullOrEmpty(PagingToken))
            {
                return new
                {
                    limit = maximoFeedItems,
                    fields = "message,picture,link,updated_time",
                };
            }
            else if (string.IsNullOrEmpty(Until))
            {
                return new
                {
                    limit = maximoFeedItems,
                    fields = "message,picture,link,updated_time",
                    __paging_token = PagingToken,
                };
            }
            else
            {
                return new
                {
                    limit = maximoFeedItems,
                    fields = "message,picture,link,updated_time",
                    __paging_token = PagingToken,
                    until = Until,
                };
            }

        }
    }
    #endregion

    #region Enum
    public enum TypePost
    {
        Facebook = 0,
        Twitter = 1,
        Instagram = 2,
        Vimeo = 3
    }
    #endregion

    #region ViewModels
    public class PostViewModel
    {
        public string Image { get; set; }
        public string Link { get; set; }
        public string Message { get; set; }
        public string Update { get; set; }
        public TypePost TypePost { get; set; }
        public string Icon { get; set; }
    }

    public class ResultViewModel
    {
        public List<PostViewModel> Feed { get; set; }
    }

    public class ResultViewModelFacebook : ResultViewModel
    {

        public string NextToken { get; set; }

        public string NextLink { get; set; }

        public int? Until { get; set; }
    }

    public class ResultTwitter : ResultViewModel
    {
        public long MaxId { get; set; }
    }

    public class ResultVimeo : ResultViewModel
    {
        public int MaxId { get; set; }
    }

    public class ResultInstagram : ResultViewModel
    {
        public string MaxId { get; set; }
    }

    public class ResultFeed : ResultViewModel
    {
        public FilterFeedViewModel Filter { get; set; }
    }

    public class FilterFeedViewModel
    {

        public long? MaxId { get; set; }

        public string MaxIdInstagram { get; set; }

        public int? MaxIdVimeo { get; set; }

        public int? Until { get; set; }

        public string NextToken { get; set; }
    }

    #endregion

}
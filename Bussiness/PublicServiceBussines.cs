﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;

namespace ECO._2015.Bussiness
{
    public class PublicServiceBussines : ApiController
    {
        protected static HttpResponseMessage CreateCSVHttpResponse<T>(List<T> data, string _fileName = "Export.csv")
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent(CreateCSVTextFile(data), Encoding.UTF8);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = _fileName };
            return result;
        }
        protected static string CreateCSVTextFile<T>(List<T> _list, string seperator = ",")
        {
            var properties = typeof(T).GetProperties();
            var result = new StringBuilder();
            result.AppendLine(string.Join(seperator, properties.Select(e => e.Name).ToList()));
            _list.ForEach(row => result.AppendLine(string.Join(seperator, properties.Select(p => CleanChartCSV(p.GetValue(row, null), seperator)))));
            return result.ToString();
        }

        private static string CleanChartCSV(object p, string seperator)
        {
            return (p == null) ? "" : p.ToString().Replace(seperator, "");
        }
        public static int EdicionFestival
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["EdicionFestival"]);
                }
                catch
                {
                    return -1;
                }
            }
        }

        public static int EdicionFestivalApp
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["EdicionFestivalApp"]);
                }
                catch
                {

                }

                return 2015;
            }
        }

        public static int VerProgramaAnnioApp
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["VerProgramaAnnoFestival"]);
                }
                catch
                {

                }

                return 2015;
            }
        }

        public static DateTime VerProgramaFechaApp
        {
            get
            {
                try
                {
                    return new DateTime(
                        int.Parse(ConfigurationManager.AppSettings["VerProgramaAnnoFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["VerProgramaMesFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["VerProgramaDiaFestival"])
                    );
                }
                catch
                {
                    return DateTime.Now;
                }
            }
        }

        public static int EtapaRegistroFin
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["EtapaRegistroFin"]);
                }
                catch
                {

                }

                return 5;
            }
        }

        public static bool isWinnersOpen
        {
            get
            {

                try
                {
                    var _dateOpen = new DateTime(
                        int.Parse(ConfigurationManager.AppSettings["WinnersAnnoFestivalApp"]),
                        int.Parse(ConfigurationManager.AppSettings["WinnersMesFestivalApp"]),
                        int.Parse(ConfigurationManager.AppSettings["WinnersDiaFestivalApp"]),
                        int.Parse(ConfigurationManager.AppSettings["WinnersHrFestivalApp"]),
                        int.Parse(ConfigurationManager.AppSettings["WinnersMinFestivalApp"]),
                        int.Parse(ConfigurationManager.AppSettings["WinnersSegFestivalApp"])
                    );

                    var today = DateTime.Now.AddHours(double.Parse(ConfigurationManager.AppSettings["ServerHr"]));

                    return (today >= _dateOpen);

                }
                catch
                {
                    return false;
                }

            }
        }

        public static bool isOficialSelectionOpen
        {
            get
            {

                try
                {
                    var _dateOpen = new DateTime(
                        int.Parse(ConfigurationManager.AppSettings["OfficialSelectionAnnoFestivalApp"]),
                        int.Parse(ConfigurationManager.AppSettings["OfficialSelectionMesFestivalApp"]),
                        int.Parse(ConfigurationManager.AppSettings["OfficialSelectionsDiaFestivalApp"])
                    );

                    var today = DateTime.Now.AddHours(double.Parse(ConfigurationManager.AppSettings["ServerHr"]));

                    return (today >= _dateOpen);

                }
                catch
                {
                    return false;
                }

            }
        }

        #region conf
        public static string HostImageAutor
        {
            get
            {
                return ConfigurationManager.AppSettings["HostImageAutor"];
            }
        }
        public static string HostImageStill
        {
            get
            {
                return ConfigurationManager.AppSettings["HostImageStill"];
            }
        }
        public static string HostImageJurado
        {
            get
            {
                return ConfigurationManager.AppSettings["HostImageJurado"];
            }
        }

        public static string HostImageMuestra
        {
            get
            {
                return ConfigurationManager.AppSettings["HostImageMuestra"];
            }
        }

        #endregion

        public static int Director {
            get {
                return 1;
            }
        }

        public static int PatrocinadorOficial
        {
            get
            {
                return 2;
            }
        }

        public static int PatrocinadorAupisiadores
        {
            get
            {
                return 1;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ECO._2015.Bussiness
{
    public class MsgBussines : Controller
    {
        public MsgBussines()
            : base()
        {
            ViewBag.CierreMsgEsp = CierreMsgEsp;
            ViewBag.CierreMsgIng = CierreMsgIng;

            ViewBag.EdicionFestivalTitleEsp = EdicionFestivalTitleEsp;
            ViewBag.EdicionFestivalTitleIng = EdicionFestivalTitleIng;

            ViewBag.FestivalTemaEsp = FestivalTemaEsp;
            ViewBag.FestivalTemaIng = FestivalTemaIng;

            //ViewBag.OpenMsgEsp = OpenMsgEsp;
            //ViewBag.OpenMsgIng = OpenMsgIng;
            ViewBag.IsValidDate = IsValidDate;
            ViewBag.DateClose = DateClose;
            ViewBag.EdicionFestival = EdicionFestival;
            ViewBag.DateCloseSting = DateCloseSting;
            ViewBag.DateHourSting = DateHourSting;
            ViewBag.DateCloseFullSting = DateCloseFullSting;

            
            ViewBag.ConfImagesAutor = ConfImagesAutor;
            ViewBag.ConfImagesStills = ConfImagesStills;
            //ViewBag.CierreFechaMsgEsp = CierreFechaMsgEsp;
            //ViewBag.CierreFechaMsgIng = CierreFechaMsgIng;
            ViewBag.EtapaRegistroFin = EtapaRegistroFin;
            ViewBag.MsgBienvenidaJuradoES = MsgBienvenidaJuradoES;
            ViewBag.MsgBienvenidaJuradoEN = MsgBienvenidaJuradoEN;
            ViewBag.MsgCierreJuradoES = MsgCierreJuradoES;
            ViewBag.MsgCierreJuradoEN = MsgCierreJuradoEN;
        }

        protected const string templateNotifiacion = "_Notification";

        #region ConfEdicion
        public int EdicionFestival
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["EdicionFestival"]);
                }
                catch
                {

                }

                return 2015;
            }
        }

        public int ConfImagesAutor
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["ConfImagesAutor"]);
                }
                catch
                {

                }

                return 1;
            }
        }

        public int ConfImagesStills
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["ConfImagesStills"]);
                }
                catch
                {

                }

                return 2;
            }
        }

        public int EtapaRegistroFin
        {
            get
            {
                try
                {
                    return int.Parse(ConfigurationManager.AppSettings["EtapaRegistroFin"]);
                }
                catch
                {

                }

                return 5;
            }
        }

        #endregion

        #region Cierre Convocatoria Valid
        public DateTime DateClose
        {
            get
            {
                try
                {
                    return new DateTime(
                        int.Parse(ConfigurationManager.AppSettings["CierreAnnoFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["CierreMesFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["CierreDiaFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["CierreHoraFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["CierreMinutoFestival"]),
                        0
                    );
                }
                catch
                {
                    return DateTime.Now;
                }
            }
        }

        public string DateCloseSting
        {
            get {
                return DateClose.ToString("dd/MM/yyyy");
            }
        }

        public string DateHourSting
        {
            get
            {
                return DateClose.AddMinutes(-1).ToString("HH:mm");
            }
        }

        public string DateCloseFullSting
        {
            get
            {
                return DateClose.AddMinutes(-1).ToString("dd/MM/yyyy HH:mm");
            }
        }

        public bool IsValidDate
        {
            get
            {
                try
                {
                    //var _dateBefore = new DateTime(2017, 07, 12, 22, 00, 00);
                    return (0 < DateTime.Compare(DateClose, /*_dateBefore*/ DateTime.Now.AddHours(2)));
                }
                catch
                {
                    return false;
                }
            }
        }

        public string RegisterUserMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["RegisterUserMailSubject"];
            }
        }

        public string ResetPasswordUserMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["ResetPasswordUserMailSubject"];
            }
        }


        public string UserAdminMMail
        {
            get
            {
                return ConfigurationManager.AppSettings["UserAdminMMail"];
            }
        }
        
        public string CierreMsgEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["CierreMsgEsp"];
            }
        }
        public string CierreMsgIng
        {
            get
            {
                return ConfigurationManager.AppSettings["CierreMsgIng"];
            }
        }

        public string EdicionFestivalTitleEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["EdicionFestivalTitleEsp"];
            }
        }
        public string EdicionFestivalTitleIng
        {
            get
            {
                return ConfigurationManager.AppSettings["EdicionFestivalTitleIng"];
            }
        }

        public string FestivalTemaEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["FestivalTemaEsp"];
            }
        }
        public string FestivalTemaIng
        {
            get
            {
                return ConfigurationManager.AppSettings["FestivalTemaIng"];
            }
        }

        //public string OpenMsgEsp
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["OpenMsgEsp"];
        //    }
        //}
        //public string OpenMsgIng
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["OpenMsgIng"];
        //    }
        //}

        //public string CierreFechaMsgEsp
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["CierreFechaMsgEsp"];
        //    }
        //}

        //public string CierreFechaMsgIng
        //{
        //    get
        //    {
        //        return ConfigurationManager.AppSettings["CierreFechaMsgIng"];
        //    }
        //}

        public string RegistroMailMsgEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["RegistroMailMsgEsp"];
            }
        }

        public string RegistroMailMsgIng
        {
            get
            {
                return ConfigurationManager.AppSettings["RegistroMailMsgIng"];
            }
        }

        public string RegisterUserMailMsgEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["   "];
            }
        }

        public string RegisterUserMailMsgIng
        {
            get
            {
                return ConfigurationManager.AppSettings["RegisterUserMailMsgIng"];
            }
        }

        public string ResetPasswordUserMailMsgEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["RegisterUserMailMsgEsp"];
            }
        }

        public string ResetPasswordUserMailMsgIng
        {
            get
            {
                return ConfigurationManager.AppSettings["RegisterUserMailMsgIng"];
            }
        }
        #endregion

        #region RegistroMailSubject
        public string RegistroMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["RegistroMailSubject"];
            }
        }
        #endregion

        #region Preseleccion
        public string PreseleccionMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["PreseleccionMailSubject"];
            }
        }
        public string PreseleccionMailMsgEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["PreseleccionMailMsgEsp"];
            }
        }
        public string PreseleccionMailMsgIng
        {
            get
            {
                return ConfigurationManager.AppSettings["PreseleccionMailMsgIng"];
            }
        }
        #endregion

        #region SelOficial
        public string SelOficialMailSubject
        {
            get
            {
                return ConfigurationManager.AppSettings["SelOficialMailSubject"];
            }
        }
        public string SelOficialMailMsgEsp
        {
            get
            {
                return ConfigurationManager.AppSettings["SelOficialMailMsgEsp"];
            }
        }
        public string SelOficialMailMsgIng
        {
            get
            {
                return ConfigurationManager.AppSettings["SelOficialMailMsgIng"];
            }
        }
        #endregion

        #region Jurado
        public string MsgBienvenidaJuradoES
        {
            get
            {
                return ConfigurationManager.AppSettings["MsgBienvenidaJuradoES"];
            }
        }
        public string MsgBienvenidaJuradoEN
        {
            get
            {
                return ConfigurationManager.AppSettings["MsgBienvenidaJuradoEN"];
            }
        }
        public string MsgCierreJuradoES
        {
            get
            {
                return ConfigurationManager.AppSettings["MsgCierreJuradoES"];
            }
        }
        public string MsgCierreJuradoEN
        {
            get
            {
                return ConfigurationManager.AppSettings["MsgCierreJuradoEN"];
            }
        }


        public DateTime DateCloseJurado
        {
            get
            {
                try
                {
                    return new DateTime(
                        int.Parse(ConfigurationManager.AppSettings["CierreJuradoAnnoFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["CierreJuradoMesFestival"]),
                        int.Parse(ConfigurationManager.AppSettings["CierreJuradoDiaFestival"])
                    );
                }
                catch
                {
                    return DateTime.Now;
                }
            }
        }


        public string DateCloseJuradoSting
        {
            get {
                return DateClose.ToString("dd/MM/yyyy");
            }
        }

        public bool IsValidDateJurado
        {
            get
            {
                try
                {
                    return (0 < DateTime.Compare(DateCloseJurado, DateTime.Now));
                }
                catch
                {
                    return false;
                }
            }
        }
        #endregion

        
    }
}
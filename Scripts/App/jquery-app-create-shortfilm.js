﻿$(document).ready(function () {
    $("#form").steps({
        enableCancelButton: false,
        showFinishButtonAlways: false,
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex) {
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex) {
                return true;
            }

            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18) {
                return false;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex) {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18) {
                $(this).steps("next");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex) {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var form = $(this);

            // Submit form input
            form.submit();
        },
        labels: {
            cancel: "Cancel",
            current: "current step:",
            pagination: "Pagination",
            finish: "Finish",
            next: '<span class="msg">Siguiente | Next</span> <i class="fa fa-chevron-circle-right"></i> ',
            previous: '<i class="fa fa-chevron-circle-left"></i> <span class="msg">Anterior | Previous</span>',
            loading: "Loading ..."
        }


    }).validate({        
        errorPlacement: function (error, element) {
            // element.before(error);            
        },
        rules: {
            confirm: {
                equalTo: "#password"
            },
            "Contacto.Telefono": {
                required: true,
                digits: true
            },
            "Director.Telefono": {
                required: true,
                digits: true
            },
            "Productor.Telefono": {
                required: true,
                digits: true
            },
            "Fotografia.Telefono": {
                required: true,
                digits: true
            },
            "Guion.Telefono": {
                required: true,
                digits: true
            },
            "Edicion.Telefono": {
                required: true,
                digits: true
            },
            "DisenoSonoro.Telefono": {
                required: true,
                digits: true
            },
            "Sonido.Telefono": {
                required: true,
                digits: true
            },
            "Musica.Telefono": {
                required: true,
                digits: true
            }
        }
    });

    $('#PaisId').change(function () {
        var id = $(this).val();
        var combo = $('#EstadoId');
        $.ajax({
            type: "POST",
            url: "/ShortFilms/DetailsPais/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var items = '';
                $.each(data, function (i, item) {
                    items += "<option value='" + item.Id + "'>" + item.Estado + "</option>";
                });
                $(combo).html(items);
                $(combo).prop('disabled', data.length == 0);
                $(combo).change();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus + ": " + XMLHttpRequest.responseText);
            }
        });
    });

    $('#EstadoId').change(function () {
        var id = $(this).val();
        var combo = $('#MunicipioId');
        $.ajax({
            type: "POST",
            url: "/ShortFilms/DetailsEstado/" + id,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var items = '';
                $.each(data, function (i, item) {
                    items += "<option value='" + item.Id + "'>" + item.Municipio + "</option>";
                });
                $(combo).html(items);
                $(combo).prop('disabled', data.length == 0);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus + ": " + XMLHttpRequest.responseText);
            }
        });
    });

    $('#EstadoId').change();
    //------------------------------------------------------------------
    //---------- 1. DATOS GENERALES | FILM DETAILS ---------------------
    //------------------------------------------------------------------
    $('#FormatoVideoId').change(function () {
        var value = $(this).val();
        var input = $('#OtroFormatoVideo');
        evalValidState((value != 5),input);
    });


    $('#FormatoAudioId').change(function () {
        var value = $(this).val();
        var input = $('#OtroFormatoAudio');
        evalValidState((value != 76), input);
    });
    //------------------------------------------------------------------
    //---------- 2. EQUIPO | TEAM --------------------------------------
    //------------------------------------------------------------------
    $('#Contacto_Casa_Productora').change(function () {
        var value = $(this).is(':checked');
        var input = $('#Contacto_Web_Site_Casa_Productora');
        evalValidState(!value, input);
    });
    //------------------------------------------------------------------
    //---------- VALIDATE FUNCTIONS ------------------------------------
    //------------------------------------------------------------------
    function evalValidState(condition, obj) {
        try {
            $(obj).prop('disabled', condition);

            if (condition) {
                $(obj).val("");
                $(obj).removeClass('error');
            }
        } catch (err) { console.error("evalValidState",err.message) }
    }    
    //------------------------------------------------------------------
    //---------- FECHA DE NACIMIENTO | TEAM --------------------------------------
    //------------------------------------------------------------------
    $('.date').mask('00/00/0000');
});
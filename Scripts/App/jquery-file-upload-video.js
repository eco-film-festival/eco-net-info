﻿
$(document).ready(function () {
    //console.info('debug !! ');
    $('.file_upload').each(function () {
        //console.info('debug !! ');
        // ----------------------------------------
        // ----------------------------------------
        // ----------------------------------------
        var progress_bar = $(this).find('.progress .progress-bar');
        var progress_count = $(this).find('.progress_count');
        var fileupload = $(this).find('.fileupload');
        var film = $(fileupload).attr("data-film");
        var id = $(fileupload).attr("data-id");
        var display = $(this).find('.display');
        var img = $(this).find('.display img');
        var url = $(fileupload).attr("data-url");
       // var next_video_upload = $(this).find('.next_video_upload');

       // $(next_video_upload).css('display', 'none');

        // ----------------------------------------
        // ----------------------------------------
        // ----------------------------------------

        $(fileupload).fileupload({
            url: url,
            dataType: 'json',
            formData: { id: id, IdShortFilm: film },
            acceptFileTypes: /(\.|\/)(mp4)$/i,
            maxFileSize: 104857600, // 40 MB 62914560
            add: function (e, data) {
                var acceptFileTypes = /(mp4)$/i;
                var filename = data.originalFiles[0]['name'];
                var uploadErrors = [];

                if (!acceptFileTypes.test(data.originalFiles[0]['type']) || filename.split('.').pop()!='mp4') {
                    uploadErrors.push('Tipo de archivo no aceptado | Not an accepted file type');
                }
                if (data.originalFiles[0]['size'] > 104857600) {
                    uploadErrors.push('Archivo muy grande | Filesize is too big');
                }
                if (uploadErrors.length > 0) {
                    alert(uploadErrors.join("\n"));
                } else {
                   data.submit();
                }
                
            },
            done: function (e, data) {
               // $(next_video_upload).fadeIn();
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(progress_bar).css(
                    'width',
                    progress + '%'
                );
                $(progress_count).html(progress);
               // console.info(progress);

            }
        }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    });

});

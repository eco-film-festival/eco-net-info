﻿angular.module('app.schedule', [
	'ui.calendar',
	'ngDragDrop',
	'ui.bootstrap.datetimepicker'
])
.controller('CalendarCtrl', CalendarCtrl)
;
function CalendarCtrl($scope, $http, $q, ApiService) {
	var ctrl = this;
	/* url Services */
	ctrl.UrlEvents = '/api/Schedule/Events/';
	ctrl.UrlType = '/api/Schedule/Type/';
	ctrl.UrlSite = '/api/Schedule/Site/';
	/* function on searchEventId */
	ctrl.searchEventIndex = function (event) {
		for (var i = 0; i < $scope.events.length; i++) {
			var _event = $scope.events[i];
			if (_event.id == event.id) {
				return i;
			}
		}
		return null;
	};
	/* function on searchEvent */
	ctrl.searchEvent = function (event) {
		return $scope.events[ctrl.searchEventIndex(event)];
	};
	/* function on copyEvent */
	ctrl.copyEvent = function (_event, event) {
		_event.title = event.title;
		_event.allDay = event.allDay;
		_event.start = ctrl.parseEventDate(event.start);
		_event.end = ctrl.parseEventDate(event.end);
		_event.id = event.id;
		_event.type = event.type;
		_event.className = ctrl.parseClass(event.className);
		_event.Descripcion_EN = event.Descripcion_EN;
		_event.Descripcion_ES = event.Descripcion_ES;
		_event.Evento_EN = event.Evento_EN;
		_event.Evento_ES = event.Evento_ES;
		_event.icon = event.icon;
		_event.site = event.site;
		_event.sede = event.sede;
		_event.direccion = event.direccion;
		_event.urlSede = event.urlSede;
	};
	/* function on parseEventDate */
	ctrl.parseEventDate = function (_date) {
		if (_date instanceof Date) { return moment(_date).format("YYYY-MM-DDThh:mm:ss"); }
		if (_date instanceof Object) { return _date.toISOString(); }
		return _date;
	};
	/* function on parseClass */
	ctrl.parseClass = function (_className) {
		if (Array.isArray(_className)) { return _className.join(' '); }
		return _className;
	};
	/* function on updateEvent */
	ctrl.updateEvent = function (event) {
		return ApiService.update(
			ctrl.UrlEvents,
			function (response) {
				var _event = ctrl.searchEvent(event);
				ctrl.copyEvent(_event, response);
				$scope.activeEvent = ctrl.addEvent(event);
			},
			event.id,
			ctrl.addEvent(event));
	};
	/* function on addEvent */
	ctrl.addEvent = function (event) {
		var _event = {};
		ctrl.copyEvent(_event, event);
		return _event;
	};
	/* function on deleteEvent */
	ctrl.removeEvent = function (event) {

		//var q = $q.defer();

		//var index = ctrl.searchEventIndex(event);
		//if (index != null) {
		//	$http.delete('/api/Schedule/Events' + event.id)
		//		.success(function (response, status, headers, config) {
		//			$scope.activeEvent = {};
		//			$scope.events.splice(index, 1);
		//			q.resolve();
		//		})
		//		.error(function (data, status, headers, config) {
		//			console.info(data);
		//			alert(status);
		//			q.reject(data);
		//		});
		//}

		//return q.promise;
		var index = ctrl.searchEventIndex(event);
		return ApiService.delete(
			ctrl.UrlEvents,
			function (response) {
				$scope.activeEvent = {};
				$scope.events.splice(index, 1);
			},
			event.id ,
			index
			);
	};
	/* function on getListEvent */
	ctrl.getListEvent = function () {

		//ApiService.get(ctrl.UrlEvents, function (response, status, headers, config) {
		//	angular.forEach(response.Events, function (v, k) {
		//		$scope.events.push(ctrl.addEvent(v));
		//	});
		//});

		ApiService.get(ctrl.UrlType, $scope.typeEvent);
		//ApiService.get(ctrl.UrlSite, $scope.siteEvent);

	};
	/* function on button eventClick */
	ctrl.onEventClick = function (event, allDay, jsEvent, view) {
		$scope.alertMessage = (event.title + ': Clicked ');
		$scope.activeEvent = ctrl.addEvent(event);
		$scope.showModalEvent = true;
		$scope.selectedItemType.id = $scope.activeEvent.type;
	};
	/* function on eventDrop */
	ctrl.onEventDrop = function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
		$scope.alertMessage = (event.title + ': Droped to make dayDelta ' + dayDelta);
		ctrl.updateEvent(event);
	};
	/* function on Resize */
	ctrl.onResize = function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
		$scope.alertMessage = (event.title + ': Resized to make dayDelta ' + minuteDelta);
		ctrl.updateEvent(event);
	};
	/* function on Drop */
	ctrl.onDrop = function (date, jsEvent, ui) {
		var element = angular.element(this);
		var type = element.context.attributes['data-type'].value;
		var title = element.context.attributes['data-title'].value;
		var _event = {
			start: date,
			allDay: true,
			type: type,
		};
		ctrl.createEvent(_event);
	};
	/* function on createEvent */
	ctrl.createEvent = function (_event) {
		ApiService.create(
			ctrl.UrlEvents,
			function (response) {
				var event = ctrl.addEvent(response);
				$scope.events.push(event);
				ctrl.onEventClick(event);
			},
			_event
			);
	};
	//---------------------------------
	/* config object */
	//---------------------------------
	$scope.uiConfig = {
		calendar: {
			height: 600,
			editable: true,
			droppable: true,
			header: {
				left: 'prev,next',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			eventClick: ctrl.onEventClick,
			eventDrop: ctrl.onEventDrop,
			eventResize: ctrl.onResize,
			drop: ctrl.onDrop,
			timezone: 'utc',
			eventRender: function (event, element) {
				var _display = '';
				if (event.Evento_ES != null) {
					_display = event.Evento_ES;
				}
				element.find('.fc-title').append("<br/>" + _display);
			}
		}
	};

	/* function on deleteEvent */
	$scope.removeEvent = function (activeEvent) {
		ctrl.removeEvent(activeEvent);
	};

	$scope.updateEvent = function (activeEvent) {
		var _event = ctrl.addEvent(activeEvent);
		ctrl.updateEvent(_event).then(function (data) {
			ctrl.removeEvent(data);
			$scope.events.push(ctrl.addEvent(data));
			$scope.showModalEvent = false;
		});
	};

	$scope.clickEvent = function (event) {
		ctrl.onEventClick(event);
	};
	// ----------------------------
    /* Event Details */
	// ----------------------------
	$scope.showModalEvent = false;
	$scope.activeEvent = {};
	$scope.events = [];
	$scope.eventSources = [$scope.events];
	// ----------------------------
	/* Type Details */
    // ----------------------------
	$scope.typeEvent = [];
	$scope.showModalType = false;
	$scope.activeType = {};
	$scope.selectedItemType = { id: null };
    // ----------------------------
	$scope.$watch('selectedItemType.id', function (newVal, oldVal) {
	    if (newVal === oldVal) { return; }
	    angular.forEach($scope.typeEvent, function (v, k) {
	        if (v.id == newVal) {
	            $scope.activeEvent.type = v.id;
	            $scope.activeEvent.className = v.className;
	            $scope.activeEvent.title = v.Tipo_ES;
	        }
	    });
	}, true);
    // ----------------------------
	$scope.clickType = function (type) {
		$scope.activeType = type;
		$scope.showModalType = true;
	};
	$scope.addType = function () {
		$scope.activeType = {
			className: 'label label-evento',
			icon: 'fa fa-plus'
		};
		$scope.showModalType = true;
	};
	$scope.removeType = function (type) {
		//$scope.activeType = type;
		//$scope.showModalType = true;


		$scope.showModalType = false;
	};
	$scope.updateType = function (_type) {
		if (_type.id == null) {
			ApiService.create(
				ctrl.UrlType,
				$scope.typeEvent,
				_type
			);
		} else {
			ApiService.update(
				ctrl.UrlType,
				$scope.typeEvent,
				_type.id,
				_type
			).then(function (response) {
			    var _Events = [];

				for (var i = 0; i < $scope.events.length; i++) {
					var _event = $scope.events[i];
					if (_event.type == response.id) {
						_Events.push(_event);
					}
				}

				for (var i = 0; i < _Events.length; i++) {
					var _event = _Events[i];
					_event.className = response.className;
					_event.title = response.Tipo_ES;
					ctrl.removeEvent(_event);
					$scope.events.push(ctrl.addEvent(_event));
				}
			});
		}
		$scope.showModalType = false;
	};
    // ----------------------------
    /* Site Details */
    // ----------------------------
	$scope.siteEvent = [];
	$scope.showModalSite = false;
	$scope.activeSite = {};
	$scope.selectedItemSide = { id: null };
    // ----------------------------
	$scope.$watch('selectedItemSide.id', function (newVal, oldVal) {
	    if (newVal === oldVal) { return; }
	    angular.forEach($scope.siteEvent, function (v, k) {
	        if (v.id == newVal) {
	            $scope.activeEvent.site = v.id;
	            $scope.activeEvent.sede = v.Sede_ES;
	            $scope.activeEvent.direccion = v.Direccion;
	            $scope.activeEvent.urlSede = v.url;
	        }
	    });
	}, true);
    // ----------------------------
	$scope.clickSite = function (site) {
	    $scope.activeSite = site;
	    $scope.showModalSite = true;
	};
	$scope.addSite = function () {
	    $scope.activeSite = {};
	    $scope.showModalSite = true;
	};
	$scope.removeSite = function (type) {
	    //$scope.activeType = type;
	    //$scope.showModalType = true;


	    $scope.showModalSite = false;
	};
	$scope.updateSite = function (_site) {
	    if (_site.id == null) {
	        ApiService.create(
				ctrl.UrlSite,
				$scope.siteEvent,
				_site
			);
	    } else {
	        ApiService.update(
	        	ctrl.UrlSite,
	        	$scope.siteEvent,
	        	_site.id,
	        	_site
			).then(function (response) {
			    var _events = [];

			    console.info(response);

			    for (var i = 0; i < $scope.events.length; i++) {
			        var _event = $scope.events[i];
			        if (_event.site == response.id) {
			            _events.push(_event);
			        }
			    }

			    for (var i = 0; i < _events.length; i++) {
			        var _event = _events[i];
			        _event.sede = response.Sede_ES;
			        _event.direccion = response.Direccion;
			        _event.urlSede = response.url;
			        ctrl.removeEvent(_event);
			        $scope.events.push(ctrl.addevent(_event));
			    }
			});
	    }
	    $scope.showModalSite = false;
	};
    // ----------------------------
	/* Console Debug*/
	// ----------------------------
	$scope.debug = false;
	// ----------------------------
	ctrl.getListEvent();
	// ----------------------------
}
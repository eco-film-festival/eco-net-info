﻿/// <reference path="angular.app.report.js" />
angular.module('App', ['chart.js', 'ngResource'])
    .factory('servicesFactory', ['$resource', function ($resource) {
        return $resource(
			'/Public/Reporte',
			{},
			{
			    query: { method: 'GET', isArray: true }
			}
		);
    }])
    .controller('appController', ['$scope', '$http', 'servicesFactory', function ($scope, $http, servicesFactory) {
        

        var ctrl = $scope;

        ctrl.init = function () {
            $scope.loading = true;
            $http.get($scope.service).
               success(function (response, status, headers, config) {
                   ctrl.defaults(response);
                   $scope.list_report = response;
                   $scope.loading = false;
               }).
               error(function (data, status, headers, config) {
                   console.error(status);
               });
        };

        ctrl.createRandomRgba = function(a) {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return 'rgba(' + r + ',' + g + ',' + b + ','+a+')';
        }

        ctrl.defaults = function (response) {
            var max = 0, list = [];
            angular.forEach(response, function (v) {
                var ListReport = v.ListReport;
                
                angular.forEach(ListReport, function (v) {
                    if (v.data.length > max) {
                        list = v;
                        max = v.data.length;
                    }

                    if (v.isSimple) {
                        v.series = [v.title];
                        v.dataSeries = [v.data];
                    }
                });
            });

            // console.info(list);

            var colours = []
            angular.forEach(list.data, function (v) {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                var obj = {
                    fillColor: ctrl.createRandomRgba('0.2'),
                    pointColor: ctrl.createRandomRgba('1'),
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: ctrl.createRandomRgba('0.8'),
                    pointStrokeColor: "#fff",
                    strokeColor: ctrl.createRandomRgba('1')
                };
                colours.push(obj);
            });
            // console.info(colours);
            Chart.defaults.global.colours = colours;
           
        };

        $scope.$watch('service', function (v) {
            if (v != undefined) ctrl.init();
        });


        
        //----------------------
        $scope.download = function ($event, id, table) {
            ctrl.exportTableToCSV.apply($event.target, [$('#' + table + id), 'export.csv']);
        };

        ctrl.exportTableToCSV = function ($table, filename) {
            var $rows = $table.find('tr:has(td)'),

            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',

            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();

                    return text.replace('"', '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim) + '"',

            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
            
            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        };


        $scope.toggleModal = function (r) {
            $scope.active = r;
            $scope.showModal = !$scope.showModal;
        };

    }])
    .directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E', 
      transclude: true,
      replace:true,
      scope: true,
      link: function postLink(scope, element, attrs) {
        
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  })
;